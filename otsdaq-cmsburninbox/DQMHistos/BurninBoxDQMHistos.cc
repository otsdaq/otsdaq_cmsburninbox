#include "otsdaq-cmsburninbox/DQMHistos/BurninBoxDQMHistos.h"
//#include "otsdaq/ConfigurationInterface/ConfigurationTree.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/MessageFacility/MessageFacility.h"

#include <iostream>
#include <sstream>
#include <string>

#include <TCanvas.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TAxis.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TH1F.h>
#include <cinttypes>

using namespace ots;

#define DEFAULT_MARKER_STYLE 20
#define DEFAULT_MARKER_SIZE 0.4
//#define TIME_FORMAT "%H:%M:%S %m-%d-%Y"
#define TIME_FORMAT "%Y-%m-%d %H:%M:%S"

//========================================================================================================================
BurninBoxDQMHistos::BurninBoxDQMHistos(bool deleteHistos)
	: cAmbientTemperature_(nullptr)
	, gAmbientTemperature_(nullptr)
	, cFrameTemperature_(nullptr)
	, gFrameTemperature_(nullptr)
	, cFrameLeftTemperature_(nullptr)
	, gFrameLeftTemperature_(nullptr)
	, cFrameRightTemperature_(nullptr)
	, gFrameRightTemperature_(nullptr)
	, cChillerSetTemperature_(nullptr)
	, gChillerSetTemperature_(nullptr)
	, cAllTemperatures_(nullptr)
	, mgAllTemperatures_(nullptr)
	, cDewPoint_(nullptr)
	, gDewPoint_(nullptr)
	, cModuleCarrier1LeftTemperature_(nullptr)
	, gModuleCarrier1LeftTemperature_(nullptr)
	, cModuleCarrier2LeftTemperature_(nullptr)
	, gModuleCarrier2LeftTemperature_(nullptr)
	, cModuleCarrier3LeftTemperature_(nullptr)
	, gModuleCarrier3LeftTemperature_(nullptr)
	, cModuleCarrier4LeftTemperature_(nullptr)
	, gModuleCarrier4LeftTemperature_(nullptr)
	, cModuleCarrier5LeftTemperature_(nullptr)
	, gModuleCarrier5LeftTemperature_(nullptr)
	, cModuleCarrier1RightTemperature_(nullptr)
	, gModuleCarrier1RightTemperature_(nullptr)
	, cModuleCarrier2RightTemperature_(nullptr)
	, gModuleCarrier2RightTemperature_(nullptr)
	, cModuleCarrier3RightTemperature_(nullptr)
	, gModuleCarrier3RightTemperature_(nullptr)
	, cModuleCarrier4RightTemperature_(nullptr)
	, gModuleCarrier4RightTemperature_(nullptr)
	, cModuleCarrier5RightTemperature_(nullptr)
	, gModuleCarrier5RightTemperature_(nullptr)
	, cAllModuleCarrierTemperatures_  (nullptr)
	, mgAllModuleCarrierTemperatures_ (nullptr)
	, cDoorLockRelay_(nullptr)
	, gDoorLockRelay_(nullptr)
	, cDryAirFluxRelay_(nullptr)
	, gDryAirFluxRelay_(nullptr)
	, cChillerStatus_(nullptr)
	, gChillerStatus_(nullptr)
	, cCycleStatus_(nullptr)
	, gCycleStatus_(nullptr)
	, deleteHistos_(deleteHistos)
{
	gStyle->SetOptTitle(0);
	gStyle->SetTitleFillColor(2);
	gStyle->SetTitleFontSize(0.2);
	gStyle->SetTimeOffset(0);
}

//========================================================================================================================
BurninBoxDQMHistos::~BurninBoxDQMHistos(void) 
{
	if(deleteHistos_)
		destroy();
}

// Emanuele Aucone
//========================================================================================================================
void BurninBoxDQMHistos::book(TDirectory *myDirectory) //, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
{
	__COUT__ << "Booking start!" << std::endl;

	myDir_ = myDirectory;
	statusDir_ = myDir_->mkdir("Status", "Status");
	statusDir_->cd();

	if (cAmbientTemperature_ == nullptr)
	{
		cAmbientTemperature_ = new TCanvas("AmbientTemperature", "Ambient temperature", 800, 600);
		statusDir_->Add(cAmbientTemperature_);
		cAmbientTemperature_->SetGridx();
		cAmbientTemperature_->SetGridy();
	}
	if (gAmbientTemperature_  == nullptr)
	{
		gAmbientTemperature_ = new TGraph();
		gAmbientTemperature_->SetNameTitle("AmbientTemperatureGraph", "Ambient temperature");
		gAmbientTemperature_->GetXaxis()->SetTitle("Local Time");
		gAmbientTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gAmbientTemperature_->SetMarkerColor(3);
		gAmbientTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gAmbientTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gAmbientTemperature_->SetLineColor(kGreen);
		gAmbientTemperature_->GetXaxis()->SetTimeDisplay(1);
		gAmbientTemperature_->GetXaxis()->SetNdivisions(503);
		gAmbientTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cAmbientTemperature_->cd();
		gAmbientTemperature_->Draw("APL");
	}
	else
	{
		gAmbientTemperature_->Clear();
	}

	if (cFrameTemperature_ == nullptr)
	{
		cFrameTemperature_ = new TCanvas("FrameTemperature", "Frame temperature", 800, 600);
		statusDir_->Add(cFrameTemperature_);
		cFrameTemperature_->SetGridx();
		cFrameTemperature_->SetGridy();
	}

	if (gFrameTemperature_ == nullptr)
	{
		gFrameTemperature_ = new TGraph();
		gFrameTemperature_->SetNameTitle("FrameTemperatureGraph", "Frame temperature");
		gFrameTemperature_->GetXaxis()->SetTitle("Local Time");
		gFrameTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gFrameTemperature_->SetMarkerColor(4);
		gFrameTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gFrameTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gFrameTemperature_->SetLineColor(kBlue);
		gFrameTemperature_->GetXaxis()->SetTimeDisplay(1);
		gFrameTemperature_->GetXaxis()->SetNdivisions(503);
		gFrameTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cFrameTemperature_->cd();
		gFrameTemperature_->Draw("APL");
	}
	else
	{
		gFrameTemperature_->Clear();
	}

	if (cFrameLeftTemperature_ == nullptr)
	{
		cFrameLeftTemperature_ = new TCanvas("FrameLeftTemperature", "Frame left temperature", 800, 600);
		statusDir_->Add(cFrameLeftTemperature_);
		cFrameLeftTemperature_->SetGridx();
		cFrameLeftTemperature_->SetGridy();
	}

	if (gFrameLeftTemperature_ == nullptr)
	{
		gFrameLeftTemperature_ = new TGraph();
		gFrameLeftTemperature_->SetNameTitle("FrameLeftTemperatureGraph", "Frame left temperature");
		gFrameLeftTemperature_->GetXaxis()->SetTitle("Local Time");
		gFrameLeftTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gFrameLeftTemperature_->SetMarkerColor(4);
		gFrameLeftTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gFrameLeftTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gFrameLeftTemperature_->SetLineColor(kBlue);
		gFrameLeftTemperature_->GetXaxis()->SetTimeDisplay(1);
		gFrameLeftTemperature_->GetXaxis()->SetNdivisions(503);
		gFrameLeftTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cFrameLeftTemperature_->cd();
		gFrameLeftTemperature_->Draw("APL");
	}
	else
	{
		gFrameLeftTemperature_->Clear();
	}

	if (cFrameRightTemperature_ == nullptr)
	{
		cFrameRightTemperature_ = new TCanvas("FrameRightTemperature", "Frame right temperature", 800, 600);
		statusDir_->Add(cFrameRightTemperature_);
		cFrameRightTemperature_->SetGridx();
		cFrameRightTemperature_->SetGridy();
	}

	if (gFrameRightTemperature_ == nullptr)
	{
		gFrameRightTemperature_ = new TGraph();
		gFrameRightTemperature_->SetNameTitle("FrameRightTemperatureGraph", "Frame right temperature");
		gFrameRightTemperature_->GetXaxis()->SetTitle("Local Time");
		gFrameRightTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gFrameRightTemperature_->SetMarkerColor(4);
		gFrameRightTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gFrameRightTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gFrameRightTemperature_->SetLineColor(kBlue);
		gFrameRightTemperature_->GetXaxis()->SetTimeDisplay(1);
		gFrameRightTemperature_->GetXaxis()->SetNdivisions(503);
		gFrameRightTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cFrameRightTemperature_->cd();
		gFrameRightTemperature_->Draw("APL");
	}
	else
	{
		gFrameRightTemperature_->Clear();
	}

	if (cChillerSetTemperature_  == nullptr)
	{
		cChillerSetTemperature_ = new TCanvas("ChillerSetTemperature", "Chiller set temperature", 800, 600);
		statusDir_->Add(cChillerSetTemperature_);
		cChillerSetTemperature_->SetGridx();
		cChillerSetTemperature_->SetGridy();
	}

	if (gChillerSetTemperature_ == nullptr)
	{
		gChillerSetTemperature_ = new TGraph();
		gChillerSetTemperature_->SetNameTitle("ChillerSetTemperatureGraph", "Chiller set temperature");
		gChillerSetTemperature_->GetXaxis()->SetTitle("Local Time");
		gChillerSetTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gChillerSetTemperature_->SetMarkerColor(93);
		gChillerSetTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gChillerSetTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gChillerSetTemperature_->SetLineColor(kOrange);
		gChillerSetTemperature_->GetXaxis()->SetTimeDisplay(1);
		gChillerSetTemperature_->GetXaxis()->SetNdivisions(503);
		gChillerSetTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cChillerSetTemperature_->cd();
		gChillerSetTemperature_->Draw("APL");
	}
	else
	{
		gChillerSetTemperature_->Clear();
	}

	if(cAllTemperatures_ == nullptr)
	{
		cAllTemperatures_ = new TCanvas("AllTemperaturesCanvas", "Frame, ambient and chiller temperatures", 800, 600);
		statusDir_->Add(cAllTemperatures_);
		cAllTemperatures_->SetGridx();
		cAllTemperatures_->SetGridy();

		mgAllTemperatures_ = new TMultiGraph();
		mgAllTemperatures_->SetNameTitle("AllTemperaturesMultiGraph", "Frame, ambient and chiller temperatures");
		mgAllTemperatures_->GetXaxis()->SetTitle("Local Time");
		mgAllTemperatures_->GetYaxis()->SetTitle("Temperature [#circC]");
		mgAllTemperatures_->GetXaxis()->SetTimeDisplay(1);
		mgAllTemperatures_->GetXaxis()->SetNdivisions(503);
		mgAllTemperatures_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		mgAllTemperatures_->Add(gAmbientTemperature_);
		mgAllTemperatures_->Add(gFrameTemperature_);
		mgAllTemperatures_->Add(gFrameLeftTemperature_);
		mgAllTemperatures_->Add(gFrameRightTemperature_);
		mgAllTemperatures_->Add(gChillerSetTemperature_);
		cAllTemperatures_->cd();
		mgAllTemperatures_->Draw("APL");
	}

	if (cDewPoint_ == nullptr)
	{
		cDewPoint_ = new TCanvas("DewPoint", "Dew point temperature", 800, 600);
		statusDir_->Add(cDewPoint_);
		cDewPoint_->SetGridx();
		cDewPoint_->SetGridy();
	}

	if (gDewPoint_ == nullptr)
	{
		gDewPoint_ = new TGraph();
		gDewPoint_->SetNameTitle("DewPointGraph", "Dew point temperature");
		gDewPoint_->GetXaxis()->SetTitle("Local Time");
		gDewPoint_->GetYaxis()->SetTitle("Dew Point [#circC]");
		gDewPoint_->SetMarkerColor(2);
		gDewPoint_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gDewPoint_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gDewPoint_->SetLineColor(kRed);
		gDewPoint_->GetXaxis()->SetTimeDisplay(1);
		gDewPoint_->GetXaxis()->SetNdivisions(503);
		gDewPoint_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cDewPoint_->cd();
		gDewPoint_->Draw("APL");
	}
	else
	{
		gDewPoint_->Clear();
	}

	if (cModuleCarrier1LeftTemperature_ == nullptr)
	{
		cModuleCarrier1LeftTemperature_ = new TCanvas("ModuleCarrier1LeftTemperature", "Module carrier 1 left temperature", 800, 600);
		statusDir_->Add(cModuleCarrier1LeftTemperature_);
		cModuleCarrier1LeftTemperature_->SetGridx();
		cModuleCarrier1LeftTemperature_->SetGridy();
	}

	if (gModuleCarrier1LeftTemperature_ == nullptr)
	{
		gModuleCarrier1LeftTemperature_ = new TGraph();
		gModuleCarrier1LeftTemperature_->SetNameTitle("ModuleCarrier1LeftTemperatureGraph", "Module carrier 1 left temperature");
		gModuleCarrier1LeftTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier1LeftTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier1LeftTemperature_->SetMarkerColor(kBlue);
		gModuleCarrier1LeftTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier1LeftTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier1LeftTemperature_->SetLineColor(60);
		gModuleCarrier1LeftTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier1LeftTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier1LeftTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier1LeftTemperature_->cd();
		gModuleCarrier1LeftTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier1LeftTemperature_->Clear();
	}

	if (cModuleCarrier2LeftTemperature_ == nullptr)
	{
		cModuleCarrier2LeftTemperature_ = new TCanvas("ModuleCarrier2LeftTemperature", "Module carrier 2 left temperature", 800, 600);
		statusDir_->Add(cModuleCarrier2LeftTemperature_);
		cModuleCarrier2LeftTemperature_->SetGridx();
		cModuleCarrier2LeftTemperature_->SetGridy();
	}

	if (gModuleCarrier2LeftTemperature_ == nullptr)
	{
		gModuleCarrier2LeftTemperature_ = new TGraph();
		gModuleCarrier2LeftTemperature_->SetNameTitle("ModuleCarrier2LeftTemperatureGraph", "Module carrier 2 left temperature");
		gModuleCarrier2LeftTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier2LeftTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier2LeftTemperature_->SetMarkerColor(kBlue);
		gModuleCarrier2LeftTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier2LeftTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier2LeftTemperature_->SetLineColor(62);
		gModuleCarrier2LeftTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier2LeftTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier2LeftTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier2LeftTemperature_->cd();
		gModuleCarrier2LeftTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier2LeftTemperature_->Clear();
	}

	if (cModuleCarrier3LeftTemperature_ == nullptr)
	{
		cModuleCarrier3LeftTemperature_ = new TCanvas("ModuleCarrier3LeftTemperature", "Module carrier 3 left temperature", 800, 600);
		statusDir_->Add(cModuleCarrier3LeftTemperature_);
		cModuleCarrier3LeftTemperature_->SetGridx();
		cModuleCarrier3LeftTemperature_->SetGridy();
	}

	if (gModuleCarrier3LeftTemperature_ == nullptr)
	{
		gModuleCarrier3LeftTemperature_ = new TGraph();
		gModuleCarrier3LeftTemperature_->SetNameTitle("ModuleCarrier3LeftTemperatureGraph", "Module carrier 3 left temperature");
		gModuleCarrier3LeftTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier3LeftTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier3LeftTemperature_->SetMarkerColor(kBlue);
		gModuleCarrier3LeftTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier3LeftTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier3LeftTemperature_->SetLineColor(64);
		gModuleCarrier3LeftTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier3LeftTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier3LeftTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier3LeftTemperature_->cd();
		gModuleCarrier3LeftTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier3LeftTemperature_->Clear();
	}

	if (cModuleCarrier4LeftTemperature_ == nullptr)
	{
		cModuleCarrier4LeftTemperature_ = new TCanvas("ModuleCarrier4LeftTemperature", "Module carrier 4 left temperature", 800, 600);
		statusDir_->Add(cModuleCarrier4LeftTemperature_);
		cModuleCarrier4LeftTemperature_->SetGridx();
		cModuleCarrier4LeftTemperature_->SetGridy();
	}

	if (gModuleCarrier4LeftTemperature_ == nullptr)
	{
		gModuleCarrier4LeftTemperature_ = new TGraph();
		gModuleCarrier4LeftTemperature_->SetNameTitle("ModuleCarrier4LeftTemperatureGraph", "Module carrier 4 left temperature");
		gModuleCarrier4LeftTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier4LeftTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier4LeftTemperature_->SetMarkerColor(kBlue);
		gModuleCarrier4LeftTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier4LeftTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier4LeftTemperature_->SetLineColor(66);
		gModuleCarrier4LeftTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier4LeftTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier4LeftTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier4LeftTemperature_->cd();
		gModuleCarrier4LeftTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier4LeftTemperature_->Clear();
	}

	if (cModuleCarrier5LeftTemperature_ == nullptr)
	{
		cModuleCarrier5LeftTemperature_ = new TCanvas("ModuleCarrier5LeftTemperature", "Module carrier 5 left temperature", 800, 600);
		statusDir_->Add(cModuleCarrier5LeftTemperature_);
		cModuleCarrier5LeftTemperature_->SetGridx();
		cModuleCarrier5LeftTemperature_->SetGridy();
	}

	if (gModuleCarrier5LeftTemperature_ == nullptr)
	{
		gModuleCarrier5LeftTemperature_ = new TGraph();
		gModuleCarrier5LeftTemperature_->SetNameTitle("ModuleCarrier5LeftTemperatureGraph", "Module carrier 5 left temperature");
		gModuleCarrier5LeftTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier5LeftTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier5LeftTemperature_->SetMarkerColor(kBlue);
		gModuleCarrier5LeftTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier5LeftTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier5LeftTemperature_->SetLineColor(68);
		gModuleCarrier5LeftTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier5LeftTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier5LeftTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier5LeftTemperature_->cd();
		gModuleCarrier5LeftTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier5LeftTemperature_->Clear();
	}

	if (cModuleCarrier1RightTemperature_ == nullptr)
	{
		cModuleCarrier1RightTemperature_ = new TCanvas("ModuleCarrier1RightTemperature", "Module carrier 1 right temperature", 800, 600);
		statusDir_->Add(cModuleCarrier1RightTemperature_);
		cModuleCarrier1RightTemperature_->SetGridx();
		cModuleCarrier1RightTemperature_->SetGridy();
	}

	if (gModuleCarrier1RightTemperature_ == nullptr)
	{
		gModuleCarrier1RightTemperature_ = new TGraph();
		gModuleCarrier1RightTemperature_->SetNameTitle("ModuleCarrier1RightTemperatureGraph", "Module carrier 1 right temperature");
		gModuleCarrier1RightTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier1RightTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier1RightTemperature_->SetMarkerColor(kRed);
		gModuleCarrier1RightTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier1RightTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier1RightTemperature_->SetLineColor(92);
		gModuleCarrier1RightTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier1RightTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier1RightTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier1RightTemperature_->cd();
		gModuleCarrier1RightTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier1RightTemperature_->Clear();
	}

	if (cModuleCarrier2RightTemperature_ == nullptr)
	{
		cModuleCarrier2RightTemperature_ = new TCanvas("ModuleCarrier2RightTemperature", "Module carrier 2 right temperature", 800, 600);
		statusDir_->Add(cModuleCarrier2RightTemperature_);
		cModuleCarrier2RightTemperature_->SetGridx();
		cModuleCarrier2RightTemperature_->SetGridy();
	}

	if (gModuleCarrier2RightTemperature_ == nullptr)
	{
		gModuleCarrier2RightTemperature_ = new TGraph();
		gModuleCarrier2RightTemperature_->SetNameTitle("ModuleCarrier2RightTemperatureGraph", "Module carrier 2 right temperature");
		gModuleCarrier2RightTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier2RightTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier2RightTemperature_->SetMarkerColor(kRed);
		gModuleCarrier2RightTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier2RightTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier2RightTemperature_->SetLineColor(94);
		gModuleCarrier2RightTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier2RightTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier2RightTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier2RightTemperature_->cd();
		gModuleCarrier2RightTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier2RightTemperature_->Clear();
	}

	if (cModuleCarrier3RightTemperature_ == nullptr)
	{
		cModuleCarrier3RightTemperature_ = new TCanvas("ModuleCarrier3RightTemperature", "Module carrier 3 right temperature", 800, 600);
		statusDir_->Add(cModuleCarrier3RightTemperature_);
		cModuleCarrier3RightTemperature_->SetGridx();
		cModuleCarrier3RightTemperature_->SetGridy();
	}

	if (gModuleCarrier3RightTemperature_ == nullptr)
	{
		gModuleCarrier3RightTemperature_ = new TGraph();
		gModuleCarrier3RightTemperature_->SetNameTitle("ModuleCarrier3RightTemperatureGraph", "Module carrier 3 right temperature");
		gModuleCarrier3RightTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier3RightTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier3RightTemperature_->SetMarkerColor(kRed);
		gModuleCarrier3RightTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier3RightTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier3RightTemperature_->SetLineColor(96);
		gModuleCarrier3RightTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier3RightTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier3RightTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier3RightTemperature_->cd();
		gModuleCarrier3RightTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier3RightTemperature_->Clear();
	}

	if (cModuleCarrier4RightTemperature_ == nullptr)
	{
		cModuleCarrier4RightTemperature_ = new TCanvas("ModuleCarrier4RightTemperature", "Module carrier 4 right temperature", 800, 600);
		statusDir_->Add(cModuleCarrier4RightTemperature_);
		cModuleCarrier4RightTemperature_->SetGridx();
		cModuleCarrier4RightTemperature_->SetGridy();
	}

	if (gModuleCarrier4RightTemperature_ == nullptr)
	{
		gModuleCarrier4RightTemperature_ = new TGraph();
		gModuleCarrier4RightTemperature_->SetNameTitle("ModuleCarrier4RightTemperatureGraph", "Module carrier 4 right temperature");
		gModuleCarrier4RightTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier4RightTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier4RightTemperature_->SetMarkerColor(kRed);
		gModuleCarrier4RightTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier4RightTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier4RightTemperature_->SetLineColor(98);
		gModuleCarrier4RightTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier4RightTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier4RightTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier4RightTemperature_->cd();
		gModuleCarrier4RightTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier4RightTemperature_->Clear();
	}

	if (cModuleCarrier5RightTemperature_ == nullptr)
	{
		cModuleCarrier5RightTemperature_ = new TCanvas("ModuleCarrier5RightTemperature", "Module carrier 5 right temperature", 800, 600);
		statusDir_->Add(cModuleCarrier5RightTemperature_);
		cModuleCarrier5RightTemperature_->SetGridx();
		cModuleCarrier5RightTemperature_->SetGridy();
	}

	if (gModuleCarrier5RightTemperature_ == nullptr)
	{
		gModuleCarrier5RightTemperature_ = new TGraph();
		gModuleCarrier5RightTemperature_->SetNameTitle("ModuleCarrier5RightTemperatureGraph", "Module carrier 5 right temperature");
		gModuleCarrier5RightTemperature_->GetXaxis()->SetTitle("Local Time");
		gModuleCarrier5RightTemperature_->GetYaxis()->SetTitle("Temperature [#circC]");
		gModuleCarrier5RightTemperature_->SetMarkerColor(kRed);
		gModuleCarrier5RightTemperature_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gModuleCarrier5RightTemperature_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gModuleCarrier5RightTemperature_->SetLineColor(100);
		gModuleCarrier5RightTemperature_->GetXaxis()->SetTimeDisplay(1);
		gModuleCarrier5RightTemperature_->GetXaxis()->SetNdivisions(503);
		gModuleCarrier5RightTemperature_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cModuleCarrier5RightTemperature_->cd();
		gModuleCarrier5RightTemperature_->Draw("APL");
	}
	else
	{
		gModuleCarrier5RightTemperature_->Clear();
	}

	if(cAllModuleCarrierTemperatures_ == nullptr)
	{
		cAllModuleCarrierTemperatures_ = new TCanvas("AllModuleCarrierTemperatures", "All module temperatures", 800, 600);
		statusDir_->Add(cAllModuleCarrierTemperatures_);
		cAllModuleCarrierTemperatures_->SetGridx();
		cAllModuleCarrierTemperatures_->SetGridy();

		mgAllModuleCarrierTemperatures_ = new TMultiGraph();
		mgAllModuleCarrierTemperatures_->SetNameTitle("AllModuleCarrierTemperaturesMultiGraph", "All module temperatures");
		mgAllModuleCarrierTemperatures_->GetXaxis()->SetTitle("Local Time");
		mgAllModuleCarrierTemperatures_->GetYaxis()->SetTitle("Temperature [#circC]");
		mgAllModuleCarrierTemperatures_->GetXaxis()->SetTimeDisplay(1);
		mgAllModuleCarrierTemperatures_->GetXaxis()->SetNdivisions(503);
		mgAllModuleCarrierTemperatures_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier1LeftTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier2LeftTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier3LeftTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier4LeftTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier5LeftTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier1RightTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier2RightTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier3RightTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier4RightTemperature_);
		mgAllModuleCarrierTemperatures_->Add(gModuleCarrier5RightTemperature_);
		cAllModuleCarrierTemperatures_->cd();
		mgAllModuleCarrierTemperatures_->Draw("APL");


		// //cAllModuleCarrierTemperatures_->Divide(2, 5);
		// cAllModuleCarrierTemperatures_->cd();
		// gModuleCarrier1LeftTemperature_->Draw("APL");
		// //cAllModuleCarrierTemperatures_->cd(3);
		// gModuleCarrier2LeftTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(5);
		// gModuleCarrier3LeftTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(7);
		// gModuleCarrier4LeftTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(9);
		// gModuleCarrier5LeftTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(2);
		// gModuleCarrier1RightTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(4);
		// gModuleCarrier2RightTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(6);
		// gModuleCarrier3RightTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(8);
		// gModuleCarrier4RightTemperature_->Draw("PL SAME");
		// //cAllModuleCarrierTemperatures_->cd(10);
		// gModuleCarrier5RightTemperature_->Draw("PL SAME");
	}

	if (cDoorLockRelay_ == nullptr)
	{
		cDoorLockRelay_ = new TCanvas("DoorLockRelay", "Door lock relay", 800, 600);
		statusDir_->Add(cDoorLockRelay_);
		cDoorLockRelay_->SetGridx();
		cDoorLockRelay_->SetGridy();
	}

	if (gDoorLockRelay_ == nullptr)
	{
		gDoorLockRelay_ = new TGraph();
		gDoorLockRelay_->SetNameTitle("DoorLockRelayGraph", "Door lock relay");
		gDoorLockRelay_->GetXaxis()->SetTitle("Local Time");
		gDoorLockRelay_->GetYaxis()->SetTitle("On/Off");
		gDoorLockRelay_->SetMarkerColor(93);
		gDoorLockRelay_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gDoorLockRelay_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gDoorLockRelay_->SetLineColor(kOrange);
		gDoorLockRelay_->GetXaxis()->SetTimeDisplay(1);
		gDoorLockRelay_->GetXaxis()->SetNdivisions(503);
		gDoorLockRelay_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cDoorLockRelay_->cd();
		gDoorLockRelay_->Draw("APL");
	}
	else
	{
		gDoorLockRelay_->Clear();
	}

	if (cDryAirFluxRelay_ == nullptr)
	{
		cDryAirFluxRelay_ = new TCanvas("DryAirFluxRelay", "Dry air flux relay", 800, 600);
		statusDir_->Add(cDryAirFluxRelay_);
		cDryAirFluxRelay_->SetGridx();
		cDryAirFluxRelay_->SetGridy();
	}

	if (gDryAirFluxRelay_ == nullptr)
	{
		gDryAirFluxRelay_ = new TGraph();
		gDryAirFluxRelay_->SetNameTitle("DryAirFluxRelayGraph", "Dry air flux relay");
		gDryAirFluxRelay_->GetXaxis()->SetTitle("Local Time");
		gDryAirFluxRelay_->GetYaxis()->SetTitle("On/Off");
		gDryAirFluxRelay_->SetMarkerColor(93);
		gDryAirFluxRelay_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gDryAirFluxRelay_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gDryAirFluxRelay_->SetLineColor(kOrange);
		gDryAirFluxRelay_->GetXaxis()->SetTimeDisplay(1);
		gDryAirFluxRelay_->GetXaxis()->SetNdivisions(503);
		gDryAirFluxRelay_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cDryAirFluxRelay_->cd();
		gDryAirFluxRelay_->Draw("APL");
	}
	else
	{
		gDryAirFluxRelay_->Clear();
	}

	if (cChillerStatus_ == nullptr)
	{
		cChillerStatus_ = new TCanvas("ChillerStatus", "Chiller status", 800, 600);
		statusDir_->Add(cChillerStatus_);
		cChillerStatus_->SetGridx();
		cChillerStatus_->SetGridy();
	}

	if (gChillerStatus_ == nullptr)
	{
		gChillerStatus_ = new TGraph();
		gChillerStatus_->SetNameTitle("ChillerStatusGraph", "Chiller status");
		gChillerStatus_->GetXaxis()->SetTitle("Local Time");
		gChillerStatus_->GetYaxis()->SetTitle("On/Off");
		gChillerStatus_->SetMarkerColor(93);
		gChillerStatus_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gChillerStatus_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gChillerStatus_->SetLineColor(kOrange);
		gChillerStatus_->GetXaxis()->SetTimeDisplay(1);
		gChillerStatus_->GetXaxis()->SetNdivisions(503);
		gChillerStatus_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cChillerStatus_->cd();
		gChillerStatus_->Draw("APL");
	}
	else
	{
		gChillerStatus_->Clear();
	}

	if (cCycleStatus_ == nullptr)
	{
		cCycleStatus_ = new TCanvas("CycleStatus", "Cycle status", 800, 600);
		statusDir_->Add(cCycleStatus_);
		cCycleStatus_->SetGridx();
		cCycleStatus_->SetGridy();
	}

	if (gCycleStatus_ == nullptr)
	{
		gCycleStatus_ = new TGraph();
		gCycleStatus_->SetNameTitle("CycleStatusGraph", "Cycle status");
		gCycleStatus_->GetXaxis()->SetTitle("Local Time");
		gCycleStatus_->GetYaxis()->SetTitle("Status");
		gCycleStatus_->SetMarkerColor(93);
		gCycleStatus_->SetMarkerStyle(DEFAULT_MARKER_STYLE);
		gCycleStatus_->SetMarkerSize(DEFAULT_MARKER_SIZE);
		gCycleStatus_->SetLineColor(kOrange);
		gCycleStatus_->GetXaxis()->SetTimeDisplay(1);
		gCycleStatus_->GetXaxis()->SetNdivisions(503);
		gCycleStatus_->GetXaxis()->SetTimeFormat(TIME_FORMAT);

		cCycleStatus_->cd();
		gCycleStatus_->Draw("APL");
	}
	else
	{
		gCycleStatus_->Clear();
	}


	__COUT__ << "Booking done!" << std::endl;
}

//========================================================================================================================
void BurninBoxDQMHistos::destroy()
{
	
	if(cAmbientTemperature_ != nullptr)
		delete cAmbientTemperature_;
	if(gAmbientTemperature_ != nullptr)
		delete gAmbientTemperature_;
	if(cFrameTemperature_ != nullptr)
		delete cFrameTemperature_;
	if(gFrameTemperature_ != nullptr)
		delete gFrameTemperature_;
	if(cFrameLeftTemperature_ != nullptr)
		delete cFrameLeftTemperature_;
	if(gFrameLeftTemperature_ != nullptr)
		delete gFrameLeftTemperature_;
	if(cFrameRightTemperature_ != nullptr)
		delete cFrameRightTemperature_;
	if(gFrameRightTemperature_ != nullptr)
		delete gFrameRightTemperature_;
	if(cChillerSetTemperature_ != nullptr)
		delete cChillerSetTemperature_;
	if(gChillerSetTemperature_ != nullptr)
		delete gChillerSetTemperature_;
	if(cAllTemperatures_ != nullptr)
		delete cAllTemperatures_;
	if(mgAllTemperatures_ != nullptr)
		delete mgAllTemperatures_;
	if(cDewPoint_ != nullptr)
		delete cDewPoint_;
	if(gDewPoint_ != nullptr)
		delete gDewPoint_;
	if(cModuleCarrier1LeftTemperature_ != nullptr)
		delete cModuleCarrier1LeftTemperature_;
	if(gModuleCarrier1LeftTemperature_ != nullptr)
		delete gModuleCarrier1LeftTemperature_;
	if(cModuleCarrier2LeftTemperature_ != nullptr)
		delete cModuleCarrier2LeftTemperature_;
	if(gModuleCarrier2LeftTemperature_ != nullptr)
		delete gModuleCarrier2LeftTemperature_;
	if(cModuleCarrier3LeftTemperature_ != nullptr)
		delete cModuleCarrier3LeftTemperature_;
	if(gModuleCarrier3LeftTemperature_ != nullptr)
		delete gModuleCarrier3LeftTemperature_;
	if(cModuleCarrier4LeftTemperature_ != nullptr)
		delete cModuleCarrier4LeftTemperature_;
	if(gModuleCarrier4LeftTemperature_ != nullptr)
		delete gModuleCarrier4LeftTemperature_;
	if(cModuleCarrier5LeftTemperature_ != nullptr)
		delete cModuleCarrier5LeftTemperature_;
	if(gModuleCarrier5LeftTemperature_ != nullptr)
		delete gModuleCarrier5LeftTemperature_;
	if(cModuleCarrier1RightTemperature_ != nullptr)
		delete cModuleCarrier1RightTemperature_;
	if(gModuleCarrier1RightTemperature_ != nullptr)
		delete gModuleCarrier1RightTemperature_;
	if(cModuleCarrier2RightTemperature_ != nullptr)
		delete cModuleCarrier2RightTemperature_;
	if(gModuleCarrier2RightTemperature_ != nullptr)
		delete gModuleCarrier2RightTemperature_;
	if(cModuleCarrier3RightTemperature_ != nullptr)
		delete cModuleCarrier3RightTemperature_;
	if(gModuleCarrier3RightTemperature_ != nullptr)
		delete gModuleCarrier3RightTemperature_;
	if(cModuleCarrier4RightTemperature_ != nullptr)
		delete cModuleCarrier4RightTemperature_;
	if(gModuleCarrier4RightTemperature_ != nullptr)
		delete gModuleCarrier4RightTemperature_;
	if(cModuleCarrier5RightTemperature_ != nullptr)
		delete cModuleCarrier5RightTemperature_;
	if(gModuleCarrier5RightTemperature_ != nullptr)
		delete gModuleCarrier5RightTemperature_;
	if(cAllModuleCarrierTemperatures_   != nullptr)
		delete cAllModuleCarrierTemperatures_;
	if(mgAllModuleCarrierTemperatures_   != nullptr)
		delete mgAllModuleCarrierTemperatures_;
	if(cDoorLockRelay_           != nullptr)
		delete cDoorLockRelay_;
	if(gDoorLockRelay_           != nullptr)
		delete gDoorLockRelay_;
	if(cDryAirFluxRelay_         != nullptr)
		delete cDryAirFluxRelay_;
	if(gDryAirFluxRelay_         != nullptr)
		delete gDryAirFluxRelay_;
	if(cChillerStatus_           != nullptr)
		delete cChillerStatus_;
	if(gChillerStatus_           != nullptr)
		delete gChillerStatus_;
	if(cCycleStatus_             != nullptr)
		delete cCycleStatus_;
	if(gCycleStatus_             != nullptr)
		delete gCycleStatus_;

	clearPointers();
}

//========================================================================================================================
void BurninBoxDQMHistos::clearPointers()
{
	cAmbientTemperature_ = nullptr;
	gAmbientTemperature_ = nullptr;
	cFrameTemperature_ = nullptr;
	gFrameTemperature_ = nullptr;
	cFrameLeftTemperature_ = nullptr;
	gFrameLeftTemperature_ = nullptr;
	cFrameRightTemperature_ = nullptr;
	gFrameRightTemperature_ = nullptr;
	cChillerSetTemperature_ = nullptr;
	gChillerSetTemperature_ = nullptr;
	cAllTemperatures_   = nullptr;
	mgAllTemperatures_  = nullptr;//I NEED TO THINK ABOUT THIS!
	cDewPoint_ = nullptr;
	gDewPoint_ = nullptr;
	cModuleCarrier1LeftTemperature_ = nullptr;
	gModuleCarrier1LeftTemperature_ = nullptr;
	cModuleCarrier2LeftTemperature_ = nullptr;
	gModuleCarrier2LeftTemperature_ = nullptr;
	cModuleCarrier3LeftTemperature_ = nullptr;
	gModuleCarrier3LeftTemperature_ = nullptr;
	cModuleCarrier4LeftTemperature_ = nullptr;
	gModuleCarrier4LeftTemperature_ = nullptr;
	cModuleCarrier5LeftTemperature_ = nullptr;
	gModuleCarrier5LeftTemperature_ = nullptr;
	cModuleCarrier1RightTemperature_ = nullptr;
	gModuleCarrier1RightTemperature_ = nullptr;
	cModuleCarrier2RightTemperature_ = nullptr;
	gModuleCarrier2RightTemperature_ = nullptr;
	cModuleCarrier3RightTemperature_ = nullptr;
	gModuleCarrier3RightTemperature_ = nullptr;
	cModuleCarrier4RightTemperature_ = nullptr;
	gModuleCarrier4RightTemperature_ = nullptr;
	cModuleCarrier5RightTemperature_ = nullptr;
	gModuleCarrier5RightTemperature_ = nullptr;
	cAllModuleCarrierTemperatures_   = nullptr;
	mgAllModuleCarrierTemperatures_  = nullptr;//I NEED TO THINK ABOUT THIS!
	cDoorLockRelay_           = nullptr;
	gDoorLockRelay_           = nullptr;
	cDryAirFluxRelay_         = nullptr;
	gDryAirFluxRelay_         = nullptr;
	cChillerStatus_           = nullptr;
	gChillerStatus_           = nullptr;
	cCycleStatus_             = nullptr;
	gCycleStatus_             = nullptr;
}

// Emanuele Aucone
//========================================================================================================================
void BurninBoxDQMHistos::fill(std::string &buffer)
{
//	__COUT__ << "Filling histos!" << std::endl;
	//burninBoxStatus_.printVariables();
	try
	{
		burninBoxStatus_.convertFromJSON(buffer);
		// Filling plots with arriving data
		double timeAxis = burninBoxStatus_.getTime();
		// printf("%" PRIu64 "\n", static_cast<uint64_t>(timeAxis));

		if(timeAxis < 1000) return;// it should be 0 but with double it might be a problem. I am sure nobody run these plots in 1974
		// std::cout<< __PRETTY_FUNCTION__ << " [" << __LINE__ << "]" << "TIME FROM STATUS: " << timeAxis << " N Points: " << gAmbientTemperature_->GetN() << std::endl;
		gAmbientTemperature_            ->SetPoint(gAmbientTemperature_             ->GetN(), timeAxis, burninBoxStatus_.getAverageAmbientTemperature());
		gFrameTemperature_              ->SetPoint(gFrameTemperature_               ->GetN(), timeAxis, burninBoxStatus_.getFrameTemperature());
		gFrameLeftTemperature_          ->SetPoint(gFrameLeftTemperature_           ->GetN(), timeAxis, burninBoxStatus_.getFrameLeftTemperature());
		gFrameRightTemperature_         ->SetPoint(gFrameRightTemperature_          ->GetN(), timeAxis, burninBoxStatus_.getFrameRightTemperature());
		gChillerSetTemperature_         ->SetPoint(gChillerSetTemperature_          ->GetN(), timeAxis, burninBoxStatus_.getChillerSetTemperature());
		gDewPoint_                      ->SetPoint(gDewPoint_                       ->GetN(), timeAxis, burninBoxStatus_.getAverageDewPointTemperature());
		gModuleCarrier1LeftTemperature_ ->SetPoint(gModuleCarrier1LeftTemperature_  ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier1LeftTemperature());
		gModuleCarrier2LeftTemperature_ ->SetPoint(gModuleCarrier2LeftTemperature_  ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier2LeftTemperature());
		gModuleCarrier3LeftTemperature_ ->SetPoint(gModuleCarrier3LeftTemperature_  ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier3LeftTemperature());
		gModuleCarrier4LeftTemperature_ ->SetPoint(gModuleCarrier4LeftTemperature_  ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier4LeftTemperature());
		gModuleCarrier5LeftTemperature_ ->SetPoint(gModuleCarrier5LeftTemperature_  ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier5LeftTemperature());
		gModuleCarrier1RightTemperature_->SetPoint(gModuleCarrier1RightTemperature_ ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier1RightTemperature());
		gModuleCarrier2RightTemperature_->SetPoint(gModuleCarrier2RightTemperature_ ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier2RightTemperature());
		gModuleCarrier3RightTemperature_->SetPoint(gModuleCarrier3RightTemperature_ ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier3RightTemperature());
		gModuleCarrier4RightTemperature_->SetPoint(gModuleCarrier4RightTemperature_ ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier4RightTemperature());
		gModuleCarrier5RightTemperature_->SetPoint(gModuleCarrier5RightTemperature_ ->GetN(), timeAxis, burninBoxStatus_.getModuleCarrier5RightTemperature());
		gDoorLockRelay_                 ->SetPoint(gDoorLockRelay_                  ->GetN(), timeAxis, burninBoxStatus_.getDoorLockRelayStatus());
		gDryAirFluxRelay_               ->SetPoint(gDryAirFluxRelay_                ->GetN(), timeAxis, burninBoxStatus_.getDryAirFluxRelayStatus());
		gChillerStatus_                 ->SetPoint(gChillerStatus_                  ->GetN(), timeAxis, burninBoxStatus_.getChillerStatus());
		gCycleStatus_                   ->SetPoint(gCycleStatus_                    ->GetN(), timeAxis, burninBoxStatus_.getCycleStatus());

		// double rangeBegin;
		// double rangeEnd;
		// const double percent = 5/100.;
		// TAxis* gAxis;
		// gAxis = mgAllTemperatures_->GetHistogram()->GetXaxis();
		// rangeBegin = gAxis->GetBinCenter(1);
		// rangeEnd   = timeAxis + (timeAxis-rangeBegin)*percent;
		// std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__  << "Range begin: " << rangeBegin << " Range end: " << rangeEnd << std::endl;
		// gAxis->SetRangeUser(rangeBegin, rangeEnd);
		
		// gAxis = mgAllModuleCarrierTemperatures_->GetHistogram()->GetXaxis();
		// rangeBegin = gAxis->GetBinCenter(1);
		// rangeEnd   = timeAxis + (timeAxis-rangeBegin)*percent;
		// gAxis->SetRangeUser(rangeBegin, rangeEnd);
	}
	catch (const std::runtime_error &e)
	{
		//The status was probably not ready so just returning...
		__COUT__ << "Error: Exception filling histos!" << std::endl;
		return;
	}
}

//========================================================================================================================
void BurninBoxDQMHistos::load(std::string fileName)
{
	/*LORE 2016 MUST BE FIXED THIS MONDAY
	DQMHistosBase::openFile (fileName);
	numberOfTriggers_ = (TH1I*)theFile_->Get("General/NumberOfTriggers");

	std::string directory = "Planes";
	std::stringstream name;
	for(unsigned int p=0; p<4; p++)
	{
	    name.str("");
	    name << directory << "/Plane_" << p << "_Occupancy";
	    //FIXME Must organize better all histograms!!!!!
	    //planeOccupancies_.push_back((TH1I*)theFile_->Get(name.str().c_str()));
	}
	//canvas_ = (TCanvas*) theFile_->Get("MainDirectory/MainCanvas");
	//histo1D_ = (TH1F*) theFile_->Get("MainDirectory/Histo1D");
	//histo2D_ = (TH2F*) theFile_->Get("MainDirectory/Histo2D");
	//profile_ = (TProfile*) theFile_->Get("MainDirectory/Profile");
	closeFile();
	 */
}
