#ifndef _ots_BurninBoxDQMHistos_h_
#define _ots_BurninBoxDQMHistos_h_

#include "otsdaq/RootUtilities/DQMHistosBase.h"
#include "BurninBox/BurninBoxUtils/BurninBoxStatus.h"
#include <string>
#include <map>

class TCanvas;
class TDirectory;
class TGraph;
class TMultiGraph;

namespace ots
{

class BurninBoxDQMHistos
{

public:
        BurninBoxDQMHistos(bool deleteHistos=true);
        //BurninBoxDQMHistos(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID);
        virtual ~BurninBoxDQMHistos(void);
        
        void book         (TDirectory *myDirectory);
        void destroy      (void);
        void clearPointers(void);
        void fill         (std::string &buffer);
        void load         (std::string fileName);

protected:
        TDirectory *myDir_;
        TDirectory *statusDir_;

        TCanvas *cAmbientTemperature_;
        TGraph *gAmbientTemperature_;

        TCanvas *cFrameTemperature_;
        TGraph *gFrameTemperature_;

        TCanvas *cFrameLeftTemperature_;
        TGraph *gFrameLeftTemperature_;

        TCanvas *cFrameRightTemperature_;
        TGraph *gFrameRightTemperature_;

        TCanvas *cChillerSetTemperature_;
        TGraph *gChillerSetTemperature_;

        TCanvas *cAllTemperatures_;
        TMultiGraph *mgAllTemperatures_;

        TCanvas *cDewPoint_;
        TGraph *gDewPoint_;

        TCanvas *cModuleCarrier1LeftTemperature_;
        TGraph *gModuleCarrier1LeftTemperature_;

        TCanvas *cModuleCarrier2LeftTemperature_;
        TGraph *gModuleCarrier2LeftTemperature_;

        TCanvas *cModuleCarrier3LeftTemperature_;
        TGraph *gModuleCarrier3LeftTemperature_;

        TCanvas *cModuleCarrier4LeftTemperature_;
        TGraph *gModuleCarrier4LeftTemperature_;

        TCanvas *cModuleCarrier5LeftTemperature_;
        TGraph *gModuleCarrier5LeftTemperature_;

        TCanvas *cModuleCarrier1RightTemperature_;
        TGraph *gModuleCarrier1RightTemperature_;

        TCanvas *cModuleCarrier2RightTemperature_;
        TGraph *gModuleCarrier2RightTemperature_;

        TCanvas *cModuleCarrier3RightTemperature_;
        TGraph *gModuleCarrier3RightTemperature_;

        TCanvas *cModuleCarrier4RightTemperature_;
        TGraph *gModuleCarrier4RightTemperature_;

        TCanvas *cModuleCarrier5RightTemperature_;
        TGraph *gModuleCarrier5RightTemperature_;

        TCanvas *cAllModuleCarrierTemperatures_;
        TMultiGraph *mgAllModuleCarrierTemperatures_;

        TCanvas *cDoorLockRelay_;
        TGraph *gDoorLockRelay_;

        TCanvas *cDryAirFluxRelay_;
        TGraph *gDryAirFluxRelay_;

        TCanvas *cChillerStatus_;
        TGraph *gChillerStatus_;

        TCanvas *cCycleStatus_;
        TGraph *gCycleStatus_;

private:
        BurninBoxStatus burninBoxStatus_;
        bool            deleteHistos_;
};

}

#endif
