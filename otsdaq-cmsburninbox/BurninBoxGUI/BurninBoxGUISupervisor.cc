#include "otsdaq-cmsburninbox/BurninBoxGUI/BurninBoxGUISupervisor.h"


#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>    
#include <utility>

using namespace ots;

#undef __MF_SUBJECT__
#define __MF_SUBJECT__ "BurninBoxGUI"

/*! the XDAQ_INSTANTIATOR_IMPL(ns1::ns2::...) macro needs to be put into the
 * implementation file (.cc) of the XDAQ application */
xdaq::Application* BurninBoxGUISupervisor::instantiate(xdaq::ApplicationStub* stub)
{
	return new BurninBoxGUISupervisor(stub);
}

//==============================================================================
// new user gets a table mgr assigned
// user can fill any of the tables (fill from version or init empty), which becomes the
// active view for that table
BurninBoxGUISupervisor::BurninBoxGUISupervisor(xdaq::ApplicationStub *stub)
	: ConfigurationGUISupervisor  (stub)
	, moduleConfigurationFileName_(__ENV__("MODULE_NAMES_FILE"))
{
	__SUP_COUT__ << "Constructor started." << __E__;
	INIT_MF("." /*directory used is USER_DATA/LOG/.*/);
	__SUP_COUT__ << "Constructor complete." << __E__;

} // end constructor()

//==============================================================================
BurninBoxGUISupervisor::~BurninBoxGUISupervisor(void) {}

//==============================================================================
/*
void BurninBoxGUISupervisor::transitionConfiguring(toolbox::Event::Reference )//event)
{
	__SUP_COUT__ << "Configuring..." << __E__;

	{  // do like start of CoreSupervisorBase::transitionConfiguring
		// activate the configuration tree (the first iteration)
		if(RunControlStateMachine::getIterationIndex() == 0 &&
		   RunControlStateMachine::getSubIterationIndex() == 0)
		{
			//std::string= group name
			std::pair<std::string , TableGroupKey> theGroup(
			    SOAPUtilities::translate(theStateMachine_.getCurrentMessage())
			        .getParameters()
			        .getValue("ConfigurationTableGroupName"),
			    TableGroupKey(
			        SOAPUtilities::translate(theStateMachine_.getCurrentMessage())
			            .getParameters()
			            .getValue("ConfigurationTableGroupKey")));

			__SUP_COUT__ << "Configuration table group name: " << theGroup.first
			             << " key: " << theGroup.second << __E__;

			//disable version tracking to accept untracked versions to be selected by the FSM transition source
			theConfigurationManager_->loadTableGroup(theGroup.first, theGroup.second, true //doActivate
			,0,0,0,0,0,0,false,0,0,ConfigurationManager::LoadGroupType::ALL_TYPES,
				true //ignoreVersionTracking
				);
		}
	}  // end start like CoreSupervisorBase::transitionConfiguring

	
	try
	{
		ConfigurationTree testAppLink = theConfigurationManager_->getNode(
		    "/" +
		    theConfigurationManager_->__GET_CONFIG__(XDAQContextTable)->getTableName() +
		    CorePropertySupervisorBase::getSupervisorConfigurationPath());
	}
	catch(const std::runtime_error& e)
	{
		__SS__ << "The link to the BurninBoxGUISupervisor configuration seems to be broken. "
		          "Please check this path: "
		       << "/" +
		              theConfigurationManager_->__GET_CONFIG__(XDAQContextTable)
		                  ->getTableName() +
		              CorePropertySupervisorBase::getSupervisorConfigurationPath()
		       << __E__ << __E__ << e.what() << __E__;
		__SS_THROW__;
	}

	// ConfigurationTree appLink = theConfigurationManager_->getNode(
	//     "/" + theConfigurationManager_->__GET_CONFIG__(XDAQContextTable)->getTableName() +
	//     CorePropertySupervisorBase::getSupervisorConfigurationPath());

//	__COUTV__(appLink.getValueAsString());

	const ConfigurationTree& appLink  = CorePropertySupervisorBase::getContextTreeNode();

	std::cout << __LINE__ << " ] " << __PRETTY_FUNCTION__ << " NodeName: " << appLink.getTableName() << std::endl;

	const std::string & configurationPath = CorePropertySupervisorBase::getSupervisorConfigurationPath();

	std::cout << __LINE__ << " ] " << __PRETTY_FUNCTION__ << std::endl;
	std::cout << __LINE__ << " ] " << __PRETTY_FUNCTION__  << "Let's see if I got the right value! Config path: " << configurationPath << std::endl;
	std::cout << __LINE__ << " ] " << __PRETTY_FUNCTION__  << "2SDefaultConfigurationName: " << appLink.getNode(configurationPath).getNode("2SDefaultConfigurationName").getValue<std::string>() << std::endl;
	HttpXmlDocument cfgXml;
	if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
	{
		cfgXml.addTextElementToData("ConfigurationDir", appLink.getNode(configurationPath).getNode("ConfigurationDir").getValue<std::string>());
		cfgXml.addTextElementToData("2SDefaultConfigurationName", appLink.getNode(configurationPath).getNode("2SDefaultConfigurationName").getValue<std::string>());
		cfgXml.addTextElementToData("PSDefaultConfigurationName", appLink.getNode(configurationPath).getNode("PSDefaultConfigurationName").getValue<std::string>());
		cfgXml.saveXmlDocument(moduleConfigurationFileName_);
	}
	else
	{
		__SS__ << "Error. Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists." << __E__;
		__SUP_COUT_ERR__ << ss.str();
		//__SS_THROW_ONLY__;
		theStateMachine_.setErrorMessage(ss.str());
		throw toolbox::fsm::exception::Exception(
		    "Transition Error" //name
			, ss.str() // message
			, "BurninBoxGUISupervisor::transitionConfiguring" //module
			, __LINE__ //line
			, __FUNCTION__ //function
		);
	}


	// just handle FSMs
	CoreSupervisorBase::transitionConfiguringFSMs();

	__SUP_COUT__ << "Configured." << __E__;

}
*/
//==============================================================================
void BurninBoxGUISupervisor::request(const std::string &requestType,
									 cgicc::Cgicc &cgiIn,
									 HttpXmlDocument &xmlOut,
									 const WebUsers::RequestUserInfo &userInfo)
{
	
	std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Request: " << requestType << std::endl; 
	if(requestType == "ModuleNameFile")  //################################################################################################################
	{
		try
		{

			//	save
			//	load
			std::string cmd = cgiIn("cmd");  // possible commands are
			__SUP_COUT__ << "cmd " << cmd << __E__;

			if(cmd == "save")
			{
				std::string configuration = CgiDataUtilities::postData(cgiIn, "configuration");
				//__SUP_COUT__ << "configuration " << configuration << __E__;

				std::ofstream configurationFile;
				configurationFile.open (moduleConfigurationFileName_.c_str());
	
				if(!configurationFile.is_open())
				{
					xmlOut.addTextElementToData("status", "Error: Can't open file " + moduleConfigurationFileName_ + ". Likely the directory doesn't exist or you don't have the permissions to write in it.");
				}
				else
				{					
					configurationFile << configuration;
					configurationFile.close();


					auto now = std::chrono::system_clock::now();
					std::time_t nowTime = std::chrono::system_clock::to_time_t(now);
					char buffer[80];
					struct tm * timeInfo;
					timeInfo = localtime(&nowTime);
					strftime(buffer, sizeof(buffer), "%Y-%m-%d_%Hh%Mm%Ss", timeInfo);
					
					std::string extFileName   = moduleConfigurationFileName_.substr(moduleConfigurationFileName_.find('.'), moduleConfigurationFileName_.length()-moduleConfigurationFileName_.find('.'));
					std::string timedFileName = moduleConfigurationFileName_.substr(0, moduleConfigurationFileName_.find('.')) + "_" + std::string(buffer) + extFileName;

					//__SUP_COUT__ << extFileName << " " << timedFileName << std::endl;
					// verify proper format through read back
					HttpXmlDocument cfgXml;
					if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
					{
						// successfully loaded, re-save for formatting
						cfgXml.saveXmlDocument(moduleConfigurationFileName_);
						cfgXml.saveXmlDocument(timedFileName);
						xmlOut.addTextElementToData("status", "Success");  // success
					}
					else  // failed to load properly
					{
						xmlOut.addTextElementToData("status", "Error: Improper file format.");
					}
				}
			}
			else if(cmd == "load")
			{
				HttpXmlDocument cfgXml;
				if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
				{
					xmlOut.addTextElementToData("status", "Success");
					xmlOut.copyDataChildren(cfgXml);  // copy file to output xml
				}
				else
				{
					xmlOut.addTextElementToData("status", "Error. Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists.");
				}
			}
			else
				xmlOut.addTextElementToData("status", "Error: Unrecognized command " + cmd);
		}
		catch (const std::runtime_error &e)
		{
			__SS__ << "A fatal error occurred while handling the request '" << requestType
				<< ".' Error: " << e.what() << __E__;
			__COUT_ERR__ << "\n"
						<< ss.str();
			xmlOut.addTextElementToData("Error", ss.str());

			try
			{
				// always add version tracking bool
				xmlOut.addTextElementToData(
					"versionTracking",
					ConfigurationInterface::isVersionTrackingEnabled() ? "ON" : "OFF");
			}
			catch (...)
			{
				__COUT_ERR__ << "Error getting version tracking status!" << __E__;
			}
		}
		catch (...)
		{
			__SS__ << "An unknown fatal error occurred while handling the request '"
				<< requestType << ".'" << __E__;
			__COUT_ERR__ << "\n"
						<< ss.str();
			xmlOut.addTextElementToData("Error", ss.str());

			try
			{
				// always add version tracking bool
				xmlOut.addTextElementToData(
					"versionTracking",
					ConfigurationInterface::isVersionTrackingEnabled() ? "ON" : "OFF");
			}
			catch (...)
			{
				__COUT_ERR__ << "Error getting version tracking status!" << __E__;
			}
		}
	}
	else if(requestType == "RunNumberRequest")  //################################################################################################################
	{

		try
		{
			std::string fsmName = "CalibrationRuns";
			std::string runNumberFileName = std::string(__ENV__("USER_DATA")) + "/ServiceData/RunNumber/" + fsmName + "NextRunNumber.txt";
			std::ifstream runNumberFile(runNumberFileName.c_str());

			if(!runNumberFile.is_open())
			{
				__COUT_ERR__ << "Can't open file: " << runNumberFileName << __E__;
			}
			std::string runNumberString;

		 	runNumberFile >> runNumberString;
		 	runNumberFile.close();

			xmlOut.addTextElementToData("RunNumber",runNumberString);
		}
		catch (const std::runtime_error &e)
		{
			xmlOut.addTextElementToData("Error", e.what());

		}
		// }
		//#define RUN_NUMBER_PATH std::string(__ENV__("SERVICE_DATA_PATH")) + "/RunNumber/"

		//==============================================================================
		// // getNextRunNumber
		// //
		// //	If fsmName is passed, then get next run number for that FSM name
		// //	Else get next run number for the active FSM name, activeStateMachineName_
		// //
		// // 	Note: the FSM name is sanitized of special characters and used in the filename.
		// unsigned int GatewaySupervisor::getNextRunNumber(const std::string& fsmNameIn)
		// {
		// 	std::string runNumberFileName = RUN_NUMBER_PATH + "/";
		// 	std::string fsmName           = fsmNameIn == "" ? activeStateMachineName_ : fsmNameIn;
		// 	// prepend sanitized FSM name
		// 	for(unsigned int i = 0; i < fsmName.size(); ++i)
		// 		if((fsmName[i] >= 'a' && fsmName[i] <= 'z') || (fsmName[i] >= 'A' && fsmName[i] <= 'Z') || (fsmName[i] >= '0' && fsmName[i] <= '9'))
		// 			runNumberFileName += fsmName[i];
		// 	runNumberFileName += RUN_NUMBER_FILE_NAME;
		// 	//__COUT__ << "runNumberFileName: " << runNumberFileName << __E__;

		// 	std::ifstream runNumberFile(runNumberFileName.c_str());
		// 	if(!runNumberFile.is_open())
		// 	{
		// 		__COUT__ << "Can't open file: " << runNumberFileName << __E__;

		// 		__COUT__ << "Creating file and setting Run Number to 1: " << runNumberFileName << __E__;
		// 		FILE* fp = fopen(runNumberFileName.c_str(), "w");
		// 		fprintf(fp, "1");
		// 		fclose(fp);

		// 		runNumberFile.open(runNumberFileName.c_str());
		// 		if(!runNumberFile.is_open())
		// 		{
		// 			__SS__ << "Error. Can't create file: " << runNumberFileName << __E__;
		// 			__COUT_ERR__ << ss.str();
		// 			__SS_THROW__;
		// 		}
		// 	}
		// 	std::string runNumberString;
		// 	runNumberFile >> runNumberString;
		// 	runNumberFile.close();
		// 	return atoi(runNumberString.c_str());
		// }

	}
	else//If it is not a specific request for the Burnin then fall back on the ConfigurationGUISupervisor::request
	{
		ConfigurationGUISupervisor::request(requestType, cgiIn, xmlOut, userInfo);
	}
}


