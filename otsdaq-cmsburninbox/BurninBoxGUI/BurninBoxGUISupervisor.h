#ifndef _ots_BurninBoxGUISupervisor_h_
#define _ots_BurninBoxGUISupervisor_h_

#include "otsdaq-utilities/ConfigurationGUI/ConfigurationGUISupervisor.h"

namespace ots
{
// clang-format off

// BurninBoxGUISupervisor
//	This class handles the user requests to read and write the Configuration Tree.
class BurninBoxGUISupervisor : public ConfigurationGUISupervisor
{
  public:
	static xdaq::Application* instantiate(xdaq::ApplicationStub* s);

				BurninBoxGUISupervisor	(xdaq::ApplicationStub* s);
	virtual 	 ~BurninBoxGUISupervisor(void);
	//virtual void transitionConfiguring(toolbox::Event::Reference event) override;

	virtual void request(const std::string& requestType, cgicc::Cgicc& cgiIn, HttpXmlDocument& xmlOut, const WebUsers::RequestUserInfo& userInfo) override;

  private:	
	std::string moduleConfigurationFileName_;
};

// clang-format on
}  // namespace ots

#endif
