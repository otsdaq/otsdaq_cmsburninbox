#include "otsdaq-cmsburninbox/FEInterfaces/FECMSBeagleBoneInterface.h"
#include "otsdaq-core/Macros/CoutMacros.h"
#include "otsdaq-core/Macros/InterfacePluginMacros.h"
#include "BeagleBone/BeagleBoneUtils/SafetyRangeDefinition.h"
#include "BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"

//#include <stdio.h>
//#include <stdlib.h>
#include <iostream>
#include <iomanip>
//#include <fstream>
#include <sstream>
#include <string>
//#include <cstring> //for memcpy
//#include <set>

using namespace ots;

//size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
//    std::string data((const char*) ptr, (size_t) size * nmemb);
//    *((std::stringstream*) stream) << data << std::endl;
//    return size * nmemb;
//}

//========================================================================================================================
ots::FECMSBeagleBoneInterface::FECMSBeagleBoneInterface(const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& interfaceConfigurationPath)
: Socket             (theXDAQContextConfigTree.getNode(interfaceConfigurationPath).getNode("HostIPAddress").getValue<std::string>()
		,             theXDAQContextConfigTree.getNode(interfaceConfigurationPath).getNode("HostPort").getValue<unsigned int>())
, FEVInterface       (interfaceUID, theXDAQContextConfigTree, interfaceConfigurationPath)
, theBeagleBoneBoard_(theXDAQContextConfigTree.getNode(interfaceConfigurationPath).getNode("InterfaceIPAddress").getValue<std::string>()
		,             theXDAQContextConfigTree.getNode(interfaceConfigurationPath).getNode("InterfacePort").getValue<unsigned int>())
, verbose_(false)
{
	Socket::initialize();
	universalAddressSize_ = 8;
	universalDataSize_    = 8;

}

//========================================================================================================================
ots::FECMSBeagleBoneInterface::~FECMSBeagleBoneInterface(void)
{
}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::halt(void)
{
	std::cout << __COUT_HDR__ << "\tHalt" << std::endl;
	sendCommand("STOP");
}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::pause(void)
{
	std::cout << __COUT_HDR__ << "\tPause" << std::endl;
	sendCommand("PAUSE");
}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::resume(void)
{
	std::cout << __COUT_HDR__ << "\tResume" << std::endl;
	sendCommand("RESUME");
}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::start(std::string runNumber)
{
	std::cout << __COUT_HDR__ << "\tStart" << std::endl;
	sendCommand("START:{RunNumber:" + runNumber + "}");

	//FE0 Temperature Controller
	//FE1  ROC

	// configure
	//		Start priority 1 for temp
	//			-- dont return until you reach temperature
	//		Start priority 2 for ROC
	//		Running --
	//			ROCrunning--FE Plugin() if(getValue(Iamconfiguring)/... {}... steps, stepsize, startval, register.
	//				loop and sleep and then return true when done
	//				HelperClass -- (uses common field names)
	//

	//  coreFE -- iterations()
	//			for ()
	//				for()
	//					iterating(fields, values);


	//			iterating(std::vector<std::string> field, std::vector<std::string> value)
	//{
	//coordinate?
	//
	//do anything you want
	//return
	//}


}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::stop(void)
{
	std::cout << __COUT_HDR__ << "\tStop" << std::endl;
	sendCommand("STOP");
}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::configure(void)
{
	std::cout << __COUT_HDR__ << "\tConfigure" << std::endl;
	if(getSelfNode().getNode("SetTemperature").getValue<float>() < SafetyRangeDefinition::minTemperatureValue || getSelfNode().getNode("SetTemperature").getValue<float>() > SafetyRangeDefinition::maxTemperatureValue)
	{
		__CFG_SS__ << "SetTemperature is not in the correct range ["
				    + std::to_string(SafetyRangeDefinition::minTemperatureValue) + ","
					+ std::to_string(SafetyRangeDefinition::maxTemperatureValue) + "]";
		__CFG_SS_THROW__;
	}

	if(getSelfNode().getNode("StopTemperature").getValue<float>() < SafetyRangeDefinition::minTemperatureValue || getSelfNode().getNode("StopTemperature").getValue<float>() > SafetyRangeDefinition::maxTemperatureValue)
	{
		__CFG_SS__ << "StopTemperature is not in the correct range ["
				    + std::to_string(SafetyRangeDefinition::minTemperatureValue) + ","
					+ std::to_string(SafetyRangeDefinition::maxTemperatureValue) + "]";
		__CFG_SS_THROW__;
	}

	if(getSelfNode().getNode("LowTolerance").getValue<float>() < SafetyRangeDefinition::minTemperatureLowToleranceValue || getSelfNode().getNode("LowTolerance").getValue<float>() > SafetyRangeDefinition::maxTemperatureLowToleranceValue)
	{
		__CFG_SS__ << "LowTolerance is not in the correct range ["
				    + std::to_string(SafetyRangeDefinition::minTemperatureLowToleranceValue) + ","
					+ std::to_string(SafetyRangeDefinition::maxTemperatureLowToleranceValue) + "]";
		__CFG_SS_THROW__;
	}

	if(getSelfNode().getNode("HighTolerance").getValue<float>() < SafetyRangeDefinition::minTemperatureHighToleranceValue || getSelfNode().getNode("HighTolerance").getValue<float>() > SafetyRangeDefinition::maxTemperatureHighToleranceValue)
	{
		__CFG_SS__ << "HighTolerance is not in the correct range ["
				    + std::to_string(SafetyRangeDefinition::minTemperatureHighToleranceValue) + ","
					+ std::to_string(SafetyRangeDefinition::maxTemperatureHighToleranceValue) + "]";
		__CFG_SS_THROW__;
	}

	//BeagleBoneConfiguration theBeagleBoneConfiguration();
	std::string command	= BeagleBoneConfiguration(
							 getSelfNode().getNode("StreamToIPAddress").getValue<std::string>()
							,getSelfNode().getNode("StreamToPort")     .getValue<int>()
							,getSelfNode().getNode("SetTemperature")   .getValue<float>()
							,getSelfNode().getNode("LowTolerance")     .getValue<float>()
							,getSelfNode().getNode("HighTolerance")    .getValue<float>()
							,getSelfNode().getNode("StopTemperature")  .getValue<float>()
							).convertToJSON();
	sendCommand(command);
}

//========================================================================================================================
bool ots::FECMSBeagleBoneInterface::running(void)
{
	//if(WorkLoop::continueWorkLoop_ == false)
	//	sendCommand("SET_STOP_TEMPERATURE");

	std::string statusBuffer;
	read("STATUS?", statusBuffer);
	std::cout << "Status buffer: " << statusBuffer << std::endl;
	sleep(1);
	//if setTemperature is reached start counting 10 minutes
	return WorkLoop::continueWorkLoop_;//otherwise it stops!!!!!
}

//========================================================================================================================
void ots::FECMSBeagleBoneInterface::sendCommand(std::string buffer)
{
	std::string readBuffer;
	read(buffer,readBuffer);
	if(buffer != readBuffer.substr(0, buffer.size()))
	{
		__COUT__ << "The acknowledgment " << readBuffer
				<< " is different from the command that I sent " << buffer
				<< "! Something is really fishy on the other side, trying to retransmit!" << std::endl;
		read(buffer,readBuffer);
	}
	__COUT__ << "Received message: " << readBuffer << std::endl;
}

//========================================================================================================================
//return -1 on failure
void ots::FECMSBeagleBoneInterface::read(const std::string& sendBuffer, std::string& receiveBuffer, unsigned int timeoutSeconds)
try
{
	//__COUT__ << "Sending Command:-" << sendBuffer << "-" << std::endl;
	TransceiverSocket::send(theBeagleBoneBoard_, sendBuffer, verbose_);
	//__COUT__ << "Command sent" << std::endl;
	if(TransceiverSocket::receive(receiveBuffer, 60, 0 /*timeoutUSeconds*/,verbose_) < 0)
	{
		__SS__ << "Read failed. " << timeoutSeconds << " second timeout period reached without response." << std::endl;
		__COUT_ERR__ << "\n" << ss.str() << std::endl;
		throw std::runtime_error(ss.str());
	}

	if(verbose_)
	{
		__COUT__ << "RECEIVED MESSAGE: ";
		for(uint32_t i=0; i<receiveBuffer.size(); i++)
			std::cout << std::setfill('0') << std::setw(2) << std::hex << (((int16_t) receiveBuffer[i]) &0xff) << "-" << std::dec;
		std::cout << std::endl;
	}
}
catch(std::runtime_error &e)
{
	throw;
}
catch(...)
{
	__SS__ << "Unrecognized exception caught!" << std::endl;
	//__SS_THROW__;
}

DEFINE_OTS_INTERFACE(ots::FECMSBeagleBoneInterface)
