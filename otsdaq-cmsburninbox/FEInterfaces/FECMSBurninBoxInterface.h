#ifndef _ots_FECMSBurninBoxInterface_h_
#define _ots_FECMSBurninBoxInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq/NetworkUtilities/TCPClient.h"
#include "otsdaq/NetworkUtilities/TCPPublishServer.h"
#include "BurninBox/BurninBoxUtils/BurninBoxController.h"

#include <atomic>
#include <mutex>
#include <thread>
#include <cinttypes>

namespace ots
{
class FECMSBurninBoxInterface : public FEVInterface, public TCPClient
{

public:
	FECMSBurninBoxInterface     (const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& interfaceConfigurationPath);
	virtual ~FECMSBurninBoxInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber="");
	void stop             (void);
	bool running          (void);

	void universalRead	  (char *address, char *returnValue) override {;}
	void universalWrite	  (char *address, char *writeValue) override {;}

	//MACROS
	void stateMachine(__ARGS__);
	void setTemperature(__ARGS__);
	void waitForTarget(__ARGS__);
	void waitAtTarget(__ARGS__);
	void startMonitoringThread(__ARGS__);
	void stopMonitoringThread(__ARGS__);


private:
	std::string     sendCommand(std::string buffer);
	BurninBoxStatus checkBurninBoxStatus(bool publish=false);
	void            monitoringThread(void);

	//TCPClient         runningClient_;
	TCPPublishServer  dataPublisher_;
	std::atomic<bool> sendingCommand_;
	std::mutex        networkMutex_; // to make receiver socket thread safe
	std::string       runNumber_;
	std::atomic<bool> runningIterator_= ATOMIC_VAR_INIT(false);
	std::atomic<bool> runningRunning_= ATOMIC_VAR_INIT(false);
	std::atomic<bool> runningMonitoringThread_= ATOMIC_VAR_INIT(false);
	std::thread*      monitoringThread_;
	uint64_t          previousTimestamp_ = 0;


	bool verbose_;
	//BurninBoxController theBurninBoxController_;
	void printTime(const std::string& message = "");
};

}

#endif
