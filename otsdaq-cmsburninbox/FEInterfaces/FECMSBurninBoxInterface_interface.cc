#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#include "BurninBox/BurninBoxUtils/SafetyRangeDefinition.h"
#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#include "BurninBox/BurninBoxUtils/BurninBoxStatus.h"
#include "otsdaq-cmsburninbox/FEInterfaces/FECMSBurninBoxInterface.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"

// #include <stdio.h>
// #include <stdlib.h>
#include <iomanip>
#include <iostream>
// #include <fstream>
#include <sstream>
#include <string>
#include <ctime>

//#include <thread>
// #include <cstring> //for memcpy
// #include <set>

using namespace ots;

// size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
//    std::string data((const char*) ptr, (size_t) size * nmemb);
//    *((std::stringstream*) stream) << data << std::endl;
//    return size * nmemb;
//}

//========================================================================================================================
FECMSBurninBoxInterface::FECMSBurninBoxInterface(const std::string &interfaceUID,
                                                 const ConfigurationTree &theXDAQContextConfigTree,
                                                 const std::string &interfaceConfigurationPath)
    : FEVInterface(interfaceUID, theXDAQContextConfigTree, interfaceConfigurationPath), TCPClient(theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
                                                                                                      .getNode("InterfaceIPAddress")
                                                                                                      .getValue<std::string>(),
                                                                                                  theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
                                                                                                      .getNode("InterfacePort")
                                                                                                      .getValue<unsigned int>())
      // , runningClient_(theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
      //                 .getNode("InterfaceIPAddress")
      //                 .getValue<std::string>(),
      //             theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
      //                 .getNode("InterfacePort")
      //                 .getValue<unsigned int>())
      ,
      dataPublisher_(theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
                         .getNode("DataPublisherPort")
                         .getValue<unsigned int>(),1),
      sendingCommand_(false)
      , verbose_(false)
{
    // MACROS REGISTRATION
    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "StateMachine",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::stateMachine), // feMacroFunction
        std::vector<std::string>{"Transition"},                                                     // START,STOP"},             // namesOfInputArgs
        std::vector<std::string>{},                                                                 // namesOfOutputArgs
        1);                                                                                         // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "SetTemperature",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::setTemperature), // feMacroFunction
        std::vector<std::string>{"Temperature"},                                                      //"},             // namesOfInputArgs
        std::vector<std::string>{},                                                                   // namesOfOutputArgs
        1);                                                                                           // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "WaitForTarget",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::waitForTarget), // feMacroFunction
        std::vector<std::string>{},                                                                  //"},        // namesOfInputArgs
        std::vector<std::string>{},                                                                  // namesOfOutputArgs
        1);                                                                                          // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "WaitAtTarget",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::waitAtTarget), // feMacroFunction
        std::vector<std::string>{"WaitTimeAtTarget"},                                               //"},        // namesOfInputArgs
        std::vector<std::string>{},                                                                 // namesOfOutputArgs
        1);                                                                                         // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(                                                                   // TODO
        "StartMonitoringThread",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::startMonitoringThread), // feMacroFunction
        std::vector<std::string>{},                                                                          //"},        // namesOfInputArgs
        std::vector<std::string>{},                                                                          // namesOfOutputArgs
        1);

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(                                                                  // TODO
        "StopMonitoringThread",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::stopMonitoringThread), // feMacroFunction
        std::vector<std::string>{},                                                                         //"},        // namesOfInputArgs
        std::vector<std::string>{},                                                                         // namesOfOutputArgs
        1);

    ////////////////////////////////////////////////////////////////////////////////////
    universalAddressSize_ = 8;
    universalDataSize_ = 8;
    dataPublisher_.startAccept();
}

//========================================================================================================================
FECMSBurninBoxInterface::~FECMSBurninBoxInterface(void) {}

//========================================================================================================================
void FECMSBurninBoxInterface::configure(void)
{
    runningIterator_ = false;

    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tConfigure" << std::endl;

    BurninBoxConfiguration theConfiguration;

    unsigned numberOfConfiguredTemperatures = 0;
    for (auto &temperature : getSelfNode().getNode("/LinkToBurninBoxCycleTable").getChildren())
    {
        if (temperature.second.getNode("InUse").getValue<std::string>() == "No")
            continue;
        std::cout << "Column: " << temperature.first << " -> " << temperature.second.getNode("Temperature").getValue<float>() << std::endl;
        if (temperature.second.getNode("Temperature").getValue<float>() < SafetyRangeDefinition::minTemperatureValue ||
            temperature.second.getNode("Temperature").getValue<float>() > SafetyRangeDefinition::maxTemperatureValue)
        {
            __CFG_SS__ << "TargetTemperature is not in the correct range [" +
                              std::to_string(SafetyRangeDefinition::minTemperatureValue) + "," +
                              std::to_string(SafetyRangeDefinition::maxTemperatureValue) + "]";
            __CFG_SS_THROW__;
        }
        theConfiguration.setTemperature(
            temperature.second.getNode("TargetID").getValue<std::string>(),
            temperature.second.getNode("Temperature").getValue<float>(),
            temperature.second.getNode("TimeAtTarget").getValue<unsigned>(),
            temperature.second.getNode("Sequence").getValue<unsigned>());
        ++numberOfConfiguredTemperatures;
    }

    std::cout << "NEW JSON: " << theConfiguration.convertToJson() << std::endl;
    if (numberOfConfiguredTemperatures < 2)
    {
        __CFG_SS__ << "The configuration must have at least 2 temperatures. The target and the stop temperature.";
        __CFG_SS_THROW__;
    }

    std::string command = "Configure:" + theConfiguration.convertToJson();

    try
    {
        TCPClient::connect(30, 1000); // Tries to connect 30 times each second for 30 seconds and then gives up!
    }
    catch (const std::runtime_error &e)
    {
        __CFG_SS__ << e.what();
        __CFG_SS_THROW__;
    }

    std::string returnMessage = sendCommand(command);

    if (returnMessage.find("ERROR:") == 0)
    {
        __CFG_SS__ << returnMessage;
        __CFG_SS_THROW__;
    }
    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "ConfigureDone!" << std::endl;
}

//========================================================================================================================
void FECMSBurninBoxInterface::start(std::string runNumber)
{
    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tStart" << std::endl;
    std::string runNumber_;
    previousTimestamp_ = 0;

    sendCommand("Start:{RunNumber:" + runNumber + "}");
}

//========================================================================================================================
void FECMSBurninBoxInterface::stop(void)
{
    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tStop" << std::endl;
    std::string message = sendCommand("Stop");
    if (message == "Stopping")
    {
        std::string statusBuffer = sendCommand("Status?");
        dataPublisher_.broadcastPacket(statusBuffer);
        VStateMachine::indicateSubIterationWork();
    }
    else if (message == "StopError")
    {
        __CFG_SS__ << "Burnin box had an error while stopping: " << message;
        __CFG_SS_THROW__;
    }
    runningIterator_ = false;

    // else everything is fine
}

//========================================================================================================================
bool FECMSBurninBoxInterface::running(void)
{
    runningRunning_ = true;
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tAsking for status. Is iterator running? " << runningIterator_ << std::endl;
	printTime("FECMSBurninBoxInterface::running() Workloop: " + std::to_string(WorkLoop::continueWorkLoop_));
    try
    {
        checkBurninBoxStatus(true);
        std::this_thread::sleep_for(std::chrono::seconds(5));
    }
    catch (const std::exception &e)
    {
        if (!runningIterator_ && WorkLoop::continueWorkLoop_ == true)
        {
            WorkLoop::continueWorkLoop_ = false;
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tException: " << e.what() << std::endl;
            __FE_SS__ << e.what() << std::endl;
            __FE_SS_THROW__;
        }
    }
    runningRunning_ = false;
    return WorkLoop::continueWorkLoop_; // otherwise it stops!!!!!
}

//========================================================================================================================
void FECMSBurninBoxInterface::halt(void)
{
    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tHalt" << std::endl;
    std::string message = sendCommand("Halt");
    //std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tMessage:" << message << std::endl;

    if (message == "Stopping")
    {
        checkBurninBoxStatus(true);
        VStateMachine::indicateSubIterationWork();
    }
    else if (message == "StopError")
    {
        __FE_SS__ << "Burnin box had an error while stopping: " << message;
        __FE_SS_THROW__;
    }
    runningIterator_ = false;
}

//========================================================================================================================
void FECMSBurninBoxInterface::pause(void)
{
    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tPause" << std::endl;
    sendCommand("Pause");
}

//========================================================================================================================
void FECMSBurninBoxInterface::resume(void)
{
    std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tResume" << std::endl;
    sendCommand("Resume");
}

//========================================================================================================================
std::string FECMSBurninBoxInterface::sendCommand(std::string buffer)
{
    std::lock_guard<std::mutex> lock(networkMutex_);
    //printTime("[264] Sending: " + buffer);
    std::string readBuffer = sendAndReceivePacket(buffer);
    //printTime("[266] Received: " + readBuffer);
    ///std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tReceived message: " << readBuffer << std::endl;
    return readBuffer;
}

//========================================================================================================================
BurninBoxStatus FECMSBurninBoxInterface::checkBurninBoxStatus(bool publish)
{
    // printTime("Begin FECMSBurninBoxInterface::checkBurninBoxStatus");
    BurninBoxStatus burninBoxStatus;
    std::string statusBuffer = sendCommand("Status?");
    //printTime("[277] StatusBuffer: " + statusBuffer);
    burninBoxStatus.convertFromJSON(statusBuffer);
    if (burninBoxStatus.getCycleStatus() == static_cast<int>(BurninBoxController::Cycle::CYCLE_ERROR))
    {
        while (burninBoxStatus.getErrorMessage() == "")
        {
            std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tWaiting for correct error message." << std::endl;
            sendCommand("Status?");
            burninBoxStatus.convertFromJSON(statusBuffer);
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        if (publish)
        {
            dataPublisher_.broadcastPacket(statusBuffer);
        }
        std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "\tI got an Error from BurninBoxController: " << burninBoxStatus.getErrorMessage() << std::endl;
        __FE_SS__ << "BurninBoxController exception: " << burninBoxStatus.getErrorMessage() << std::endl;
        __FE_SS_THROW__;
    }
    else if (publish)
    {
        uint64_t timeAxis = burninBoxStatus.getTime();
        // std::cout << "TIMESTAMPS:" << std::endl;
        // printf("%" PRIu64 "\n", timeAxis);
        // printf("%" PRIu64 "\n", previousTimestamp_);
        if (timeAxis != previousTimestamp_)
        {
            dataPublisher_.broadcastPacket(statusBuffer);
            previousTimestamp_ = timeAxis;
        }
    }
    return burninBoxStatus;
}

//========================================================================================================================
void FECMSBurninBoxInterface::stateMachine(__ARGS__)
{
    runningIterator_ = true;
    // std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "# of input args = " << argsIn.size() << std::endl;
    // std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "# of output args = " << argsOut.size() << std::endl;
    for (auto &argIn : argsIn)
        std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << std::endl;

    std::string transition = __GET_ARG_IN__("Transition", std::string);
    if (transition == "START")
    {
        // RUN NUMBER!!!!!!!
        start(runNumber_);
    }
    else if (transition == "STOP")
    {
        stop();
    }
    else
    {
        __FE_SS__ << "Cannot recognize state machine command " << transition << std::endl;
        __FE_SS_THROW__;
    }
}

//========================================================================================================================
void FECMSBurninBoxInterface::setTemperature(__ARGS__)
{
    runningIterator_ = true;
    printTime("Begin FECMSBurninBoxInterface::setTemperature");
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro setTemperature!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of input args = " << argsIn.size() << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of output args = " << argsOut.size() << std::endl;
    try
    {
        checkBurninBoxStatus();
    }
    catch (const std::exception &e)
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in setTemperature! The exception is: " << e.what() << std::endl;
        WorkLoop::continueWorkLoop_ = false;
        while (runningRunning_)
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWaiting for Running state to end!" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        halt();
        runningIterator_ = false;
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in setTemperature! The exception is: " << e.what() << std::endl;
        __FE_SS__ << e.what() << std::endl;
        __FE_SS_THROW__;
    }

    // for (auto &argIn : argsIn)
    //     std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << std::endl;

    std::string temperature = __GET_ARG_IN__("Temperature", std::string);
    // SEND SET TEMPERATURE COMMAND TO BURNIN BOX!
    // std::this_thread::sleep_for (std::chrono::seconds(1));
    const int maxAttempts = 5;
    int attempt = 0;
    while(true)
    {
        std::string message = sendCommand("SetTargetTemperature:{Temperature:" + temperature + "}");
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << message << std::endl;
        if(message == "TemperatureSet")
            break;
        else if(message.find("ERROR:") != std::string::npos)
        {
            if(message.find("Timeout") != std::string::npos)
            {
                if(attempt < maxAttempts)
                {
                    ++attempt;
                    std::this_thread::sleep_for(std::chrono::seconds(5));
                    continue;
                }
            }
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in FECMSBurninBoxInterface::setTemperature! The error is: " << message << std::endl;
            __FE_SS__ << message << __E__;
            __FE_SS_THROW__;
        }
    }
    printTime("End   FECMSBurninBoxInterface::setTemperature");
    //std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Done Macro setTemperature!!!!" << std::endl;
}

//========================================================================================================================
void FECMSBurninBoxInterface::waitForTarget(__ARGS__)
{
    runningIterator_ = true;
    printTime("Begin FECMSBurninBoxInterface::waitForTarget");
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro waitForTarget!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro waitForTarget!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro waitForTarget!!!!" << std::endl;
    BurninBoxStatus burninBoxStatus;
    while (true) // should it be an atomic?
    {
        // std::string statusBuffer = sendCommand("Status?");
        try
        {
            burninBoxStatus = checkBurninBoxStatus();
            if (burninBoxStatus.getCycleStatus() == static_cast<int>(BurninBoxController::Cycle::CYCLE_TARGET))
                break;
            std::this_thread::sleep_for(std::chrono::seconds(10));
        }
        catch (const std::exception &e)
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in waitForTarget! The exception is: " << e.what() << std::endl;
            WorkLoop::continueWorkLoop_ = false;
            while (runningRunning_)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWaiting for Running state to end!" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
            halt();
            runningIterator_ = false;
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in waitForTarget! The exception is: " << e.what() << std::endl;
            __FE_SS__ << e.what() << __E__;
            __FE_SS_THROW__;
        }
    }
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone Macro waitForTarget!!!!" << std::endl;
    printTime("End   FECMSBurninBoxInterface::waitForTarget");
}

//========================================================================================================================
void FECMSBurninBoxInterface::waitAtTarget(__ARGS__)
{
    runningIterator_ = true;
    printTime("Begin FECMSBurninBoxInterface::waitatTarget");
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro WAIT_AT_TARGET!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro WAIT_AT_TARGET!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro WAIT_AT_TARGET!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro WAIT_AT_TARGET!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of input args = " << argsIn.size() << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of output args = " << argsOut.size() << std::endl;
    // checkBurninBoxStatus();
    // for (auto &argIn : argsIn)
    //     std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << std::endl;

    int waitAtTargetTime = stoi(__GET_ARG_IN__("WaitTimeAtTarget", std::string));
    int checkTime = 5; // seconds

    while (waitAtTargetTime > 0)
    {
        try
        {
            checkBurninBoxStatus();
        }
        catch (const std::exception &e)
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in waitAtTarget! The exception is: " << e.what() << std::endl;
            WorkLoop::continueWorkLoop_ = false;
            while (runningRunning_)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWaiting for Running state to end!" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
            halt();
            runningIterator_ = false;
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThrowing an exception in waitAtTarget! The exception is: " << e.what() << std::endl;
            __FE_SS__ << e.what() << std::endl;
            __FE_SS_THROW__;
        }
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWaiting at target for another " << waitAtTargetTime << " seconds." << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(checkTime));
        waitAtTargetTime -= checkTime;
    }

    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone Macro waitAtTarget!!!!" << std::endl;
    printTime("End   FECMSBurninBoxInterface::waitAtTarget");
}

//========================================================================================================================
void FECMSBurninBoxInterface::startMonitoringThread(__ARGS__)
{
    runningIterator_ = true;
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro startMonitoringThread!!!!" << std::endl;
    // checkBurninBoxStatus();

    // for (auto &argIn : argsIn)
    //     std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << std::endl;

    runningMonitoringThread_ = true;
    monitoringThread_ = new std::thread(&FECMSBurninBoxInterface::monitoringThread, this);
}

//========================================================================================================================
void FECMSBurninBoxInterface::stopMonitoringThread(__ARGS__)
{
    runningIterator_ = true;
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin Macro stopMonitoringThread!!!!" << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of input args = " << argsIn.size() << std::endl;
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t# of output args = " << argsOut.size() << std::endl;
    // checkBurninBoxStatus();

    // for (auto &argIn : argsIn)
    //     std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << argIn.first << ": " << argIn.second << std::endl;

    runningMonitoringThread_ = false;
    monitoringThread_->join();
    delete monitoringThread_;
}

//========================================================================================================================
void FECMSBurninBoxInterface::monitoringThread()
{
    while (runningMonitoringThread_)
    {
        std::cout << "[" << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Running thread!" << std::endl;
        try
        {
            checkBurninBoxStatus(false);
        }
        catch (const std::exception &e)
        {
            WorkLoop::continueWorkLoop_ = false;
            runningMonitoringThread_ = false;
            __SS__ << "BurninBoxController exception: " << e.what() << std::endl;
            __SS_THROW__;
        }
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
}

//========================================================================================================================
void FECMSBurninBoxInterface::printTime(const std::string& message)
{
    auto now = std::chrono::system_clock::now();
    std::time_t current_time = std::chrono::system_clock::to_time_t(now);
    std::tm local_time = *std::localtime(&current_time);

    // Custom format: YYYY-MM-DD HH:MM:SS
    std::cout << "Current time: " 
              << std::put_time(&local_time, "%Y-%m-%d %H:%M:%S") 
			  << ".\t" << message
              << std::endl;
}

DEFINE_OTS_INTERFACE(ots::FECMSBurninBoxInterface)
