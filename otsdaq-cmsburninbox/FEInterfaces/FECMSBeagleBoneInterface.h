#ifndef _ots_FECMSBeagleBoneInterface_h_
#define _ots_FECMSBeagleBoneInterface_h_

#include "otsdaq-core/FECore/FEVInterface.h"
#include "otsdaq-core/NetworkUtilities/TransceiverSocket.h"

namespace ots
{
class FECMSBeagleBoneInterface : public FEVInterface, public TransceiverSocket
{

public:
	FECMSBeagleBoneInterface     (const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& interfaceConfigurationPath);
	virtual ~FECMSBeagleBoneInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber="");
	void stop             (void);
	bool running          (void);

	void universalRead	  (char *address, char *returnValue) override {;}
	void universalWrite	  (char *address, char *writeValue) override {;}

	void sendCommand(std::string buffer);

private:
	void read(const std::string& sendBuffer , std::string& receiveBuffer, unsigned int timeoutSeconds = 5);
	Socket theBeagleBoneBoard_;
	bool   verbose_;
};

}

#endif
