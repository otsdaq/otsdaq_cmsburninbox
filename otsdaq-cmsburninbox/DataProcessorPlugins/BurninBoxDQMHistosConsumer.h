#ifndef _ots_BurninBoxDQMHistosConsumer_h_
#define _ots_BurninBoxDQMHistosConsumer_h_

#include "otsdaq/Configurable/Configurable.h"
#include "otsdaq/DataManager/DQMHistosConsumerBase.h"

namespace ots
{

	class BurninBoxDQMHistos;

	class BurninBoxDQMHistosConsumer : public DQMHistosConsumerBase, public Configurable
	{
	public:
		BurninBoxDQMHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree &theXDAQContextConfigTree, const std::string &configurationPath);
		virtual ~BurninBoxDQMHistosConsumer(void);

		// void configure(void) override;
		void startProcessingData(std::string runNumber) override;
		void stopProcessingData(void) override;
		void pauseProcessingData(void) override;
		void resumeProcessingData(void) override;
		void load(std::string fileName) { ; }

	private:
		bool workLoopThread(toolbox::task::WorkLoop *workLoop);
		void fastRead(void);
		void slowRead(void);

		// For fast read
		std::string *dataP_;
		std::map<std::string, std::string> *headerP_;

		bool saveFile_; // yes or no
		std::string filePath_;
		std::string radixFileName_;
		BurninBoxDQMHistos *dqmHistos_;
	};
}

#endif
