#include "otsdaq-cmsburninbox/DataProcessorPlugins/BurninBoxDQMHistosConsumer.h"
#include "otsdaq-cmsburninbox/DQMHistos/BurninBoxDQMHistos.h"
#include "otsdaq/MessageFacility/MessageFacility.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/ProcessorPluginMacros.h"

#include <TDirectory.h>
#include <TFile.h>

#include <chrono>
#include <thread>

using namespace ots;

//========================================================================================================================
BurninBoxDQMHistosConsumer::BurninBoxDQMHistosConsumer(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
: WorkLoop             (processorUID)
, DQMHistosConsumerBase(supervisorApplicationUID, bufferUID, processorUID, LowConsumerPriority)
, Configurable         (theXDAQContextConfigTree, configurationPath)
, saveFile_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("SaveFile").getValue<bool>())
, filePath_            (theXDAQContextConfigTree.getNode(configurationPath).getNode("FilePath").getValue<std::string>())
, radixFileName_       (theXDAQContextConfigTree.getNode(configurationPath).getNode("RadixFileName").getValue<std::string>())
, dqmHistos_           (nullptr)

{
	//	gStyle->SetPalette(1);
}

//========================================================================================================================
BurninBoxDQMHistosConsumer::~BurninBoxDQMHistosConsumer(void)
{
	DQMHistosBase::closeFile();
	if(dqmHistos_ != nullptr)
	{
		delete dqmHistos_;
		dqmHistos_ = nullptr;
	}
}

//========================================================================================================================
//void BurninBoxDQMHistosConsumer::configure(void)
//{
//}

//========================================================================================================================
void BurninBoxDQMHistosConsumer::startProcessingData(std::string runNumber)
{

	DQMHistosBase::openFile(filePath_ + "/" + radixFileName_ + runNumber + ".root");
	DQMHistosBase::myDirectory_ = DQMHistosBase::theFile_->mkdir("BurninBox", "BurninBox");
	DQMHistosBase::myDirectory_->cd();

	__COUT__ << "Creating Histograms" << std::endl;
	dqmHistos_ = new BurninBoxDQMHistos(false);
	dqmHistos_->book(myDirectory_);
	DataConsumer::startProcessingData(runNumber);
	__COUT__ << __PRETTY_FUNCTION__ << ": Done starting DQM!" << std::endl;
}

//========================================================================================================================
void BurninBoxDQMHistosConsumer::stopProcessingData(void)
{
	DataConsumer::stopProcessingData();
	if(saveFile_)
	{
		DQMHistosBase::save();
	}
	closeFile();
}

//========================================================================================================================
void BurninBoxDQMHistosConsumer::pauseProcessingData(void)
{
	DataConsumer::stopProcessingData();
}

//========================================================================================================================
void BurninBoxDQMHistosConsumer::resumeProcessingData(void)
{
	DataConsumer::startProcessingData("");
}

//========================================================================================================================
bool BurninBoxDQMHistosConsumer::workLoopThread(toolbox::task::WorkLoop* workLoop)
{
	//std::cout << __COUT_HDR_FL__ << __PRETTY_FUNCTION__ << DataProcessor::processorUID_ << " running, because workloop: " << WorkLoop::continueWorkLoop_ << std::endl;
	fastRead();
	return WorkLoop::continueWorkLoop_;
}

//========================================================================================================================
void BurninBoxDQMHistosConsumer::fastRead(void)
{
	if(DataConsumer::read(dataP_, headerP_) < 0)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		return;
	}
	{
		std::unique_lock<std::mutex> lock(DQMHistosConsumerBase::fillHistoMutex_);
	    dqmHistos_->fill(*dataP_);
		if (saveFile_)
		{
			DQMHistosBase::autoSave();//Saves every 5 minutes
		}
	}
	DataConsumer::setReadSubBuffer<std::string, std::map<std::string, std::string>>();
	// __COUT__ << __PRETTY_FUNCTION__ << ": Done Processing buffer block." << std::endl;
}

DEFINE_OTS_PROCESSOR(BurninBoxDQMHistosConsumer)
