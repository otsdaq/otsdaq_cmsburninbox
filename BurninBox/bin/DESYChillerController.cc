// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <iostream>
#include <string>
#include <sstream>

#include "BurninBox/BurninBoxUtils/ControllerBoard.h"
#include "BurninBox/BurninBoxUtils/Device.h"


#define PORT 10001
#define MAXLINE 1024

int openSocket(std::string ipAddress)
{
  int sockfd;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in servaddr;
  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT);
  if (inet_aton(ipAddress.c_str(), &servaddr.sin_addr) == 0)
  {
    std::cout << "Invalid port" << std::endl;
    exit(EXIT_FAILURE);
  }

  memset(&(servaddr.sin_zero), '\0', 8); // zero the rest of the struct

  if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
  {
    perror("Error connecting socket!");
    exit(EXIT_FAILURE);
  };

  return sockfd;
}

using namespace ots;

// Driver code
int main(int argc, char *argv[])
{
  Device theChillerDevice_ = Device("Chiller", "/dev/ttyUSB0", "Chiller", "System chiller", "on");

  if (theChillerDevice_.getControllerConnector() != "BurninController" && 
      theChillerDevice_.getControllerConnector().find("/dev/tty") != 0)
  {
      throw std::runtime_error("ERROR: The Chiller can only be connected to the BurninController or any /dev/tty serial!");
  }

  ControllerBoard* theControllerBoard_ = new ControllerBoard("Network",
                                                             "v1",
                                                             "192.168.0.187",
                                                             10001);

  int socket;
  std::stringstream commandList;
  commandList << std::endl
    << "set-setpoint0" << std::endl
    << "set-temp value" << std::endl
    << "read-set-temp" << std::endl
    << "read-temp" << std::endl
    << "start" << std::endl
    << "stop" << std::endl
    << "ison" << std::endl;

  std::string command = "";
  std::string value = "";

  std::cout << "Argc: " << argc << std::endl; 
  if(argc == 1 || argc > 3)
  {
    std::cout << "Usage: ./DESYChillerController command" << std::endl;
    std::cout << "Command list:" << std::endl;
    std::cout << commandList.str() << std::endl;
    exit(0);
  }
  
  command = argv[1];
  if(argc == 3)//This must be the set temp command
  {
    value = argv[2];
  }

  std::cout << "Command: " << command << std::endl;

  float returnMessage;
  if (command.find("set-setpoint0") == 0)
    theControllerBoard_->setChillerSetPoint(theChillerDevice_);
  else if (command.find("set-temp") == 0)
    theControllerBoard_->setChillerTemperature(theChillerDevice_, std::stof(value));
  else if (command.find("read-set-temp") == 0)
    returnMessage = theControllerBoard_->readChillerSetTemperature(theChillerDevice_);
  else if (command.find("read-temp") == 0)
    returnMessage = theControllerBoard_->readTemperature(theChillerDevice_);
  else if (command.find("ison") == 0)
    returnMessage = theControllerBoard_->isChillerOn(theChillerDevice_);
  else if (command.find("start") == 0)
    theControllerBoard_->turnOnChiller(theChillerDevice_);
  else if (command.find("stop") == 0)
    theControllerBoard_->turnOffChiller(theChillerDevice_);
  else
  {
    std::cout << "Unrecognized command: " << command << std::endl;
    exit(0);
  }

  if(command.find("read") != std::string::npos)
    std::cout << command.substr(5,command.length()-5) << ": " << returnMessage << std::endl;
  else if (command.find("ison") != std::string::npos)
    std::cout << command << ": " << returnMessage << std::endl;

  return 0;
}
