// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <iostream>
#include <string>

#define PORT 5001
#define ADDRESS "127.0.0.1"
#define MAXLINE 1024

std::string encode(const std::string& message)
{
	uint32_t    size   = htonl(4 + message.length());
	std::string buffer = std::string(4, ' ') + message;  // THE HEADER LENGTH IS SET TO 4 = sizeof(uint32_t)
	buffer[0]          = (size)&0xff;
	buffer[1]          = (size >> 8) & 0xff;
	buffer[2]          = (size >> 16) & 0xff;
	buffer[3]          = (size >> 24) & 0xff;
	return buffer;
}

std::string fBuffer;  // This is Header + Message
//==============================================================================
bool decode(std::string& message)
{
	const unsigned headerLength = 4;
	if(fBuffer.length() < headerLength)
		return false;
	uint32_t length = ntohl(reinterpret_cast<uint32_t&>(fBuffer.at(0)));  // THE HEADER IS FIXED TO SIZE 4 = SIZEOF(uint32_t)
	if(fBuffer.length() == length)
	{
		message = fBuffer.substr(headerLength);
		fBuffer.clear();
		return true;
	}
	else if(fBuffer.length() > length)
	{
		message = fBuffer.substr(headerLength, length - headerLength);
		fBuffer.erase(0, length);
		return true;
	}
	else
	{
		// std::cout << __PRETTY_FUNCTION__ << "Can't decode an incomplete message! Length is only: " << fBuffer.length() << std::endl;
		return false;
	}
}

int openSocket()
{
  int sockfd;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in servaddr;
  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT);
  if (inet_aton(ADDRESS, &servaddr.sin_addr) == 0)
  {
    std::cout << "Invalid port" << std::endl;
    exit(EXIT_FAILURE);
  }

  memset(&(servaddr.sin_zero), '\0', 8); // zero the rest of the struct

  if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
  {
    perror("Error connecting socket!");
    exit(EXIT_FAILURE);
  };

  return sockfd;
}

// Driver code
int main(int argc, char *argv[])
{
  int socket;

  if (argc < 2)
  {
    std::cout << "Usage: ./CommandBurninBoxController command" << std::endl;
    std::cout << "Command list:" << std::endl;
    std::cout << "firmware" << std::endl;
    std::cout << "lock-on" << std::endl;
    std::cout << "lock-off" << std::endl;
    std::cout << "gas-on" << std::endl;
    std::cout << "gas-off" << std::endl;
    std::cout << "set-setpoint0" << std::endl;
    std::cout << "set-temp value" << std::endl;
    std::cout << "read-set-temp" << std::endl;
    std::cout << "read-bath" << std::endl;
    std::cout << "read-temp" << std::endl;
    std::cout << "start" << std::endl;
    std::cout << "stop" << std::endl;
    std::cout << "ison" << std::endl;
    std::cout << "read-digitals" << std::endl;
    std::cout << "read-dewpoint" << std::endl;
    std::cout << "read-otherdewpoint" << std::endl;
    std::cout << "read-rtds" << std::endl;
    exit(0);
  }


  std::string command = argv[1];
  std::string message;
  std::string value;
  if (command.find("firmware") == 0)
    message = "[02]";
  else if (command.find("lock-on") == 0)
    message = "[5011]";
  else if (command.find("lock-off") == 0)
    message = "[5010]";
  else if (command.find("gas-on") == 0)
    message = "[5021]";
  else if (command.find("gas-off") == 0)
    message = "[5020]";
  else if (command.find("set-setpoint0") == 0)
    message = "[60OUT_MODE_01 0]";
  else if (command.find("set-temp") == 0)
    message = "[60OUT_SP_00 " + std::string(argv[2]) + "]";
  else if (command.find("read-set-temp") == 0)
    message = "[61IN_SP_00]";
  else if (command.find("read-bath") == 0)
    message = "[61IN_PV_00]";
  else if (command.find("read-temp") == 0)
    message = "[61IN_PV_02]";
  else if (command.find("ison") == 0)
    message = "[61IN_MODE_05]";
  else if (command.find("start") == 0)
    message = "[60OUT_MODE_05 1]";
  else if (command.find("stop") == 0)
    message = "[60OUT_MODE_05 0]";
  else if (command.find("read-digitals") == 0)
    message = "[40]";
  else if (command.find("read-dewpoint") == 0)
    message = "[30]";
  else if (command.find("read-otherdewpoint") == 0)
    message = "[31]";
  else if (command.find("read-rtds") == 0)
    message = "[20]";
  else
  {
    std::cout << "Unrecognized command: " << command << std::endl;
    exit(0);
  }
  
  std::cout << "Executing command: " << command << std::endl;
  std::cout << "Opening socket" << std::endl;
  socket = openSocket();
  std::cout << "Socket open." << std::endl;
  
  std::string messageToSend = encode("COMMAND:" + message);

  int bytes;

  for (unsigned i = 0; i < 1; i++)
  {
    if ((bytes = send(socket, messageToSend.c_str(), messageToSend.size(), 0)) == -1)
    {
      perror("Error sending message!");
    }
    std::cout << "Message-" << message << "- sent!" << std::endl;

	std::string buffer;
	buffer.resize(MAXLINE);
	fBuffer.clear();
	std::string retVal = "";
	unsigned fPacket = 0;
	do
	{
		fPacket += recv(socket, &(buffer.at(0)), MAXLINE, 0);
		fBuffer += buffer;
	}
	while(!decode(retVal));
	std::cout << __PRETTY_FUNCTION__ << "Returned message: " << retVal << std::endl;
    
  }
  close(socket);

  return 0;
}
