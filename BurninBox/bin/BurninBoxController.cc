#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/ConfigurationReader.h"
#include "BurninBox/BurninBoxUtils/DeviceContainer.h"
#include "BurninBox/BurninBoxUtils/BurninBoxController.h"
#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#else
#include "ConfigurationReader.h"
#include "DeviceContainer.h"
#include "BurninBoxController.h"
#endif

#include <iostream>
#include <thread>
#include <chrono>

using namespace ots;

int main(int argc, char **argv)
{
	std::cout << "Starting the BurninBox" << std::endl;
	//This class is responsible for reading the configuration xml files and parse it producing a list of the configured devices to the console
	ConfigurationReader theConfiguration;

	//This class knows how to control the BurninBox using the devices that have been configured
	BurninBoxController theBurninBoxController;

	std::cout << "Initialize the BurninBox" << std::endl;
	try
	{
		theConfiguration.readConfiguration();
		theBurninBoxController.checkConfiguration(theConfiguration);
	}
	catch(const std::runtime_error& error)
	{
		std::cout << "////////////////       ERROR     /////////////////////////////" << std::endl;
		std::cout << error.what() << std::endl;
		std::cout << "//////////////////////////////////////////////////////////////" << std::endl;
        return 1; // Indicate an error occurred
	}

	std::cout << "Running BurninBox!" << std::endl;
	theBurninBoxController.startAccept();

	while(1) //listen to port BurninBox_PORT
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		//break;
	}

	return 0;
}
