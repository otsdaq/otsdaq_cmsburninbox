// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <iostream>
#include <string>
#include "BurninBoxConfiguration.h"

#define PORT 5001
#define ADDRESS "127.0.0.1"
#define MAXLINE 1024

std::string encode(const std::string &message)
{
  uint32_t size = htonl(4 + message.length());
  std::string buffer = std::string(4, ' ') + message; // THE HEADER LENGTH IS SET TO 4 = sizeof(uint32_t)
  buffer[0] = (size)&0xff;
  buffer[1] = (size >> 8) & 0xff;
  buffer[2] = (size >> 16) & 0xff;
  buffer[3] = (size >> 24) & 0xff;
  return buffer;
}

std::string fBuffer; // This is Header + Message
//==============================================================================
bool decode(std::string &message)
{
  const unsigned headerLength = 4;
  if (fBuffer.length() < headerLength)
    return false;
  uint32_t length = ntohl(reinterpret_cast<uint32_t &>(fBuffer.at(0))); // THE HEADER IS FIXED TO SIZE 4 = SIZEOF(uint32_t)
  if (fBuffer.length() == length)
  {
    message = fBuffer.substr(headerLength);
    fBuffer.clear();
    return true;
  }
  else if (fBuffer.length() > length)
  {
    message = fBuffer.substr(headerLength, length - headerLength);
    fBuffer.erase(0, length);
    return true;
  }
  else
  {
    // std::cout << __PRETTY_FUNCTION__ << "Can't decode an incomplete message! Length is only: " << fBuffer.length() << std::endl;
    return false;
  }
}

int openSocket()
{
  int sockfd;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in servaddr;
  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT);
  if (inet_aton(ADDRESS, &servaddr.sin_addr) == 0)
  {
    std::cout << "Invalid port" << std::endl;
    exit(EXIT_FAILURE);
  }

  memset(&(servaddr.sin_zero), '\0', 8); // zero the rest of the struct

  if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
  {
    perror("Error connecting socket!");
    exit(EXIT_FAILURE);
  };

  return sockfd;
}

void sendMessage(std::string message)
{
  int socket;
  std::cout << "Opening socket" << std::endl;
  socket = openSocket();
  std::cout << "Socket open." << std::endl;

  std::string messageToSend = encode(message);

  int bytes;

  for (unsigned i = 0; i < 1; i++)
  {
    if ((bytes = send(socket, messageToSend.c_str(), messageToSend.size(), 0)) == -1)
    {
      perror("Error sending message!");
    }
    std::cout << "Message-" << message << "- sent!" << std::endl;

    std::string buffer;
    buffer.resize(MAXLINE);
    fBuffer.clear();
    std::string retVal = "";
    unsigned fPacket = 0;
    do
    {
      fPacket += recv(socket, &(buffer.at(0)), MAXLINE, 0);
      fBuffer += buffer;
    } while (!decode(retVal));
    std::cout << __PRETTY_FUNCTION__ << "Returned message: " << retVal << std::endl;
  }
  close(socket);
  usleep(1000000);
}
// Driver code
int main(int argc, char *argv[])
{
  std::cout << "Command list:" << std::endl;
  std::cout << "start" << std::endl;
  std::cout << "stop" << std::endl;
  std::cout << "set-temp value" << std::endl;

  bool status = true;
  std::string command;
  std::string value;
  unsigned sequence = 0;
  while (status)
  {
    std::cout << "Enter command (start, stop, set-temp value): ";
    std::cin >> command;
    std::cout << command << std::endl;

    std::string message;
    std::string value;
    if (command.find("start") == 0)
    {
      sequence = 0;
      ots::BurninBoxConfiguration theConfiguration;

      theConfiguration.setTemperature("SafeTemperature", 20, -1, 1000);
      theConfiguration.setTemperature("StopTemperature", 20, 0, 1001);

      message = "CONFIGURE:" + theConfiguration.convertToJson();
      sendMessage(message);
      message = "START:{RunNumber: 1}";
      sendMessage(message);
    }
    else if (command.find("stop") == 0)
    {
      message = "HALT";
      sendMessage(message);
    }
    else if (command.find("set-temp") == 0)
    {
      std::cin >> value;
      int len;
      float fValue;
      int ret = sscanf(&value.at(0), "%f %n", &fValue, &len);
      if (ret == 0 || (ret == 1 && len != (int)value.length()))
      {
        std::cout << "ERROR: The temperature value: " << value << " is not a float! Retry :).\nWaiting 5 seconds before you can insert a new value!" << std::endl;
        usleep(5000000);
        continue;
      }
      if (fValue < -40 || fValue > 40)
      {
        std::cout << "ERROR: The temperature value: " << fValue << " is outside the range -40C - +40C! Retry :).\nWaiting 5 seconds before you can insert a new value!" << std::endl;
        usleep(5000000);
        continue;
      }

      std::cout << ret << " : " << fValue << " : " << len - value.length() << std::endl;
      printf("%d", ret == 1 && !value.length());
      
      message = "AddTemperature:{Name:Temp" + value + ", Temperature:" + value + ", Time:-1,Sequence:" + std::to_string(++sequence) + "}";
      sendMessage(message);
      message = "SetTargetTemperature:{Temperature:Temp" + value + "}";
      sendMessage(message);
    }
    else
    {
      std::cout << "ERROR: Unrecognized command: " << command << ".Retry :).\nWaiting 5 seconds before you can insert a new value!" << std::endl;
      usleep(5000000);
      continue;
    }
  }
  return 0;
}
