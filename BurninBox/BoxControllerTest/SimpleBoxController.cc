// Client side implementation of UDP client-server model
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <iostream>
#include <string>

#define PORT 10001
#define ADDRESS "192.168.0.187"
#define MAXLINE 1024

int openSocket()
{
  int sockfd;

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in servaddr;
  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(PORT);
  if (inet_aton(ADDRESS, &servaddr.sin_addr) == 0)
  {
    std::cout << "Invalid port" << std::endl;
    exit(EXIT_FAILURE);
  }

  memset(&(servaddr.sin_zero), '\0', 8); // zero the rest of the struct

  if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
  {
    perror("Error connecting socket!");
    exit(EXIT_FAILURE);
  };

  return sockfd;
}

// Driver code
int main(int argc, char *argv[])
{
  int socket;

  if (argc < 2)
  {
    std::cout << "Usage: ./SimpleBoxController command" << std::endl;
    std::cout << "Command list:" << std::endl;
    std::cout << "lock-on" << std::endl;
    std::cout << "lock-off" << std::endl;
    std::cout << "gas-on" << std::endl;
    std::cout << "gas-off" << std::endl;
    std::cout << "set-temp value" << std::endl;
    std::cout << "read-set-temp" << std::endl;
    std::cout << "read-temp" << std::endl;
    std::cout << "start" << std::endl;
    std::cout << "stop" << std::endl;
    std::cout << "ison" << std::endl;
    std::cout << "read-digitals" << std::endl;
    std::cout << "read-dewpoint" << std::endl;
    std::cout << "read-rtds" << std::endl;
    exit(0);
  }


  std::string command = argv[1];
  std::string message;
  std::string value;
  if (command.find("lock-on") == 0)
    message = "[5011]";
  else if (command.find("lock-off") == 0)
    message = "[5010]";
  else if (command.find("gas-on") == 0)
    message = "[5021]";
  else if (command.find("gas-off") == 0)
    message = "[5020]";
  else if (command.find("set-temp") == 0)
    message = "[60OUT_SP_00 " + std::string(argv[2]) + "]";
  else if (command.find("read-set-temp") == 0)
    message = "[61IN_SP_00]";
  else if (command.find("read-temp") == 0)
    message = "[61IN_PV_02]";
  else if (command.find("ison") == 0)
    message = "[61IN_MODE_05]";
  else if (command.find("start") == 0)
  {
    message = "[60OUT_MODE_05 1]";
    value = "[61IN_SP_00]";
    std::cout << "temp value " << value << std::endl;
  }
  else if (command.find("stop") == 0)
    message = "[60OUT_MODE_05 0]";
  else if (command.find("read-digitals") == 0)
    message = "[40]";
  else if (command.find("read-dewpoint") == 0)
    message = "[30]";
  else if (command.find("read-rtds") == 0)
    message = "[20]";
  else
  {
    std::cout << "Unrecognized command: " << command << std::endl;
    exit(0);
  }
  std::cout << "Running command: " << command << std::endl;

  socket = openSocket();

  std::string buffer("", MAXLINE);
  int  messageLength;
  std::string retMessage;
  int bytes;

  for (unsigned i = 0; i < 1; i++)
  {
    retMessage.resize(MAXLINE);
    if ((bytes = send(socket, message.c_str(), message.size(), 0)) == -1)
    {
      perror("Error sending message!");
    }
    std::cout << "Message-" << message << "- sent!" << std::endl;

    messageLength = 0;
    retMessage.clear();
    do
    {
      if ((bytes = recv(socket, &(buffer.at(0)), MAXLINE, 0)) == -1)
      {
        perror("Error receiving message!");
      }
      //std::cout << "Bytes received: " << bytes << " buffer:-" << buffer.substr(0,bytes) << "-" << std::endl;
      messageLength += bytes;
      retMessage += buffer.substr(0,bytes);
    } while (buffer[bytes-1] != ']');

    std::cout << "Bytes received: " << messageLength << " buffer:-" << retMessage << "-" << std::endl;

    
  }
  close(socket);

  return 0;
}

//UDP
// Driver code
// int main() {
//     int sockfd;
//     char buffer[MAXLINE];
//     char hello[5] = "[00]";
//     struct sockaddr_in     servaddr;

//     // Creating socket file descriptor
//     if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
//         perror("socket creation failed");
//         exit(EXIT_FAILURE);
//     }

//     memset(&servaddr, 0, sizeof(servaddr));

//     // Filling server information
//     servaddr.sin_family = AF_INET;
//     servaddr.sin_port = htons(PORT);
// 	if(inet_aton("169.254.226.56", &servaddr.sin_addr) == 0)
// 	{
//         std::cout << "Invalid port" << std::endl;
// 	}

// 	memset(&(servaddr.sin_zero), '\0', 8);  // zero the rest of the struct
//     // servaddr.sin_addr.s_addr = "169.254.226.56";

//     int n;
//     socklen_t len;

//     sendto(sockfd, (const char *)hello, strlen(hello),
//         MSG_CONFIRM, (const struct sockaddr *) &servaddr,
//             sizeof(servaddr));
//     printf("Hello message sent.\n");

//     n = recvfrom(sockfd, (char *)buffer, MAXLINE,
//                 MSG_WAITALL, (struct sockaddr *) &servaddr,
//                 &len);
//     buffer[n] = '\0';
//     printf("Server : %s\n", buffer);

//     close(sockfd);
//     return 0;
// }
