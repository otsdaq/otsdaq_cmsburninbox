#include "BurninBoxDQMHistos.h"


#include <iostream>
#include <string>
#include <sstream>

#include <TH1.h>
#include <TH2.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TRandom.h>
#include <TThread.h>
#include <TROOT.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TMultiGraph.h>


using namespace ots;

//========================================================================================================================
BurninBoxDQMHistos::BurninBoxDQMHistos(void)
: cAmbientTemperature_ (nullptr)
, cPlateTemperature_   (nullptr)
, cDewPoint_           (nullptr)
, cTemperatureMonitor_ (nullptr)
, gAmbientTemperature_ (nullptr)
, gPlateTemperature_   (nullptr)
, gDewPoint_           (nullptr)
, mgAllTemperatures_   (nullptr)
{
	gStyle->SetOptTitle      (0);
	gStyle->SetTitleFillColor(2);
	gStyle->SetTitleFontSize (0.2);
	gStyle->SetTimeOffset    (0);
}

//========================================================================================================================
BurninBoxDQMHistos::~BurninBoxDQMHistos(void)
{
}

//Emanuele Aucone
//========================================================================================================================
void BurninBoxDQMHistos::book(TDirectory* myDirectory)//, const ConfigurationTree& theXDAQContextConfigTree, const std::string& configurationPath)
{
	clear();
	std::cout << "Booking start!" << std::endl;

	myDir_     = myDirectory;
	statusDir_ = myDir_-> mkdir("StatusPlot", "Status Plot");
	statusDir_-> cd();

	cAmbientTemperature_ = new TCanvas("AmbientTemperature", "Ambient Temperature Plot", 800, 600);
	statusDir_          -> Add(cAmbientTemperature_);
	cAmbientTemperature_-> SetGridx();
	cAmbientTemperature_-> SetGridy();
	cAmbientTemperature_-> cd();

	gAmbientTemperature_ = new TGraph();
	gAmbientTemperature_-> SetNameTitle("AmbientTemperatureGraph", "Ambient Temperature Graph");
	gAmbientTemperature_-> SetMarkerColor(3);
	gAmbientTemperature_-> SetMarkerStyle(20);
	gAmbientTemperature_-> SetMarkerSize(0.3);
	gAmbientTemperature_-> SetLineColor(kGreen);
	gAmbientTemperature_-> Draw("APL");

	cPlateTemperature_   = new TCanvas("PlateTemperature", "Plate Temperature Plot", 800, 600);
	statusDir_          -> Add(cPlateTemperature_);
	cPlateTemperature_  -> SetGridx();
	cPlateTemperature_  -> SetGridy();
	cPlateTemperature_  -> cd();

	gPlateTemperature_   = new TGraph();
	gPlateTemperature_  -> SetNameTitle("PlateTemperatureGraph", "Plate Temperature Graph");
	gPlateTemperature_  -> SetMarkerColor(4);
	gPlateTemperature_  -> SetMarkerStyle(20);
	gPlateTemperature_  -> SetMarkerSize(0.3);
	gPlateTemperature_  -> SetLineColor(kBlue);
	gPlateTemperature_  -> Draw("APL");

	cDewPoint_           = new TCanvas("DewPoint", "Dew Point Temperature Plot", 800, 600);
	statusDir_          -> Add(cDewPoint_);
	cDewPoint_          -> SetGridx();
	cDewPoint_          -> SetGridy();
	cDewPoint_          -> cd();

	gDewPoint_           = new TGraph();
	gDewPoint_          -> SetNameTitle("DewPointGraph", "Dew Point Graph");
	gDewPoint_          -> SetMarkerColor(2);
	gDewPoint_          -> SetMarkerStyle(20);
	gDewPoint_          -> SetMarkerSize(0.3);
	gDewPoint_          -> SetLineColor(kRed);
	gDewPoint_          -> Draw("APL");

	cHumidity_           = new TCanvas("Humidity", "Humidity Plot", 800, 600);
	statusDir_          -> Add(cHumidity_);
	cHumidity_          -> SetGridx();
	cHumidity_          -> SetGridy();
	cHumidity_          -> cd();

	gHumidity_           = new TGraph();
	gHumidity_          -> SetNameTitle("HumidityGraph", "Humidity Graph");
	gHumidity_          -> SetMarkerColor(1);
	gHumidity_          -> SetMarkerStyle(20);
	gHumidity_          -> SetMarkerSize(0.3);
	gHumidity_          -> SetLineColor(kBlack);
	gHumidity_          -> Draw("APL");

	cLeftPeltier_        = new TCanvas("LeftPeltier", "Left Peltier Plot", 800, 600);
	statusDir_          -> Add(cLeftPeltier_);
	cLeftPeltier_       -> SetGridx();
	cLeftPeltier_       -> SetGridy();
	cLeftPeltier_       -> cd();

	gLeftPeltier_        = new TGraph();
	gLeftPeltier_       -> SetNameTitle("LeftPeltierGraph", "Left Peltier Graph");
	gLeftPeltier_       -> SetMarkerColor(93);
	gLeftPeltier_       -> SetMarkerStyle(20);
	gLeftPeltier_       -> SetMarkerSize(0.3);
	gLeftPeltier_       -> SetLineColor(kOrange);
	gLeftPeltier_       -> Draw("APL");

	cRightPeltier_       = new TCanvas("RightPeltier", "Right Peltier Plot", 800, 600);
	statusDir_          -> Add(cRightPeltier_ );
	cRightPeltier_      -> SetGridx();
	cRightPeltier_      -> SetGridy();
	cRightPeltier_      -> cd();

	gRightPeltier_       = new TGraph();
	gRightPeltier_      -> SetNameTitle("RightPeltierGraph", "Right Peltier Graph");
	gRightPeltier_      -> SetMarkerColor(93);
	gRightPeltier_      -> SetMarkerStyle(20);
	gRightPeltier_      -> SetMarkerSize(0.3);
	gRightPeltier_      -> SetLineColor(kOrange);

	gRightPeltier_      -> Draw("APL");

	cChiller_            = new TCanvas("Chiller", "Chiller Plot", 800, 600);
	statusDir_           -> Add(cChiller_);
	cChiller_           -> SetGridx();
	cChiller_           -> SetGridy();
	cChiller_           -> cd();

	gChiller_            = new TGraph();
	gChiller_           -> SetNameTitle("ChillerGraph", "Chiller Graph");
	gChiller_           -> SetMarkerColor(93);
	gChiller_           -> SetMarkerStyle(20);
	gChiller_           -> SetMarkerSize(0.3);
	gChiller_           -> SetLineColor(kOrange);
	gChiller_            -> Draw("APL");

	cWarmup_             = new TCanvas("WarmupRelay", "Warmup Relay Plot", 800, 600);
	statusDir_          -> Add(cWarmup_);
	cWarmup_            -> SetGridx();
	cWarmup_            -> SetGridy();
	cWarmup_            -> cd();

	gWarmup_             = new TGraph();
	gWarmup_            -> SetNameTitle("WarmupRelayGraph", "Warmup Relay Graph");
	gWarmup_            -> SetMarkerColor(93);
	gWarmup_            -> SetMarkerStyle(20);
	gWarmup_            -> SetMarkerSize(0.3);
	gWarmup_            -> SetLineColor(kOrange);
	gWarmup_            -> Draw("APL");

	cDryAirFlux_         = new TCanvas("DryAirFluxRelay", "DryAir Flux Relay Plot", 800, 600);
	statusDir_          -> Add(cDryAirFlux_);
	cDryAirFlux_        -> SetGridx();
	cDryAirFlux_        -> SetGridy();
	cDryAirFlux_        -> cd();

	gDryAirFlux_         = new TGraph();
	gDryAirFlux_        -> SetNameTitle("DryAirFluxRelayGraph", "DryAir Flux Relay Graph");
	gDryAirFlux_        -> SetMarkerColor(93);
	gDryAirFlux_        -> SetMarkerStyle(20);
	gDryAirFlux_        -> SetMarkerSize(0.3);
	gDryAirFlux_        -> SetLineColor(kOrange);
	gDryAirFlux_        -> Draw("APL");

	cRelays_             = new TCanvas("AllRelays", "All Relays Plot", 800, 600);
	statusDir_          -> Add(cRelays_);
	cRelays_            -> SetGridx();
	cRelays_            -> SetGridy();

	cRelays_            -> Divide(1,5);
	cRelays_            -> cd(1);
	gLeftPeltier_       -> Draw("APL");
	cRelays_            -> cd(2);
	gRightPeltier_      -> Draw("APL");
	cRelays_            -> cd(3);
	gChiller_           -> Draw("APL");
	cRelays_            -> cd(4);
	gWarmup_            -> Draw("APL");
	cRelays_            -> cd(5);
	gDryAirFlux_        -> Draw("APL");

	cTemperatureMonitor_ = new TCanvas("TemperatureMonitor", "Temperature Monitor Plots", 800, 600);
	statusDir_          -> Add(cTemperatureMonitor_);
	cTemperatureMonitor_-> SetGridx();
	cTemperatureMonitor_-> SetGridy();
	cTemperatureMonitor_-> cd();

	//gAmbientTemperature_     ->GetYaxis()->SetRangeUser(-300., 30.);
	//gAmbientTemperature_     ->Draw("AL");
	//gPlateTemperature_       ->GetYaxis()->SetRangeUser(-300., 30.);
	//gPlateTemperature_       ->Draw("L");
	//gDewPoint_               ->GetYaxis()->SetRangeUser(-300., 30.);
	//gDewPoint_               ->Draw("L");

	mgAllTemperatures_  = new TMultiGraph();
	mgAllTemperatures_ -> SetNameTitle("AllTemperature", "All Temperature Plots");
	mgAllTemperatures_ -> Add(gAmbientTemperature_);
	mgAllTemperatures_ -> Add(gPlateTemperature_);
	mgAllTemperatures_ -> Add(gDewPoint_);
	mgAllTemperatures_ -> Draw("APL");

	std::cout << "Booking done!" << std::endl;
}

    
 //========================================================================================================================
void BurninBoxDQMHistos::bookPads(TDirectory* myDirectory)
{
	clear();
	std::cout << "Booking start!" << std::endl;

	myDir_      = myDirectory;
	statusDir_  = myDir_-> mkdir("MultiPlot", "Multi-Plot");
	statusDir_ -> cd();
	
	cTemperaturePads_ = new TCanvas("TempMonitor", "Temperature Plot", 800, 600);
	statusDir_        -> Add(cTemperaturePads_);
	cTemperaturePads_ -> cd();
	cTemperaturePads_ -> Divide(2,2);
	
	pad1_ = (TPad*) cTemperaturePads_->GetListOfPrimitives()->FindObject("TempMonitor_1");
	pad2_ = (TPad*) cTemperaturePads_->GetListOfPrimitives()->FindObject("TempMonitor_2");
	pad3_ = (TPad*) cTemperaturePads_->GetListOfPrimitives()->FindObject("TempMonitor_3");
	pad4_ = (TPad*) cTemperaturePads_->GetListOfPrimitives()->FindObject("TempMonitor_4");

	pad1_ -> SetGrid();
	pad1_ -> Draw();
	pad2_ -> SetGrid();
	pad2_ -> Draw();
	pad3_ -> SetGrid();
	pad3_ -> Draw();
	pad4_ -> SetGrid();
	pad4_ -> Draw();
	
	gAmbientTemperature_ = new TGraph();
	gAmbientTemperature_-> SetNameTitle("AmbientTemperatureGraph", "Ambient Temperature Graph");
	gAmbientTemperature_-> SetMarkerColor(kGreen);
	gAmbientTemperature_-> SetMarkerStyle(20);
	gAmbientTemperature_-> SetMarkerSize(0.3);
	gAmbientTemperature_-> SetLineColor(kGreen);
	gAmbientTemperature_-> SetLineWidth(2);
	gAmbientTemperature_-> GetYaxis()-> SetTitle("Ambient Temperature");
	gAmbientTemperature_-> GetXaxis()-> SetTitle("Time ");	
	gAmbientTemperature_-> GetXaxis()-> SetNdivisions(1005);
	pad1_               -> cd();
	gAmbientTemperature_-> Draw("APL");

	gPlateTemperature_   = new TGraph();
	gPlateTemperature_  -> SetNameTitle("PlateTemperatureGraph", "Plate Temperature Graph");
	gPlateTemperature_  -> SetMarkerColor(kBlue);
	gPlateTemperature_  -> SetMarkerStyle(20);
	gPlateTemperature_  -> SetMarkerSize(0.3);
	gPlateTemperature_  -> SetLineColor(kBlue);
	gPlateTemperature_  -> SetLineWidth(2);
	gPlateTemperature_  -> GetYaxis()-> SetTitle("Plate Temperature");
	gPlateTemperature_  -> GetXaxis()-> SetTitle("Time ");
	gPlateTemperature_  -> GetXaxis()-> SetNdivisions(1005);
	pad2_               -> cd();
	gPlateTemperature_  -> Draw("APL");

	gDewPoint_           = new TGraph();
	gDewPoint_          -> SetNameTitle("DewPointGraph", "Dew Point Graph");
	gDewPoint_          -> SetMarkerColor(kRed);
	gDewPoint_          -> SetMarkerStyle(20);
	gDewPoint_          -> SetMarkerSize(0.3);
	gDewPoint_          -> SetLineColor(kRed);
	gDewPoint_          -> SetLineWidth(2);
	gDewPoint_          -> GetYaxis()-> SetTitle("Dew Point Temperature");
	gDewPoint_          -> GetXaxis()-> SetTitle("Time ");
	gDewPoint_          -> GetXaxis()-> SetNdivisions(1005);
	pad3_               -> cd();
	gDewPoint_          -> Draw("APL");

	gHumidity_           = new TGraph();
	gHumidity_          -> SetNameTitle("HumidityGraph", "Humidity Graph");
	gHumidity_          -> SetMarkerColor(kTeal);
	gHumidity_          -> SetMarkerStyle(20);
	gHumidity_          -> SetMarkerSize(0.3);
	gHumidity_          -> SetLineColor(kTeal);
	gHumidity_          -> SetLineWidth(2);
	gHumidity_          -> GetYaxis()-> SetTitle("Humidity");
	gHumidity_          -> GetXaxis()-> SetTitle("Time ");
	gHumidity_          -> GetXaxis()-> SetNdivisions(1005);
	pad4_               -> cd();
	gHumidity_          -> Draw("APL");

	cRelaysPads_  = new TCanvas("RelaysMonitor", "Relays Monitor Plot", 800, 800);
	statusDir_   -> Add(cRelaysPads_);
	cRelaysPads_ -> cd();
	cRelaysPads_ -> Divide(1,5);
	
	padR1_ = (TPad*) cRelaysPads_->GetListOfPrimitives()->FindObject("RelaysMonitor_1");
	padR2_ = (TPad*) cRelaysPads_->GetListOfPrimitives()->FindObject("RelaysMonitor_2");
	padR3_ = (TPad*) cRelaysPads_->GetListOfPrimitives()->FindObject("RelaysMonitor_3");
	padR4_ = (TPad*) cRelaysPads_->GetListOfPrimitives()->FindObject("RelaysMonitor_4");
	padR5_ = (TPad*) cRelaysPads_->GetListOfPrimitives()->FindObject("RelaysMonitor_5");

	padR1_ -> SetGrid();
	padR1_ -> Draw();
	padR2_ -> SetGrid();
	padR2_ -> Draw();
	padR3_ -> SetGrid();
	padR3_ -> Draw();
	padR4_ -> SetGrid();
	padR4_ -> Draw();
	padR5_ -> SetGrid();
	padR5_ -> Draw();
		
	gLeftPeltier_        = new TGraph();
	gLeftPeltier_       -> SetNameTitle("LeftPeltierGraph", "Left Peltier Graph");
	gLeftPeltier_       -> SetMarkerColor(93);
	gLeftPeltier_       -> SetMarkerStyle(20);
	gLeftPeltier_       -> SetMarkerSize(0.3);
	gLeftPeltier_       -> SetLineColor(kOrange+2);
	gLeftPeltier_       -> SetLineWidth(2);
	gLeftPeltier_       -> GetYaxis()-> SetTitle("Left Peltier Status");
	gLeftPeltier_       -> GetXaxis()-> SetTitle("Time ");
	gLeftPeltier_       -> GetXaxis()-> SetNdivisions(1005);
	padR1_              -> cd();
	gLeftPeltier_       -> Draw("APL");

	gRightPeltier_       = new TGraph();
	gRightPeltier_      -> SetNameTitle("RightPeltierGraph", "Right Peltier Graph");
	gRightPeltier_      -> SetMarkerColor(93);
	gRightPeltier_      -> SetMarkerStyle(20);
	gRightPeltier_      -> SetMarkerSize(0.3);
	gRightPeltier_      -> SetLineColor(kOrange+2);
	gRightPeltier_      -> SetLineWidth(2);
	gRightPeltier_      -> GetYaxis()-> SetTitle("Right Peltier Status");
	gRightPeltier_      -> GetXaxis()-> SetTitle("Time ");
	gRightPeltier_      -> GetXaxis()-> SetNdivisions(1005);
	padR2_              -> cd();
	gRightPeltier_      -> Draw("APL");

	gChiller_            = new TGraph();
	gChiller_           -> SetNameTitle("ChillerGraph", "Chiller Graph");
	gChiller_           -> SetMarkerColor(93);
	gChiller_           -> SetMarkerStyle(20);
	gChiller_           -> SetMarkerSize(0.3);
	gChiller_           -> SetLineColor(kOrange+1);
	gChiller_           -> SetLineWidth(2);
	gChiller_           -> GetYaxis()-> SetTitle("Chiller Status");
	gChiller_           -> GetXaxis()-> SetTitle("Time ");
	gChiller_           -> GetXaxis()-> SetNdivisions(1005);
	padR3_              -> cd();
	gChiller_           -> Draw("APL");

	gWarmup_             = new TGraph();
	gWarmup_            -> SetNameTitle("WarmupRelayGraph", "Warmup Relay Graph");
	gWarmup_            -> SetMarkerColor(93);
	gWarmup_            -> SetMarkerStyle(20);
	gWarmup_            -> SetMarkerSize(0.3);
	gWarmup_            -> SetLineColor(kOrange);
	gWarmup_            -> SetLineWidth(2);
	gWarmup_            -> GetYaxis()-> SetTitle("Warm up Realys");
	gWarmup_            -> GetXaxis()-> SetTitle("Time ");
	gWarmup_            -> GetXaxis()-> SetNdivisions(1005);
	padR4_              -> cd();
	gWarmup_            -> Draw("APL");

	gDryAirFlux_         = new TGraph();
	gDryAirFlux_        -> SetNameTitle("DryAirFluxRelayGraph", "DryAir Flux Relay Graph");
	gDryAirFlux_        -> SetMarkerColor(93);
	gDryAirFlux_        -> SetMarkerStyle(20);
	gDryAirFlux_        -> SetMarkerSize(0.3);
	gDryAirFlux_        -> SetLineColor(kOrange-2);
	gDryAirFlux_        -> SetLineWidth(2);
	gDryAirFlux_        -> GetYaxis()-> SetTitle("Dry Air Flux");
	gDryAirFlux_        -> GetXaxis()-> SetTitle("Time ");
	gDryAirFlux_        -> GetXaxis()-> SetNdivisions(1005);
	padR5_              -> cd();
	gDryAirFlux_        -> Draw("APL");
	
	std::cout << "Booking done!" << std::endl;
}

    


//========================================================================================================================
void BurninBoxDQMHistos::clear()
{
	cAmbientTemperature_ = nullptr;
	cPlateTemperature_   = nullptr;
	cDewPoint_           = nullptr;
	cHumidity_           = nullptr;
	cLeftPeltier_        = nullptr;
	cRightPeltier_       = nullptr;
	cChiller_            = nullptr;
	cWarmup_             = nullptr;
	cDryAirFlux_         = nullptr;
	cRelays_             = nullptr;
	cTemperatureMonitor_ = nullptr;
	gAmbientTemperature_ = nullptr;
	gPlateTemperature_   = nullptr;
	gDewPoint_           = nullptr;
	gHumidity_           = nullptr;
	gLeftPeltier_        = nullptr;
	gRightPeltier_       = nullptr;
	gChiller_            = nullptr;
	gWarmup_             = nullptr;
	gDryAirFlux_         = nullptr;
	mgAllTemperatures_   = nullptr;
	cTemperaturePads_    = nullptr;
	cRelaysPads_         = nullptr;
	
	
}

//Emanuele Aucone
//========================================================================================================================
void BurninBoxDQMHistos::fill(std::string& buffer)
{
	BeagleBoneStatus_.convertFromJSON(buffer);

	std::cout << "Filling start!" << std::endl;

	//Filling plots with incoming data
	double timeAxis           = BeagleBoneStatus_.getTime();

	gAmbientTemperature_     ->SetPoint(gAmbientTemperature_->GetN(), timeAxis,BeagleBoneStatus_.getAmbientTemperature());
	gPlateTemperature_       ->SetPoint(gPlateTemperature_  ->GetN(), timeAxis,BeagleBoneStatus_.getPlateTemperature());
	gDewPoint_               ->SetPoint(gDewPoint_          ->GetN(), timeAxis,BeagleBoneStatus_.getDewPointTemperature());
	gHumidity_               ->SetPoint(gHumidity_          ->GetN(), timeAxis,BeagleBoneStatus_.getHumidity());
	gLeftPeltier_            ->SetPoint(gLeftPeltier_       ->GetN(), timeAxis,BeagleBoneStatus_.getLeftPeltier());
	gRightPeltier_           ->SetPoint(gRightPeltier_      ->GetN(), timeAxis,BeagleBoneStatus_.getRightPeltier());
	gChiller_                ->SetPoint(gChiller_           ->GetN(), timeAxis,BeagleBoneStatus_.getChiller());
	gWarmup_                 ->SetPoint(gWarmup_            ->GetN(), timeAxis,BeagleBoneStatus_.getWarmupRelay());
	gDryAirFlux_             ->SetPoint(gDryAirFlux_        ->GetN(), timeAxis,BeagleBoneStatus_.getDryAirFluxRelay());

	cTemperatureMonitor_-> cd();
	mgAllTemperatures_  -> Draw("APL");
	mgAllTemperatures_  ->GetXaxis()->SetTimeDisplay(1);
	mgAllTemperatures_  ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	mgAllTemperatures_  ->GetHistogram()->GetXaxis()->SetRangeUser(mgAllTemperatures_->GetHistogram()->GetXaxis()->GetBinCenter(1), timeAxis+60);


	//Setting the x-axis format in order to obtain the correct date (in UTC?)
	gAmbientTemperature_     ->GetXaxis()->SetTimeDisplay(1);
	gAmbientTemperature_     ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gPlateTemperature_       ->GetXaxis()->SetTimeDisplay(1);
	gPlateTemperature_       ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gDewPoint_               ->GetXaxis()->SetTimeDisplay(1);
	gDewPoint_               ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gHumidity_               ->GetXaxis()->SetTimeDisplay(1);
	gHumidity_               ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gLeftPeltier_            ->GetXaxis()->SetTimeDisplay(1);
	gLeftPeltier_            ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gLeftPeltier_            ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gRightPeltier_           ->GetXaxis()->SetTimeDisplay(1);
	gRightPeltier_           ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gRightPeltier_           ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gChiller_                ->GetXaxis()->SetTimeDisplay(1);
	gChiller_                ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gChiller_                ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gWarmup_                 ->GetXaxis()->SetTimeDisplay(1);
	gWarmup_                 ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gWarmup_                 ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gDryAirFlux_             ->GetXaxis()->SetTimeDisplay(1);
	gDryAirFlux_             ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gDryAirFlux_             ->GetYaxis()->SetRangeUser(-0.1, 1.1);
	
	
	//Refreshing
	cRelays_             ->Modified();
	cRelays_             ->Update();
	cAmbientTemperature_ ->Modified();
	cAmbientTemperature_ ->Update();
	cPlateTemperature_   ->Modified();
	cPlateTemperature_   ->Update();
	cDewPoint_           ->Modified();
	cDewPoint_           ->Update();
	cHumidity_           ->Modified();
	cHumidity_           ->Update();
	cLeftPeltier_        ->Modified();
	cLeftPeltier_        ->Update();
	cRightPeltier_       ->Modified();
	cRightPeltier_       ->Update();
	cChiller_            ->Modified();
	cChiller_            ->Update();
	cWarmup_             ->Modified();
	cWarmup_             ->Update();
	cDryAirFlux_         ->Modified();
	cDryAirFlux_         ->Update();
	cTemperatureMonitor_ ->Modified();
	cTemperatureMonitor_ ->Update();
	
	std::cout<<"updated"<<std::endl;
	
}


void BurninBoxDQMHistos::fillPads(std::string& buffer)
{
	BeagleBoneStatus_.convertFromJSON(buffer);
	
	//Filling plots with incoming data
	double timeAxis           = BeagleBoneStatus_.getTime();
	gAmbientTemperature_     ->SetPoint(gAmbientTemperature_->GetN(), timeAxis,BeagleBoneStatus_.getAmbientTemperature());
	gPlateTemperature_       ->SetPoint(gPlateTemperature_  ->GetN(), timeAxis,BeagleBoneStatus_.getPlateTemperature());
	gDewPoint_               ->SetPoint(gDewPoint_          ->GetN(), timeAxis,BeagleBoneStatus_.getDewPointTemperature());
	gHumidity_               ->SetPoint(gHumidity_          ->GetN(), timeAxis,BeagleBoneStatus_.getHumidity());
	gLeftPeltier_            ->SetPoint(gLeftPeltier_       ->GetN(), timeAxis,BeagleBoneStatus_.getLeftPeltier());
	gRightPeltier_           ->SetPoint(gRightPeltier_      ->GetN(), timeAxis,BeagleBoneStatus_.getRightPeltier());
	gChiller_                ->SetPoint(gChiller_           ->GetN(), timeAxis,BeagleBoneStatus_.getChiller());
	gWarmup_                 ->SetPoint(gWarmup_            ->GetN(), timeAxis,BeagleBoneStatus_.getWarmupRelay());
	gDryAirFlux_             ->SetPoint(gDryAirFlux_        ->GetN(), timeAxis,BeagleBoneStatus_.getDryAirFluxRelay());
	
	//Setting the x-axis format in order to obtain the correct date
	gAmbientTemperature_     ->GetXaxis()->SetTimeDisplay(1);
	gAmbientTemperature_     ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gPlateTemperature_       ->GetXaxis()->SetTimeDisplay(1);
	gPlateTemperature_       ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gDewPoint_               ->GetXaxis()->SetTimeDisplay(1);
	gDewPoint_               ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gHumidity_               ->GetXaxis()->SetTimeDisplay(1);
	gHumidity_               ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");

	gLeftPeltier_            ->GetXaxis()->SetTimeDisplay(1);
	gLeftPeltier_            ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gLeftPeltier_            ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gRightPeltier_           ->GetXaxis()->SetTimeDisplay(1);
	gRightPeltier_           ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gRightPeltier_           ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gChiller_                ->GetXaxis()->SetTimeDisplay(1);
	gChiller_                ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gChiller_                ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gWarmup_                 ->GetXaxis()->SetTimeDisplay(1);
	gWarmup_                 ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gWarmup_                 ->GetYaxis()->SetRangeUser(-0.1, 1.1);

	gDryAirFlux_             ->GetXaxis()->SetTimeDisplay(1);
	gDryAirFlux_             ->GetXaxis()->SetTimeFormat("%m-%d-%Y %H:%M:%S");
	gDryAirFlux_             ->GetYaxis()->SetRangeUser(-0.1, 1.1);
	
	
	//Refreshing
	pad1_->Modified();
	pad1_->Update();
	pad2_->Modified();
	pad2_->Update();
	pad3_->Modified();
	pad3_->Update();
	pad4_->Modified();
	pad4_->Update();
	
	padR1_->Modified();
	padR1_->Update();
	padR2_->Modified();
	padR2_->Update();
	padR3_->Modified();
	padR3_->Update();
	padR4_->Modified();
	padR4_->Update();
	padR5_->Modified();
	padR5_->Update();

}

//========================================================================================================================
void BurninBoxDQMHistos::load(std::string fileName)
{
	/*LORE 2016 MUST BE FIXED THIS MONDAY
	DQMHistosBase::openFile (fileName);
	numberOfTriggers_ = (TH1I*)theFile_->Get("General/NumberOfTriggers");

	std::string directory = "Planes";
	std::stringstream name;
	for(unsigned int p=0; p<4; p++)
	{
		name.str("");
		name << directory << "/Plane_" << p << "_Occupancy";
		//FIXME Must organize better all histograms!!!!!
		//planeOccupancies_.push_back((TH1I*)theFile_->Get(name.str().c_str()));
	}
	//canvas_ = (TCanvas*) theFile_->Get("MainDirectory/MainCanvas");
	//histo1D_ = (TH1F*) theFile_->Get("MainDirectory/Histo1D");
	//histo2D_ = (TH2F*) theFile_->Get("MainDirectory/Histo2D");
	//profile_ = (TProfile*) theFile_->Get("MainDirectory/Profile");
	closeFile();
	 */
}

//========================================================================================================================
void BurninBoxDQMHistos::writeGraphs(void)
{
	//saving graphs with no canvas
	gAmbientTemperature_ ->Write();
	gPlateTemperature_   ->Write();
	gDewPoint_           ->Write();
	gHumidity_           ->Write();
	gLeftPeltier_        ->Write();
	gRightPeltier_       ->Write();
	gChiller_            ->Write();
	gWarmup_             ->Write();
	gDryAirFlux_         ->Write();

}
