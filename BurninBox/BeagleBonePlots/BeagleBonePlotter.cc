#include <stdio.h>
#include <string.h>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>

#include "BeagleBoneStatus.h"

#include <fstream>
#include "TFile.h"
#include "TApplication.h"
#include "BurninBoxDQMHistos.h"


using namespace ots;

int main(int argc, char **argv)
{
	TApplication *theApp=new TApplication("Application",0,0);
	BurninBoxDQMHistos theBurninBoxDQMHistos;
	BeagleBoneStatus localStatus;

	TFile* rootFile = TFile::Open("plots.root", "RECREATE");
	rootFile->cd();
	theBurninBoxDQMHistos.bookPads(rootFile);

	std::string filename="../logfile_Run1785.txt";

	if(argv[1]!=NULL)
	{
		printf("%s is new filename \n",argv[1]);
		filename=argv[1];
	}

	std::string line;
	std::ifstream myfile (filename);

	struct timeval currentTime, beginTime;

	if (myfile.is_open())
	{
		while(1)
		{
			while ( getline (myfile,line) )
			{
				//std::cout << line << '\n';
				gettimeofday(&beginTime, nullptr);

				if (line.substr(0,7) == "STATUS:")
				{
					//check line and print
					localStatus.convertFromJSON(line);
					std::cout << "local status" << std::endl;
					localStatus.printToScreen();

					//plotting
					theBurninBoxDQMHistos.fillPads(line);

				}
			}

			//keep reading at the end
			if (!myfile.eof()) break;
			myfile.clear();

			//30 sec timeout between updates to stop plotter
			gettimeofday(&currentTime, nullptr);
			if (float( currentTime.tv_sec- beginTime.tv_sec ) > 40) break;

			sleep(1);

		}

		theBurninBoxDQMHistos.writeGraphs();
		std::cout << "closing file" << std::endl;
		myfile.close();

	}
	else
	{
		std::cout << "Unable to open txt file";
	}

	rootFile->Write();
	rootFile->Close();
	return 1;

}

