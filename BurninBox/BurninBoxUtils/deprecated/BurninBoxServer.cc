#include <stdio.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxServer.h"
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxController.h"
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxStatus.h"
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#else
#include "BurninBoxServer.h"
#include "BurninBoxController.h"
#include "BurninBoxStatus.h"
#include "BurninBoxConfiguration.h"
#endif

using namespace ots;

//========================================================================================================================
BurninBoxServer::BurninBoxServer(BurninBoxController &burningBoxController, int serverPort, int bufferSize)
	: TCPNetworkServer(serverPort, bufferSize), theBurninBoxController_(burningBoxController)
{
	theBurninBoxConfiguration_.setSetTemperature(1.0);
	theBurninBoxConfiguration_.setStopTemperature(1.0); // just defaults to be adjusted

	//####################### MATT COWANS #######################
	//Removing the Tolerance not needed any more	

	//theBurninBoxConfiguration_.setLowTolerance(1.0);
	//theBurninBoxConfiguration_.setHighTolerance(1.0);
	//############################################################	
}
//========================================================================================================================
BurninBoxServer::~BurninBoxServer(void)
{
}
//========================================================================================================================
// virtual function to interpret messages
// interacting with theBurninBoxController_ using theBurninBoxConfiguration_ as helper class
std::string BurninBoxServer::readMessage(const std::string &buffer)
{
	BurninBoxStatus theBurninBoxStatus;
	struct timeval currentTime;

	std::cout << "Received: " << buffer << std::endl;

	if (buffer == "Initialize")
	{
		return "InitializeDone";
	}
	else if (buffer.substr(0, 5) == "START") //changing the status changes the mode in threadMain (BBC) function.
	{
		currentRun_ = getVariableValue("RunNumber", buffer);
		std::cout << __PRETTY_FUNCTION__ << "Run " << currentRun_ << " started!" << std::endl;

		std::cout << __PRETTY_FUNCTION__ << "SetTemperature " << theBurninBoxConfiguration_.getSetTemperature() << " temp!" << std::endl;
		theBurninBoxController_.setTargetTemperature(theBurninBoxConfiguration_.getSetTemperature());		
		theBurninBoxController_.setStateMachineStatus(theBurninBoxController_.STARTING);

		running_ = true;
		paused_ = false;
		return "StartDone";
	}
	else if (buffer.substr(0, 4) == "STOP")
	{
		//######## MATT COWANS ###########
		//We need to think :)
		//theBurninBoxController_.setTargetTemperature(theBurninBoxConfiguration_.getStopTemperature(), 1, 1);
		theBurninBoxController_.setTargetTemperature(theBurninBoxConfiguration_.getStopTemperature());
		
		theBurninBoxController_.switchThermalRelays(false);

		//--->Don't forget that the dry air flux relay is still working

		theBurninBoxController_.setStateMachineStatus(theBurninBoxController_.STOPPING);

		running_ = false;
		paused_ = false;
		theBurninBoxController_.setEnableTemperature(false);
		std::cout << "Run " << currentRun_ << " stopped!" << std::endl;
		return "StopDone";
	}
	else if (buffer == "PAUSE")
	{
		//We need to think :)
		//--->Maybe the temperature could be kept until start button is pressed
		running_ = false;
		paused_ = true;
		theBurninBoxController_.switchThermalRelays(false);
		return "PauseDone";
	}
	else if (buffer == "RESUME")
	{
		//We need to think :)
		running_ = true;
		paused_ = false;
		return "ResumeDone";
	}
	//CONFIGURE
	else if (buffer.substr(0, theBurninBoxConfiguration_.getClassName().size()) == theBurninBoxConfiguration_.getClassName())
	{

		std::cout << "We are in the configuration submodule" << std::endl;
		theBurninBoxConfiguration_.convertFromJSON(buffer);

		std::cout << "This is the set temperature:  " << theBurninBoxConfiguration_.getSetTemperature() << std::endl;		

		theBurninBoxController_.setTargetTemperature(theBurninBoxConfiguration_.getSetTemperature());

		theBurninBoxController_.setStateMachineStatus(theBurninBoxController_.CONFIGURE);
		//MATTHEW NEEDS TO CONFIGURE THE DATA STREAMER
		std::cout << "Out of configuration submodule" << std::endl;
		return "ConfigureDone";
	}
	else if (buffer == "STATUS?")
	{
		if (running_ || paused_) // read again
		{
			std::cout << "getting time and status here" << std::endl;

			gettimeofday(&currentTime, nullptr);
			theBurninBoxController_.running();
			theBurninBoxStatus.setTime(currentTime.tv_sec);

			theBurninBoxStatus.setAmbientTemperature(theBurninBoxController_.readAmbientTemperature());
			theBurninBoxStatus.setCarrierTemperature(theBurninBoxController_.readCarrierTemperature());
			theBurninBoxStatus.setDewPointTemperature(theBurninBoxController_.readDewPointTemperature());
			// theBurninBoxStatus.setHumidity(theBurninBoxController_.readHumidity());
			// theBurninBoxStatus.setLeftPeltier(theBurninBoxController_.readLeftPeltierStatus());
			// theBurninBoxStatus.setRightPeltier(theBurninBoxController_.readRightPeltierStatus());
			// theBurninBoxStatus.setChiller(theBurninBoxController_.readChillerStatus());
			// theBurninBoxStatus.setWarmupRelay(theBurninBoxController_.readWarmupRelayStatus());
			// theBurninBoxStatus.setDryAirFluxRelay(theBurninBoxController_.readDryAirFluxRelayStatus());
			theBurninBoxStatus.setModule1LeftTemperature(theBurninBoxController_.readModule1LeftTemperature());
			theBurninBoxStatus.setModule2LeftTemperature(theBurninBoxController_.readModule2LeftTemperature());
			theBurninBoxStatus.setModule3LeftTemperature(theBurninBoxController_.readModule3LeftTemperature());
			theBurninBoxStatus.setModule4LeftTemperature(theBurninBoxController_.readModule4LeftTemperature());
			theBurninBoxStatus.setModule5LeftTemperature(theBurninBoxController_.readModule5LeftTemperature());
			theBurninBoxStatus.setModule1RightTemperature(theBurninBoxController_.readModule1RightTemperature());
			theBurninBoxStatus.setModule2RightTemperature(theBurninBoxController_.readModule2RightTemperature());
			theBurninBoxStatus.setModule3RightTemperature(theBurninBoxController_.readModule3RightTemperature());
			theBurninBoxStatus.setModule4RightTemperature(theBurninBoxController_.readModule4RightTemperature());
			theBurninBoxStatus.setModule5RightTemperature(theBurninBoxController_.readModule5RightTemperature());

			//print on screen
			theBurninBoxStatus.printVariables();

			BurninBoxController::Status status = theBurninBoxController_.getStateMachineStatus();
			if (status == theBurninBoxController_.ERROR)
				std::cout << "I have a big problem" << std::endl;
		}

		return theBurninBoxStatus.convertToJSON(); // send createdempty status (?)
	}
	else if (buffer == "Config?")
	{
		return theBurninBoxConfiguration_.convertToJSON();
	}
	else if (buffer == "Run?")
	{
		std::cout << "checking the Run number: " << currentRun_ << std::endl;
		return currentRun_;
	}
	else if (buffer == "running?")
	{
		if (running_)
			return "running";
		else
			return "no";
	}
	else if (buffer == "paused?")
	{
		if (paused_)
			return "paused";
		else
			return "no";
	}

	std::cout << __PRETTY_FUNCTION__ << "Running: " << running_ << std::endl;
	if (running_ || paused_) //we go through here after start and resume or pause: sending back current status
	{
		std::cout << __PRETTY_FUNCTION__ << "Getting time and status here" << std::endl;

		gettimeofday(&currentTime, nullptr);
		theBurninBoxController_.running();
		theBurninBoxStatus.setTime(currentTime.tv_sec);

		theBurninBoxStatus.setAmbientTemperature(theBurninBoxController_.readAmbientTemperature());
		theBurninBoxStatus.setCarrierTemperature(theBurninBoxController_.readCarrierTemperature());
		theBurninBoxStatus.setDewPointTemperature(theBurninBoxController_.readDewPointTemperature());
		// theBurninBoxStatus.setHumidity(theBurninBoxController_.readHumidity());
		// theBurninBoxStatus.setLeftPeltier(theBurninBoxController_.readLeftPeltierStatus());
		// theBurninBoxStatus.setRightPeltier(theBurninBoxController_.readRightPeltierStatus());
		// theBurninBoxStatus.setChiller(theBurninBoxController_.readChillerStatus());
		// theBurninBoxStatus.setWarmupRelay(theBurninBoxController_.readWarmupRelayStatus());
		// theBurninBoxStatus.setDryAirFluxRelay(theBurninBoxController_.readDryAirFluxRelayStatus());
		theBurninBoxStatus.setModule1LeftTemperature(theBurninBoxController_.readModule1LeftTemperature());
		theBurninBoxStatus.setModule2LeftTemperature(theBurninBoxController_.readModule2LeftTemperature());
		theBurninBoxStatus.setModule3LeftTemperature(theBurninBoxController_.readModule3LeftTemperature());
		theBurninBoxStatus.setModule4LeftTemperature(theBurninBoxController_.readModule4LeftTemperature());
		theBurninBoxStatus.setModule5LeftTemperature(theBurninBoxController_.readModule5LeftTemperature());
		theBurninBoxStatus.setModule1RightTemperature(theBurninBoxController_.readModule1RightTemperature());
		theBurninBoxStatus.setModule2RightTemperature(theBurninBoxController_.readModule2RightTemperature());
		theBurninBoxStatus.setModule3RightTemperature(theBurninBoxController_.readModule3RightTemperature());
		theBurninBoxStatus.setModule4RightTemperature(theBurninBoxController_.readModule4RightTemperature());
		theBurninBoxStatus.setModule5RightTemperature(theBurninBoxController_.readModule5RightTemperature());

		//print on screen
		theBurninBoxStatus.printVariables();

		BurninBoxController::Status status = theBurninBoxController_.getStateMachineStatus();
		if (status == theBurninBoxController_.ERROR)
			std::cout << "I have a big problem" << std::endl;

		//return theBurninBoxStatus.convertToJSON();
	}

	return "I didn't get the message: " + buffer;
}
