#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/TCPNetworkSocket.h"
#else
#include "TCPNetworkSocket.h"
#endif

#include <iostream>
#include <cassert>
#include <sstream>

#include <unistd.h>

#include <stdio.h>		    // printf
#include <stdlib.h>		    // exit
#include <strings.h>	    // bzero
#include <sys/socket.h>     /* inet_aton, socket, bind, listen, accept */
#include <netinet/in.h>     /* inet_aton, struct sockaddr_in */
#include <arpa/inet.h>      /* inet_aton */
#include <netdb.h>          /* gethostbyname */
#include <errno.h>		    // errno

#include <sys/types.h>	    // socket, bind, listen, accept
#include <string.h>		    // bzero
#include <fcntl.h>
#include <ifaddrs.h>
#include <linux/if_link.h>
#include <regex>

using namespace ots;

//========================================================================================================================
TCPNetworkSocket::TCPNetworkSocket(const std::string &senderHost, unsigned int senderPort, int receiveBufferSize)
	: host_(senderHost)
	, port_(senderPort)
	, TCPSocketNumber_(-1)
	, SendSocket_(-1)
	, isSender_(false)
	, bufferSize_(receiveBufferSize)
	, chunkSize_(65000)
{}

//========================================================================================================================
TCPNetworkSocket::TCPNetworkSocket(unsigned int listenPort, int sendBufferSize)
	: port_(listenPort)
	, TCPSocketNumber_(-1)
	, SendSocket_(-1)
	, isSender_(true)
	, bufferSize_(sendBufferSize)
	, chunkSize_(65000)
{
	TCPSocketNumber_ = TCP_listen_fd(listenPort, 0);
	if (bufferSize_ > 0)
	{
		int len;
		socklen_t lenlen = sizeof(len);
		len = 0;
		auto sts = getsockopt(TCPSocketNumber_, SOL_SOCKET, SO_SNDBUF, &len, &lenlen);
		//TRACE(3, "TCPConnect SNDBUF initial: %d sts/errno=%d/%d lenlen=%d", len, sts, errno, lenlen);
		printf("TCPConnect SNDBUF initial: %d sts/errno=%d/%d lenlen=%d \n", len, sts, errno, lenlen);
		len = bufferSize_;
		sts = setsockopt(TCPSocketNumber_, SOL_SOCKET, SO_SNDBUF, &len, lenlen);
		if (sts == -1)
			//TRACE(0, "Error with setsockopt SNDBUF %d", errno);
			printf("Error with setsockopt SNDBUF %d \n", errno);
		len = 0;
		sts = getsockopt(TCPSocketNumber_, SOL_SOCKET, SO_SNDBUF, &len, &lenlen);
		if (len < (bufferSize_ * 2))
			//TRACE(1, "SNDBUF %d not expected (%d) sts/errno=%d/%d", len, bufferSize_, sts, errno);
			printf("SNDBUF %d not expected (%d) sts/errno=%d/%d \n", len, bufferSize_, sts, errno);
		else
			//TRACE(3, "SNDBUF %d sts/errno=%d/%d", len, sts, errno);
			printf("SNDBUF %d sts/errno=%d/%d", len, sts, errno);

	}
}

//========================================================================================================================
//protected constructor
TCPNetworkSocket::TCPNetworkSocket(void){

	std::stringstream ss;
	ss << "ERROR: This method should never be called. This is the protected constructor. There is something wrong in your inheritance scheme!" << std::endl;
	std::cout << "\n" << ss.str();
	throw std::runtime_error(ss.str());
}

//========================================================================================================================
TCPNetworkSocket::~TCPNetworkSocket(void)
{
	std::cout << "CLOSING THE TCPSocket #" << TCPSocketNumber_ << " IP: " << host_ << " port: " << port_ << std::endl;
	if (TCPSocketNumber_ != -1)
		close(TCPSocketNumber_);
	if (SendSocket_ != -1)
		close(SendSocket_);
}

//========================================================================================================================
int TCPNetworkSocket::TCP_listen_fd(int port, int rcvbuf)
{
	int sts;
	int listener_fd;
	struct sockaddr_in sin;

	listener_fd = socket(PF_INET, SOCK_STREAM, 0); /* man TCP(7P) */
	if (listener_fd == -1)
	{
		std::cout << "Could not open listen socket! Exiting with code 1!";
		perror("socket error");
		exit(1);
	}

	int opt = 1; // SO_REUSEADDR - man socket(7)
	sts = setsockopt(listener_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
	if (sts == -1)
	{
		std::cout << "Could not set SO_REUSEADDR! Exiting with code 2!";
		perror("setsockopt SO_REUSEADDR");
		return (2);
	}

	bzero((char *)&sin, sizeof(sin));
	sin.sin_family      = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port        = htons(port);

	//printf( "bind..." );fflush(stdout);
	sts = bind(listener_fd, (struct sockaddr *)&sin, sizeof(sin));
	if (sts == -1)
	{
		std::cout << "Could not bind socket! Exiting with code 3!";
		perror("bind error");
		exit(3);
	}
	//printf( " OK\n" );

	int len = 0;
	socklen_t arglen = sizeof(len);
	sts = getsockopt(listener_fd, SOL_SOCKET, SO_RCVBUF, &len, &arglen);
	std::cout << "RCVBUF initial: " << len << " sts/errno=" << sts << "/" << errno << " arglen=" << arglen << " rcvbuf=" << rcvbuf << " listener_fd=" << listener_fd;
	if (rcvbuf > 0)
	{
		len = rcvbuf;
		sts = setsockopt(listener_fd, SOL_SOCKET, SO_RCVBUF, &len, arglen);
		if (sts == -1)
			std::cout << "Error with setsockopt SNDBUF " << errno;
		len = 0;
		sts = getsockopt(listener_fd, SOL_SOCKET, SO_RCVBUF, &len, &arglen);
		if (len < (rcvbuf * 2))
			std::cout << "RCVBUF " << len << " not expected (" << rcvbuf << " sts/errno=" << sts << "/" << errno;
		else
			std::cout << "RCVBUF " << len << " sts/errno=" << sts << "/" << errno;
	}

	//printf( "listen..." );fflush(stdout);
	sts = listen(listener_fd, 5/*QLEN*/);
	if (sts == -1)
	{
		perror("listen error");
		exit(1);
	}
	//printf( " OK\n" );

	return (listener_fd);
} // TCP_listen_fd

//========================================================================================================================
int TCPNetworkSocket::ResolveHost(char const* host_in, int dflt_port, sockaddr_in& sin)
{
	int port;
	std::string host;
	struct hostent* hostent_sp;
	std::cmatch mm;
	//  Note: the regex expression used by regex_match has an implied ^ and $
	//        at the beginning and end respectively.
	if (regex_match(host_in, mm, std::regex("([^:]+):(\\d+)")))
	{
		host = mm[1].str();
		port = strtoul(mm[2].str().c_str(), NULL, 0);
	}
	else if (regex_match(host_in, mm, std::regex(":{0,1}(\\d+)")))
	{
		host = std::string("127.0.0.1");
		port = strtoul(mm[1].str().c_str(), NULL, 0);
	}
	else if (regex_match(host_in, mm, std::regex("([^:]+):{0,1}")))
	{
		host = mm[1].str().c_str();
		port = dflt_port;
	}
	else
	{
		host = std::string("127.0.0.1");
		port = dflt_port;
	}
	std::cout << "Resolving host " << host << ", on port " << port << std::endl;

	if (host == "localhost") host = "127.0.0.1";

	bzero((char *)&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port); // just a guess at an open port

	if (regex_match(host.c_str(), mm, std::regex("\\d+(\\.\\d+){3}")))
		inet_aton(host.c_str(), &sin.sin_addr);
	else
	{
		hostent_sp = gethostbyname(host.c_str());
		if (!hostent_sp)
		{
			perror("gethostbyname");
			return (-1);
		}
		sin.sin_addr = *(struct in_addr *)(hostent_sp->h_addr_list[0]);
	}
	return 0;
}

//========================================================================================================================
int TCPNetworkSocket::TCPConnect(char const* host_in, int dflt_port, long flags, int sndbufsiz)
{
	int s_fd, sts;
	struct sockaddr_in sin;

	s_fd = socket(PF_INET, SOCK_STREAM/*|SOCK_NONBLOCK*/, 0); // man socket,man TCP(7P)

	if (s_fd == -1)
	{
		perror("socket error");
		return (-1);
	}

	sts = ResolveHost(host_in, dflt_port, sin);
	if (sts == -1)
	{
		close(s_fd);
		return -1;
	}

	sts = connect(s_fd, (struct sockaddr *)&sin, sizeof(sin));
	if (sts == -1)
	{
		//perror( "connect error" );
		close(s_fd);
		return (-1);
	}

	if (flags)
	{
		sts = fcntl(s_fd, 0, flags);
		std::cout<< "TCPConnect fcntl(fd=" << s_fd << ",flags=0x" << std::hex << flags << std::dec << ") =" << sts;
	}

	if (sndbufsiz > 0)
	{
		int len;
		socklen_t lenlen = sizeof(len);
		len = 0;
		sts = getsockopt(s_fd, SOL_SOCKET, SO_SNDBUF, &len, &lenlen);
		std::cout<<  "TCPConnect SNDBUF initial: " << len << " sts/errno=" << sts << "/" << errno << " lenlen=" << lenlen << std::endl;
		len = sndbufsiz;
		sts = setsockopt(s_fd, SOL_SOCKET, SO_SNDBUF, &len, lenlen);
		if (sts == -1)
			std::cout<<  "Error with setsockopt SNDBUF " << errno;
		len = 0;
		sts = getsockopt(s_fd, SOL_SOCKET, SO_SNDBUF, &len, &lenlen);
		if (len < (sndbufsiz * 2))
			std::cout<<  "SNDBUF " << len << " not expected (" << sndbufsiz << " sts/errno=" << sts << "/" << errno << ")" << std::endl;
		else
			std::cout<<  "SNDBUF " << len << " sts/errno=" << sts << "/" << errno << std::endl;
	}
	return (s_fd);
}

//========================================================================================================================
void TCPNetworkSocket::connectSocket()
{
	if (isSender_)
	{
		sockaddr_in addr;
		socklen_t arglen = sizeof(addr);
		SendSocket_ = accept(TCPSocketNumber_, (struct sockaddr *)&addr, &arglen);

		if (SendSocket_ == -1)
		{
			perror("accept");
			exit(1);
		}

		MagicPacket m;
		auto sts = read(SendSocket_, &m, sizeof(MagicPacket));
		if (!checkMagicPacket(m) || sts != sizeof(MagicPacket))
		{
			perror("magic");
			exit(1);
		}
	}
	else
	{
		TCPSocketNumber_ = TCPConnect(host_.c_str(), port_, 0, O_NONBLOCK);
		auto m = makeMagicPacket(port_);
		auto sts = ::send(TCPSocketNumber_, &m, sizeof(MagicPacket), 0);
		std::cout << "Sent magic packet!" << std::endl;
		sleep(5);
		if (sts != sizeof(MagicPacket))
		{
			perror("connect");
			exit(1);
		}
	}
}

//========================================================================================================================
int TCPNetworkSocket::send(const uint8_t* data, size_t size)
{
	std::unique_lock<std::mutex> lk(socketMutex_);
	if (SendSocket_ == -1)
	{
		connectSocket();
	}

	size_t offset = 0;
	int sts = 1;

	while (offset < size && sts > 0)
	{
		auto thisSize = size - offset > chunkSize_ ? chunkSize_ : size - offset;

		std::cout << "Sending data!" << std::endl;
		sts = ::send(SendSocket_, data + offset, thisSize, 0);

		// Dynamically resize chunk size to match send calls
		if (static_cast<size_t>(sts) != size - offset && static_cast<size_t>(sts) < chunkSize_)
		{
			chunkSize_ = sts;
		}
		offset += sts;
	}

	if (sts <= 0)
	{
		std::cout << "Error writing buffer for port " << port_ << ": " << strerror(errno) << std::endl;
		return -1;
	}
	return 0;
}

//========================================================================================================================
int TCPNetworkSocket::send(const std::string& buffer)
{
	return send(reinterpret_cast<const uint8_t*>(buffer.c_str()), buffer.size());
}

//========================================================================================================================
int TCPNetworkSocket::receive(uint8_t* buffer, unsigned int timeoutSeconds, unsigned int timeoutUSeconds)
{
	//lockout other receivers for the remainder of the scope
	std::unique_lock<std::mutex> lk(socketMutex_);

	if (TCPSocketNumber_ == -1)
	{
		connectSocket();
	}

	//set timeout period for select()
	struct timeval timeout;
	timeout.tv_sec = timeoutSeconds;
	timeout.tv_usec = timeoutUSeconds;

	fd_set fdSet;
	FD_ZERO(&fdSet);
	FD_SET(TCPSocketNumber_, &fdSet);
	select(TCPSocketNumber_ + 1, &fdSet, 0, 0, &timeout);

	if (FD_ISSET(TCPSocketNumber_, &fdSet))
	{
		auto sts = -1;
		if ((sts = read(TCPSocketNumber_, buffer, chunkSize_)) == -1)
		{
			std::cout << "Error reading buffer from: " << host_ << ":" << port_ << std::endl;
			return -1;
		}

		//NOTE: this is inexpensive according to Lorenzo/documentation in C++11 (only increases size once and doesn't decrease size)
		return sts;
	}

	return -1;
}

//========================================================================================================================
//receive ~~
//	returns 0 on success, -1 on failure
//	NOTE: must call Socket::initialize before receiving!
int TCPNetworkSocket::receive(std::string& buffer, unsigned int timeoutSeconds, unsigned int timeoutUSeconds)
{
	buffer.resize(chunkSize_);
	auto sts = receive(reinterpret_cast<uint8_t*>(&buffer[0]), timeoutSeconds, timeoutUSeconds);
	if (sts > 0)
	{
		buffer.resize(sts);
		return 0;
	}
	return -1;
}



