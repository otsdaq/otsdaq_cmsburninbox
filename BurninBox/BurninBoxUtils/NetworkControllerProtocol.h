#ifndef _ots_NetworkControllerProtocol_h_
#define _ots_NetworkControllerProtocol_h_

#include "ControllerProtocol.h"
#include "otsdaq/NetworkUtilities/TCPClient.h"
#include <mutex>
#include <fstream>

namespace ots
{
class NetworkControllerProtocol
    : public ControllerProtocol
    , public TCPClient
{
  public:
    NetworkControllerProtocol(std::string IPAddress, int port);
    virtual ~NetworkControllerProtocol(void);

    void        write(const std::string& command) override;
    std::string read(const std::string& command) override;
    std::string readValue(const std::string& command) override;

  private:
    void sendMail(const std::vector<std::string>& toList,
                  const std::string&              from,
                  const std::string&              subject,
                  const std::string&              message);
    std::mutex    readWriteMutex_; // to make socket thread safe
    std::ofstream logFile_;

};
} // namespace ots
#endif
