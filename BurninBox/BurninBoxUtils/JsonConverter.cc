/*
 * JsonConverter.cc
 *
 *  Created on: Sep 12, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/JsonConverter.h"
#else
#include "JsonConverter.h"
#endif

#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace ots;

//========================================================================================================================
JsonConverter::JsonConverter(std::string name)
: className_(name)
{
}

//========================================================================================================================
JsonConverter::~JsonConverter()
{
}

//========================================================================================================================
const std::string& JsonConverter::getClassName(void)
{
	return className_;
}

//========================================================================================================================
void JsonConverter::printVariables(void)
{
	std::cout << className_ << " -> ";
	for(auto variable : variables_)
			std::cout << variable.first << " : " << variable.second << std::endl;
}

//========================================================================================================================
void JsonConverter::printToFile(std::ofstream& logfile)
{
	logfile << className_ << " -> \n";
	for(auto variable : variables_)
	{
		logfile << variable.first << " : ";
		for (unsigned int i = (unsigned int)variable.first.size(); i<20; i++)
			logfile << " ";
		logfile << variable.second << "\n";
	}
	logfile << "\n" ;
}

//========================================================================================================================
void JsonConverter::printToScreen(void)
{
	std::cout << "\t\033[1;42m" << className_ << " -> \033[0m \n";
	for(auto variable : variables_)
	{
		std::cout << "\t\033[1;32m" << variable.first << " : \033[0m ";
        for (unsigned int i = (unsigned int)variable.first.size(); i<20; i++)
        	std::cout << " ";
        std::cout << variable.second << "\n";
     }
     std::cout << "\n" ;
}

//========================================================================================================================
const std::string& JsonConverter::convertToJSON(void)
{
	json_ =  className_ + ":{";
	for(auto& variable : variables_)
			json_ += variable.first + ":" + variable.second + ",";
	json_.replace(json_.length()-1, 1, "}");
	return json_;
}

//========================================================================================================================
void JsonConverter::convertFromJSON(const std::string& json)
{
	variables_.clear();
	size_t begin = json.find('{');
	size_t colon   = begin;
	size_t comma   = begin;
	while(((comma = json.find(',', comma+1)) != std::string::npos) || ((comma = json.find('}', comma+1)) != std::string::npos))
	{
		colon = json.find(':',begin);
		variables_[json.substr(begin+1,colon-begin-1)] = json.substr(colon+1,comma-colon-1);
		//std::cout << "---" << json.substr(begin+1,colon-begin-1) << "---" << json.substr(colon+1,comma-colon-1) << "---" << std::endl;
		if(comma == json.length()-1) break;
		begin = comma;
	}
}

//========================================================================================================================
void JsonConverter::getVariableValue(const std::string& variable, std::string& variableValue, const std::string& json)
{
	size_t begin = json.find(variable)+variable.size()+1;
	size_t end   = json.find(',', begin);
	if(end == std::string::npos)
		end = json.find('}', begin);
        if(end == std::string::npos)
                end = json.size()-1;

	std::cout << variable << " : " << json.substr(begin,end-begin) << std::endl;
	variableValue = json.substr(begin,end-begin);
}
