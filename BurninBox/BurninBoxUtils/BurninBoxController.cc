#include "Device.h"
//#include "Relay.h"
//#include "TemperatureSensor.h"
//#include "DewPointSensor.h"
#include "BurninBoxController.h"
#include "BurninBoxStatus.h"
#include "ConfigurationReader.h"
#include "ControllerBoard.h"
#include "NameDefinition.h"
#include "SafetyRangeDefinition.h"
#include <chrono> //Timer();
#include <iostream>
#include <math.h>
#include <numeric>
#include <stdio.h>
#include <sys/time.h>
#include <thread>
#include <unistd.h> 
#include <limits.h>


using namespace ots;

#define SERVER_PORT 5001

// Emanuele Aucone
//===========================================================================
BurninBoxController::BurninBoxController()
    : TCPServer(SERVER_PORT, 1)
      //    , runningFuture_(runningPromise_.get_future())
      ,
      theControllerBoard_(nullptr)
      //    , theStreamer_(new UDPNetworkSocket("192.168.0.101", 10000))
      //, targetTemperature_(20.0)
      //, setTemperatureLowTolerance_ (0.2)
      //, setTemperatureHighTolerance_(0.1)
      //, currentTargetTemperature_    (targetTemperature_)
      ,
      theStateMachineStatus_(HALTED), theCycleStatus_(CYCLE_START)
// , dewPointSafetyMargin_(SafetyRangeDefinition::dewPointSafetyMarginDefault)
// , dewPointSafetyHighMargin_(SafetyRangeDefinition::dewPointSafetyMinusHighDefault)
// , dewPointHighLowMargin_(SafetyRangeDefinition::dewPointHighMinusLowDefault)
// , running_(false)
// , enableTemperature_(false)
// , dewPointRecursiveCounter_(0)
{

    char hostname[HOST_NAME_MAX];
    char username[LOGIN_NAME_MAX];
    gethostname(hostname, HOST_NAME_MAX);
    getlogin_r(username, LOGIN_NAME_MAX);

    computerName_ = hostname;
    userName_     = username;
}

// Leonardo
//===========================================================================
// BurninBoxController::BurninBoxController() : theControllerBoard_(nullptr) {}

// Leonardo init after declaration
//===========================================================================
// void BurninBoxController::init(const DeviceContainer& DeviceContainer)
// {
// 	theDeviceContainer_          = DeviceContainer;
//     theControllerBoard_          = new ControllerBoard("Network");
//     theStreamer_                 = new UDPNetworkSocket("192.168.0.101", 10000);
// 	theStreamer_->initialize();
// 	targetTemperature_              = 20.0;
// 	//setTemperatureLowTolerance_  = 0.2;
// 	//setTemperatureHighTolerance_ = 0.1;
// 	//, currentTargetTemperature_    =targetTemperature_;
// 	dewPointSafetyMargin_        = SafetyRangeDefinition::dewPointSafetyMarginDefault;
// 	dewPointSafetyHighMargin_    = SafetyRangeDefinition::dewPointSafetyMinusHighDefault;
// 	dewPointHighLowMargin_       = SafetyRangeDefinition::dewPointHighMinusLowDefault;
// 	running_                     = false;
// 	enableTemperature_           = false;
// 	dewPointRecursiveCounter_    = 0;
// 	getStateMachineStatus()       = INITIAL;
// 	theCycleStatus_        = START;

// 	checkConfiguration();

// 	temperatureStructure_.push_back(&currentFrameTemperature_);
// 	temperatureStructure_.push_back(&currentFrameLeftTemperature_);
// 	temperatureStructure_.push_back(&currentFrameRightTemperature_);
// 	getStateMachineStatus() = HALTED;
// }

//===========================================================================
BurninBoxController::~BurninBoxController()
{
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDeleting the controller board" << std::endl;
    if(theControllerBoard_ != nullptr)
        delete theControllerBoard_;
    // delete theStreamer_;

    // targetTemperature_    = 25.0;
    // enableTemperature_ = false;
    // turn off all relays
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone destructor" << std::endl;
}

//===========================================================================
void BurninBoxController::checkConfiguration(const ConfigurationReader &configuration)
{
    // CHECK THESE THINGS WITH THE ALIAS AND PRINT A WARNING!
    // std::cout << "WARNING: WHEN RUNNING THE COLD BOX IN THE STANDARD CONFIGURATION:" << std::endl;
    // std::cout << "RTD1 is the FrameleftController" << std::endl;
    // std::cout << "RTD2 is ........." << std::endl;

    // devices_[FrameTemperature]["RTD1"]       = false;
    // devices_[FrameTemperature]["RTD2"]       = false;

    // devices_[AmbientTemperature]["RTD3"]     = false;
    // devices_[AmbientTemperature]["RTD4"]     = false;

    // devices_[ModuleCarrierTemperature]["DTS1"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS2"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS3"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS4"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS5"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS6"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS7"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS8"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS9"]     = false;
    // devices_[ModuleCarrierTemperature]["DTS10"]    = false;

    // devices_[AmbientDewPoint]["H&T1"]    = false;
    // devices_[AmbientDewPoint]["H&T2"]    = false;

    theFrameDevice_.setAttributes(configuration.getTemperatureSensor(
        NameDefinition::Frame)); // Throws an exception if it is not in the configuration
    if (!theFrameDevice_.isOn())
        throw std::runtime_error("ERROR: The Frame temperature device MUST be configured and turned on!");

    theFrameLeftDevice_.setAttributes(configuration.getTemperatureSensor(
        NameDefinition::FrameLeft)); // Throws an exception if it is not in the configuration
    if (!theFrameLeftDevice_.isOn())
        throw std::runtime_error("ERROR: The FrameLeft temperature device MUST be configured and turned on!");

    theFrameRightDevice_.setAttributes(configuration.getTemperatureSensor(
        NameDefinition::FrameRight)); // Throws an exception if it is not in the configuration
    if (!theFrameRightDevice_.isOn())
        throw std::runtime_error("ERROR: The FrameRight temperature device MUST be configured and turned on!");

    bool ambientTemperatureFlag = false;
    std::vector<std::string> ambientNames;
    ambientNames.push_back(NameDefinition::Ambient1);
    ambientNames.push_back(NameDefinition::Ambient2);

    for (auto name : ambientNames)
    {
        try
        {
            theAmbientDevices_[name].setAttributes(
                configuration.getTemperatureSensor(name)); // Throws an exception if it is not in the configuration
            if (theAmbientDevices_[name].isOn())
                ambientTemperatureFlag = true;
        }
        catch (const std::exception &e)
        {
            continue;
        }
    }
    if (!ambientTemperatureFlag)
    {
        std::cout << "WARNING: You didn't configure any module ambient temperature device!" << std::endl;
        std::cout << "WARNING: You didn't configure any module ambient temperature device!" << std::endl;
        std::cout << "WARNING: You didn't configure any module ambient temperature device!" << std::endl;
        std::cout << "WARNING: You didn't configure any module ambient temperature device!" << std::endl;
    }

    bool moduleTemperatureFlag = false;
    std::vector<std::string> moduleNames;
    moduleNames.push_back(NameDefinition::ModuleCarrier1Left);
    moduleNames.push_back(NameDefinition::ModuleCarrier2Left);
    moduleNames.push_back(NameDefinition::ModuleCarrier3Left);
    moduleNames.push_back(NameDefinition::ModuleCarrier4Left);
    moduleNames.push_back(NameDefinition::ModuleCarrier5Left);
    moduleNames.push_back(NameDefinition::ModuleCarrier1Right);
    moduleNames.push_back(NameDefinition::ModuleCarrier2Right);
    moduleNames.push_back(NameDefinition::ModuleCarrier3Right);
    moduleNames.push_back(NameDefinition::ModuleCarrier4Right);
    moduleNames.push_back(NameDefinition::ModuleCarrier5Right);

    for (auto name : moduleNames)
    {
        try
        {
            theModuleCarrierDevices_[name].setAttributes(
                configuration.getTemperatureSensor(name)); // Throws an exception if it is not in the configuration
            if (theModuleCarrierDevices_[name].isOn())
                moduleTemperatureFlag = true;
        }
        catch (const std::exception &e)
        {
            continue;
        }
    }
    if (!moduleTemperatureFlag)
    {
        std::cout << "WARNING: You didn't configure any module temperature device!" << std::endl;
        std::cout << "WARNING: You didn't configure any module temperature device!" << std::endl;
        std::cout << "WARNING: You didn't configure any module temperature device!" << std::endl;
        std::cout << "WARNING: You didn't configure any module temperature device!" << std::endl;
    }

    bool ambientDewPointFlag = false;
    std::vector<std::string> dewPointNames;
    dewPointNames.push_back(NameDefinition::DewPoint1);
    dewPointNames.push_back(NameDefinition::DewPoint2);

    for (auto name : dewPointNames)
    {
        try
        {
            configuration.getDewPointSensor(name); // Throws an exception if it is not in the configuration
        }
        catch (const std::exception &e)
        {
            continue;
        }
        theDewPointDevices_[name].setAttributes(
            configuration.getDewPointSensor(name)); // Throws an exception if it is not in the configuration
        if (theDewPointDevices_[name].isOn())
            ambientDewPointFlag = true;
        else
            theDewPointDevices_.erase(theDewPointDevices_.find(name));
    }

    if (!ambientDewPointFlag)
    {
        throw std::runtime_error("ERROR: At least one of the DewPoint1 or DewPoint2 MUST be configured and turned on!");
    }

    theDoorLockDevice_.setAttributes(
        configuration.getRelay("DoorLock")); // Throws an exception if it is not in the configuration

    theDryAirFluxDevice_.setAttributes(
        configuration.getRelay("DryAirFlux")); // Throws an exception if it is not in the configuration

    theChillerDevice_.setAttributes(configuration.getChiller("Chiller"));
    // if (!theChillerDevice_.isOn())
    // {
    //     throw std::runtime_error("ERROR: The Chiller MUST be configured and turned on!");
    // }
    if (theChillerDevice_.getControllerConnector() != "BurninController" && 
        theChillerDevice_.getControllerConnector().find("/dev/tty") != 0)
    {
        throw std::runtime_error("ERROR: The Chiller can only be connected to the BurninController or any /dev/tty serial!");
    }

    const ConfigurationAttributes &connection = configuration.getConnection();
    theControllerBoard_ = new ControllerBoard(connection.find("Protocol")->second,
                                              connection.find("Version")->second,
                                              connection.find("IPAddress")->second,
                                              stoi(connection.find("Port")->second)
                                             );

    mailList_           = configuration.getEmail().find("Users")->second;
    turnChillerOff_     = configuration.turnChillerOff();
    unlockDoor_         = configuration.unlockDoor();
    keepDryAirFlowHigh_ = configuration.keepDryAirFlowHigh();

}

//========================================================================================================================
// virtual function to interpret messages
// interacting with theBurninBoxController_ using theBurninBoxConfiguration_ as helper class
std::string BurninBoxController::interpretMessage(const std::string &buffer)
{
    std::cout << "[" << __LINE__ << "] "<< __PRETTY_FUNCTION__ << "\tReceived: " << buffer << std::endl;
    //printTime();
//    return buffer + "Done";
//    std::lock_guard<std::mutex> lock(statusMutex_);
    if (buffer == "Initialize")
    {
        return "InitializeDone";
    }
    else if (buffer.substr(0, 9) == "Configure")
    {
        theBurninBoxConfiguration_.convertFromJson(buffer.substr(10, buffer.length() - 10));
        return configure();
    }
    else if (buffer.substr(0, 5) == "Start") // changing the status changes the mode in threadMain (BBC) function.
    {
        currentRun_ = getVariableValue("RunNumber", buffer);
        //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tRun " << currentRun_ << " started!" << std::endl;

        return start(currentRun_);
    }
    else if (buffer.substr(0, 4) == "Stop")
    {
        return stop();
    }
    else if (buffer.substr(0, 4) == "Halt")
    {        
        //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tHalting" << std::endl;
        return halt();
    }
    else if (buffer == "Pause")
    {
        // We need to think :)
        //--->Maybe the temperature could be kept until start button is pressed
        // running_ = false;
        // paused_  = true;
        //        switchThermalRelays(false);
        return "PauseDone";
    }
    else if (buffer == "Resume")
    {
        // We need to think :)
        // running_ = true;
        // paused_  = false;
        return "ResumeDone";
    }
    else if (buffer == "Status?")
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tReading status!" << std::endl;
        BurninBoxStatus theBurninBoxStatus = getStatus(); // For thread safety
        // print on screen
        // theBurninBoxStatus.printVariables();
        if (getStateMachineStatus() == STATE_MACHINE_ERROR)
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThe state machine is in the error state!" << std::endl;
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone reading status!" << std::endl;
        return theBurninBoxStatus.convertToJSON(); // send createdempty status (?)
    }
    else if (buffer == "Config?")
    {
        return theBurninBoxConfiguration_.convertToJson();
    }
    else if (buffer == "Run?")
    {
        return currentRun_;
    }
    // changing the status changes the mode in threadMain (BBC) function.
    else if (buffer.substr(0, 20) == "SetTargetTemperature") 
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSetting temperature: " << getVariableValue("Temperature", buffer.substr(20)) << std::endl;
        std::string retVal = setTargetTemperature(getVariableValue("Temperature", buffer.substr(20)));
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tTemperature set: " << retVal << std::endl;
       
        return retVal;
    }
    else if (buffer.substr(0, 14) == "AddTemperature") 
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tAdding..."
        << "Name: " << getVariableValue("Name", buffer.substr(14)) 
        << "Temp: " << getVariableValue("Temperature", buffer.substr(14)) 
        << "Time: " << getVariableValue("Time", buffer.substr(14)) 
        << "Sequ: " << getVariableValue("Sequence", buffer.substr(14)) 
        << std::endl;
        return addTemperature(getVariableValue("Name", buffer.substr(14))
        , std::stof(getVariableValue("Temperature", buffer.substr(14)))
        , std::stoi(getVariableValue("Time", buffer.substr(14)))
        , std::stoi(getVariableValue("Sequence", buffer.substr(14))));
    }
    else if (buffer == "Running?")
    {
        // if(running_)
        //     return "running";
        // else
        //     return "no";
    }
    else if (buffer == "Paused?")
    {
        // if(paused_)
        //     return "paused";
        // else
        //     return "no";
    }
    else if (buffer.substr(0, 8) == "COMMAND:")
    {
        return theControllerBoard_->executeCommand(buffer.substr(8, buffer.length()-8));
    }
    else if (buffer.substr(0, 6) == "Error:")
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tGot some sort of error: " << buffer << std::endl;
    }

    return "I don't know how to handle message: " + buffer;
}

//===========================================================================
std::string BurninBoxController::configure()
{
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "SM status: " << getStateMachineStatus() << std::endl;
    if (getStateMachineStatus() != HALTED)
    {
        setStateMachineStatus(STATE_MACHINE_ERROR);
        errorMessage_.str("");
        errorMessage_ << "ERROR: The BurninBox state machine was not in the HALTED state. Halting everything!";
        halt();
        return errorMessage_.str();
    }

    setStateMachineStatus(CONFIGURE);
 
    theTemperatureSequence_.setErrorTemperature(SafetyRangeDefinition::safeTemperatureValue);
    theTemperatureSequence_ = theBurninBoxConfiguration_.getTemperatures();
    for (auto temperature : theTemperatureSequence_)
    {
        if (temperature.second.temperature < SafetyRangeDefinition::minTemperatureValue ||
            temperature.second.temperature > SafetyRangeDefinition::maxTemperatureValue)
        {
            setStateMachineStatus(STATE_MACHINE_ERROR);
            errorMessage_.str("");
            errorMessage_ << "ERROR: TargetTemperature is not in the correct range ["
                          << SafetyRangeDefinition::minTemperatureValue << "C,"
                          << SafetyRangeDefinition::maxTemperatureValue << "C]."
                          << " Setting TargetTemperature and StopTemperature to the safety value: "
                          << SafetyRangeDefinition::safeTemperatureValue << "C.";
            //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << errorMessage_.str() << std::endl;

            return errorMessage_.str();
        }
    }
    // THERE MUST BE AT LEAST 2 TEMPERATURES, the target, the stop temperatures (the error is always set to the
    // SafetyRangeDefinition::safeTemperatureValue) 

    if (theTemperatureSequence_.getNumberOfTemperatures() < 2)
    {
        errorMessage_.str("");
        errorMessage_
            << "ERROR: The configuration must have at least 2 temperatures. The target and the stop temperature.";
            //std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << errorMessage_.str() << std::endl;
        return errorMessage_.str();
    }
    //Setting immediately the setpoint controller for the chiller
    try
    {
        theControllerBoard_->setChillerSetPoint(theChillerDevice_, 0);
    }
    catch(const std::exception& e)
    {
        errorMessage_.str("");
        errorMessage_
            << "ERROR: " << e.what();
        return errorMessage_.str();
    }
    
    setStateMachineStatus(CONFIGURED);

    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tConfigure Done!" << std::endl;
    return "ConfigureDone";
}

//===========================================================================
std::string BurninBoxController::start(std::string runNumber) // Do I want to call configure() in this function or in main?
{
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
    if (getStateMachineStatus() != CONFIGURED)
    {
        setStateMachineStatus(STATE_MACHINE_ERROR);
        errorMessage_.str("");
        errorMessage_ << "ERROR: The BurninBox state machine was not in the CONFIGURED state. Halting everything!";
        halt();
        return errorMessage_.str();
    }

    setStateMachineStatus(STARTING);
    try
    {
        updateStatus(0);
    }
    catch(const std::exception& e)
    {
        errorMessage_.str("");
        errorMessage_ << e.what();
        halt();
        return errorMessage_.str();
    }
    runningFuture_ = std::async(std::launch::async, &BurninBoxController::running, this);
    setStateMachineStatus(RUNNING);
    return "StartDone";
}

//===========================================================================
std::string BurninBoxController::halt(void)
{
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "SM status: " << getStateMachineStatus() << std::endl;
    //Right now the state machine is set to error only in configure and start (before starting)
    //so I am sure that the chiller is not on...but to be sure I just issue a stop
    if (getStateMachineStatus() == STATE_MACHINE_ERROR)
    {
        setStateMachineStatus(STOPPING);
    }
    
    std::string message = stop();
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tMessage: " << message << std::endl;
    if(message == "StopDone")
    {
        setStateMachineStatus(HALTED);
        return "HaltDone";
    }
    else
    {
        return message;
    }
}
//===========================================================================
std::string BurninBoxController::stop(void)
{
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSM status: " << getStateMachineStatus() << std::endl;
    if (getStateMachineStatus() == CONFIGURED)
        return "StopDone";
    else if (getStateMachineStatus() == RUNNING || getStateMachineStatus() == PAUSED)
    {
        setStateMachineStatus(STOPPING);
        std::this_thread::sleep_for (std::chrono::seconds(1));//Wait 1 second to give some time to stop the cycle
    }
    
    if (getStateMachineStatus() == STOPPING)
    {
        if(runningFuture_.valid() && runningFuture_.wait_for(std::chrono::seconds(1)) != std::future_status::ready)
        {
            //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tThe cycle state machine is still running..." << std::endl;
            return "Stopping";
        }
        else
        {
            setStateMachineStatus(CONFIGURED);
            return "StopDone";
        }        
    }
    //Should never get here!
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tI should never get here. State machine status: " << std::endl;
    return "StopError";
}

//===========================================================================
void BurninBoxController::running()
{
    //Locking the door
    if(theDoorLockDevice_.isOn())
    {
         theControllerBoard_->relayOn(theDoorLockDevice_);
         theDoorLockStatus_ = true;
    }
    
    std::cout << "Things that must still be done:" << std::endl
              << "1) handle module temperatures check and errors" << std::endl
              << "2) make sure the cycles don't switch using the avoidSwitching variables...right now is not used"
              << std::endl
              << "3) plots" << std::endl
              << "4) improve the error messages" << std::endl
              << "5) exchange messages between controller and otsdaq" << std::endl
              //DONE << "6) door lock" << std::endl
              //DONE<< "7) ADD RELAY ON THE DRY GAS!!!!!!!" << std::endl
              << std::endl;
    //        exit(1);

    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tState machine: " << getStateMachineStatus() << std::endl;
    errorMessage_.str("");
    errorMessage_ << "No error.";
    updatingStatus_ = false;

    // CONFIGURABLES or SAFETY RANGE DEFINITION?
    const float maxDeltaDewPointWithAverage = 5;
    const float dewPointGoingDown = -0.1;
    const float dewPointGoingUp = 0.1;
    const unsigned operationTimeout =
        SafetyRangeDefinition::errorTimeout; // Timeout in seconds that MUST be configurable! Right now 600seconds
    const unsigned operationStoppingTimeout =
        SafetyRangeDefinition::errorTimeout*3; // Special timeout in seconds while stopping, in order to reach the 
                                               // stop temperature to turn off the chiller that MUST be configurable! 
                                               //Right now 600*3seconds
    const unsigned statusUpdateTimeout = 5;    // Read attempts!
    int statusUpdateTimeoutCounter = 0;
    const unsigned outOfRangeTimeout = 5;    // Read attempts!
    int outOfRangeTimeoutCounter = 0;

    // History vectors
    const unsigned historySize = 6;
    unsigned historyIndex = 0;
    unsigned historyPreviousIndex = historySize - 1;

    std::vector<float> dewPointHistory(historySize, 0);
    std::vector<float> frameHistory(historySize, 0);

    std::vector<float> dewPointAverageHistory(historySize, 0);
    std::vector<float> frameAverageHistory(historySize, 0);
    std::vector<float> dewPointAverageTrendHistory(historySize, 0);
    std::vector<float> frameAverageTrendHistory(historySize, 0);
    std::vector<unsigned> timeHistory(historySize, 0);

    // Helper variables
    //unsigned targetTemperatureIndex = 0;
    float dewPointTemperature;
    float frameTemperature;
    float dewPointAverageLastDelta;
    float frameAverageLastDelta;

    struct timeval currentTime;
    struct timeval beginOperationTime;
    struct timeval beginTargetTime;

    // INITIALIZE VALUES BEFORE FIRST LOOP!
    bool errorCycle = false;
    bool changeCycle = false;
    gettimeofday(&currentTime, nullptr);
    gettimeofday(&beginOperationTime, nullptr);
    gettimeofday(&beginTargetTime, nullptr);
    setCycleStatus(CYCLE_INITIAL);
    // This temperature can typically be targetTemperature_ or stoptemperature_
    float chillerSetTemperature = SafetyRangeDefinition::safeTemperatureValue;
    BurninBoxStatus status;
    // std::cout << "]\t" << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tUpdateting status!" << std::endl;
    status = updateStatus(0); // The first time this forces an update of the values
    // std::cout << "]\t" << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tStatus updated!" << std::endl;

    theTemperatureSequence_.resetSequence();
    // std::cout << "]\t" << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tCycle status: " << translateCycleStatus(getCycleStatus()) << std::endl;
    while (getCycleStatus() != CYCLE_STOPPED &&
           getCycleStatus() != CYCLE_STOPPED_WITH_ERROR) // listen to port BurninBox_PORT
    {
        //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tBegin while loop." << std::endl;
        gettimeofday(&currentTime, nullptr); // Get new time every time I am cycling
        // No matter what is happening if the operation timeout is reached then throw an exception,
        // unless the state machine is in stopping mode then timeout when operationStoppingTimeout is passed
        if (((currentTime.tv_sec - beginOperationTime.tv_sec) >= operationTimeout && getStateMachineStatus() != STOPPING)
            || (currentTime.tv_sec - beginOperationTime.tv_sec) >= operationStoppingTimeout)
        {
            gettimeofday(&beginOperationTime, nullptr); // Reset begin operation timer
            setCycleError(CYCLE_ERROR_TIMEOUT);
            setCycleStatus(CYCLE_ERROR);
        }

        //////////////////////////////////////////////////////////////////////////////////
        // Reading BurninBox sensors status and calculating all necessary variable values
        try
        {
            status = updateStatus(0);// The first time this forces an update of the values
            statusUpdateTimeoutCounter = statusUpdateTimeout;
        }
        catch(const std::exception& e)
        {
             // NEED TO DECIDE WHAT TO DO!!!!!
            std::cout << e.what() << std::endl;
            std::cout << "I will try to read again the values " << statusUpdateTimeoutCounter
                      << " times and then go into error!" << std::endl;
            if (statusUpdateTimeoutCounter > 0) // If 0 then go to error state
            {
                --statusUpdateTimeoutCounter;
                continue;
            }
            else
            {
                //If after statusUpdateTimeout times we go in error, in that state the chiller 
                // will be set to a safe temperature (the error temperature)
                gettimeofday(&beginOperationTime, nullptr); // Reset begin operation timer
                setCycleError(CYCLE_ERROR_STATUS_READING);
                setCycleStatus(CYCLE_ERROR);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////
        // Checking possible out of range error conditions
        // If an out of range condition is detected in checkOutOfRangeTemperature(status), then an exception is thrown.
        // The outOfRangeTimeoutCounter is set to outOfRangeTimeout (right now 5) and for 5 times it will 
        // try to read again the values just to make sure it is not a glitch while reading
        try
        {
            checkOutOfRangeTemperature(status);
            outOfRangeTimeoutCounter = outOfRangeTimeout;
        }
        catch (const std::exception &e)
        {
            // NEED TO DECIDE WHAT TO DO!!!!!
            std::cout << e.what() << std::endl;
            std::cout << "I will try to read again the values " << outOfRangeTimeoutCounter
                      << " times and then go into error!" << std::endl;
            if (outOfRangeTimeoutCounter > 0) // If 0 then go to error state
            {
                // RIGHT NOW I WILL FORCE ANOTHER READING AND IF I KEEP COMING IN HERE THEN EVENTUALLY IT WILL TIMEOUT!
                // 0 force a reading and then continue will go back to the top where there will be another update.
                // If one day the top update timeout will be 0 then this one can be commented out
                status = updateStatus(0);
                --outOfRangeTimeoutCounter;
                continue;
            }
            else
            {
                //If after outOfRangeTimeout times we go in error, in that state the chiller 
                // will be set to a safe temperature (the error temperature)
                gettimeofday(&beginOperationTime, nullptr); // Reset begin operation timer
                setCycleError(CYCLE_ERROR_OUT_OF_RANGE_READINGS);
                setCycleStatus(CYCLE_ERROR);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        // NO MATTER WHAT MAKE SURE THAT THE CHILLER SET TEMPERATURE IS SAFE IF THE CHILLER IS ON!
        if (status.getChillerStatus())
        {
            theControllerBoard_->setChillerTemperature(theChillerDevice_,
                                                       (chillerSetTemperature = safeChillerSetTemperature(
                                                            theTemperatureSequence_.getCurrentTemperature(), status)));
        }
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////
        // Compare modules temperatures between them and with the frame
        //     if(getMaxModuleCarrierTemperature(void) > )
        // float  getMinModuleCarrierTemperature(void) const;
        // float  getAverageModuleCarrierTemperature(void) const;
        // float  getMaxModuleCarrierTemperature(void) const;

        // SETTING HELPER VARIABLES FROM STATUS
        frameTemperature = status.getFrameTemperature();
        dewPointTemperature = status.getMaxDewPointTemperature(); // This is the max between different dew point sensors

        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        // IF THE DEW POPINT IS BELOW THE TARGET TEMPERATURE THEN REDUCE THE DRY AIR FLUX TURNING ON THE RELAY.
        // THE DEFAULT IS THE DRY AIR FLUX RELAY ALWAYS OFF
        if(theDryAirFluxDevice_.isOn() && (dewPointTemperature + SafetyRangeDefinition::dewPointSafetyMarginDefault) < theTemperatureSequence_.getLowestTemperature())
        {
            theControllerBoard_->relayOn(theDryAirFluxDevice_);//Lower the dry air flux
            theDryAirFluxStatus_ = true;
        }
        else
        {
            theControllerBoard_->relayOff(theDryAirFluxDevice_);//This keeps the air flux always at the max
            theDryAirFluxStatus_ = false;
        }
        //////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////

        timeHistory[historyIndex] = status.getTime();
        dewPointHistory[historyIndex] = dewPointTemperature;
        frameHistory[historyIndex] = frameTemperature;

        dewPointAverageHistory[historyIndex] =
            accumulate(dewPointHistory.begin(), dewPointHistory.end(), 0.0) / dewPointHistory.size();
        frameAverageHistory[historyIndex] =
            accumulate(frameHistory.begin(), frameHistory.end(), 0.0) / frameHistory.size();

        // dewPointAverageLastDelta = dewPointAverageHistory[historyIndex]-dewPointAverageHistory[historyPreviousIndex];
        dewPointAverageLastDelta =
            dewPointAverageHistory[historyIndex] - dewPointAverageHistory[(historyIndex + 1) % historySize];

        frameAverageLastDelta =
            frameAverageTrendHistory[historyIndex] - frameAverageTrendHistory[historyPreviousIndex];
        dewPointAverageTrendHistory[historyIndex] =
            dewPointAverageLastDelta / (timeHistory[historyIndex] - timeHistory[historyPreviousIndex]);
        frameAverageTrendHistory[historyIndex] =
            frameAverageLastDelta / (timeHistory[historyIndex] - timeHistory[historyPreviousIndex]);

        // HISTORY INDEX CANNOT BE USED LATER ON AND YOU MUS RELY ONLY ON HELPER VARIABLES!
        historyIndex = (historyIndex + 1) % historySize;
        historyPreviousIndex = (historyIndex == 0) ? historySize - 1 : historyIndex - 1;

        // status.printToScreen();
        //////////////////////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////////////////
        // Checking the status of the state machine and eventually STOP the CYCLE
        if (getStateMachineStatus() == STOPPING 
            && getCycleStatus() != CYCLE_STOP
            && getCycleStatus() != CYCLE_ERROR
            && getCycleStatus() != CYCLE_STOPPED_WITH_ERROR 
           )
            setCycleStatus(CYCLE_STOP);

        //////////////////////////////////////////////////////////////////////////////////
        // BurninBox CYCLE
        time_t nowTime;
        struct tm *nowTimeInfo;
        char timeInfoBuf[64];

        nowTime = currentTime.tv_sec;
        nowTimeInfo = localtime(&nowTime);
        strftime(timeInfoBuf, sizeof timeInfoBuf, "%Y-%m-%d %H:%M:%S", nowTimeInfo);

        std::cout
            //<< "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl
            << "Time:                         " << timeInfoBuf << std::endl
            << "Cycle:                        " << translateCycleStatus(getCycleStatus())
            //<< " --> 0=INITIAL, 1=START, 2=COOL, 3=TARGET, 4=WARM, 5=STOP, 6=ERROR, 7=STOPPED, 8=STOPPED_WITH_ERROR "
            << std::endl
            << "State machine status:         " << translateStateMachineStatus(getStateMachineStatus()) << std::endl
            << "Elapsed time in cycle:        " << (currentTime.tv_sec - beginCycleTime_.tv_sec) << std::endl
            //<< "History index:                " << historyIndex << std::endl
            << "Target name:                  " << theTemperatureSequence_.getCurrentName() << std::endl
            << "Target temperature index:     " << theTemperatureSequence_.getCurrentSequenceNumber() << std::endl
            << "Target temperature:           " << theTemperatureSequence_.getCurrentTemperature() << std::endl
            << "Target time:                  " << theTemperatureSequence_.getCurrentTime() << std::endl
            << "Dew point temperature:        " << dewPointTemperature << std::endl
            << "Dew point average last delta: " << dewPointAverageLastDelta << " Going down? "
            << ((dewPointAverageLastDelta < dewPointGoingDown) ? "Yes" : "No")  << std::endl
            << "Chiller set temperature:      " << chillerSetTemperature << std::endl
            << "Frame temperature:            " << frameTemperature << std::endl
            << std::endl;
        switch (getCycleStatus())
        {
        case CYCLE_INITIAL:
            theTemperatureSequence_.resetSequence();
            // Wait until there are enough values to calculate the averages and trends
            // THIS IS THE ONLY PLACE WHERE I CAN USE THE HISTORY INDEXES!
            dewPointAverageHistory[historyPreviousIndex] = dewPointTemperature;
            frameAverageHistory[historyPreviousIndex] = frameTemperature;
            if (historyIndex == 0)
            {
                gettimeofday(&beginOperationTime, nullptr);
                setCycleStatus(CYCLE_START);
            }

            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tCYCLE_INITIAL) historyIndex: " << historyIndex
                      << " dewPointAverageLastDelta: " << dewPointAverageLastDelta
                      << " dewPointTemperature: " << dewPointTemperature
                      << " targetTemperature_: " << theTemperatureSequence_.getCurrentTemperature()
                      << " dewPointSafetyMarginDefault: " << SafetyRangeDefinition::dewPointSafetyMarginDefault
                      << " frameTemperature: " << frameTemperature << std::endl;

            break;
        case CYCLE_GO_TO_START:
            goToStartAknowledgment_ = true;
            setCycleStatus(CYCLE_START);
            break;
        case CYCLE_START:

            changeCycle = false;
            // Necessary condition to move to the next cycle
            if (frameTemperature > (dewPointTemperature + SafetyRangeDefinition::dewPointSafetyMarginDefault))
            {
                // FOR SURE CAN GO TO THE NEXT CYCLE since the main safety conditions are satisfied
                if (theTemperatureSequence_.getCurrentTemperature() >
                    (dewPointTemperature + SafetyRangeDefinition::dewPointSafetyMarginDefault))
                {
                    changeCycle = true;
                }
                // Can change cycle if the dew point is going in the right direction
                else if (dewPointAverageLastDelta < dewPointGoingDown)
                {
                    changeCycle = true;
                }
                else
                {
                    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__  
                              << "INFO: The frame temperature is greater than the dewPoint + safety, the target "
                                 "temperature is lower than the dew point and the dew point is not going down."
                                 "NOT CHANGING CYCLE FROM START!"
                              << std::endl;
                }
            }

            if (changeCycle)
            {
                gettimeofday(&beginOperationTime, nullptr);
                changeCycle = false;
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: Conditions are met to change cycle status!" << std::endl;
                // Set chiller temperature and turn on chiller
                theControllerBoard_->setChillerTemperature(theChillerDevice_,
                                                           (chillerSetTemperature = safeChillerSetTemperature(
                                                                theTemperatureSequence_.getCurrentTemperature(), status)));

                if (!theControllerBoard_->isChillerOn(theChillerDevice_))
                    theControllerBoard_->turnOnChiller(theChillerDevice_);

                if (compareWithTargetTemperatureTolerance(frameTemperature,
                                                          theTemperatureSequence_.getCurrentTemperature()) == 0)
                {
                    gettimeofday(&beginTargetTime, nullptr);
                    setCycleStatus(CYCLE_TARGET);
                }
                else if (compareWithTargetTemperatureTolerance(frameTemperature,
                                                               theTemperatureSequence_.getCurrentTemperature()) > 0)
                    setCycleStatus(CYCLE_COOLING);
                else if (compareWithTargetTemperatureTolerance(frameTemperature,
                                                               theTemperatureSequence_.getCurrentTemperature()) < 0)
                    setCycleStatus(CYCLE_WARMING);
                continue;
            }
            else
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: It is likely that the dew point temperature is too high "
                          << status.getMaxDewPointTemperature() << " C + Safety "
                          << SafetyRangeDefinition::dewPointSafetyMarginDefault
                          << "C. If the Dew point is not going down, then the system will timeout in "
                          << operationTimeout - (currentTime.tv_sec - beginOperationTime.tv_sec) << " seconds."
                          << std::endl;
            }

            if (dewPointAverageLastDelta >= dewPointGoingDown)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__  
                          << "INFO: Dew point temperature is not going down: " << dewPointAverageLastDelta
                          << ">=" << dewPointGoingDown << std::endl;
            }

            if (frameTemperature < (dewPointTemperature + SafetyRangeDefinition::dewPointSafetyMarginDefault))
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: Frame temperature still not safe to allow chiller on! "
                          << frameTemperature << "(Frame) < " << dewPointTemperature << "(Dew point) + "
                          << SafetyRangeDefinition::dewPointSafetyMarginDefault << "(Safety)" << std::endl;
            }
            if (theTemperatureSequence_.getCurrentTemperature() <
                (dewPointTemperature + SafetyRangeDefinition::dewPointSafetyMarginDefault))
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: Target temperature still not safe to allow chiller on! "
                          << theTemperatureSequence_.getCurrentTemperature() << "(Frame) < " << dewPointTemperature
                          << "(Dew point) + " << SafetyRangeDefinition::dewPointSafetyMarginDefault << "(Safety)"
                          << std::endl;
            }

            break;
        case CYCLE_COOLING:
            // 1) The chiller set temperature is ALWAYS checked at every cycle to make sure that it is not below the dew
            // point safety limits 2) There is always an operationTimeout that is also ALWAYS checked at every cycle in
            // case things don't go as expected

            changeCycle = false;
            // Check weird conditions and go back to previous status
            if (!status.getChillerStatus())
            {
                std::cout
                    << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__  
                    << "ERROR: The chiller was OFF while we are in the CYCLE_COOLING cycle. Going back to CYCLE_START!"
                    << std::endl;
                gettimeofday(&beginOperationTime, nullptr);
                setCycleStatus(CYCLE_START);
                continue;
            }

            if (dewPointAverageLastDelta >= dewPointGoingUp)
            {
                std::cout << __PRETTY_FUNCTION__
                          << "INFO: Dew point temperature is going up: " << dewPointAverageLastDelta
                          << ">=" << dewPointGoingUp << std::endl;
            }

            if (theTemperatureSequence_.getCurrentTemperature() != chillerSetTemperature)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: The chiller set temperature " << chillerSetTemperature
                          << "C is different than the target temperature "
                          << theTemperatureSequence_.getCurrentTemperature()
                          << "C. It is likely that the dew point temperature is too high "
                          << status.getMaxDewPointTemperature() << " C + Safety "
                          << SafetyRangeDefinition::dewPointSafetyMarginDefault
                          << "C. If the Dew point is not going down, then the system will timeout in "
                          << operationTimeout - (currentTime.tv_sec - beginOperationTime.tv_sec) << " seconds."
                          << std::endl;
                if (dewPointAverageLastDelta < dewPointGoingDown)
                {
                    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__  
                              << "INFO: Since the Dew point is going down, the timeout will be reset!" << std::endl;
                    gettimeofday(&beginOperationTime, nullptr); // Reset timer if at least the dew point is going down
                }
            }
            else //       if(theTemperatureSequence_.getCurrentTemperature() == chillerSetTemperature)
            {
                gettimeofday(&beginOperationTime, nullptr); // Reset timer if everything looks good
            }

            //    theControllerBoard_->turnOnChiller(theChillerDevice_);
            // IF CONDITIONS ARE MET, THEN CHANGE CYCLE STATUS!
            if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) ==
                0)
            {
                changeCycle = true;
                gettimeofday(&beginTargetTime, nullptr);
                setCycleStatus(CYCLE_TARGET);
            }
            else if (compareWithTargetTemperatureTolerance(frameTemperature,
                                                           theTemperatureSequence_.getCurrentTemperature()) < 0)
            {
                changeCycle = true;
                setCycleStatus(CYCLE_WARMING);
            }

            if (changeCycle)
            {
                gettimeofday(&beginOperationTime, nullptr);
            }

            break;
        case CYCLE_TARGET:
            /* DO SAFETY CHECKS */
            changeCycle = false;
            // Make sure chiller is on
            if (!status.getChillerStatus())
            {
                std::cout
                    << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ 
                    << "ERROR: The chiller was OFF while we are in the CYCLE_TARGET cycle. Going back to CYCLE_START!"
                    << std::endl;
                gettimeofday(&beginOperationTime, nullptr);
                setCycleStatus(CYCLE_START);
                continue;
            }
            // Quick check on Dew point status but NOT CRITICAL
            if (dewPointAverageLastDelta >= dewPointGoingUp)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ 
                          << "INFO: Dew point temperature is going up: " << dewPointAverageLastDelta
                          << ">=" << dewPointGoingUp << std::endl;
            }

            // Check if the chiller set temeprature is different from the target.
            // This condition typically happens if the dew point is too high.
            // If the temeprature doesn't stabilize, then the system will timeout!
            if (theTemperatureSequence_.getCurrentTemperature() != chillerSetTemperature)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: The chiller set temperature " << chillerSetTemperature
                          << "C is different than the target temperature "
                          << theTemperatureSequence_.getCurrentTemperature() << "C." << std::endl
                          << "It is likely that the dew point temperature is too high "
                          << status.getMaxDewPointTemperature() << "C + Safety "
                          << SafetyRangeDefinition::dewPointSafetyMarginDefault << "C." << std::endl
                          << "ATTENTION: If the Dew point is not going down, then the system will timeout in "
                          << operationTimeout - (currentTime.tv_sec - beginOperationTime.tv_sec) << " seconds."
                          << std::endl;
            }

            // Check if the temperature is at the target value otherwise it will timeout!
            if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) !=
                0)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: The frame temperature " << frameTemperature
                          << "C is different than the target temperature "
                          << theTemperatureSequence_.getCurrentTemperature() << "C." << std::endl
                          << "It is likely that the chiller has not yet stabilized at the target temperature."
                          << std::endl
                          << "ATTENTION: If the chiller won't stabilize at the target temperature, then the system "
                             "will timeout in "
                          << operationTimeout - (currentTime.tv_sec - beginOperationTime.tv_sec) << " seconds."
                          << std::endl;
            }

            // If everything looks good (the chiller is set at the target temperature and the frame is at the target
            // temperature) then reset the timeout!
            if (theTemperatureSequence_.getCurrentTemperature() == chillerSetTemperature &&
                compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) ==
                    0)
            {
                gettimeofday(&beginOperationTime, nullptr); // Reset timer if everything looks good
            }

            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: "
                      << "Target time: " << theTemperatureSequence_.getCurrentTime() << "s"
                      << " Time at target: "<< status.getTime() - beginTargetTime.tv_sec << "s"
                      << " Timeout in: "<< theTemperatureSequence_.getCurrentTime() -
                             (status.getTime() - beginTargetTime.tv_sec) << "s"
                      << std::endl;

            // Time at TARGET done so change target index for next temperature target!
            if (status.getTime() - beginTargetTime.tv_sec > theTemperatureSequence_.getCurrentTime())
            {
                gettimeofday(&beginOperationTime, nullptr); // Reset timer because of change
                if (theTemperatureSequence_.getCurrentSequenceNumber() > theTemperatureSequence_.getLastSequenceNumber())
                {
                    gettimeofday(&beginOperationTime, nullptr); // Reset timer
                    setCycleError(CYCLE_ERROR_WRONG_TEMPERATURE_INDEX);
                    setCycleStatus(CYCLE_ERROR);
                }
                else if (theTemperatureSequence_.getCurrentSequenceNumber() == theTemperatureSequence_.getLastSequenceNumber())
                {
                    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: Last target temperature reached! Going to CYCLE STOP."
                              << std::endl;
                    setCycleStatus(CYCLE_STOP);
                }
                else
                {
                    theTemperatureSequence_.next();
                    if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) == 0)
                        setCycleStatus(CYCLE_TARGET);
                    else if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) > 0)
                        setCycleStatus(CYCLE_COOLING);
                    else if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) < 0)
                        setCycleStatus(CYCLE_WARMING);
                }
            }

            break;
        case CYCLE_WARMING:
            // 1) The chiller set temperature is ALWAYS checked at every cycle to make sure that it is not below the dew
            // point safety limits 2) There is always an operationTimeout that is also ALWAYS checked at every cycle in
            // case things don't go as expected

            changeCycle = false;
            // Check weird conditions and go back to previous status
            if (!status.getChillerStatus())
            {
                std::cout
                    << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__  
                    << "ERROR: The chiller was OFF while we are in the CYCLE_WARMING cycle. Going back to CYCLE_START!"
                    << std::endl;
                gettimeofday(&beginOperationTime, nullptr);
                setCycleStatus(CYCLE_START);
                continue;
            }

            if (dewPointAverageLastDelta >= dewPointGoingUp)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ 
                          << "INFO: Dew point temperature is going up: " << dewPointAverageLastDelta
                          << ">=" << dewPointGoingUp << std::endl;
            }

            if (theTemperatureSequence_.getCurrentTemperature() != chillerSetTemperature)
            {
                std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tINFO: The chiller set temperature " << chillerSetTemperature
                          << "C is different than the target temperature "
                          << theTemperatureSequence_.getCurrentTemperature()
                          << "C. It is likely that the dew point temperature is too high "
                          << status.getMaxDewPointTemperature() << " C + Safety "
                          << SafetyRangeDefinition::dewPointSafetyMarginDefault
                          << "C. If the Dew point is not going down, then the system will timeout in "
                          << operationTimeout - (currentTime.tv_sec - beginOperationTime.tv_sec) << " seconds."
                          << std::endl;
            }

            if (theTemperatureSequence_.getCurrentTemperature() == chillerSetTemperature)
            {
                gettimeofday(&beginOperationTime, nullptr); // Reset timer if everything looks good
            }

            //    theControllerBoard_->turnOnChiller(theChillerDevice_);
            // IF CONDITIONS ARE MET, THEN CHANGE CYCLE STATUS!
            if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) ==
                0)
            {
                changeCycle = true;
                gettimeofday(&beginTargetTime, nullptr);
                setCycleStatus(CYCLE_TARGET);
            }
            else if (compareWithTargetTemperatureTolerance(frameTemperature,
                                                           theTemperatureSequence_.getCurrentTemperature()) > 0)
            {
                changeCycle = true;
                setCycleStatus(CYCLE_COOLING);
            }

            if (changeCycle)
            {
                gettimeofday(&beginOperationTime, nullptr);
            }

            break;
        case CYCLE_STOP:
            changeCycle = false;
            //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\ttargetTemperatureIndex: " << theTemperatureSequence_.getCurrentSequenceNumber()
            //          << " != " << theTemperatureSequence_.getLastSequenceNumber() << "stopTemperatureIndex ?" << std::endl;
            if (theTemperatureSequence_.getCurrentSequenceNumber() != theTemperatureSequence_.getLastSequenceNumber())
            {
                theTemperatureSequence_.gotoLast();
                if (getStateMachineStatus() == STOPPING)
                {
                    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ 
                              << "INFO: I received a state machine STOP and got to CYCLE_STOP but I didn't go through "
                                 "all temperatures. "
                              << " I will set the target temperature to the stop temperature (the last one in the "
                                 "list) which is: "
                              << theTemperatureSequence_.getCurrentTemperature() << "C." << std::endl;
                }
                else
                {
                    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ 
                              << "ERROR: I arrived at CYCLE_STOP but I didn't go through all temperatures. "
                              << " Since something is wrong I will set the target temperature to the stop temperature "
                                 "(the last one in the list) which is: "
                              << theTemperatureSequence_.getCurrentTemperature() << "C." << std::endl;
                }
                theControllerBoard_->setChillerTemperature(theChillerDevice_,
                                                           (chillerSetTemperature = safeChillerSetTemperature(
                                                                theTemperatureSequence_.getCurrentTemperature(), status)));
            }

            if (compareWithTargetTemperatureTolerance(frameTemperature, theTemperatureSequence_.getCurrentTemperature()) >=
                0)
            {
                if (turnChillerOff_ && theControllerBoard_->isChillerOn(theChillerDevice_))
                    theControllerBoard_->turnOffChiller(theChillerDevice_);
                setCycleStatus(CYCLE_STOPPED); // DONE!
            }
            else
            {
                if (!status.getChillerStatus())
                {
                    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ 
                              << "ERROR: The chiller was OFF while we are in the CYCLE_STOP cycle. "
                              << "Since the temperature is not at target, I am going to turn the chiller on move to "
                                 "CHILLER_WARMING or CHILLER_COOLING"
                              << std::endl;
                    gettimeofday(&beginOperationTime, nullptr);
                    theControllerBoard_->turnOnChiller(theChillerDevice_);
                }
                if (getStateMachineStatus() != STOPPING)
                {
                    // if(compareWithTargetTemperatureTolerance(frameTemperature,
                    //                                          theTemperatureSequence_.getCurrentTemperature()) > 0)
                    // {
                    //     changeCycle = true;
                    //     setCycleStatus(CYCLE_COOLING);
                    // }
                    // Do not cool down only warm up if frame temperature is lower than target
                    if (compareWithTargetTemperatureTolerance(frameTemperature,
                                                              theTemperatureSequence_.getCurrentTemperature()) < 0)
                    {
                        changeCycle = true;
                        setCycleStatus(CYCLE_WARMING);
                    }
                }
            }
            break;
        case CYCLE_STOPPED:
            break;
        case CYCLE_ERROR:
            errorMessage_.str("");
            // First time coming to this error state
            if (!errorCycle)
            {
                errorMessage_ << "ERROR: The BurninBoxController is in the error cycle state." << std::endl
                              << "The error occurred during a temperature cycle while in status: "
                              << translateCycleStatus(getPreviousCycleStatus()) << "." << std::endl
                              << "The error is: " << getCycleErrorMessage() << std::endl;
                std::cout << errorMessage_.str() << std::endl;
                status = updateStatus(0, errorMessage_.str());

                std::stringstream emailError;
                emailError
                            << "<line style=background-color:#ff0000>ERROR:</line> The BurninBoxController is in the error cycle state.<br>"
                            << "The error occurred during a temperature cycle while in status: " << translateCycleStatus(getPreviousCycleStatus()) << ".<br>"
                            << "<line style=background-color:#ff0000>The error is: " << getCycleErrorMessage() << "</line><br>"
                            << "The system will raise the temperature to the safety temperature of "
                            << SafetyRangeDefinition::safeTemperatureValue << " C and then shut down everything.<br>" << std::endl
                            << "An email will also be sent when the temperature cycle is done and the chiller is turned off!<br>"
                            << std::endl;
                sendMail(mailList_, "no-reply-burninbox@fnal.gov", "Temperature cycle ERROR!", emailError.str());
                errorCycle = true;
            }

            if (theTemperatureSequence_.getErrorTemperature() != SafetyRangeDefinition::safeTemperatureValue)
            {
                std::cout
                    << __PRETTY_FUNCTION__
                    << "THIS IS A PROGRAMMING ERROR AND MUST NEVER HAPPENS...ABORTING BADLY BECAUSE IT MUST BE FIXED!"
                    << std::endl;
                exit(1);
            }

            // WHAT DO I DO WITH THE TIMER??????
            gettimeofday(&beginOperationTime, nullptr); // Like this it means no timer in this status!

            theTemperatureSequence_.gotoError(); // Going directly to the last temperature to set which
                                                            // should be the error temperature

            // I stay in the error state until the chiller and everything is in a safe state
            theControllerBoard_->setChillerTemperature(theChillerDevice_,
                                                       (chillerSetTemperature = safeChillerSetTemperature(
                                                            theTemperatureSequence_.getCurrentTemperature(), status)));
            std::cout << "INFO: Setting chiller temperature to safe temperature " << chillerSetTemperature
                      << " and wait until it gets there." << std::endl;
            // Do not cool down only warm up if frame temperature is lower than target
            std::cout << "Check if done: "
                      << compareWithTargetTemperatureTolerance(frameTemperature, chillerSetTemperature)
                      << " frameTemperature: " << frameTemperature
                      << " chillerSetTemperature: " << chillerSetTemperature << std::endl;
            if (compareWithTargetTemperatureTolerance(frameTemperature, chillerSetTemperature) >= 0)
            {
                if (turnChillerOff_ && theControllerBoard_->isChillerOn(theChillerDevice_))
                    theControllerBoard_->turnOffChiller(theChillerDevice_);
                setCycleStatus(CYCLE_STOPPED_WITH_ERROR); // DONE!
            }
            else
            {
                if (!status.getChillerStatus())
                {
                    std::cout
                        << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tERROR: The chiller was OFF while we are in the CYCLE_ERROR cycle. "
                        << "Since the temperature is not at target, I am going to turn the chiller on to reach it!"
                        << std::endl;
                    theControllerBoard_->turnOnChiller(theChillerDevice_);
                }
            }
            break;
        case CYCLE_STOPPED_WITH_ERROR:
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSHOULD I THROW AN ERROR?" << std::endl;
            // throw std::runtime_error(errorMessage_.str());//Somehow I am not catching the exception and the thing get
            // super stuck!
            //setCycleStatus(CYCLE_ERROR);
            break;
        default:
            setCycleError(CYCLE_ERROR_WRONG_STATUS);
            setCycleStatus(CYCLE_ERROR);
            break;
        }
        //////////////////////////////////////////////////////////////////////////////////
        if (getCycleStatus() != CYCLE_STOPPED && getCycleStatus() != CYCLE_STOPPED_WITH_ERROR)
            std::this_thread::sleep_for(std::chrono::seconds(5));
    }
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone running" << std::endl;

    bool okFlag = false;
    //This is a special case because I have troubles reading the values. 
    //I am going to try to set stuff to go in safe mode but the controller might be down
    if(getCycleStatus() == CYCLE_STOPPED_WITH_ERROR && theCycleError_== CYCLE_ERROR_STATUS_READING)
    {
        try
        {
            theControllerBoard_->turnOffChiller(theChillerDevice_);
            theControllerBoard_->relayOff(theDryAirFluxDevice_);
            theDryAirFluxStatus_ = false;
            theControllerBoard_->relayOn(theDoorLockDevice_);
            theDoorLockStatus_ = true;
        }
        catch(const std::exception& e)
        {
            errorMessage_ << e.what();
        }
    }
    else
    {
        //Only if everything went well, the chiller is off 
        //(if the option in the configuration is set to turn it off) 
        //and the dewpoint is lower than the frame temperature
        //leave the dry air flux into low mode
        //else leave it in high dry air mode
        if(getCycleStatus() == CYCLE_STOPPED 
            && (!theControllerBoard_->isChillerOn(theChillerDevice_) 
            || (!turnChillerOff_ && theControllerBoard_->readChillerSetTemperature(theChillerDevice_) >=  SafetyRangeDefinition::safeTemperatureValue))
            && frameTemperature > (dewPointTemperature + SafetyRangeDefinition::dewPointSafetyMarginDefault)
            && !keepDryAirFlowHigh_
            )
        {
            theControllerBoard_->relayOn(theDryAirFluxDevice_);
            theDryAirFluxStatus_ = true;
        }
        else
        {
            theControllerBoard_->relayOff(theDryAirFluxDevice_);
            theDryAirFluxStatus_ = false;
        }

        //Unlocking the door
        if(unlockDoor_ && theDoorLockDevice_.isOn())
        {
            theControllerBoard_->relayOff(theDoorLockDevice_);
            theDoorLockStatus_ = false;
        }

        if(getCycleStatus() != CYCLE_STOPPED)
        {
            if(errorMessage_.str() == "No error.")
                errorMessage_.str("");
            errorMessage_ << "Last temperature cycle state should be stopped, while instead is " << translateCycleStatus(getCycleStatus()) << ". ";
        }
        if(turnChillerOff_ == theControllerBoard_->isChillerOn(theChillerDevice_))
        {
            if(errorMessage_.str() == "No error.")
                errorMessage_.str("");
            if(turnChillerOff_)
                errorMessage_ << "The chiller should be OFF while it is instead STILL ON. ";
            else
                errorMessage_ << "The chiller should be ON while it is instead it is OFF. ";
        
        }
        if(unlockDoor_ == theDoorLockDevice_.isOn())
        {
            if(errorMessage_.str() == "No error.")
                errorMessage_.str("");
            if(unlockDoor_)
                errorMessage_ << "The door should be UNLOCKED while it is STILL LOCKED. ";
            else
                errorMessage_ << "The door should be LOCKED while it is UNLOCKED. ";
        
        }
        if(keepDryAirFlowHigh_ == theDryAirFluxDevice_.isOn())
        {
            if(errorMessage_.str() == "No error.")
                errorMessage_.str("");
            if(keepDryAirFlowHigh_)
                errorMessage_ << "The dry air flux should be set to HIGH while it is on LOW. ";
            else
                errorMessage_ << "The dry air flux should be set to LOW while it is on HIGH. ";
        
        }
    }
    if(dewPointTemperature+SafetyRangeDefinition::dewPointSafetyMarginDefault >= frameTemperature)
    {
        if(errorMessage_.str() == "No error.")
            errorMessage_.str("");
        errorMessage_ << "The frame is colder than the dew point! THIS REQUIRES IMMEDIATE ACTION. Increase the nitrogen flow, increase the chiller set point, turn off the modules IMMEDIATELY.";
    }
    if(static_cast<int>(theTemperatureSequence_.getCurrentTemperature()*100) != static_cast<int>(chillerSetTemperature*100))
    {
        if(errorMessage_.str() == "No error.")
            errorMessage_.str("");
        errorMessage_ << "The last set temperature on the chiller does NOT correspond to the temperature that should be set when the system stops.";
    }
    if(errorMessage_.str() == "No error.")
    {
        okFlag = true;
    }

    std::string subject;
    if(okFlag)
        subject = "BurninBox cycle finished successfully. Run " + currentRun_;
    else
        subject = "BurninBox cycle finished with ERRORS, Run " + currentRun_;

    //Sendmail has a 100 chars limitation of chars not interrupted by \r\n. I am adding it to every line to be in control of the \n sequence
    std::stringstream message;
    message.str("");
            message
            << "<table class=\"email\">\r\n"
            << "  <tbody>\r\n"
            << "    <tr class=\"even\"><td>User</td><td class=\"green\">"<< userName_ << "</td></tr>\r\n"
            << "    <tr class=\"odd\"><td>Server</td><td class=\"green\">"<< computerName_ << "</td></tr>\r\n"
            << "    <tr class=\"even\"><td>BurninBox cycle completion</td>" << (okFlag ? "<td class=\"green\">OK" : "<td class=\"red\">ERROR") << "</td></tr>\r\n"
            << "    <tr class=\"odd\"><td>Error message</td>" << ((errorMessage_.str() == "No error.") ? "<td class=\"green\">" : "<td class=\"red\">") << errorMessage_.str() << "</td></tr>\r\n"
            << "    <tr class=\"even\"><td>Last temperature cycle state</td>" << ((getCycleStatus() == CYCLE_STOPPED) ? "<td class=\"green\">" : "<td class=\"red\">") << translateCycleStatus(getCycleStatus()) << "</td></tr>\r\n"
            << "    <tr class=\"odd\"><td>Chiller status</td>" << (theControllerBoard_->isChillerOn(theChillerDevice_) ? ((turnChillerOff_) ? "<td class=\"red\">On" : "<td class=\"yellow\">On") : "<td class=\"green\">Off") << "</td></tr>\r\n"
            << "    <tr class=\"even\"><td>Dry air status</td>" << (theDryAirFluxStatus_ ? ((keepDryAirFlowHigh_) ? "<td class=\"red\">Low" : "<td class=\"green\">Low") : ((keepDryAirFlowHigh_) ? "<td class=\"green\">High" : "<td class=\"yellow\">High")) << "</td></tr>\r\n"
            << "    <tr class=\"odd\"><td>Door lock status</td>" << (theDoorLockStatus_ ? ((unlockDoor_) ? "<td class=\"red\">Locked" : "<td class=\"green\">Locked") : ((unlockDoor_) ? "<td class=\"green\">Unlocked" : "<td class=\"red\">Unlocked")) << "</td></tr>\r\n"
            << "    <tr class=\"even\"><td>Last chiller set temperature</td>" << (static_cast<int>(theTemperatureSequence_.getCurrentTemperature()*100) == static_cast<int>(chillerSetTemperature*100) ? "<td class=\"green\">" : "<td class=\"red\">") << chillerSetTemperature  << " C" << "</td></tr>\r\n"
            << "    <tr class=\"odd\"><td>Last dew point measured</td>" << (dewPointTemperature+SafetyRangeDefinition::dewPointSafetyMarginDefault >= frameTemperature ? "<td class=\"red\">" : "<td class=\"green\">") << dewPointTemperature << " C" << "</td></tr>\r\n"
            << "    <tr class=\"even\"><td>Last frame temperature</td>" << (dewPointTemperature+SafetyRangeDefinition::dewPointSafetyMarginDefault >= frameTemperature ? "<td class=\"red\">" : "<td class=\"green\">") << frameTemperature  << " C" << "</td></tr>\r\n"
            << "  </tbody>\r\n"
            << "</table>\r\n"
            ;

    sendMail(mailList_, "no-reply-burninbox@fnal.gov", subject, message.str());

    //std::cout << message.str() << std::endl;
}

// Emanuele Aucone
//===========================================================================
void BurninBoxController::checkOutOfRangeTemperature(const BurninBoxStatus &status)
{
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
    //This method checks for possible out of range situation in case the values read out are outside the
    //safety ranges definitions.
    //This method does not take any action but throws an exception that is checked in the main loop.
    //In the main loop action is taken if the out of range exception persist for a N (right now = 5) number of times 
    //Check dew point out of range 
    if (status.getMinDewPointTemperature() < SafetyRangeDefinition::minDewPointValue ||
        status.getMaxDewPointTemperature() > SafetyRangeDefinition::maxDewPointValue)
    {
        std::stringstream error;
        error << "ERROR: The dew point is out of range! status.getMinDewPointTemperature(): "
              << status.getMinDewPointTemperature()
              << " < minDewPointValue: " << SafetyRangeDefinition::minDewPointValue
              << " || status.getMaxDewPointTemperature(): " << status.getMinDewPointTemperature()
              << " > maxDewPointValue: " << SafetyRangeDefinition::maxDewPointValue
              << " Setting chiller temperature to the safe value: " << SafetyRangeDefinition::safeTemperatureValue
              << " ...and thowing an exception.";
        //theControllerBoard_->setChillerTemperature(theChillerDevice_, SafetyRangeDefinition::safeTemperatureValue);
        throw std::runtime_error(error.str());
        //return;
    }

    //Check ambient temperature out of range 
    if (status.getAverageAmbientTemperature() != atof(OFF_VALUE) &&
        (status.getMinAmbientTemperature() < SafetyRangeDefinition::minTemperatureValue ||
         status.getMaxAmbientTemperature() > SafetyRangeDefinition::maxTemperatureValue))
    {
        //theControllerBoard_->setChillerTemperature(theChillerDevice_, SafetyRangeDefinition::safeTemperatureValue);
        throw std::runtime_error("ERROR: The ambient temperature is out of range!");
        //return;
    }

    //Check dew point out of range 
    if (status.getFrameTemperature() < SafetyRangeDefinition::minTemperatureValue ||
        status.getFrameTemperature() > SafetyRangeDefinition::maxTemperatureValue)
    {
        //theControllerBoard_->setChillerTemperature(theChillerDevice_, SafetyRangeDefinition::safeTemperatureValue);
        throw std::runtime_error("ERROR: The frame temperature is out of range!");
        //return;
    }
}

//===========================================================================
BurninBoxStatus BurninBoxController::updateStatus(unsigned int updateInterval, std::string error)
{
    updatingStatus_ = true;
//    std::lock_guard<std::mutex> lock(statusMutex_);

    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tUpdating status!" << std::endl;
    struct timeval currentTime;
    gettimeofday(&currentTime, nullptr);
    if (currentTime.tv_sec - theBurninBoxStatus_.getTime() < updateInterval) // Only update if updateInterval seconds have passed
        return theBurninBoxStatus_;

    theBurninBoxStatus_.setTime();
    if (theAmbientDevices_.find(NameDefinition::Ambient1) != theAmbientDevices_.end() &&
        theAmbientDevices_.find(NameDefinition::Ambient1)->second.isOn())
        theBurninBoxStatus_.setAmbient1Temperature(
            theControllerBoard_->readTemperature(theAmbientDevices_.find(NameDefinition::Ambient1)->second));
    if (theAmbientDevices_.find(NameDefinition::Ambient2) != theAmbientDevices_.end() &&
        theAmbientDevices_.find(NameDefinition::Ambient2)->second.isOn())
        theBurninBoxStatus_.setAmbient2Temperature(
            theControllerBoard_->readTemperature(theAmbientDevices_.find(NameDefinition::Ambient2)->second));
    theBurninBoxStatus_.setFrameTemperature(theControllerBoard_->readTemperature(theFrameDevice_));
    theBurninBoxStatus_.setFrameLeftTemperature(theControllerBoard_->readTemperature(theFrameLeftDevice_));
    theBurninBoxStatus_.setFrameRightTemperature(theControllerBoard_->readTemperature(theFrameRightDevice_));
    theBurninBoxStatus_.setChillerSetTemperature(theControllerBoard_->readChillerSetTemperature(theChillerDevice_));
    if (theDewPointDevices_.find(NameDefinition::DewPoint1) != theDewPointDevices_.end() &&
        theDewPointDevices_.find(NameDefinition::DewPoint1)->second.isOn())
        theBurninBoxStatus_.setDewPoint1Temperature(
            theControllerBoard_->readDewPoint(theDewPointDevices_.find(NameDefinition::DewPoint1)->second));
    if (theDewPointDevices_.find(NameDefinition::DewPoint2) != theDewPointDevices_.end() &&
        theDewPointDevices_.find(NameDefinition::DewPoint2)->second.isOn())
        theBurninBoxStatus_.setDewPoint2Temperature(
            theControllerBoard_->readDewPoint(theDewPointDevices_.find(NameDefinition::DewPoint2)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Left) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Left)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier1LeftTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Left)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Left) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Left)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier2LeftTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Left)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Left) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Left)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier3LeftTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Left)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Left) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Left)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier4LeftTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Left)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Left) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Left)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier5LeftTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Left)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Right) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Right)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier1RightTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Right)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Right) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Right)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier2RightTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Right)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Right) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Right)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier3RightTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Right)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Right) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Right)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier4RightTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Right)->second));
    if (theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Right) != theModuleCarrierDevices_.end() &&
        theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Right)->second.isOn())
        theBurninBoxStatus_.setModuleCarrier5RightTemperature(
            theControllerBoard_->readTemperature(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Right)->second));

    theBurninBoxStatus_.setChillerStatus(theControllerBoard_->isChillerOn(theChillerDevice_));
    theBurninBoxStatus_.setCycleStatus  (static_cast<int>(theCycleStatus_));
    theBurninBoxStatus_.setDoorLockRelayStatus(theDoorLockStatus_);
    theBurninBoxStatus_.setDryAirFluxRelayStatus(theDryAirFluxStatus_);
    theBurninBoxStatus_.setErrorMessage(error);
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDone updating status!" << std::endl;
    updatingStatus_ = false;
    return theBurninBoxStatus_;
}
//===========================================================================
BurninBoxStatus BurninBoxController::getStatus(void)
{
//    return updateStatus(0);
    return theBurninBoxStatus_;
}

//===========================================================================
// Ideally is the target temperature but can't be lower than dewpoint + safetydewppoint
float BurninBoxController::safeChillerSetTemperature(float chillerSetTemperature, const BurninBoxStatus &status)
{
    return (chillerSetTemperature >
            (status.getMaxDewPointTemperature() + SafetyRangeDefinition::dewPointSafetyMarginDefault))
               ? chillerSetTemperature
               : (status.getMaxDewPointTemperature() + SafetyRangeDefinition::dewPointSafetyMarginDefault);
}

//===========================================================================
float BurninBoxController::compareWithTargetTemperatureTolerance(
    float temperature,
    float target) //>0 temperature>target; <0 temperature<target; =0 temperature=target
{
    if (temperature > (target - SafetyRangeDefinition::targetTemperatureLowToleranceValue) &&
        temperature < (target + SafetyRangeDefinition::targetTemperatureHighToleranceValue))
        return 0;
    else if (temperature >= (target + SafetyRangeDefinition::targetTemperatureHighToleranceValue))
        return temperature - target; //>0
    else if (temperature <= (target - SafetyRangeDefinition::targetTemperatureLowToleranceValue))
        return temperature - target; //<0
    else
        throw std::runtime_error(
            "ERROR: This target temperature condition is impossible. You must fix the code or something else...");
}

//===========================================================================
std::string BurninBoxController::setTargetTemperature(std::string name)
{
    //std::lock_guard<std::mutex> lock(statusMutex_);
    std::cout << "[" << __LINE__ << "] "<< __PRETTY_FUNCTION__ << "\tSetting temperature: " << name << std::endl;
    const int millisecondsWait = 500;
    const int millisecondsTimeout = 1000*60*5;//5 minutes
    int timer = 0;
    try
    {
        goToStartAknowledgment_ = false;
        while(goToStartAknowledgment_ == false || theTemperatureSequence_.getCurrentName() != name)
        {
            if(theTemperatureSequence_.getCurrentName() != name)
                theTemperatureSequence_.gotoTemperature(name);
            if(goToStartAknowledgment_ == false)
                setCycleStatus(CYCLE_GO_TO_START);
            std::cout << "Changing temperature to " << name 
                << ". The cycle is now at temperature: " << theTemperatureSequence_.getCurrentName()
                << " and waiting until cycle is at " <<  translateCycleStatus(CYCLE_START)
                << " Right now: " << translateCycleStatus(getCycleStatus()) << std::endl;
            
            if(getStateMachineStatus() != RUNNING 
            && theTemperatureSequence_.getCurrentName() == name 
            && getCycleStatus() == CYCLE_GO_TO_START) 
            {
                break;
            }
            std::this_thread::sleep_for (std::chrono::milliseconds(millisecondsWait));
            timer += millisecondsWait;
            if(timer >= millisecondsTimeout)
            {
                throw std::runtime_error("ERROR: Timeout reached while trying to change temperature!");
            }
        }
        std::cout << "[" << __LINE__ << "] "<< __PRETTY_FUNCTION__ << "\tDone Setting temperature: " << name << std::endl;
        return "TemperatureSet";
    }
    catch(const std::exception& e)
    {
        std::cout << "[" << __LINE__ << "] "<< __PRETTY_FUNCTION__ << "\tI have an error setting " << name << std::endl;
        errorMessage_ << e.what();
        return errorMessage_.str();
    }
}

//===========================================================================
std::string BurninBoxController::addTemperature(std::string name, double temperature, unsigned time, unsigned sequence)
{
    theTemperatureSequence_.setTemperature(name, temperature, time, sequence);
    return "TemperatureAdded";
}

//===========================================================================
void BurninBoxController::setStateMachineStatus(BurninBoxController::StateMachineStatus status)
{
    // Thread::mutex_.lock();
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSetting State Machine Status to: " << status << std::endl;
    theStateMachineStatus_ = status;
    // Thread::mutex_.unlock();
}

//===========================================================================
BurninBoxController::StateMachineStatus BurninBoxController::getStateMachineStatus()
{
    // Thread::mutex_.lock();
    // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tGetting State Machine Status: " << theStateMachineStatus_ << std::endl;
    return theStateMachineStatus_;
    // Thread::mutex_.unlock();
}

// Emanuele Aucone
//===========================================================================
void BurninBoxController::setCycleStatus(Cycle cycle)
{
    // Thread::mutex_.lock();
    //std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
    gettimeofday(&beginCycleTime_, nullptr);//Reset cycle timer
    thePreviousCycleStatus_ = theCycleStatus_;
    theCycleStatus_ = cycle;
    // Thread::mutex_.unlock();
}

//===========================================================================
void BurninBoxController::setCycleError(CycleErrorCode code)
{
    // Thread::mutex_.lock();
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
    theCycleError_ = code;

    switch (code)
    {
    case NO_CYCLE_ERROR:
        theCycleErrorMessage_ = "No error.";
        break;
    case CYCLE_ERROR_WRONG_STATUS:
        theCycleErrorMessage_ = "ERROR: Unexpected status.";
        break;
    case CYCLE_ERROR_WRONG_TEMPERATURE_INDEX:
        theCycleErrorMessage_ = "ERROR: The target index is greater than the target temperatures vector.";
        break;
    case CYCLE_ERROR_STATUS_READING:
        theCycleErrorMessage_ = "ERROR: Error reading the status of the sensor values.";
        break;
    case CYCLE_ERROR_OUT_OF_RANGE_READINGS:
        theCycleErrorMessage_ = "ERROR: Error reading the sensor values.";
        break;
    case CYCLE_ERROR_TIMEOUT:
        theCycleErrorMessage_ = "ERROR: Timeout error.";
        break;

    default:
        theCycleErrorMessage_ = "No error.";
        break;
    }
    // Thread::mutex_.unlock();
}

//===========================================================================
void BurninBoxController::sendMail(const std::string &toList,
                                   const std::string &from,
                                   const std::string &subject,
                                   const std::string &message)
{
    std::cout << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tSending a cool email!" << std::endl; 
    
    std::stringstream messageStyle;
        messageStyle 
            << "<style>"
            << "* {font: bold 18px Arial, sans-serif;}"
            << "table.email {"
            << "    border: solid 1px #DDEEEE;"
            << "    border-collapse: collapse;"
            << "    border-spacing: 0;"
            //<< "    font: bold 13px Arial, sans-serif;"
            << "}"
            << "table.email thead th {"
            << "    background-color: #DDEFEF;"
            << "    border: solid 1px #DDEEEE;"
            << "    color: #336B6B;"
            << "    padding: 10px;"
            << "    text-align: left;"
            << "    text-shadow: 1px 1px 1px #fff;"
            << "}"
            << "table.email tbody td {"
            << "    border: solid 1px #DDEEEE;"
            << "    color: #333;"
            << "    padding: 10px;"
            << "    text-shadow: 1px 1px 1px #fff;"
            << "}"
            << "tr.even td {background-color:#ffffff00}"
            << "tr.odd td {background-color:#f1f1f1}"
            << "tr.even td.red, tr.odd td.red {background-color:#ff0000}"
            << "tr.even td.green, tr.odd td.green {background-color:#00ff00}"
            << "tr.even td.yellow, tr.odd td.yellow {background-color:#ffff00}"
            << "</style>";

    FILE *mailpipe = popen("/usr/sbin/sendmail -t", "w");
    if (mailpipe != nullptr)
    {
        fprintf(mailpipe, "To: %s\n", toList.c_str());        
        fprintf(mailpipe, "From: %s\n", from.c_str());
        fprintf(mailpipe, "Subject: %s\n", subject.c_str());
        fprintf(mailpipe, "Content-Type: text/html\n");
        fprintf(mailpipe, "MIME-Version: 1.0\n");
        fprintf(mailpipe, "\n");
        fwrite(messageStyle.str().c_str(), 1, messageStyle.str().length(), mailpipe);
        fwrite(message.c_str(), 1, message.length(), mailpipe);
        fwrite(".\n", 1, 2, mailpipe);
        pclose(mailpipe);
        // std::cout << "Length: " << messageStyle.str().length() << std::endl << messageStyle.str().c_str() << std::endl;
        // std::cout << "Length: " << message.length() << std::endl << message.c_str() << std::endl;
    }
    else
    {
        std::cout << "/usr/sbin/sendmail didn't work. Make sure it is installed on your linux distribution." << std::endl
                  << "> sudo yum install -y mailx" << std::endl;
    }
}

//========================================================================================================================
// void BurninBoxController::printTime(const std::string& message)
// {
//     auto now = std::chrono::system_clock::now();
//     std::time_t current_time = std::chrono::system_clock::to_time_t(now);
//     std::tm local_time = *std::localtime(&current_time);

//     // Custom format: YYYY-MM-DD HH:MM:SS
//     std::cout << "Current time: " 
//               << std::put_time(&local_time, "%Y-%m-%d %H:%M:%S") 
// 			  << ".\t" << message
//               << std::endl;
// }

//===========================================================================
//===========================================================================
//===========================================================================
//===========================================================================
//===========================================================================
//===========================================================================
//===========================================================================
//===========================================================================
//===========================================================================
// void BurninBoxController::switchThermalRelays(bool on)
// {
//     std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     if(on)
//     {
//         // LORENZOUPLEGGER
//         // if(theChillerRelay_     ->isOff()) theChillerRelay_     ->on();
//         // if(theFrameLeftRelay_ ->isOff()) theFrameLeftRelay_ ->on();
//         // if(theFrameRightRelay_->isOff()) theFrameRightRelay_->on();
//         // if(theWarmupRelay_      ->isOff()) theWarmupRelay_      ->on();
//     }
//     else
//     {
//         // LORENZOUPLEGGER
//         // if(theChillerRelay_     ->isOn()) theChillerRelay_     ->off();
//         // if(theFrameLeftRelay_ ->isOn()) theFrameLeftRelay_ ->off();
//         // if(theFrameRightRelay_->isOn()) theFrameRightRelay_->off();
//         // if(theWarmupRelay_      ->isOn()) theWarmupRelay_      ->off();
//     }
// }

//===========================================================================
// float BurninBoxController::readAmbientTemperature(void)
// {
//     float ambientTemperature = 25;
//     if(theAmbientDevices_.size() == 0)
//     {
//         std::cout << "WARNING: There are no configured ambient temperature sensors. I will return a value of "
//                   << ambientTemperature << "C but it is NOT A READING!" << std::endl;
//         return ambientTemperature;
//     }
//     for(auto temperature: theAmbientDevices_)
//     {
//         ambientTemperature += theControllerBoard_->readTemperature(temperature.second);
//         // ambientTemperature += temperature.second->readTemperature();
//     }
//     return ambientTemperature / theAmbientDevices_.size();
// }

// Emanuele Aucone
//===========================================================================
// float BurninBoxController::readFrameLeftTemperature(void)
// {
//     //    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     return theControllerBoard_->readTemperature(theFrameLeftDevice_);
//     // return theFrameLeftDevice_->readTemperature();
// }

// Emanuele Aucone
//===========================================================================
// float BurninBoxController::readFrameRightTemperature(void)
// {
//     //    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     return theControllerBoard_->readTemperature(theFrameRightDevice_);
//     // eturn theFrameRightDevice_->readTemperature();
// }

// // Emanuele Aucone
// //===========================================================================
// float BurninBoxController::readFrameTemperature(void)
// {
//     //    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     return theControllerBoard_->readTemperature(theFrameDevice_);
//     // return (theFrameLeftDevice_->readTemperature() + theFrameRightDevice_->readTemperature()) / 2;
// }

// // Emanuele Aucone
// //===========================================================================
// float BurninBoxController::readDewPointTemperature(void)
// {
//     //    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     int numberOfrecursiveCycle = 5;
//     if(dewPointRecursiveCounter_ <= numberOfrecursiveCycle)
//     {
//         float              ambientDewPoint = 0;
//         std::vector<float> dewPointMeasurements;

//         unsigned int numberOfMeasurements = 5;
//         unsigned int numberOfSigmas       = 5;

//         float average = 0;
//         for(unsigned int i = 0; i < numberOfMeasurements; i++)
//         {
//             for(auto dewPoint: theDewPointDevices_)
//             {
//                 ambientDewPoint += theControllerBoard_->readDewPoint(dewPoint.second);
//                 // ambientDewPoint += dewPoint.second->readDewPoint();
//             }
//             dewPointMeasurements.push_back(ambientDewPoint / theDewPointDevices_.size());
//             average += dewPointMeasurements[i];
//             ambientDewPoint = 0;
//         }
//         average /= numberOfMeasurements;

//         float sigma = 0;
//         for(auto dewPoint: dewPointMeasurements)
//         {
//             sigma += pow(dewPoint - average, 2.0);
//         }
//         sigma = sqrt(sigma) / numberOfMeasurements;

//         for(std::vector<float>::iterator it = dewPointMeasurements.begin(); it != dewPointMeasurements.end();)
//         {
//             if((*it) < average - numberOfSigmas * sigma || (*it) > average + numberOfSigmas * sigma)
//                 dewPointMeasurements.erase(it);
//             else
//                 ++it;
//         }

//         if(dewPointMeasurements.size() == numberOfMeasurements)
//         {
//             dewPointRecursiveCounter_ = 0;
//             return average;
//         }
//         else if(dewPointMeasurements.size() == 0)
//         {
//             // std::cout << "Dew point size: " << dewPointMeasurements.size() << std::endl;
//             ++dewPointRecursiveCounter_;
//             return readDewPointTemperature();
//         }
//         else
//         {
//             ambientDewPoint = 0;
//             for(auto dewPoint: dewPointMeasurements)
//             {
//                 ambientDewPoint += dewPoint;
//             }
//             dewPointRecursiveCounter_ = 0;
//             return ambientDewPoint / dewPointMeasurements.size();
//         }
//     }
//     else
//     {
//         throw std::runtime_error("ERROR: DewPoint sensor might be damaged!");
//     }
// }

// // Emanuele Aucone
// //===========================================================================
// // float BurninBoxController::readHumidity(void)
// // {
// // 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
// // 	//humidity sensors not connected yet, then the function will be modyfied
// // 	float humidity = 0;
// // 	for(auto dewPoint : theDewPointDevices_)
// // 		humidity += dewPoint.second->calculateHumidity(readAmbientTemperature());
// // 	return humidity/theDewPointDevices_.size();
// // }

// // Emanuele Auconetarget
// //===========================================================================
// // bool BurninBoxController::readFrameLeftStatus(void)
// // {
// // 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
// // 	return false;
// // 	//return theFrameLeftRelay_->isOn();
// // }

// // Emanuele Aucone
// //===========================================================================
// // bool BurninBoxController::readFrameRightStatus(void)
// // {
// // 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
// // 	return false;
// // 	//return theFrameRightRelay_->isOn();
// // }

// // Emanuele Aucone
// //===========================================================================
// // bool BurninBoxController::readChillerStatus(void)
// // {
// // 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
// // 	return false;
// // 	//return theChillerRelay_->isOn();
// // }

// // Emanuele Aucone
// //===========================================================================
// // bool BurninBoxController::readWarmupRelayStatus(void)
// // {
// // 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
// // 	return false;
// // 	//return theWarmupRelay_->isOn();
// // }

// // Emanuele Aucone
// //===========================================================================
// // bool BurninBoxController::readDryAirFluxRelayStatus(void)
// // {
// // 	std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
// // 	return false;
// // //	return theLowHighFluxDryAirRelay_->isOn();
// // }

// //===========================================================================
// float BurninBoxController::readModuleCarrier1LeftTemperature(void)
// {
//     // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Left) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier1A is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier1Left]);
//     // return theModuleCarrierDevices_[NameDefinition::ModuleCarrier1Left]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier2LeftTemperature(void)
// {
//     // std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Left) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier2A is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier2Left]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier2Left]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier3LeftTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Left) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier3A is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier3Left]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier3Left]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier4LeftTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Left) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier4A is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier4Left]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier4Left]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier5LeftTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Left) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier5A is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier5Left]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier5Left]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier1RightTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier1Right) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier1B is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier1Right]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier1Right]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier2RightTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier2Right) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDevice: ModuleCarrier2B is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier2Right]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier2Right]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier3RightTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier3Right) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Device: ModuleCarrier3B is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier3Right]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier3Right]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier4RightTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier4Right) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Device: ModuleCarrier4B is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier4Right]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier4Right]->readTemperature();
// }

// //===========================================================================
// float BurninBoxController::readModuleCarrier5RightTemperature(void)
// {
//     if(theModuleCarrierDevices_.find(NameDefinition::ModuleCarrier5Right) == theModuleCarrierDevices_.end())
//     {
//         std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Device: ModuleCarrier5B is off or not configured!" << std::endl;
//         return -999;
//     }
//     return theControllerBoard_->readTemperature(theModuleCarrierDevices_[NameDefinition::ModuleCarrier5Right]);
//     //	return theModuleCarrierDevices_[NameDefinition::ModuleCarrier5Right]->readTemperature();
// }

// // Emanuele Aucone
// //===========================================================================
// /*
// void BurninBoxController::keepTemperature(float setTemperature, float keepTime)//keepTime in seconds
// {
//     currentAmbientTemperature_ = theDeviceContainer_.readTemperatures();
//     currentDewPoint_    = theDeviceContainer_.readDewPoints();
//     struct timeval currentTime;

//     //Activate Relays to reach the setTemperature (smaller tolerance)
//     while(relayTemperatureControl(currentAmbientTemperature_, setTemperature-0.3, setTemperature+0.3) != 0)
//     {
//         currentAmbientTemperature_ = theDeviceContainer_.readTemperatures();
//         theDeviceContainer_.printTemperatures();
//         currentDewPoint_ = theDeviceContainer_.readDewPoints();
//         theDeviceContainer_.printDewPoints();
//         if(currentDewPoint_ >= currentAmbientTemperature_)
//         {
//             if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();
//         }
//         else
//             if(theLowHighFluxDryAirRelay_->isOff()) theLowHighFluxDryAirRelay_->on();
//         gettimeofday(&currentTime, nullptr);
//         logFile_ << (int) currentTime.tv_sec << ";" << currentAmbientTemperature_ << ";" << currentDewPoint_ << ";"
//         <<
// std::endl; sleep(1);
//     }

//     gettimeofday(&currentTime, nullptr);	//Use the gettimeofday function to get the time right now and store it in
// the struct double startTime = currentTime.tv_sec;	//Save the starting time in seconds

//     //Activate Relays to keep the setTemperature for the established keepTime
//     while(currentTime.tv_sec < startTime + keepTime)
//     {
//         currentAmbientTemperature_ = theDeviceContainer_.readTemperatures();
//         theDeviceContainer_.printTemperatures();
//         currentDewPoint_ = theDeviceContainer_.readDewPoints();
//         theDeviceContainer_.printDewPoints();
//         if(currentDewPoint_ >= currentAmbientTemperature_)
//         {
//             if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();
//         }
//         else
//             if(theLowHighFluxDryAirRelay_->isOff()) theLowHighFluxDryAirRelay_->on();
//         gettimeofday(&currentTime, nullptr);
//         logFile_ << (int) currentTime.tv_sec << ";" << currentAmbientTemperature_ << ";" << currentDewPoint_ << ";"
//         <<
// std::endl; relayTemperatureControl(currentAmbientTemperature_, setTemperature-0.2, setTemperature+0.2); sleep(1);
//     }
// }

// //Emanuele Aucone
// //===========================================================================
// void BurninBoxController::controlLoop(void)
// {
//     logFile_.open("log1.txt");
//     double testTime = 100;	//seconds
//     int setTemperature1 = 10;			//first loop temperature set point
//     int setTemperature2 = 15;			//second loop temperature set point
//     int setTemperature3 = 20;			//third loop temperature set point

//     if(theChillerRelay_->isOff()) theChillerRelay_->on();

//     keepTemperature(setTemperature1, testTime);
//     keepTemperature(setTemperature2, testTime);
//     keepTemperature(setTemperature3, 0);

//     switchThermalRelays(false);

//     logFile_.close();

//     std::cout << "Cycle Done!" << std::endl;
// }*/

// // Emanuele Aucone
// //===========================================================================
// int BurninBoxController::relayTemperatureControl(float currentTemperature, float min, float max)
// {
//     std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     /*
//         if(getCycleStatus() == CYCLE_START || getCycleStatus() == CYCLE_TARGET)
//         {
//             if(currentTemperature >= max)
//             {
//                 // if(theChillerRelay_     ->isOff()) theChillerRelay_     ->on();
//                 // if(theFrameLeftRelay_ ->isOff()) theFrameLeftRelay_ ->on();
//                 // if(theFrameRightRelay_->isOff()) theFrameRightRelay_->on();
//                 // if(theWarmupRelay_      ->isOn())  theWarmupRelay_      ->off();
//                 setCycleStatus(CYCLE_COOLING);
//                 // currentTargetTemperature_ = targetTemperature_ - setTemperatureTolerance_;
//                 return 1;
//             }
//             else if(currentTemperature <= min)
//             {
//                 // if(theChillerRelay_     ->isOn())  theChillerRelay_     ->off();
//                 // if(theFrameLeftRelay_ ->isOn())  theFrameLeftRelay_ ->off();
//                 // if(theFrameRightRelay_->isOn())  theFrameRightRelay_->off();
//                 // if(theWarmupRelay_      ->isOff()) theWarmupRelay_      ->on();
//                 setCycleStatus(WARMING);
//                 // currentTargetTemperature_ = targetTemperature_ + setTemperatureTolerance_;
//                 return -1;
//             }
//             else if(currentTemperature > min && currentTemperature < max)
//             {
//                 setCycleStatus(TARGET);
//                 return 0; // still on target
//             }
//             else
//             {
//                 std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "I SHOULD NEVER EVER BE HERE!...crashing now" << std::endl;
//                 exit(0);
//             }
//         }
//         else if(getCycleStatus() == COOLING)
//         {
//             if(currentTemperature <= targetTemperature_)
//             {
//                 // if(theChillerRelay_     ->isOff()) theChillerRelay_     ->on();
//                 // if(theFrameLeftRelay_ ->isOn()) theFrameLeftRelay_ ->off();
//                 // if(theFrameRightRelay_->isOn()) theFrameRightRelay_->off();
//                 // if(theWarmupRelay_      ->isOn()) theWarmupRelay_      ->off();
//                 setCycleStatus(TARGET);
//                 // currentTargetTemperature_ = targetTemperature_ + setTemperatureTolerance_;
//                 return 0;
//             }
//             else // still going down
//             {
//                 // if(theFrameLeftRelay_ ->isOff()) theFrameLeftRelay_ ->on();
//                 // if(theFrameRightRelay_->isOff()) theFrameRightRelay_->on();
//                 // if(theWarmupRelay_      ->isOn())  theWarmupRelay_      ->off();
//                 // setCycleStatus(COOLING);
//                 return 1;
//             }
//         }
//         else if(getCycleStatus() == WARMING)
//         {
//             if(currentTemperature >= targetTemperature_)
//             {
//                 // if(theChillerRelay_     ->isOn())  theChillerRelay_     ->off();
//                 // if(theFrameLeftRelay_ ->isOn()) theFrameLeftRelay_ ->off();
//                 // if(theFrameRightRelay_->isOn()) theFrameRightRelay_->off();
//                 // if(theWarmupRelay_      ->isOn()) theWarmupRelay_      ->off();
//                 setCycleStatus(TARGET);
//                 // currentTargetTemperature_ = targetTemperature_ + setTemperatureTolerance_;
//                 return 0;
//             }
//             else // still going up
//             {
//                 // if(theFrameLeftRelay_ ->isOn())  theFrameLeftRelay_ ->off();
//                 // if(theFrameRightRelay_->isOn())  theFrameRightRelay_->off();
//                 // if(theWarmupRelay_      ->isOff()) theWarmupRelay_      ->on();
//                 // setCycleStatus(WARMING);
//                 return -1;
//             }
//         }
//         else
//         {
//             std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "I SHOULD NEVER EVER BE HERE!...crashing now" << std::endl;
//             exit(0);
//         }
//         */
//     return 1;
// }

// //===========================================================================
// void BurninBoxController::threadMain(void)
// {
//     std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << std::endl;
//     // while(!Thread::stopThread_)
//     // {
//     //     std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "SM status: " << getStateMachineStatus() << std::endl;
//     //     if(getStateMachineStatus() == STARTING)
//     //         start();
//     //     else if(getStateMachineStatus() == RUNNING)
//     //         running();
//     //     else if(getStateMachineStatus() == STOPPING)
//     //         stop();
//     //     else if(getStateMachineStatus() == ERROR)
//     //         error();
//     //     else
//     //         error();
//     // }
// }
