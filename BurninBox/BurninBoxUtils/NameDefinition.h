/*
 * NameDefinition.h
 *
 *  Created on: Sep 18, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_NameDefinition_h_
#define _ots_NameDefinition_h_

namespace ots
{
class NameDefinition
{
  public:
    NameDefinition() { ; }
    virtual ~NameDefinition() { ; }
    static constexpr char Ambient1[]            = "Ambient1";
    static constexpr char Ambient2[]            = "Ambient2";
    static constexpr char Frame[]               = "Frame";
    static constexpr char FrameLeft[]           = "FrameLeft";
    static constexpr char FrameRight[]          = "FrameRight";
    static constexpr char ChillerSet[]          = "ChillerSet";
    static constexpr char DewPoint1[]           = "DewPoint1";
    static constexpr char DewPoint2[]           = "DewPoint2";
    static constexpr char ModuleCarrier1Left[]  = "ModuleCarrier1Left";
    static constexpr char ModuleCarrier2Left[]  = "ModuleCarrier2Left";
    static constexpr char ModuleCarrier3Left[]  = "ModuleCarrier3Left";
    static constexpr char ModuleCarrier4Left[]  = "ModuleCarrier4Left";
    static constexpr char ModuleCarrier5Left[]  = "ModuleCarrier5Left";
    static constexpr char ModuleCarrier1Right[] = "ModuleCarrier1Right";
    static constexpr char ModuleCarrier2Right[] = "ModuleCarrier2Right";
    static constexpr char ModuleCarrier3Right[] = "ModuleCarrier3Right";
    static constexpr char ModuleCarrier4Right[] = "ModuleCarrier4Right";
    static constexpr char ModuleCarrier5Right[] = "ModuleCarrier5Right";
    static constexpr char DoorLockStatus[]      = "DoorLockStatus" ;
    static constexpr char DryAirFluxStatus[]    = "DryAirFluxStatus" ;
    static constexpr char CycleStatus[]         = "CycleStatus" ;
    static constexpr char ChillerStatus[]       = "ChillerStatus" ;
    static constexpr char ErrorMessage[]        = "ErrorMessage" ;
};

} // namespace ots
#endif /* _ots_NameDefinition_h_ */
