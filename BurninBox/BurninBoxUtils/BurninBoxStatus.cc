/*
 * BurninBoxStatus.cc
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/BurninBoxStatus.h"
#include "BurninBox/BurninBoxUtils/NameDefinition.h"
#else
#include "BurninBoxStatus.h"
#include "NameDefinition.h"
#endif

#include <vector>
#include <iostream>
#include <sstream>
#include <time.h>

using namespace ots;

//========================================================================================================================
BurninBoxStatus::BurninBoxStatus() : JsonConverter("STATUS")
{
    ambientNames_.push_back(NameDefinition::Ambient1);
    ambientNames_.push_back(NameDefinition::Ambient2);

    dewPointNames_.push_back(NameDefinition::DewPoint1);
    dewPointNames_.push_back(NameDefinition::DewPoint2);

    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier1Left);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier2Left);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier3Left);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier4Left);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier5Left);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier1Right);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier2Right);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier3Right);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier4Right);
    moduleCarrierNames_.push_back(NameDefinition::ModuleCarrier5Right);

    JsonConverter::variables_["Time"]                              = "0";
    JsonConverter::variables_[NameDefinition::Ambient1]            = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::Ambient2]            = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::Frame]               = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::FrameLeft]           = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::FrameRight]          = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ChillerSet]          = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::DewPoint1]           = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::DewPoint2]           = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier1Left]  = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier2Left]  = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier3Left]  = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier4Left]  = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier5Left]  = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier1Right] = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier2Right] = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier3Right] = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier4Right] = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::ModuleCarrier5Right] = OFF_VALUE;
    JsonConverter::variables_[NameDefinition::DoorLockStatus]      = "0";
    JsonConverter::variables_[NameDefinition::DryAirFluxStatus]    = "0";
    JsonConverter::variables_[NameDefinition::CycleStatus]         = "0";
    JsonConverter::variables_[NameDefinition::ChillerStatus]       = "0";
    JsonConverter::variables_[NameDefinition::ErrorMessage]        = "";
}

//========================================================================================================================
BurninBoxStatus::~BurninBoxStatus() {}

//========================================================================================================================
void BurninBoxStatus::setTime(void) 
{ 
	struct timeval currentTime;
	gettimeofday(&currentTime, nullptr);
    JsonConverter::variables_["Time"] = std::to_string(currentTime.tv_sec); 
}

//========================================================================================================================
void BurninBoxStatus::setAmbient1Temperature(float value)
{
    JsonConverter::variables_[NameDefinition::Ambient1] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setAmbient2Temperature(float value)
{
    JsonConverter::variables_[NameDefinition::Ambient2] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setFrameTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::Frame] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setFrameLeftTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::FrameLeft] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setFrameRightTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::FrameRight] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setChillerSetTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ChillerSet] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setDewPoint1Temperature(float value)
{
    JsonConverter::variables_[NameDefinition::DewPoint1] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setDewPoint2Temperature(float value)
{
    JsonConverter::variables_[NameDefinition::DewPoint2] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setDoorLockRelayStatus(bool value)
{
    JsonConverter::variables_[NameDefinition::DoorLockStatus] = std::to_string(int(value));
}

//========================================================================================================================
void BurninBoxStatus::setDryAirFluxRelayStatus(bool value)
{
    JsonConverter::variables_[NameDefinition::DryAirFluxStatus] = std::to_string(int(value));
}

//========================================================================================================================
void BurninBoxStatus::setChillerStatus(bool value)
{
    JsonConverter::variables_[NameDefinition::ChillerStatus] = std::to_string(int(value));
}

//========================================================================================================================
void BurninBoxStatus::setCycleStatus(int value)
{
    JsonConverter::variables_[NameDefinition::CycleStatus] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setErrorMessage(std::string value)
{
    JsonConverter::variables_[NameDefinition::ErrorMessage] = value;
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier1LeftTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier1Left] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier2LeftTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier2Left] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier3LeftTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier3Left] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier4LeftTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier4Left] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier5LeftTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier5Left] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier1RightTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier1Right] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier2RightTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier2Right] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier3RightTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier3Right] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier4RightTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier4Right] = std::to_string(value);
}

//========================================================================================================================
void BurninBoxStatus::setModuleCarrier5RightTemperature(float value)
{
    JsonConverter::variables_[NameDefinition::ModuleCarrier5Right] = std::to_string(value);
}

//========================================================================================================================
time_t BurninBoxStatus::getTime(void) const 
{ 
    return std::stoll(variables_.find("Time")->second.c_str()); 
}

//========================================================================================================================
float BurninBoxStatus::getAmbient1Temperature(void) const
{
    return atof(variables_.find(NameDefinition::Ambient1)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getAmbient2Temperature(void) const
{
    return atof(variables_.find(NameDefinition::Ambient2)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getFrameTemperature(void) const
{
    return atof(variables_.find(NameDefinition::Frame)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getFrameLeftTemperature(void) const
{
    return atof(variables_.find(NameDefinition::FrameLeft)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getFrameRightTemperature(void) const
{
    return atof(variables_.find(NameDefinition::FrameRight)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getChillerSetTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ChillerSet)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getDewPoint1Temperature(void) const
{
    return atof(variables_.find(NameDefinition::DewPoint1)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getDewPoint2Temperature(void) const
{
    return atof(variables_.find(NameDefinition::DewPoint2)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier1LeftTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier1Left)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier2LeftTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier2Left)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier3LeftTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier3Left)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier4LeftTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier4Left)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier5LeftTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier5Left)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier1RightTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier1Right)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier2RightTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier2Right)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier3RightTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier3Right)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier4RightTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier4Right)->second.c_str());
}

//========================================================================================================================
float BurninBoxStatus::getModuleCarrier5RightTemperature(void) const
{
    return atof(variables_.find(NameDefinition::ModuleCarrier5Right)->second.c_str());
}

//========================================================================================================================
bool BurninBoxStatus::getDoorLockRelayStatus(void) const { return atoi(variables_.find(NameDefinition::DoorLockStatus)->second.c_str()); }

//========================================================================================================================
bool BurninBoxStatus::getDryAirFluxRelayStatus(void) const { return atoi(variables_.find(NameDefinition::DryAirFluxStatus)->second.c_str()); }

//========================================================================================================================
bool BurninBoxStatus::getChillerStatus(void) const { return atoi(variables_.find(NameDefinition::ChillerStatus)->second.c_str()); }

//========================================================================================================================
int BurninBoxStatus::getCycleStatus(void) const { return atoi(variables_.find(NameDefinition::CycleStatus)->second.c_str()); }

//========================================================================================================================
std::string BurninBoxStatus::getErrorMessage(void) const { return variables_.find(NameDefinition::ErrorMessage)->second; }

//========================================================================================================================
float  BurninBoxStatus::getMinAmbientTemperature(void) const
{
    try
    {
        return getMinTemperature(ambientNames_);
    }
    catch(const std::exception& e)
    {
        std::cout << "WARNING: There are no Ambient temperature devices configured, returning " + std::string(OFF_VALUE) + "C" << std::endl;
        return atof(OFF_VALUE);
    }
}
//========================================================================================================================
float  BurninBoxStatus::getAverageAmbientTemperature(void) const
{
    try
    {
        return getAverageTemperature(ambientNames_);
    }
    catch(const std::exception& e)
    {
        std::cout << "WARNING: There are no Ambient temperature devices configured, returning " + std::string(OFF_VALUE) + "C" << std::endl;
        return atof(OFF_VALUE);
    }
}
//========================================================================================================================
float  BurninBoxStatus::getMaxAmbientTemperature(void) const
{
    try
    {
        return getMaxTemperature(ambientNames_);
    }
    catch(const std::exception& e)
    {
        std::cout << "WARNING: There are no Ambient temperature devices configured, returning " + std::string(OFF_VALUE) + "C" << std::endl;
        return atof(OFF_VALUE);
    }
}
//========================================================================================================================
float BurninBoxStatus::getMinDewPointTemperature(void) const
{
    try
    {
        //std::cout << "Reading dew point size: " << variables_.size() << std::endl;
        return getMinTemperature(dewPointNames_);
    }
    catch(const std::exception& e)
    {
        std::stringstream error;
        error << __PRETTY_FUNCTION__ << "DEWPOINT: " << e.what();
        throw std::runtime_error(error.str());
    }
}
//========================================================================================================================
float  BurninBoxStatus::getAverageDewPointTemperature(void) const
{
    try
    {
        return getAverageTemperature(dewPointNames_);
    }
    catch(const std::exception& e)
    {
        throw std::runtime_error(e.what());
    }
}
//========================================================================================================================
float  BurninBoxStatus::getMaxDewPointTemperature(void) const
{
    try
    {
        return getMaxTemperature(dewPointNames_);
    }
    catch(const std::exception& e)
    {
        throw std::runtime_error(e.what());
    }
}
//========================================================================================================================
float BurninBoxStatus::getMinModuleCarrierTemperature(void) const
{
    try
    {
        return getMinTemperature(moduleCarrierNames_);
    }
    catch(const std::exception& e)
    {
        std::cout << "WARNING: There are no ModuleCarrier temperature devices configured, returning " + std::string(OFF_VALUE) + "C" << std::endl;
        return atof(OFF_VALUE);
    }

}
//========================================================================================================================
float BurninBoxStatus::getAverageModuleCarrierTemperature(void) const
{
    try
    {
        return getAverageTemperature(moduleCarrierNames_);
    }
    catch(const std::exception& e)
    {
        std::cout << "WARNING: There are no ModuleCarrier temperature devices configured, returning " + std::string(OFF_VALUE) + "C" << std::endl;
        return atof(OFF_VALUE);
    }

}
//========================================================================================================================
float BurninBoxStatus::getMaxModuleCarrierTemperature(void) const
{
    try
    {
        return getMaxTemperature(moduleCarrierNames_);
    }
    catch(const std::exception& e)
    {
        std::cout << "WARNING: There are no ModuleCarrier temperature devices configured, returning " + std::string(OFF_VALUE) + "C" << std::endl;
        return atof(OFF_VALUE);
    }

}

//========================================================================================================================
float BurninBoxStatus::getMinTemperature(const std::vector<std::string>& names) const
{
    float val = 100000;
    std::string tmpVal;
    for(auto name: names)
    {
        //std::cout << __PRETTY_FUNCTION__ << name << std::endl;
        tmpVal = variables_.find(name)->second.c_str();
        //std::cout << __PRETTY_FUNCTION__ << name << " : " << tmpVal << std::endl;
        if(tmpVal != OFF_VALUE)
            if(stof(tmpVal) < val)
                val = stof(tmpVal);
    }
    if(val < 100000)
        return val;
    else
        throw std::runtime_error("ERROR: There are no devices configured!");
}
//========================================================================================================================
float  BurninBoxStatus::getAverageTemperature(const std::vector<std::string>& names) const
{
    float val = 0;
    unsigned int nDevices = 0;
    std::string tmpVal;
    for(auto name: names)
    {
        tmpVal = variables_.find(name)->second.c_str();
        if(tmpVal != OFF_VALUE)
        {
            ++nDevices;
            val += stof(tmpVal);
        }
    }
    if(nDevices > 0)
        return val/nDevices;
    else
        throw std::runtime_error("ERROR: There are no devices configured!");

}
//========================================================================================================================
float  BurninBoxStatus::getMaxTemperature(const std::vector<std::string>& names) const
{
    float val = -100000;
    std::string tmpVal;
    for(auto name: names)
    {
        tmpVal = variables_.find(name)->second.c_str();
        if(tmpVal != OFF_VALUE)
            if(stof(tmpVal) > val)
                val = stof(tmpVal);
    }
    if(val > -100000)
        return val;
    else
        throw std::runtime_error("ERROR: There are no devices configured!");
}
