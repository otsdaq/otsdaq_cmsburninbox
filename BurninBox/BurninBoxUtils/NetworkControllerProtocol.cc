#include "NetworkControllerProtocol.h"

using namespace ots;
#include <iostream>
#include <sstream>
#include <unistd.h> 


//===========================================================================
NetworkControllerProtocol::NetworkControllerProtocol(std::string IPAddress, int port) 
: TCPClient(IPAddress, port)
{
    connect();
    logFile_.open("NetworkControllerProtocolLog.log");
}

//===========================================================================
NetworkControllerProtocol::~NetworkControllerProtocol(void)
{
	//std::cout << __PRETTY_FUNCTION__ << "Closing!" << std::endl;
	disconnect();
    logFile_.close();
 }

//===========================================================================
void NetworkControllerProtocol::write(const std::string& command)
{
    std::lock_guard<std::mutex> lock(readWriteMutex_);
    // std::cout << __PRETTY_FUNCTION__ << "Sending command: " << command << std::endl;
    send(command);
    // std::cout << __PRETTY_FUNCTION__ << "Done sending command: " << command << std::endl;
}

//===========================================================================
std::string NetworkControllerProtocol::read(const std::string& command)
{
    std::lock_guard<std::mutex> lock(readWriteMutex_);
    // std::cout << __PRETTY_FUNCTION__ << "Sending command: " << command << std::endl;
    std::string value = sendAndReceive(command);
    // std::cout << __PRETTY_FUNCTION__ << "Done sending command: " << command << std::endl;
    return value;
}

//===========================================================================
std::string NetworkControllerProtocol::readValue(const std::string& command)
{
    std::lock_guard<std::mutex> lock(readWriteMutex_);
    // std::cout << __PRETTY_FUNCTION__ << "Sending command: " << command << std::endl;
    std::string retVal;
    std::string buffer;
    send(command);
    logFile_ << "Cmd: " << command << std::endl;
    do
    {
        //std::cout << "Reading:-" << buffer << "-" << std::endl;
        buffer = receive<std::string>();
//        std::cout << "Reading size:-" << buffer.size() << "- buffer: " << buffer << std::endl;
        if(buffer.size() > 0)
        {
             retVal += buffer;
        }
        else if(buffer.size() == 0)
        {
			char hostname[50];
            char username[50];
            gethostname(hostname, 50);
            getlogin_r(username, 50);

            time_t now = time(0);
			// convert now to string form
			char*             dt = ctime(&now);
			std::stringstream error;
			error << __PRETTY_FUNCTION__ << std::endl 
            << "ERROR: I got an empty message from the controller board!" << std::endl
            << "Time:                  " << dt << std::endl
            << "Hostname:              " << hostname  << std::endl
            << "Username:              " << username  << std::endl
            << "The message so far is:-" << retVal << "-";
			//std::cout << error.str() << std::endl;
            logFile_ << error.str() << std::endl;

			std::vector<std::string> mailList(1, "uplegger@fnal.gov");
			sendMail(mailList, "uplegger@fnal.gov", "Controller board TCP error", error.str());
            // std::cout << "Reading size:-" << buffer.size() << "- buffer: " << buffer << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            // std::cout << __PRETTY_FUNCTION__ << "ERROR: There is no communication with the controller board." << std::endl;
            //throw std::runtime_error("ERROR: There is no communication with the controller board. Make sure the controller board is on and that the chiller is also on. If everything is on, then reset the controller board power.");
            //if(retVal == "")
            //    return "";
        }

    } while (retVal.back() != ']');
    logFile_ << "Rcv: " << retVal << std::endl << std::endl;
    // std::cout << __PRETTY_FUNCTION__ << "Done sending command: " << command << std::endl;
    return retVal.substr(1,retVal.length()-2);
}

//===========================================================================
void NetworkControllerProtocol::sendMail(const std::vector<std::string>& toList, const std::string& from, const std::string& subject, const std::string& message)
{
    FILE* mailpipe = popen("/usr/lib/sendmail -t", "w");
    if(mailpipe != nullptr)
    {
        for(auto to: toList)
        {
            fprintf(mailpipe, "To: %s\n", to.c_str());
        }
        fprintf(mailpipe, "From: %s\n", from.c_str());
        fprintf(mailpipe, "Subject: %s\n\n", subject.c_str());
        fwrite(message.c_str(), 1, message.length(), mailpipe);
        fwrite(".\n", 1, 2, mailpipe);
        pclose(mailpipe);
    }
    else
    {
        std::cout << "/usr/lib/sendmail didn't work. Make sure it is installed on your linux distribution." << std::endl
                  << "> sudo yum install -y mailx" << std::endl;
    }
}
