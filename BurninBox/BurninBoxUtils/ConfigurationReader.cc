#include "ConfigurationReader.h"

#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <sstream>
#include <vector>
#include <map>

using namespace ots;
//===========================================================================
ConfigurationReader::ConfigurationReader(void)
: fileName_("")
{
}

//===========================================================================
ConfigurationReader::~ConfigurationReader(void)
{
}

//===========================================================================
void ConfigurationReader::readConfiguration(void)
{
	std::string line;

	bool emailFound = false;
	std::string email = "EMAIL";
	std::vector<std::string> emailAttributes;
	emailAttributes.push_back("Users");

	bool optionsFound = false;
	std::string options = "OPTIONS";
	std::vector<std::string> optionsAttributes;
	optionsAttributes.push_back("TurnChillerOff");
	optionsAttributes.push_back("UnlockDoor");
	optionsAttributes.push_back("KeepDryAirFlowHigh");

	bool controllerFound = false;
	std::string controller = "CONTROLLER";
	std::vector<std::string> controllerAttributes;
	controllerAttributes.push_back("Protocol");
	controllerAttributes.push_back("Version");
	controllerAttributes.push_back("IPAddress");
	controllerAttributes.push_back("Port");

	std::vector<std::string> categoryList;
	categoryList.push_back("TEMPERATURE");
	categoryList.push_back("DEWPOINT");
	categoryList.push_back("RELAY");
	categoryList.push_back("CHILLER");

	std::vector<std::string> attributeList;
	attributeList.push_back("Name");
	attributeList.push_back("Status");
	attributeList.push_back("ControllerConnector");
	attributeList.push_back("DisplayName");
	attributeList.push_back("Comment");

	std::string category;
	bool inConfiguration = false;
	bool comment = false;
	int index = -1;
	unsigned int beginQuote;
	unsigned int endQuote;
	std::size_t beginSensor;
	if(getenv("BURNINBOX_CONFIGURATION_FILE") == nullptr)
	{
		throw std::runtime_error("ERROR: The environment variable BURNINBOX_CONFIGURATION_FILE must be set and must point to the xml configuration file containing your specific Burninbox hardware configuration! Aborting...");
	}
	else
	{
		fileName_ = getenv("BURNINBOX_CONFIGURATION_FILE");
	}
	std::cout << "[" << __LINE__ << "] "<< __PRETTY_FUNCTION__ << "\tReading hardware configuration file: " << fileName_ << std::endl;
	
	std::ifstream configurationFile;
	configurationFile.open(fileName_, std::fstream::in | std::fstream::out | std::fstream::app);
	if (configurationFile.is_open())
	{
		while (!configurationFile.eof())
		{
			getline(configurationFile, line);
			if (line == "")
				continue;
			if (line.find("<!--") != std::string::npos)
			{
				comment = true;
			}
			if (comment)
			{
				if (line.find("-->") != std::string::npos)
					comment = false;
				continue;
			}
			//std::cout << "LINE: " << line << std::endl;

			if (!inConfiguration)
			{
				if (line.find("<" + email + ">") != std::string::npos)
				{
					category = email;
					inConfiguration = true;
					index = -1;
				}
				else if (line.find("<" + options + ">") != std::string::npos)
				{
					category = options;
					inConfiguration = true;
					index = -1;
				}
				else if (line.find("<" + controller + ">") != std::string::npos)
				{
					category = controller;
					inConfiguration = true;
					index = -1;
				}
				else
				{
					for (auto element : categoryList)
					{
						if (line.find("<" + element + ">") != std::string::npos)
						{
							category = element;
							inConfiguration = true;
							index = -1;
							break;
						}
					}
				}

				if (!inConfiguration)
				{
					throw std::runtime_error(std::string("ERROR: unrecognized category at line: ") + line);
					// std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]\t" << "UNRECOGNIZED CATEGORY AT LINE:" << line << std::endl;
					// continue;
				}
			}
			else
			{
				//std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]\t "<< "Category: " << category << std::endl;
				if (line == ("</" + category + ">"))
				{
					inConfiguration = false;
					continue;
				}

				else if ((beginSensor = line.find('<')) != std::string::npos)
				{
					if (category == email)
					{
						emailFound = true;
						for (auto attribute: emailAttributes)
						{
							//std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]\t "<< "Category: " << category  << "-> Looking for " << attribute << std::endl;
							if (line.find(attribute) == std::string::npos)
							{
								throw std::runtime_error(std::string("ERROR: Can't find attribute ") + attribute + std::string(" in ") + fileName_ );
							}
							index = (int)line.find(attribute);
							index = index + attribute.size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							email_[attribute] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							//std::cout << "Attribute: " << attribute
							//		  << " Value: " << connection_[attribute] << std::endl;
						}
					}
					else if (category == options)
					{
						optionsFound = true;
						for (auto attribute: optionsAttributes)
						{
							//std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]\t "<< "Category: " << category  << "-> Looking for: " << attribute << " in line: " << line << std::endl;
							if (line.find(attribute) == std::string::npos)
							{
								std::string errorMessage = std::string("ERROR: Can't find attribute ") + attribute + " in " + fileName_ + ".\n"
								+ "Please make sure you have the following line in " + fileName_ + "\n"
								+ "  <TurnChillerOff=\"Yes\" UnlockDoor=\"No\" KeepDryAirFlowHigh=\"Yes\"/>\n";
								throw std::runtime_error(errorMessage);
							}
							index = (int)line.find(attribute);
							index = index + attribute.size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							options_[attribute] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]\t" << "Attribute: " << attribute
									  << " Value: " << options_[attribute] << std::endl;
						}
					}
					else if (category == controller)
					{
						controllerFound = true;
						for (auto attribute: controllerAttributes)
						{
							if (line.find(attribute) == std::string::npos)
							{
								throw std::runtime_error(std::string("ERROR: Can't find attribute ") + attribute + std::string(" in ") + fileName_ );
							}
							index = (int)line.find(attribute);
							index = index + attribute.size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							connection_[attribute] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]\t" << "Attribute: " << attribute
									  << " Value: " << connection_[attribute] << std::endl;
						}
					}
					else
					{
						devices_[category].push_back(std::map<std::string, std::string>());
						int vectorIndex = devices_[category].size() - 1;
						for (unsigned attributeN = 0; attributeN < attributeList.size(); attributeN++)
						{
							if (line.find(attributeList[attributeN]) == std::string::npos)
							{
								throw std::runtime_error(std::string("ERROR: Can't find attribute ") + attributeList[attributeN] + std::string(" in ") + fileName_ );
							}
							index = (int)line.find(attributeList[attributeN]);
							index = index + attributeList[attributeN].size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							devices_[category][vectorIndex][attributeList[attributeN]] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							//std::cout << "Attribute: " << attributeList[attributeN]
							//		  << " Value: " << devices_[category][vectorIndex][attributeList[attributeN]] << std::endl;
						}
					}

					if (line.find("/>", index) != std::string::npos)
						index = -1;
					//   if("do all checks for consistency")
				}
				else
					throw std::runtime_error("ERROR: Didn't find opening bra < !");
			}
		}
		if(!emailFound)
		{
			std::string errorMessage = "ERROR: Didn't find tag <EMAIL> in " + fileName_ + ".\n"
			+ "Please make sure you have the following lines in " + fileName_ +":\n"
			+ "<EMAIL>\n"
  			+ "  <!--Comma separated list of emails-->"
  			+ "  <Users=\"uplegger@fnal.gov, YourEmail@email.com\"/>\n"
			+ "</EMAIL>\n";
			throw std::runtime_error(std::string(errorMessage));
		}
		if(!optionsFound)
		{
			std::string errorMessage = "ERROR: Didn't find tag <OPTIONS> in " + fileName_ + ".\n"
			+ "It is likely that you have an old version of the configuration file.\n"
			+ "Please make sure to add the following lines in " + fileName_ +":\n"
			+ "<OPTIONS>\n"
			+ "  <!-- TurnChillerOff=\"Yes/No\" UnlockDoor=\"Yes/No\" KeepDryAirFlowHigh=\"Yes/No\"-->\n"
			+ "  <TurnChillerOff=\"Yes\" UnlockDoor=\"No\" KeepDryAirFlowHigh=\"Yes\"/>\n"
			+ "</OPTIONS>\n";
			throw std::runtime_error(std::string(errorMessage));
		}
		if(!controllerFound)
		{
			std::string errorMessage = "ERROR: Didn't find tag <CONTROLLER> in " + fileName_ + ".\n"
			+ "Please make sure you have the following lines in " + fileName_ +":\n"
			+ "<CONTROLLER>\n"
  			+ "  <!--Protocol: Network, BeagleBone.-->"
  			+ "  <Protocol=\"Network\" Version=\"v1\" IPAddress=\"192.168.0.187\" Port=\"10001\"/>\n"
			+ "</CONTROLLER>\n";
			throw std::runtime_error(std::string(errorMessage));
		}
		configurationFile.close();
	}
	else
		throw std::runtime_error(std::string("ERROR: Can't open file ") + fileName_ + std::string("!"));

}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getDevice(std::string category, std::string name) const
{
	auto it = devices_.find(category);
	if (it == devices_.end())
	{
		throw std::runtime_error("ERROR: Can't find category " + category);
	}
	else
	{
		for(const ConfigurationAttributes& vIt: it->second)
		{
			if(vIt.find("Name") != vIt.end())
			{
				if(vIt.find("Name")->second.compare(name) == 0)
					return vIt;
			}
			else
			{
				throw std::runtime_error("ERROR: Can't find attribute Name!");
			}
		}
		throw std::runtime_error(std::string("WARNING: Didn't find sensor with name ") + name + std::string(" in category ") + category);
	}
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getTemperatureSensor(std::string name) const
{
	return getDevice("TEMPERATURE", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getDewPointSensor(std::string name) const
{
	return getDevice("DEWPOINT", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getRelay(std::string name) const
{
	return getDevice("RELAY", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getChiller(std::string name) const
{
	return getDevice("CHILLER", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getEmail(void) const
{
	return email_;
}

//===========================================================================
bool ConfigurationReader::turnChillerOff(void) const
{
	if(options_.find("TurnChillerOff")->second.find("No") || options_.find("TurnChillerOff")->second.find("NO") || options_.find("TurnChillerOff")->second.find("no"))
		return false;
	else
		return true;
}

//===========================================================================
bool ConfigurationReader::unlockDoor(void) const
{
	if(options_.find("UnlockDoor")->second.find("No") || options_.find("UnlockDoor")->second.find("NO") || options_.find("UnlockDoor")->second.find("no"))
		return false;
	else
		return true;
}

//===========================================================================
bool ConfigurationReader::keepDryAirFlowHigh(void) const
{
	if(options_.find("KeepDryAirFlowHigh")->second.find("No") || options_.find("KeepDryAirFlowHigh")->second.find("NO") || options_.find("KeepDryAirFlowHigh")->second.find("no"))
		return false;
	else
		return true;
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getConnection(void) const
{
	return connection_;
}

//===========================================================================
void ConfigurationReader::printConfiguration(void)
{
	for (auto device : devices_)
	{
		std::cout << "Device:" << device.first << " :" << std::endl;
		for (auto sensor : device.second)
		{
			for (auto attribute : sensor)
			{
				std::cout << attribute.first << ": " << attribute.second << ", ";
			}
			std::cout << std::endl;
		}
	}
}
