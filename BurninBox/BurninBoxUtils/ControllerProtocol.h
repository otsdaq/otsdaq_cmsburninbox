#ifndef _ots_ControllerProtocol_h_
#define _ots_ControllerProtocol_h_

#include <string>

namespace ots
{
class ControllerProtocol
{
public:
	ControllerProtocol(void){;}
	virtual ~ControllerProtocol(void){;}

	virtual void        write     (const std::string& command) = 0;
	virtual std::string read      (const std::string& command) = 0;
	//This one strips extra characters if necessary
	virtual std::string readValue (const std::string& command) = 0;

};
}
#endif
