#ifndef _ots_ConfigurationReader_h_
#define _ots_ConfigurationReader_h_

#include "Device.h"
#include <string>
#include <vector>
#include <map>

namespace ots
{
	//This class is responsible for reading the configuration xml files and parse it producing a list of the configured devices
	class ConfigurationReader
	{
	public:
		ConfigurationReader(void);
		virtual ~ConfigurationReader(void);

		void readConfiguration(void);
		void printConfiguration(void);

		const ConfigurationAttributes &getTemperatureSensor(std::string name) const;
		const ConfigurationAttributes &getDewPointSensor(std::string name) const;
		const ConfigurationAttributes &getChiller(std::string name) const;
		const ConfigurationAttributes &getRelay(std::string name) const;
		const ConfigurationAttributes &getConnection(void) const;
		const ConfigurationAttributes &getEmail(void) const;
		                        
		bool turnChillerOff(void) const;
		bool unlockDoor(void) const;
		bool keepDryAirFlowHigh(void) const;

	private:
		std::string fileName_;
		const ConfigurationAttributes & getDevice(std::string category, std::string name) const;
		ConfigurationAttributes email_;
		ConfigurationAttributes options_;
		ConfigurationAttributes connection_;
		// example:
		// sensor_["TEMPERATURE"]  [0]             ["name"]   =    "RTD0";//Device*
		std::map<std::string, std::vector<ConfigurationAttributes>> devices_;
	};
} // namespace ots
#endif
