/*
 * SafetyRangeDefinition.h
 *
 *  Created on: Sep 18, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_SafetyRangeDefinition_h_
#define _ots_SafetyRangeDefinition_h_

namespace ots
{
class SafetyRangeDefinition
{
  public:
    SafetyRangeDefinition() { ; }
    virtual ~SafetyRangeDefinition() { ; }

    // Timeout
    static constexpr float errorTimeout = 1200; // In seconds

    // Dew point
    static constexpr float dewPointSafetyMarginDefault = 5.0;
    static constexpr float minDewPointValue            = -80.0;
    static constexpr float maxDewPointValue            = 20.0;

    // Any temperature measurement inside the box
    static constexpr float safeTemperatureValue    = 20.0;
    static constexpr float minTemperatureValue     = -50.0;
    static constexpr float maxTemperatureValue     = 55.0;

    // Modules temperaures
    static constexpr float carrierToModuleSafetyMarginDifferential = 15.0;
    static constexpr float moduleToModuleSafetyMarginDifferential  = 5.0;
    static constexpr float minModuleTemperature                    = -40.0;
    static constexpr float maxModuleTemperature                    = 50.0;

    // Ranges
    // The chiller has oscillation of ~0.2C degrees so it is better to keep this value > 0.2C
    static constexpr float targetTemperatureLowToleranceValue = 0.5;
    // The chiller has oscillation of ~0.2C degrees so it is better to keep this value > 0.2C
    static constexpr float targetTemperatureHighToleranceValue = 0.5;
};

} // namespace ots
#endif /* _ots_SafetyRangeDefinition_h_ */
