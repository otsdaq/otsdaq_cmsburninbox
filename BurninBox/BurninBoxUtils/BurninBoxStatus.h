/*
 * BurninBoxStatus.h
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_BurninBoxStatus_h_
#define _ots_BurninBoxStatus_h_

#include "JsonConverter.h"
#include <string>
#include <vector>
#include <sys/time.h>

#define OFF_VALUE "-99"

namespace ots
{
class BurninBoxStatus : public JsonConverter
{
  public:
    BurninBoxStatus();
    virtual ~BurninBoxStatus();
    // Setters
    void setTime(void);
    void setAmbient1Temperature(float value);
    void setAmbient2Temperature(float value);
    void setFrameTemperature(float value);
    void setFrameLeftTemperature(float value);
    void setFrameRightTemperature(float value);
    void setChillerSetTemperature(float value);
    void setDewPoint1Temperature(float value);
    void setDewPoint2Temperature(float value);
    void setModuleCarrier1LeftTemperature(float value);
    void setModuleCarrier2LeftTemperature(float value);
    void setModuleCarrier3LeftTemperature(float value);
    void setModuleCarrier4LeftTemperature(float value);
    void setModuleCarrier5LeftTemperature(float value);
    void setModuleCarrier1RightTemperature(float value);
    void setModuleCarrier2RightTemperature(float value);
    void setModuleCarrier3RightTemperature(float value);
    void setModuleCarrier4RightTemperature(float value);
    void setModuleCarrier5RightTemperature(float value);

    void setDoorLockRelayStatus(bool value);
    void setDryAirFluxRelayStatus(bool value);
    void setChillerStatus(bool status);
    void setCycleStatus  (int status);
    void setErrorMessage(std::string errorMessage);

    // Getters
    time_t getTime(void) const;
    float  getAmbient1Temperature(void) const;
    float  getAmbient2Temperature(void) const;
    float  getFrameTemperature(void) const;
    float  getFrameLeftTemperature(void) const;
    float  getFrameRightTemperature(void) const;
    float  getChillerSetTemperature(void) const;
    float  getDewPoint1Temperature(void) const;
    float  getDewPoint2Temperature(void) const;
    float  getModuleCarrier1LeftTemperature(void) const;
    float  getModuleCarrier2LeftTemperature(void) const;
    float  getModuleCarrier3LeftTemperature(void) const;
    float  getModuleCarrier4LeftTemperature(void) const;
    float  getModuleCarrier5LeftTemperature(void) const;
    float  getModuleCarrier1RightTemperature(void) const;
    float  getModuleCarrier2RightTemperature(void) const;
    float  getModuleCarrier3RightTemperature(void) const;
    float  getModuleCarrier4RightTemperature(void) const;
    float  getModuleCarrier5RightTemperature(void) const;

    bool   getDoorLockRelayStatus(void) const;
    bool   getDryAirFluxRelayStatus(void) const;
    bool   getChillerStatus(void) const;
    int    getCycleStatus  (void) const;
    std::string getErrorMessage(void) const;

    float  getMinAmbientTemperature(void) const;
    float  getAverageAmbientTemperature(void) const;
    float  getMaxAmbientTemperature(void) const;
    float  getMinDewPointTemperature(void) const;
    float  getAverageDewPointTemperature(void) const;
    float  getMaxDewPointTemperature(void) const;
    float  getMinModuleCarrierTemperature(void) const;
    float  getAverageModuleCarrierTemperature(void) const;
    float  getMaxModuleCarrierTemperature(void) const;

  private:
    float  getMinTemperature(const std::vector<std::string>& names) const;
    float  getAverageTemperature(const std::vector<std::string>& names) const;
    float  getMaxTemperature(const std::vector<std::string>& names) const;

    std::vector<std::string> ambientNames_;
    std::vector<std::string> dewPointNames_;
    std::vector<std::string> moduleCarrierNames_;
    std::string              errorMessage_;
    

};

} // namespace ots
#endif /* _ots_BurninBoxStatus_h_ */
