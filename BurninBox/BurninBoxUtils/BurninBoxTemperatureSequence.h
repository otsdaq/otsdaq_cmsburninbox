/*
 * BurninBoxTemperatureSequence.h
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_BurninBoxTemperatureSequence_h_
#define _ots_BurninBoxTemperatureSequence_h_

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/BurninBoxTemperatures.h"
#else
#include "BurninBoxTemperatures.h"
#endif

#include <map>
#include <string>
#include <vector>
#include <iostream>

namespace ots
{
  class BurninBoxTemperatureSequence : public BurninBoxTemperatures
  {
  public:
    BurninBoxTemperatureSequence();
    virtual ~BurninBoxTemperatureSequence();

    void operator=(const BurninBoxTemperatures &temperature);

    void next(void);
    void gotoTemperature(std::string name);
    void gotoLast(void);
    void gotoError(void);
    void resetSequence(void);

    //Setters
    void setTemperature(std::string name, double temperature, unsigned time, unsigned sequence=-1) override;
    void setTemperature(std::string name, const Temperature &temperature) override;
    void setErrorTemperature(double temperature);

    // Getters
    // BurninBoxTemperatures getTemperatures(std::string name) const;
    double getErrorTemperature(void) const;
    double getCurrentTemperature(void) const;
    double getLowestTemperature(void) const;
    double getLastTemperature(void) const;
    unsigned getCurrentTime(void) const;
    unsigned getCurrentSequenceNumber(void) const;
    std::string getCurrentName(void) const;
    unsigned getLastSequenceNumber(void) const;
    unsigned getNumberOfTemperatures(void) const;

  private:
    const std::string errorTemperatureName_;
    Temperature errorTemperature_;
    const uint errorTemperatureSequenceNumber_ = -1;
    std::map<unsigned, Temperature*> sequenceToTemperature_;
    std::map<unsigned, Temperature*>::const_iterator currentTemperature_;
  };

} // namespace ots
#endif /* _ots_BurninBoxTemperatureSequence_h_ */
