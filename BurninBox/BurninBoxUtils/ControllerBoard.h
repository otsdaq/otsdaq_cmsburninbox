#ifndef _ots_ControllerBoard_h_
#define _ots_ControllerBoard_h_

#include "Device.h"
#include <string>
#include <fstream>

namespace ots
{
class ControllerProtocol;

class ControllerBoard
{
  public:
    ControllerBoard(std::string protocol, std::string version, std::string IPAddress, int port);
    ~ControllerBoard(void);

    float readTemperature(const Device& device);
    // std::vector<float> readAllDigitalTemperatures();
    float readHumidity(const Device& device);
    float readDewPoint(const Device& device);
    float readChillerSetTemperature(const Device& device);

    void relayOn(const Device& device);
    void relayOff(const Device& device);

    //Chiller commands
    //There are 3 setpoints that control the temperature. 
    //0 is the default and must be set before using it to set the chiller temperatures
    //It should be set up on the chiller page 1 of the manual (press T and then select the setpoint)
    void        setChillerSetPoint(const Device& device, int setpoint=0);
    void        setChillerTemperature(const Device& device, float temperature);
    bool        isChillerOn(const Device& device);
    void        turnOnChiller(const Device& device);
    void        turnOffChiller(const Device& device);
    void        writeChiller(const std::string& command);
    std::string readChiller(const std::string& command);

    std::string executeCommand(std::string command);
    
    //This is the special case when the chiller is the DESY chiller
    std::string useDESYChiller(const Device& device, std::string command, int attempts=10);

  private:
    ControllerProtocol* theProtocol_;
    std::ofstream logFile_;

};
} // namespace ots
#endif
