#ifndef _ots_BurninBoxController_h_
#define _ots_BurninBoxController_h_

#include "BurninBoxConfiguration.h"
#include "BurninBoxTemperatureSequence.h"
#include "BurninBoxStatus.h"
#include "Device.h"
#include "otsdaq/NetworkUtilities/TCPServer.h"

#include <atomic>
#include <future>
#include <mutex> //for std::mutex
#include <sstream>
#include <vector>

namespace ots
{
class ControllerBoard;
class ConfigurationReader;

class BurninBoxController : public TCPServer // Connects to the FEInterface
{

  public:
    enum Cycle
    {
        CYCLE_INITIAL            = 0,
        CYCLE_GO_TO_START        = 1,
        CYCLE_START              = 2,
        CYCLE_COOLING            = 3,
        CYCLE_TARGET             = 4,
        CYCLE_WARMING            = 5,
        CYCLE_STOP               = 6,
        CYCLE_ERROR              = 7,
        CYCLE_STOPPED            = 8,
        CYCLE_STOPPED_WITH_ERROR = 9
    };

  private:
    enum StateMachineStatus
    {
        INITIAL,    // 0->HALTED
        HALTED,     // 1->CONFIGURE
        CONFIGURE,  // 2->CONFIGURED
        CONFIGURED, // 3->HALTED, STARTING
        STARTING,   // 4->RUNNING
        STOPPING,   // 5->CONFIGURED
        RUNNING,    // 6->CONFIGURED, HALTED, PAUSED
        PAUSED,     // 7->HALTED, CONFIGURED, RUNNING
        RESUME,     // 8->RUNNING
        SHUTDOWN,   // 9->INITIAL
        STATE_MACHINE_ERROR       // 10
    };

    std::string translateCycleStatus(Cycle status)
    {
      switch (status)
      {
      case CYCLE_INITIAL:
        return "Initial";
      case CYCLE_GO_TO_START:
        return "Go to Start";
      case CYCLE_START:
        return "Start";
      case CYCLE_COOLING:
        return "Cooling";
      case CYCLE_TARGET:
        return "Target";
      case CYCLE_WARMING:
        return "Warming";
      case CYCLE_STOP:
        return "Stop";
      case CYCLE_ERROR:
        return "Error";
      case CYCLE_STOPPED:
        return "Stopped";
      case CYCLE_STOPPED_WITH_ERROR:
        return "Stopped with error";      
      default:
        return "Unknown";
      }
    }

    std::string translateStateMachineStatus(StateMachineStatus status)
    {
      switch (status)
      {
      case INITIAL:
        return "Initial";
      case HALTED:
        return "Halted";
      case CONFIGURE:
        return "Configuring";
      case CONFIGURED:
        return "Configured";
      case STARTING:
        return "Starting";
      case STOPPING:
        return "Stopping";
      case RUNNING:
        return "Running";
      case PAUSED:
        return "Paused";
      case RESUME:
        return "Resuming";      
      case SHUTDOWN:
        return "Shutdown";      
      case STATE_MACHINE_ERROR:
        return "Error";      
      default:
        return "Unknown";
      }
    }

    enum CycleErrorCode
    {
        NO_CYCLE_ERROR,
        CYCLE_ERROR_WRONG_STATUS,
        CYCLE_ERROR_WRONG_TEMPERATURE_INDEX,
        CYCLE_ERROR_STATUS_READING,
        CYCLE_ERROR_OUT_OF_RANGE_READINGS,
        CYCLE_ERROR_TIMEOUT
    };

  public:
    BurninBoxController(void);
    virtual ~BurninBoxController(void);

    void checkConfiguration(const ConfigurationReader& configuration);
    //    void init(const DeviceContainer& DeviceContainer); //?

    /////////////////////////////////////////////////////////////////
    // THESE WERE PART OF THE SERVER THAT CONNECTS TO THE FEINTERFACE
    std::string interpretMessage(const std::string& buffer) override;
    std::string getVariableValue(std::string variable, std::string buffer)
    {
        size_t begin = buffer.find(variable) + variable.size() + 1;
        size_t end   = buffer.find(',', begin);
        if(end == std::string::npos)
            end = buffer.size() - 1;
        return buffer.substr(begin, end - begin);
    }
    /////////////////////////////////////////////////////////////////
    // STATE MACHINE
    std::string        configure();
    std::string        start(std::string runNumber);
    std::string        stop(void);
    std::string        halt(void);
    void               running(void);
    StateMachineStatus getStateMachineStatus(void);
    std::future<void>  runningFuture_;

    std::string            currentRun_ = "0";
    bool                   paused_     = false;
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // SENSORS
    BurninBoxStatus updateStatus(unsigned updateInterval=0, std::string error=""); // returns a copy for thread safety
    BurninBoxStatus getStatus   (void);    // returns a copy for thread safety
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // TEMPERATURE CYCLE
    void  setCycleStatus(Cycle cycle);
    void  setCycleError(CycleErrorCode code);
    CycleErrorCode  getCycleError(){ return theCycleError_; }
    std::string  getCycleErrorMessage(){ return theCycleErrorMessage_; }
    Cycle getCycleStatus(void) { return theCycleStatus_; }
    std::string setTargetTemperature(std::string name);
    std::string addTemperature(std::string name, double temperature, unsigned time, unsigned sequence=-1);

    void checkOutOfRangeTemperature(const BurninBoxStatus& status);

    /////////////////////////////////////////////////////////////////
    // CHECKS
    float compareWithTargetTemperatureTolerance(float temperature, float target); //>0 temperature>target; <0 temperature<target; =0 temperature=target
//    int compareWithStopTemperature(float temperature);   //>0 carrier>stop; <0 carrier<stop; =0 carrier=stop

  private:
    void sendMail(const std::string& toList, const std::string& from, const std::string& subject, const std::string& message);
    void setStateMachineStatus(StateMachineStatus status);
    float safeChillerSetTemperature(float chillerSetTemperature, const BurninBoxStatus& status);//Ideally is the setTemperature but can't be lower than dewpoint + safetydewppoint

    ControllerBoard*              theControllerBoard_;
    BurninBoxConfiguration        theBurninBoxConfiguration_;
    BurninBoxTemperatureSequence  theTemperatureSequence_;
    bool                          turnChillerOff_;
    bool                          unlockDoor_;
    bool                          keepDryAirFlowHigh_;
    int                           cycleNumber_;

    Device                        theFrameDevice_;
    Device                        theFrameLeftDevice_;
    Device                        theFrameRightDevice_;
    Device                        theDoorLockDevice_;
    Device                        theDryAirFluxDevice_;
    Device                        theChillerDevice_;
    std::map<std::string, Device> theDewPointDevices_;
    std::map<std::string, Device> theAmbientDevices_;
    std::map<std::string, Device> theModuleCarrierDevices_;

    // CONFIGURATION TEMPERATURES
    // BurninBox target temperatures. The last one is the one used to stop the cycle even if, for any reasons, other targets has not been achieved
    std::vector<float> targetTemperatures_; 
    std::vector<unsigned> secondsAtTargetTemperatures_; 

    // STATUS
    BurninBoxStatus                 theBurninBoxStatus_;
    std::mutex                      statusMutex_; // to make receiver socket thread safe
    std::atomic<StateMachineStatus> theStateMachineStatus_;
    std::atomic<Cycle>              theCycleStatus_;
    std::atomic<CycleErrorCode>     theCycleError_;
    std::atomic<bool>               updatingStatus_;
    std::string                     theCycleErrorMessage_;
    bool                            theDoorLockStatus_ = false;
    bool                            theDryAirFluxStatus_ = false;
    bool                            goToStartAknowledgment_ = false;

    std::stringstream errorMessage_;
    std::string       mailList_;
    std::string       computerName_;
    std::string       userName_;



    private:
    //Stuff that it is likely to be discarded
    Cycle getPreviousCycleStatus(void) { return thePreviousCycleStatus_; }
    Cycle                           thePreviousCycleStatus_;
    struct timeval                  beginCycleTime_;
    //void printTime(const std::string& message = "");

    // float           readAmbientTemperature(void);
    // float           readFrameLeftTemperature(void);
    // float           readFrameRightTemperature(void);
    // float           readFrameTemperature(void);
    // float           readDewPointTemperature(void);

    // //	bool   readDryAirFluxRelayStatus   (void);
    // float readModuleCarrier1LeftTemperature(void);
    // float readModuleCarrier2LeftTemperature(void);
    // float readModuleCarrier3LeftTemperature(void);
    // float readModuleCarrier4LeftTemperature(void);
    // float readModuleCarrier5LeftTemperature(void);
    // float readModuleCarrier1RightTemperature(void);
    // float readModuleCarrier2RightTemperature(void);
    // float readModuleCarrier3RightTemperature(void);
    // float readModuleCarrier4RightTemperature(void);
    // float readModuleCarrier5RightTemperature(void);

    // // void  setEnableTemperature(bool flag);

    // // void setDewPointSafetyMargin(float dewPointSafetyMargin);
    // // void setDewPointSafetyHighMargin(float dewPointSafetyHighMargin);
    // // void setDewPointHighLowMargin(float dewPointHighLowMargin);

    // // float getDewPointSafetyMargin(float) const { return dewPointSafetyMargin_; }
    // // float getDewPointSafetyHighMargin(void) const { return dewPointSafetyHighMargin_; }
    // // float getDewPointHighLowMargin(void) const { return dewPointHighLowMargin_; }
    // void switchThermalRelays(bool);
    // void controlLoop(void);
    // int  relayTemperatureControl(float currentTemperature, float min, float max);
    // void keepTemperature(float setTemperature, float keepTime); // keepTime expressed in seconds
    // void threadMain(void);
    // // float dewPointSafetyMargin_;
    // // float dewPointSafetyHighMargin_;
    // // float dewPointHighLowMargin_;

    // std::vector<float*>             temperatureStructure_;
    // float runningDewPoint_;
    // bool  running_;
    // bool  timeCycle;
    // bool  enableTemperature_;
    // int   dewPointRecursiveCounter_;

};
} // namespace ots
#endif
