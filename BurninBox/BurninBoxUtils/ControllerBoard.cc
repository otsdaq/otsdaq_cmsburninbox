#include "ControllerBoard.h"
#include "NetworkControllerProtocol.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <thread>
#include <chrono>

// For serial communication for DESY
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

using namespace ots;

//===========================================================================
ControllerBoard::ControllerBoard(std::string protocol, std::string version, std::string IPAddress, int port)
{
    if (protocol == "Network")
    {
        theProtocol_ = new NetworkControllerProtocol(IPAddress, port);
        dynamic_cast<NetworkControllerProtocol *>(theProtocol_)->setReceiveTimeout(10, 0);
    }
    // else if(protocol == "BeagleBone")
    //    theProtocol_ = new BeagleBoneProtocol;
    logFile_.open("ControllerBoardLog.log");
}

//===========================================================================
ControllerBoard::~ControllerBoard(void) 
{ 
    delete theProtocol_; 
    logFile_.close();
}

//===========================================================================
void ControllerBoard::setChillerSetPoint(const Device &device, int setpoint)
{

    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_MODE_01 " + std::to_string(setpoint) + "]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot set the chiller setpoint using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot set the chiller setpoint using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        // Do nothing
        return;
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[60OUT_MODE_01 " + std::to_string(setpoint) + "]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    // // Check return value...
    // if(value != "*")
    //     throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
}

//===========================================================================
void ControllerBoard::setChillerTemperature(const Device &device, float temperature)
{
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Begin" << std::endl;            
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_SP_00 " + std::to_string(temperature) + "]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot set the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot set the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M00" << std::hex << std::uppercase << std::setw(4) << std::setfill('0') << int16_t(temperature * 100) << std::dec << "\r\n";
        std::string value = useDESYChiller(device, command.str());
        std::cout << "[" << __LINE__ << "] "<< __PRETTY_FUNCTION__ << "Command: " << command.str().substr(0,command.str().length()-2) << " Return value: " << value << std::endl;
        if (value.substr(0, 4) != "{S00")
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Done" << std::endl;            
}

//===========================================================================
float ControllerBoard::readChillerSetTemperature(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[61IN_SP_00]";
        std::string value = theProtocol_->readValue(command);
        try
        {
           return strtof(&value.at(0), nullptr);
        }
        catch(const std::exception& e)
        {
            std::stringstream error;
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot read the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: float";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            try
            {
                return strtof(&value.at(0), nullptr);
            }
            catch(const std::exception& e)
            {
                error.str("");
                error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot read the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: float";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }       
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M00****\r\n";
        std::string value = useDESYChiller(device, command.str());
        if (value.substr(0, 4) != "{S00")
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
        std::stringstream returnValue;
        int32_t rawValue;
        returnValue << std::hex << "0x" + value.substr(4, 4);
        returnValue >> rawValue;  // Convert to integer
        return static_cast<int16_t>(rawValue) / 100.0;
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }

    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[61IN_SP_00]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // return strtof(&value.at(0), nullptr);
}

//===========================================================================
void ControllerBoard::turnOnChiller(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_MODE_05 1]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot turn on the chiller using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot turn on the chiller using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M140001\r\n";
        std::string value = useDESYChiller(device, command.str());
        //std::cout << value << std::endl;
        //        if(value != "*")
        //           throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }

    // std::cout << __PRETTY_FUNCTION__ << "Turning on chiller" << std::endl;
    //  std::string command;
    //  if(device.getControllerConnector() == "BurninController")
    //      command = "[60OUT_MODE_05 1]";
    //  else
    //  {
    //      throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    //  }
    //  std::string value = theProtocol_->readValue(command);
    //  //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    //  // Check return value...
    //  if(value != "*")
    //      throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
    //  //Wait 2 seconds before the chiller actually turns on
    //  std::this_thread::sleep_for(std::chrono::seconds(2));
}

//===========================================================================
void ControllerBoard::turnOffChiller(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_MODE_05 0]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot turn off the chiller using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot turn off the chiller using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M140000\r\n";
        std::string value = useDESYChiller(device, command.str());
        //std::cout << value << std::endl;
        //       if(value != "*")
        //           throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::cout << __PRETTY_FUNCTION__ << "Turning off chiller" << std::endl;
    //  std::string command;
    //  if(device.getControllerConnector() == "BurninController")
    //      command = "[60OUT_MODE_05 0]";
    //  else
    //  {
    //      throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    //  }
    //  std::string value = theProtocol_->readValue(command);
    //  //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    //  // Check return value...
    //  if(value != "*")
    //      throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
    //  //Wait 2 seconds before the chiller actually turns off
    //  std::this_thread::sleep_for(std::chrono::seconds(2));
}

//===========================================================================
bool ControllerBoard::isChillerOn(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[61IN_MODE_05]";
        std::string value = theProtocol_->readValue(command);
        if (value == "0")
            return false;
        else if (value == "1")
            return true;
        else
        {
            std::stringstream error;
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot check if the chiller is on using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: 0/1";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value == "0")
                return false;
            else if (value == "1")
                return true;
            else
            {
                error.str("");
                error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot check if the chiller is on using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: 0/1";
                logFile_ << error.str() << std::endl;
                return true;//ASSUMING THAT THE CHILLER IS ON AND EVENTUALLY MUST BE TURNED OFF!
                //Removing exceptions because I don't want to stop everything if I get an error once!
                // std::stringstream error;
                // error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "Cannot set the chiller temperature using command [60OUT_SP_00], make sure the chiller is turned on and connected!";
                // throw std::runtime_error(error.str());           
            }
        }
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M14****\r\n";
        std::string value = useDESYChiller(device, command.str());
        if (value.substr(0, 4) != "{S14")
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
        if (value.substr(4, 4) == "0000")
            return false;
        else if (value.substr(4, 4) == "0001")
            return true;
        else
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
            throw std::runtime_error("Weird chiller response: " + value);
        }
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[61IN_MODE_05]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    // // Check return value...
    // if(value == "0")
    //     return false;
    // else if (value == "1")
    //     return true;
    // else
    //     throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
}

//===========================================================================
float ControllerBoard::readTemperature(const Device &device)
{
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << device.getControllerConnector() << std::endl;            
    std::string command;
    if (device.getControllerConnector() == "RTD1")
        command = "[21]";
    else if (device.getControllerConnector() == "RTD2")
        command = "[22]";
    else if (device.getControllerConnector() == "RTD3")
        command = "[23]";
    else if (device.getControllerConnector() == "RTD4")
        command = "[24]";
    else if (device.getControllerConnector() == "DTS1")
        command = "[4101]";
    else if (device.getControllerConnector() == "DTS2")
        command = "[4102]";
    else if (device.getControllerConnector() == "DTS3")
        command = "[4103]";
    else if (device.getControllerConnector() == "DTS4")
        command = "[4104]";
    else if (device.getControllerConnector() == "DTS5")
        command = "[4105]";
    else if (device.getControllerConnector() == "DTS6")
        command = "[4106]";
    else if (device.getControllerConnector() == "DTS7")
        command = "[4107]";
    else if (device.getControllerConnector() == "DTS8")
        command = "[4108]";
    else if (device.getControllerConnector() == "DTS9")
        command = "[4109]";
    else if (device.getControllerConnector() == "DTS10")
        command = "[4110]";
    else if (device.getControllerConnector() == "ChillerBath")
        command = "[61IN_PV_00]";
    else if (device.getControllerConnector() == "ChillerExternRTD")
        command = "[61IN_PV_02]";
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::string command = "{M07****\r\n";
        std::string value = useDESYChiller(device, command);
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "Command: " << command.substr(0, command.length()-2) << " Return value: " << value << std::endl;            
        if (value.substr(0, 4) != "{S07")
        {
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
        std::stringstream returnValue;
        int32_t rawValue;
        returnValue << std::hex << "0x" + value.substr(4, 4);
        returnValue >> rawValue;  // Convert to integer
        float temperature = static_cast<int16_t>(rawValue) / 100.0;

        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Return temperature: " << temperature << std::endl;                 
        return temperature;
    }
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " is not a temperature device!");
    }

    //    std::cout << __PRETTY_FUNCTION__ << device.getControllerConnector() << " sending command: " << command << std::endl;
    std::string value = theProtocol_->readValue(command);
    try
    {
        return strtof(&value.at(0), nullptr);
    }
    catch(const std::exception& e)
    {
        std::stringstream error;
        error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot read temperature using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: float";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        try
        {
            return strtof(&value.at(0), nullptr);
        }
        catch(const std::exception& e)
        {
            error.str("");
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot read temperature using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: float";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }       
}

//===========================================================================
float ControllerBoard::readHumidity(const Device &device) { return 100; }

//===========================================================================
float ControllerBoard::readDewPoint(const Device &device)
{
    //    std::cout << __PRETTY_FUNCTION__ << "Reading device: " << device.getControllerConnector() << std::endl;
    std::string command;
    if (device.getControllerConnector() == "H&T1")
        command = "[30]";
    else if (device.getControllerConnector() == "H&T2")
        command = "[31]";
    else
    {
        std::cout << __PRETTY_FUNCTION__ << "Device: " << device.getControllerConnector()
                  << " is not in the list of readable devices! Crashing now...!" << std::endl;
        abort();
    }

    std::string value = theProtocol_->readValue(command);
    try
    {
        return strtof(&value.at(0), nullptr);
    }
    catch(const std::exception& e)
    {
        std::stringstream error;
        error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot read dew point using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: float";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        try
        {
            return strtof(&value.at(0), nullptr);
        }
        catch(const std::exception& e)
        {
            error.str("");
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot read dew point using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: float";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }       
}

//===========================================================================
void ControllerBoard::relayOn(const Device &device)
{
    // std::cout << __PRETTY_FUNCTION__ << "Turning on relay: " << device.getControllerConnector() << std::endl;
    // return;
    std::string command;
    if (device.getControllerConnector() == "RLY1")
        command = "[5011]";
    else if (device.getControllerConnector() == "RLY2")
        command = "[5021]";
    else if (device.getControllerConnector() == "RLY3")
        command = "[5031]";
    else if (device.getControllerConnector() == "RLY4")
        command = "[5041]";
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " is not a relay!");
    }
    std::string value = theProtocol_->readValue(command);
    if (value != "*")
    {
        std::stringstream error;
        error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot turn on relay using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: *";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        if (value != "*")
        {
            error.str("");
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot turn on relay using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }
}

//===========================================================================
void ControllerBoard::relayOff(const Device &device)
{
    // std::cout << __PRETTY_FUNCTION__ << "Turning off relay: " << device.getControllerConnector() << std::endl;
    // return;
    std::string command;
    if (device.getControllerConnector() == "RLY1")
        command = "[5010]";
    else if (device.getControllerConnector() == "RLY2")
        command = "[5020]";
    else if (device.getControllerConnector() == "RLY3")
        command = "[5030]";
    else if (device.getControllerConnector() == "RLY4")
        command = "[5040]";
    else
    {
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << " Throwing an exception!" << std::endl;            
        throw std::runtime_error("Device: " + device.getControllerConnector() + " is not a relay!");
    }
    std::string value = theProtocol_->readValue(command);
    if (value != "*")
    {
        std::stringstream error;
        error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "1]Cannot turn off relay using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: *";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        if (value != "*")
        {
            error.str("");
            error << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__  << "2]Cannot turn off relay using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }
}

//===========================================================================
void ControllerBoard::writeChiller(const std::string &command) {}

//===========================================================================
std::string ControllerBoard::readChiller(const std::string &command) { return "ok"; }

//===========================================================================
std::string ControllerBoard::executeCommand(std::string command)
{
    // std::cout << __PRETTY_FUNCTION__ << "Executing command: " << command << std::endl;
    std::string value = theProtocol_->readValue(command);
    // std::cout << __PRETTY_FUNCTION__ << "Command returned: " << value << std::endl;
    return value;
}

//===========================================================================
std::string ControllerBoard::useDESYChiller(const Device &device, std::string command, int attempts)
{
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDESY begin: " << device.getControllerConnector() << " Command: " << command << std::endl;
    // Open the serial port
    int fd = open(device.getControllerConnector().c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd == -1)
    {
        if(attempts <= 0)
        {
            std::string error = "ERROR: after 10 attempts to send the command: " + command + " to the DESY chiller, I had trouble opening the serial port " + device.getControllerConnector() + ". I am giving up and throw an exception.";
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << error << std::endl;
            throw std::runtime_error(error);
        }
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tERROR: can't open serial port " <<  device.getControllerConnector() << "...retrying " << attempts-1 << " more times." << std::endl;
        return useDESYChiller(device, command, --attempts);
    }

    // Configure the serial port
    termios options;
    tcgetattr(fd, &options);
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    //    options.c_cflag &= ~PARENB;
    //    options.c_cflag &= ~CSTOPB;
    //    options.c_cflag &= ~CSIZE;
    //    options.c_cflag |= CS8;
    //    options.c_cflag &= ~CRTSCTS;
    //    options.c_cflag |= CREAD | CLOCAL;
    //    options.c_iflag &= ~(IXON | IXOFF | IXANY);
    //    options.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    //    options.c_oflag &= ~OPOST;
    //    options.c_cc[VMIN] = 0;
    //    options.c_cc[VTIME] = 10;
    //    tcsetattr(fd, TCSANOW, &options);

    // Send a command to the device
    // std::string command = "{M00****\r\n";//THIS IS AREAL DESY CHILLER COMMAND
    //std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Command: " << command << " : " << command.length() << std::endl;
    write(fd, command.c_str(), command.length());
    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tWritten" << std::endl;

    // Read the response from the device
    char buffer[1024];

    //    std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Reading" << std::endl;
    int bytes_read = 0;
    unsigned timeout = 0;
    for (timeout = 0; timeout < 10 && bytes_read <= 0; timeout++)
    {
        usleep(200000);
        bytes_read = read(fd, buffer, sizeof(buffer) - 1);
        //	std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Buffer: " <<  buffer << " : " << bytes_read << std::endl;
    }
    // Close the serial port
    close(fd);

    //std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Buffer: " << buffer << std::endl;
    if (bytes_read == -1)
    {
        if(attempts <= 0)
        {
            std::string error = "ERROR: after 10 attempts to send the command: " + command + " to the DESY chiller, I had trouble reading from the serial port " + device.getControllerConnector() + ". I am giving up and throw an exception.";
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << error << std::endl;
            throw std::runtime_error(error);
        }
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tERROR: can't read from serial port " <<  device.getControllerConnector() << "...retrying " << attempts-1 << " more times." << std::endl;
        return useDESYChiller(device, command, --attempts);
    }
    else if (bytes_read != 10)
    {
        if(attempts <= 0)
        {
            std::string error = "ERROR: after 10 attempts to send the command: " + command + " to the DESY chiller, I never got a response from it, so I am giving up and throw an exception.";
            std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\t" << error << std::endl;
            throw std::runtime_error(error);
        }
        std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tI didn't receive the 10 bytes of reply that I am expecting and timed out...retrying " << attempts-1 << " more times." << std::endl;
        return useDESYChiller(device, command, --attempts);
    }
    buffer[bytes_read] = '\0';

    std::cout << "[" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tDESY end. Buffer: " << buffer  << " Bytes read: " << bytes_read << std::endl;
    return buffer;
}
