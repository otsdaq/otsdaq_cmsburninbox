/*
 * BurninBoxTemperatureSequence.cc
 *
 *  Created on: Sep 13, 2018
 *      Author: otsdaq
 */

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/BurninBoxTemperatureSequence.h"
#include "BurninBox/BurninBoxUtils/SafetyRangeDefinition.h"
#else
#include "BurninBoxTemperatureSequence.h"
#include "SafetyRangeDefinition.h"
#endif

#include <iostream>

using namespace ots;

//========================================================================================================================
BurninBoxTemperatureSequence::BurninBoxTemperatureSequence()
: errorTemperatureName_ ("ErrorTemperature")
, errorTemperature_(errorTemperatureName_, SafetyRangeDefinition::safeTemperatureValue, 0, -1)
{
}

//========================================================================================================================
BurninBoxTemperatureSequence::~BurninBoxTemperatureSequence()
{
}

//========================================================================================================================
void BurninBoxTemperatureSequence::operator=(const BurninBoxTemperatures &temperature)
{
    temperatures_ = temperature.getTemperatures();
    errorTemperature_ = Temperature(errorTemperatureName_, SafetyRangeDefinition::safeTemperatureValue, 0, -1);
    resetSequence();
}

//========================================================================================================================
void BurninBoxTemperatureSequence::next()
{
    ++currentTemperature_;
    if(currentTemperature_ == sequenceToTemperature_.end())
        throw std::runtime_error("Last element of the sequence was already reached previously!");
}

//========================================================================================================================
void BurninBoxTemperatureSequence::gotoTemperature(std::string name)
{
    if(BurninBoxTemperatures::temperatures_.find(name) == BurninBoxTemperatures::temperatures_.end())
    {
        std::cout << " [" << __LINE__ << "] " << __PRETTY_FUNCTION__ << "\tTemperature " + name + " is not in the list of configured temperatures." << std::endl;
        throw std::runtime_error("Temperature " + name + "is not in the list of configured temperatures.");
    }
    
    for(auto it=sequenceToTemperature_.begin(); it!=sequenceToTemperature_.end(); it++)
        if(it->second->name == name)
        {
            currentTemperature_ = it;
            break;
        }
}

//========================================================================================================================
void BurninBoxTemperatureSequence::gotoLast()
{
    currentTemperature_ = std::prev(std::prev(sequenceToTemperature_.end()));
}

//========================================================================================================================
void BurninBoxTemperatureSequence::gotoError()
{
    currentTemperature_ = std::prev(sequenceToTemperature_.end());
}

//========================================================================================================================
void BurninBoxTemperatureSequence::resetSequence()
{
    sequenceToTemperature_.clear();
    //Insert temperatures that have a sequence number
    unsigned int lastSequenceNumber = 0;
    for(auto& temperature: temperatures_)
    {
        if(temperature.second.sequence != (unsigned)-1)
        {
            sequenceToTemperature_[temperature.second.sequence] = &temperature.second;
            if(lastSequenceNumber < temperature.second.sequence)
                lastSequenceNumber = temperature.second.sequence;
        }
    }
    //Insert temperatures that DO NOT HAVE a sequence number after the last sequence number added
    for(auto& temperature: temperatures_)
    {
        if(temperature.second.sequence == (unsigned)-1)
        {
            ++lastSequenceNumber;
            sequenceToTemperature_[lastSequenceNumber] = &temperature.second;
        }
    }
    
    sequenceToTemperature_[errorTemperature_.sequence] = &errorTemperature_;

    currentTemperature_ = sequenceToTemperature_.begin();
}

//========================================================================================================================
void BurninBoxTemperatureSequence::setTemperature(std::string name, double temperature, unsigned time, unsigned sequence)
{
    BurninBoxTemperatures::setTemperature(name, temperature, time, sequence);
    resetSequence();
}

//========================================================================================================================
void BurninBoxTemperatureSequence::setTemperature(std::string name, const Temperature& temperature )
{
    BurninBoxTemperatures::setTemperature(name, temperature);
    resetSequence();
}

//========================================================================================================================
void BurninBoxTemperatureSequence::setErrorTemperature(double temperature)
{
    errorTemperature_ = Temperature(errorTemperatureName_, temperature, 0, errorTemperatureSequenceNumber_);
    resetSequence();
}

//========================================================================================================================
double BurninBoxTemperatureSequence::getErrorTemperature(void) const
{
    return errorTemperature_.temperature;
}

//========================================================================================================================
double BurninBoxTemperatureSequence::getCurrentTemperature(void) const
{
    return currentTemperature_->second->temperature;
}

//========================================================================================================================
double BurninBoxTemperatureSequence::getLowestTemperature(void) const
{
    double lowestTemperature = 40;
    for(auto& temperature: temperatures_)
        if (temperature.second.temperature < lowestTemperature)
            lowestTemperature = temperature.second.temperature;

    return lowestTemperature;
}

//========================================================================================================================
double BurninBoxTemperatureSequence::getLastTemperature(void) const
{
    return sequenceToTemperature_.rbegin()->second->temperature;
}

//========================================================================================================================
unsigned BurninBoxTemperatureSequence::getCurrentTime(void) const
{
    return currentTemperature_->second->time;
}

//========================================================================================================================
unsigned BurninBoxTemperatureSequence::getCurrentSequenceNumber(void) const
{
    return currentTemperature_->second->sequence;
}

//========================================================================================================================
std::string BurninBoxTemperatureSequence::getCurrentName(void) const
{
    return currentTemperature_->second->name;
}

//========================================================================================================================
unsigned BurninBoxTemperatureSequence::getLastSequenceNumber(void) const
{
    return std::prev(std::prev(sequenceToTemperature_.end()))->second->sequence;
}    

//========================================================================================================================
unsigned BurninBoxTemperatureSequence::getNumberOfTemperatures(void) const
{
    return sequenceToTemperature_.size()-1;//Ignoring the error temperature!
}