#ifndef _ots_UDPNetworkSocket_h_
#define _ots_UDPNetworkSocket_h_

#include <string>
#include <netinet/in.h>

namespace ots
{

class UDPNetworkSocket
{
public:
	UDPNetworkSocket();
	UDPNetworkSocket(std::string IPAddress, unsigned int port);
	virtual ~UDPNetworkSocket(void);

	virtual void                initialize            (unsigned int socketReceiveBufferSize = defaultSocketReceiveSize_);
	const struct sockaddr_in&   getSocketAddress      (void);
	int                         getSocketNumber       (void);
	const std::string&          getIPAddress          (void);
	uint16_t                    getPort               (void);
	void                        send                  (UDPNetworkSocket toSocket, const std::string& buffer);
	void                        sendAndReceive        (UDPNetworkSocket toSocket, const std::string& buffer);
	int                         receive               (std::string& buffer, UDPNetworkSocket* sender=nullptr, unsigned int timeoutSeconds=5, unsigned int timeoutUSeconds=0);
	int                         receiveAndAcknowledge (std::string& buffer, UDPNetworkSocket* sender=nullptr, unsigned int timeoutSeconds=5, unsigned int timeoutUSeconds=0);
	void                        setSocketAddress      (struct sockaddr_in socketAddress);

protected:
	enum {maxSocketSize_ = 0x10000, defaultSocketReceiveSize_ = 0x10000};//0x10000=65536

	struct sockaddr_in socketAddress_;
	int                socketNumber_;
	std::string        IPAddress_;
	unsigned int       port_;

};

}

#endif
