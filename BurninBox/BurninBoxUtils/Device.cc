#include "Device.h"

#include <iostream>

using namespace ots;

//===========================================================================
Device::Device(void) : name_(""), controllerConnector_(""), displayName_(""), comment_(""), status_(false)
{
}

//===========================================================================
Device::Device(std::string name,
               std::string connector,
               std::string displayName,
               std::string comment,
               std::string status)
    : name_(name), controllerConnector_(connector), displayName_(displayName), comment_(comment)
{
    status_ = isOn(status);
}

//===========================================================================
Device::Device(const ConfigurationAttributes& attributes)
{
	setAttributes(attributes);
}

//===========================================================================
Device::~Device(void){ ; }

//===========================================================================
void Device::setAttributes(std::string name, std::string connector, std::string displayName, std::string comment, std::string status)
{
	name_ = name;
	controllerConnector_ = connector;
	displayName_ = displayName;
	comment_ = comment;
	status_ = isOn(status);
}

//===========================================================================
void Device::setAttributes(const ConfigurationAttributes& attributes) 
{
    if(attributes.find("Name") != attributes.end())
        name_ = attributes.find("Name")->second;
    else
        throw std::runtime_error("ERROR: Can't find property Name in device configuration.");

    if(attributes.find("ControllerConnector") != attributes.end())
        controllerConnector_ = attributes.find("ControllerConnector")->second;
    else
        throw std::runtime_error("ERROR: Can't find property Connector in device configuration.");

    if(attributes.find("DisplayName") != attributes.end())
        displayName_ = attributes.find("DisplayName")->second;
    else
        throw std::runtime_error("ERROR: Can't find property DisplayName in device configuration.");

    if(attributes.find("Comment") != attributes.end())
        comment_ = attributes.find("Comment")->second;
    else
        throw std::runtime_error("ERROR: Can't find property Comment in device configuration.");

    if(attributes.find("Status") != attributes.end())
	{
        status_ = isOn(attributes.find("Status")->second);
		std::cout << __PRETTY_FUNCTION__ << name_ << "Status: " << attributes.find("Status")->second << " bool: " << status_ << std::endl;
	}
    else
        throw std::runtime_error("ERROR: Can't find property Status in device configuration.");
}

//===========================================================================
std::string Device::getName(void) const { return name_; }

//===========================================================================
std::string Device::getControllerConnector(void) const { return controllerConnector_; }

//===========================================================================
std::string Device::getDisplayName(void) const { return displayName_; }

//===========================================================================
std::string Device::getComment(void) const { return comment_; }

//===========================================================================
bool Device::isOn(void) const { return status_; }
