#ifndef _ots_BeagleBoneClient_h_
#define _ots_BeagleBoneClient_h_

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/TCPNetworkClient.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"
#else
#include "TCPNetworkClient.h"
#include "BeagleBoneConfiguration.h"
#endif

#include <string>

namespace ots
{


class BeagleBoneClient: public TCPNetworkClient
{
public:

	BeagleBoneClient(const std::string& serverIP, int serverPort);
	virtual ~BeagleBoneClient(void);

	void halt                (void);
	void pause               (void);
	void resume              (void);
	void start               (std::string runNumber, std::string& statusBuffer);
	void stop                (void);
	void configure           (void);
	void setConfiguration    (float SetTemperature=20.0, float LowTolerance=1.0, float HighTolerance=1.0, float StopTemperature=22.0);
	std::string status       (void);
	std::string readConfig   (void);
	void interactiveCommand  (void); //settings frm terminal
	void getConfigFromInput  (bool color = true);
	void writeToFile         (std::string buffer);
	void printToScreen       (std::string buffer);
	std::string getRun       (void);
	bool isRunning           (void);
	bool isPaused            (void);
	void terminalInterface   (void); //supervised settings from terminal

protected:

	BeagleBoneConfiguration theBeagleBoneConfiguration_;
	void cinThread(std::string& statusBuffer);

};
}

#endif
