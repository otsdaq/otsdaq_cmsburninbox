#include "Thread.h"

#include <iostream>
#include <string>

using namespace ots;

//===========================================================================
Thread::Thread(void)
: stopThread_(false)
, theThread_ ()
{
}

//===========================================================================
Thread::~Thread(void)
{
	stop();
}

//===========================================================================
void Thread::start(void)
{
	theThread_ = std::thread(&Thread::threadMain,this);
}

//===========================================================================
void Thread::stop(void)
{
	stopThread_ = true;
	if(theThread_.joinable()) theThread_.join();
}
