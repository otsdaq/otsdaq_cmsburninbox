#include <stdio.h>
#include <string.h>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneClient.h"
#else
#include "BeagleBoneClient.h"
#endif

//WARNING THE STREAM DESTINATION IP AND PORT CAN BE OVERWRITTEN IF YOU GO THROUGH THE CONFIGURE STATE!
#define BEAGLEBONE_IP     "192.168.0.241"  // The destination IP of the datastream
#define BEAGLEBONE_PORT   5000           // The destination port of the datastream

using namespace ots;

//MAIN: Example usage of the class BeagleBoneClient
int main(int argc, char **argv)
{
	std::cout<< "start master" << std::endl;

	BeagleBoneClient theCommunication(BEAGLEBONE_IP,BEAGLEBONE_PORT);

	std::map<std::string,std::string> mapOfParams;
	std::map<std::string,std::string>::iterator it;

	//defaults
	mapOfParams["HighTolerance"]="20.0";
	mapOfParams["LowTolerance"]="1.0";
	mapOfParams["SetTemperature"]="1.0";
	mapOfParams["StopTemperature"]="20.0";
	mapOfParams["RunNumber"]="1";

	//parse argv
	int i=0;
	while(argv[i]!=NULL)
	{
		printf("%s is argv %d \n",argv[i],i);

		std::string s="";
		s=+argv[i];

		for (it = mapOfParams.begin(); it != mapOfParams.end(); it++)
		{
			if (!(s.find(it->first)))
			{
				std::cout<< "found parameter  " << it->first <<std::endl;
				size_t begin =(it->first).size()+1;
				size_t end = s.size();
				std::cout<<"value found for parameter "<<s.substr(begin,end-begin)<<std::endl;
				it->second = s.substr(begin,end-begin);
			}
		}

		i++;
	}


	for (it = mapOfParams.begin(); it != mapOfParams.end(); it++)
	{
		std::cout << it->first << " = " << mapOfParams[it->first] << std::endl;
	}


	//launch interface
	theCommunication.terminalInterface();
	return 1;

}
