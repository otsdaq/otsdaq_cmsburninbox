#ifndef _ots_DewPointSensor_h_
#define _ots_DewPointSensor_h_

#include <string>
#include <math.h>

#include "Device.h"
#include <tgmath.h>

namespace ots
{
class DewPointSensor : virtual public Device
{
public:
	DewPointSensor(std::string name, std::string id, std::string status)
: Device(name, id, status)
{;}
	~DewPointSensor(void){;}

	virtual float readDewPoint(void) = 0;

	float calculateTemperature(float humidity)
	{
	    float dewPoint = readDewPoint();
		return 243.04 * ( ( (17.625*dewPoint)/(243.04+dewPoint) ) - log(humidity/100) ) / ( 17.625 + log(humidity/100) - ( (17.625*dewPoint)/(243.04+dewPoint) ) );
	}

	float calculateHumidity(float temperature)
	{
	    float dewPoint = readDewPoint();
	    return  100 * ( exp( (17.625*dewPoint)/(243.04+dewPoint) ) / exp( (17.625*temperature)/(243.04+temperature) ) );
	}

};
}
#endif
