#ifndef _ots_AnalogSensor_h_
#define _ots_AnalogSensor_h_

//#include <string>
#include "Device.h"

namespace ots
{
  class AnalogSensor : virtual public Device
  {
  public:
	  AnalogSensor (std::string name, std::string id, std::string status);
	  ~AnalogSensor(void);
    
	  float readValue(std::string path);

  protected:
	  std::string sensorPath_;
  };
}
#endif
