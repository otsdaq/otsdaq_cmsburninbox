#include "AnalogSensor.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace ots;

//===========================================================================
AnalogSensor::AnalogSensor(std::string name, std::string id, std::string status)
: Device(name, id, status)
, sensorPath_("/sys/bus/iio/devices/iio:device0/" + id)
{
	std::ifstream file;
	std::string line;
	const std::string capemgrFileName = "/sys/devices/bone_capemgr.9/slots";

	file.open(capemgrFileName.c_str());
	if(!file.is_open())
	{
		std::cout << "IMPOSSIBLE: Could not open device configuration file: " << capemgrFileName << ". Crashing now..." << std::endl;
		exit(0);
	}
	bool found = false;
	while (getline(file,line))
	{
		if(line.find("Manuf,BB-ADC") != std::string::npos)
		{
			found = true;
			break;
		}
	}

	if(!found)
		system(("echo BB-ADC > " + capemgrFileName).c_str());
	file.close();
}

//===========================================================================
AnalogSensor::~AnalogSensor(void)
{;}

//===========================================================================
float AnalogSensor::readValue(std::string path)
{
	std::ifstream file;
	file.open(path.c_str());
	if(!file.is_open())
	{
		std::cout << "Could not open sensor: " << name_ << "." << std::endl;
		return 0;
	}

	std::string value = "0";
	getline(file,value);
	file.close();
	return strtof(value.c_str(), NULL)*1.800/4096.;
} 


