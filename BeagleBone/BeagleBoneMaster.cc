#include <stdio.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/UDPNetworkSocket.h"
#else
#include "UDPNetworkSocket.h"
#endif

#define COMMUNICATION_PORT 10000             // The port on THIS device to communicate with XDAQ

//WARNING THE STREAM DESTINATION IP AND PORT WILL BE OVERWRITTEN IF YOU GO THROUGH THE CONFIGURE STATE!
//#define BEAGLEBONE_IP     "192.168.0.241"  // The destination IP of the datastream
//#define BEAGLEBONE_PORT   5000          // The destination port of the datastream
#define BEAGLEBONE_IP     "192.168.0.100"  // The destination IP of the datastream
#define BEAGLEBONE_PORT   10001          // The destination port of the datastream

using namespace ots;


//MAIN
int main(int argc, char **argv)
{
	std::cout<<"start master"<<std::endl;
	UDPNetworkSocket theCommunication("192.168.0.100",COMMUNICATION_PORT);
	theCommunication.initialize();

	UDPNetworkSocket theBeagleBone(BEAGLEBONE_IP, BEAGLEBONE_PORT);

	//theCommunication.send(theBeagleBone, "START");
	theCommunication.sendAndReceive(theBeagleBone, "START");

	return 0;
}
