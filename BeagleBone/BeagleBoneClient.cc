#include <stdio.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <thread>
#include <pthread.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneServer.h"
#include "otsdaq_cmsburninbox/BeagleBone/BurninBoxController.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneClient.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneStatus.h"
#else
#include "BeagleBoneClient.h"
#include "BurninBoxController.h"
#include "BeagleBoneStatus.h"
#include "BeagleBoneConfiguration.h"
#include "BeagleBoneStatus.h"
#endif

using namespace ots;


//========================================================================================================================
BeagleBoneClient::BeagleBoneClient(const std::string& serverIP, int serverPort)
: TCPNetworkClient(serverIP, serverPort)
{
	//some defaults to be updated
	theBeagleBoneConfiguration_.setSetTemperature(1.);
	theBeagleBoneConfiguration_.setLowTolerance(1.);
	theBeagleBoneConfiguration_.setHighTolerance(1.);
	theBeagleBoneConfiguration_.setStopTemperature(1);

}
//========================================================================================================================
BeagleBoneClient::~BeagleBoneClient(void)
{
}
//========================================================================================================================
void BeagleBoneClient::halt(void)
{
	std::cout << "\n\tHalt --> to BeagleBone\n" << std::endl;
//	logFile_.close();
	send("STOP");
}
//========================================================================================================================
void BeagleBoneClient::pause(void)
{
	std::cout << "\n\tPause --> to BeagleBone\n" << std::endl;
	send("PAUSE");
}
//========================================================================================================================
void BeagleBoneClient::resume(void)
{
	std::cout << "\n\tResume --> to BeagleBone\n" << std::endl;
	send("RESUME");
}
//========================================================================================================================
void BeagleBoneClient::start(std::string runNumber,std::string& statusBuffer)
{
	std::cout << "\n\tStart --> to BeagleBone\n" << std::endl;
	send("START:{RunNumber:" + runNumber + "}");
	sleep(3);
	receive(statusBuffer);

}
//========================================================================================================================
void BeagleBoneClient::stop(void)
{
	std::cout << "\n\tStop --> to BeagleBone\n" << std::endl;
	send("STOP");
}


//========================================================================================================================
void BeagleBoneClient::setConfiguration(float SetTemperature, float LowTolerance, float HighTolerance, float StopTemperature)
{
	//some defaults to be updated
	theBeagleBoneConfiguration_.setSetTemperature(SetTemperature);
	theBeagleBoneConfiguration_.setLowTolerance(LowTolerance);
	theBeagleBoneConfiguration_.setHighTolerance(HighTolerance);
	theBeagleBoneConfiguration_.setStopTemperature(StopTemperature);

}

//========================================================================================================================
void BeagleBoneClient::configure(void)
{
	std::string command	= theBeagleBoneConfiguration_.convertToJSON();
	std::cout << "\n\t" << command << "" <<std::endl; //debug
	std::cout  << "\n\tConfigure --> to BeagleBone\n" << std::endl;
	send(command);
    //ref: otsdaq/srcs/otsdaq_cmsburninbox/otsdaq-cmsburninbox/FEInterfaces/FECMSBurninBoxInterface
}

//========================================================================================================================
std::string BeagleBoneClient::status(void)
{
	std::string statusBuffer;
	send("STATUS?");
	sleep(3);
	receive(statusBuffer);
	sleep(1);
	writeToFile(statusBuffer);
	return statusBuffer;
}

//========================================================================================================================
std::string BeagleBoneClient::readConfig(void)
{
	BeagleBoneConfiguration localConfiguration;
	std::string configBuffer;
	send("CONFIG?");
	sleep(1);
	receive(configBuffer);
	sleep(1);
	writeToFile(configBuffer);
	return configBuffer;
}

//========================================================================================================================
void BeagleBoneClient::interactiveCommand(void)
{

	std::cout << "\n\t Type command: " << std::endl;
	std::string readBuffer;
	std::cin>>readBuffer;
	std::string outBuffer;

	if (readBuffer.substr(0,5) == "START") // start: get runNumber
	{
		std::cout << "\n\t (re)-type Run Number: " << std::endl;
		std::string runNum;
		std::cin>>runNum;
		bool isNumber = true;
		for(std::string::const_iterator k = runNum.begin(); k != runNum.end(); ++k)
			isNumber = isNumber && isdigit(*k);
		if (isNumber)
			start(runNum,outBuffer);
		else
			std::cout << "\n\t !! command not valid" << std::endl;
	}
	else if (readBuffer.substr(0,4) == "Stop")
		stop();
	else if (readBuffer.substr(0,6) == "Resume")
		resume();
	else if (readBuffer.substr(0,5) == "Pause")
		pause();
	else if (readBuffer.substr(0,4) == "Halt")
		halt();
	else if (readBuffer.substr(0,4) == "Run?")
		std::cout<< getRun() << std::endl;
	else if (readBuffer.substr(0,11) == "Reconfigure")//reconfigure to defaults
	{
		setConfiguration();
		configure();
	}
	else if (readBuffer.substr(0,9) == "Configure")//configure with keyboard input
	{
		getConfigFromInput();
	}
	else if (readBuffer.substr(0,7) == "Status?")//check status
		std::cout<< status() << std::endl;
	else if (readBuffer.substr(0,7) == "Config?")//check configuration
		std::cout<< readConfig() << std::endl;
	else if (readBuffer.substr(0,4) == "Quit")
	{
		std::cout<< "Exit now" << std::endl;
		exit(0);
	}
	else
		std::cout << "command not valid" << std::endl;
}

void BeagleBoneClient::getConfigFromInput(bool color)
{
	float setTemperature, LowTolerance, HighTolerance, StopTemperature;

	//temperature
	if (color)
		std::cout << "\n\t\033[1;32mSetTemperature\033[0m  :  " ;
	else
		std::cout << "\n\tSetTemperature  :  " ;
	std::cin>>setTemperature;
	if (std::cin.fail())
	{
		std::cout << "\tFAIL: -> default" << std::endl;
		std::cin.clear(); std::cin.ignore();
		setTemperature=20;
	}

	//low tolerance
	if (color)
		std::cout << "\n\t\033[1;32mLowTolerance\033[0m  :  " ;
	else
		std::cout << "\n\tLowTolerance  :  " ;
	std::cin>>LowTolerance;
	if (std::cin.fail())
	{
		std::cout << "\tFAIL: -> default" << std::endl;
		std::cin.clear(); std::cin.ignore();
		LowTolerance=1;
	}

	//high tolerance
	if (color)
		std::cout << "\n\t\033[1;32mHighTolerance\033[0m  :  " ;
	else
		std::cout << "\n\tHighTolerance  :  " ;
	std::cin>>HighTolerance;
	if (std::cin.fail())
	{
		std::cout << "\tFAIL: -> default" << std::endl;
		std::cin.clear(); std::cin.ignore();
		HighTolerance=1;
	}

	//stop temperature
	if (color)
		std::cout << "\n\t\033[1;32mStopTemperature\033[0m  :  " ;
	else
		std::cout << "\n\tStopTemperature  :  " ;
	std::cin>>StopTemperature;
	if (std::cin.fail())
	{
		std::cout << "\tFAIL: -> default" << std::endl;
		std::cin.clear(); std::cin.ignore();
		StopTemperature=20;
	}

	//configuration
	setConfiguration(setTemperature, LowTolerance, HighTolerance ,StopTemperature);
	configure();

}

//========================================================================================================================
void BeagleBoneClient::writeToFile(std::string buffer)
{
	std::string runBuffer=getRun();
	std::ofstream logFile_("logfile_Run"+runBuffer+".txt", std::fstream::in | std::fstream::out | std::fstream::app);

	time_t now = time(0);
	char* dateAndTime = ctime(&now);
	logFile_ << "The local date and time is: " << dateAndTime;
	logFile_ << buffer << "; " << std::endl;

	if (buffer.substr(0,6) == "STATUS")
	{
		BeagleBoneStatus localStatus;
		localStatus.convertFromJSON(buffer);
		localStatus.printToFile(logFile_);
	}
	else if (buffer.substr(0,13) == "CONFIGURATION")
	{
		BeagleBoneConfiguration localConfiguration;
		localConfiguration.convertFromJSON(buffer);
		localConfiguration.printToFile(logFile_);
	}
	else
	{
		logFile_ << buffer << "; " << std::endl;
	}

	logFile_.close();

}

//========================================================================================================================
void BeagleBoneClient::printToScreen(std::string buffer)
{
	std::string runBuffer=getRun();

	time_t now = time(0);
	char* dateAndTime = ctime(&now);

	std::cout << "\tRun Number: " << runBuffer << std::endl;
	std::cout << "\tThe local date and time is: " << dateAndTime << std::endl;

	if (buffer.substr(0,6) == "STATUS")
	{
		BeagleBoneStatus localStatus;
		localStatus.convertFromJSON(buffer);
		localStatus.printToScreen();
	}
	else if (buffer.substr(0,13) == "CONFIGURATION")
	{
		BeagleBoneConfiguration localConfiguration;
		localConfiguration.convertFromJSON(buffer);
		localConfiguration.printToScreen();
	}
	else
	{
		std::cout << buffer << "; " << std::endl;
	}
}

//========================================================================================================================
std::string  BeagleBoneClient::getRun(void)
{
	std::string buffer;
	send("Run?");
	sleep(1);
	receive(buffer);
	return buffer;
}

//========================================================================================================================
bool BeagleBoneClient::isRunning(void)
{
	std::string buffer;
	send("running?");
	sleep(1);
	receive(buffer);
	if (buffer=="running")
		return true;
	else
		return false;
}

//========================================================================================================================
bool BeagleBoneClient::isPaused(void)
{
	std::string buffer;
	send("paused?");
	sleep(1);
	receive(buffer);
	if (buffer=="paused")
		return true;
	else
		return false;
}

//========================================================================================================================
void BeagleBoneClient::terminalInterface(void)
{
	bool updating = false;

	system ("clear");
	std::cout<< "\n\n\t\033[1;44m####################################\033[0m" <<std::endl;
	std::cout<< "\t\033[1;44m##### Interface to BeagleBone ######\033[0m" <<std::endl;
	std::cout<< "\t\033[1;44m####################################\033[0m\n\n" <<std::endl;

	std::cout << "\t\033[1;34mrunning on machine:\033[0m " << std::endl; //get local ip if you can
	std::cout << "\t\033[1;34mbeaglebone at IP:\033[0m " << serverIP_ << "\n\n" << std::endl;

	send("ciao"); //to start the connection

	if (isConnected())
		std::cout << "\n\t\033[1;44mcheck connection --> BB connected!\033[0m\n" <<  std::endl;
	else
	{
		std::cout << "\n\033[1;44mcheck connection --> BB not connected\033[0m\n" <<  std::endl;
	}

	//what to do ?
	//if running get status and config
	//pause halt stop?

	bool quit=false;
	while(isConnected())
	{
		if (quit)
		{
			std::cout<<"\n\t\033[1;34mType QUIT to confirm \033[0m\t" ;
			std::string readBuffer;
			std::cin>>readBuffer;
			std::cout << '\n';
			if (readBuffer.substr(0,4) == "QUIT")
			{
				std::cout<< "\n\n\t\033[1;44m####################################\033[0m" <<std::endl;
				std::cout<< "\t\033[1;44m####### Exiting Interface ### ######\033[0m" <<std::endl;
				std::cout<< "\t\033[1;44m####################################\033[0m\n\n" <<std::endl;
				return;
			}
			else
				quit=false;
		} //confirm QUIT

		std::string readBuffer="";

		if (!updating)
		{
			std::cout <<"\t\033[1;34mcheck beaglebone? (Y) \033[0m\t";

			//std::cin>>readBuffer;
			//no timeout would be cin. timeout with timed thread

			std::thread thrd(&BeagleBoneClient::cinThread, this, std::ref(readBuffer));
			auto thrd_id = thrd.native_handle();
			thrd.detach();
			std::cout << "\n\t";
			sleep(2);
			std::cout << "\n\t----  \n";//timeout
			pthread_cancel(thrd_id);

			if (readBuffer.size()<1)
				readBuffer="Y"; //default, keep checking status while connected

		}
		else
			readBuffer="Y";


		if (readBuffer.substr(0,1) != "Y")
		{
			quit=true;
			continue; //check connection and restart loop
		}

		if (isRunning())
		{
			if (!updating)
				system ("clear");
			else
				printf("\033[%d;%dH", 0, 0);
				//using escape sequence to refresh screen in the same position
				//https://linux.die.net/man/4/console_codes
				//only xterm and VT100 compatible

			updating=true;

			std::cout<<"\n\t\033[1;34mthe BeagleBone is running\033[0m\n"<<std::endl;
			std::cout << "\tchecking current state...\n";
			std::string buffer=status();
			printToScreen(buffer);
			buffer = readConfig();
			printToScreen(buffer);

			unsigned int N_max=5;
			for (unsigned int n=0; n<N_max; n++)
			{
				std::cout<<"\n\t\033[1;41mSTOP \033[0m or \033[1;41m PAUSE \033[0m the Run\n"<<std::endl;
				std::cout<<"\n\tType STOP, PAUSE or QUIT \n\n\t";

				std::string readBuffer="";
				//std::cin>>readBuffer;
				//using thread instead of cin
				std::thread thrd(&BeagleBoneClient::cinThread, this, std::ref(readBuffer));
				auto thrd_id = thrd.native_handle();
				thrd.detach();
				std::cout << "\n\t";
				sleep(10);
				pthread_cancel(thrd_id);
				std::cout<<"\n";

				if (readBuffer.size()<1)
				{
					break;
				}
				if (readBuffer.substr(0,4) == "STOP")
				{
					updating=false;
					stop();
					break;
				}
				else if (readBuffer.substr(0,5) == "PAUSE")
				{
					updating=false;
					pause();
					break;
				}
				else if (readBuffer.substr(0,4) == "QUIT")
				{
					updating=false;
					quit=true;
					break;
				}
				else
				{
					updating=false;
					std::cout << "\t   command not valid" << std::endl;
					if (n==4) std::cout << "  ... " << std::endl;
				}
			}
		} // if (isPaused())

		else if (isPaused())
		{
			updating=false;

			system ("clear");
			std::cout<<"\n\t\033[1;34mthe BeagleBone is paused \033[0m\n"<<std::endl;
			std::cout << "\tchecking current state...\n";
			std::string buffers=status();
			printToScreen(buffers);
			std::string buffer = readConfig();
			printToScreen(buffer);

			unsigned int N_max=5;
			for (unsigned int n=0; n<N_max; n++)
			{
				std::cout<<"\n\t\033[1;41mSTOP \033[0m or \033[1;41m RESUME \033[0m the Run\n"<<std::endl;
				std::cout<<"\n\tType STOP, RESUME or QUIT \n\n\t";
				std::string readBuffer;
				std::cin>>readBuffer;
				if (readBuffer.substr(0,4) == "STOP")
				{
					stop();
					break;
				}
				else if (readBuffer.substr(0,6) == "RESUME")
				{
					resume();
					break;
				}
				else if (readBuffer.substr(0,4) == "QUIT")
				{
					quit=true;
					break;
				}
				else
				{
					std::cout << "\t   command not valid" << std::endl;
					if (n==4)
						std::cout << " ... " << std::endl;
				}
			}

		} // if (isPaused())

		else
		{
			updating=false;

			system ("clear");
			std::cout<<"\n\t\033[1;34mthe BeagleBone is not running \033[0m\n"<<std::endl;

			std::cout<<"\n\t\033[1;34mcurrent configuration \033[0m\n"<<std::endl;
			std::string buffer = readConfig();
			printToScreen(buffer);

			std::cout<<"\n\t\033[1;44mCONFIGURE \033[0m then \033[1;41m START \033[0m the Run\n"<<std::endl;
			std::cout<<"\n\tType \033[1;41m QUIT \033[0m or start configuration \n\n\t";
			std::string readBuffer;

			std::cin>>readBuffer;
			if (readBuffer.substr(0,4) == "QUIT")
			{
			quit=true;
			continue;
			}
			else getConfigFromInput();
                       
			std::cout <<"\t\033[1;34mstart now? (Y) \033[0m\t";
			std::cin>>readBuffer;

			if (readBuffer.substr(0,1) == "Y")
			{
				std::cout << "\n\t\033[1;34mtype Run Number: \033[0m   ";
				std::string runNum;
				std::cin>>runNum;
				bool isNumber = true;

				for(std::string::const_iterator k = runNum.begin(); k != runNum.end(); ++k)
					isNumber = isNumber && isdigit(*k);

				if (isNumber)
				{
					std::string outBuffer;
					start(runNum,outBuffer);
				}
				else std::cout << "\n\t  !! command not valid" << std::endl;

			}

		} // not running

	}

	std::cout << "\n\t\033[1;44mReturning for no connection  \033[0m\n" <<  std::endl;

}

//========================================================================================================================
void BeagleBoneClient::cinThread(std::string& buffer)
{
	std::cin>>buffer;
	return;
}
