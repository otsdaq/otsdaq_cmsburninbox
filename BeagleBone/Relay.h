#ifndef _ots_Relay_h_
#define _ots_Relay_h_

#include "Device.h"

#include <string>
#include <fstream>

namespace ots
{
class Relay : public Device
{
public:
	Relay(std::string name, std::string id, std::string status);
	~Relay(void);

	void on    (void);
	void off   (void);
	bool isOn  (void);
	bool isOff (void);

private:
	void writeValue(std::string value);

	std::string  sensorPath_;
	std::string  directionPath_;
	std::string  valuePath_;
};
}
#endif
