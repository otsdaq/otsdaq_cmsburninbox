#ifndef _ots_PT100RTD_h_
#define _ots_PT100RTD_h_

#include "AnalogSensor.h"
#include "TemperatureSensor.h"
#include <string>

namespace ots
{
class PT100RTD : public TemperatureSensor, public AnalogSensor
{
public:
	PT100RTD(std::string name, std::string id, std::string status);
	~PT100RTD(void);

	float readTemperature(void);

private:
};
}
#endif
