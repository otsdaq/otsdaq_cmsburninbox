#include <stdio.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/TCPNetworkServer.h"
#else
#include "TCPNetworkServer.h"
#endif

#define COMMUNICATION_PORT 10000             // The port on THIS device to communicate with XDAQ

#define BEAGLEBONE_IP     "192.168.0.241"  // The destination IP of the datastream
#define BEAGLEBONE_PORT   10001          // The destination port of the datastream

using namespace ots;

class newServer: public TCPNetworkServer
{
public:

	newServer(unsigned int listenPort, int bufferSize = 0x10000)
		: TCPNetworkServer(listenPort, bufferSize)  {}

	std::string readMessage(const std::string& buffer)

	{

		if (buffer=="START")
			return "got a start";
		else if (buffer=="END")
			return "";
		else
			return " error message";

	}

};


//MAIN
int main(int argc, char **argv)
{
	std::cout<<"start test"<<std::endl;

	newServer theBeagleBone(BEAGLEBONE_PORT);

	std::string buffer;
	while(1)
	{
		theBeagleBone.accept(1, 0);
		//theBeagleBone.send("received");
	}
	//theCommunication.send(theBeagleBone, "START");
	//theBeagleBone.send("START");

	return 0;
}
