//#include <string.h>
#include <fcntl.h>
#include <unistd.h> //usleep();
#include <string>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <map>
#include <time.h>
#include <sys/time.h>

//LORE
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <ctype.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/JsonConverter.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneStatus.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/UDPNetworkSocket.h"
#else
#include "JsonConverter.h"
#include "BeagleBoneStatus.h"
#include "BeagleBoneConfiguration.h"
#include "UDPNetworkSocket.h"
#endif

#include "BurninBoxController.h"
#include "DeviceController.h"
#include "SensorMapping.h"

#define COMMUNICATION_PORT "5000"             // The port on THIS device to communicate with XDAQ
#define STREAMING_PORT     "5001"             // The port on THIS device to stream to XDAQ

//WARNING THE STREAM DESTINATION IP AND PORT WILL BE OVERWRITTEN IF YOU GO THROUGH THE CONFIGURE STATE!
#define STREAM_DESTINATION_IP     "192.168.133.100"  // The destination IP of the datastream
#define STREAM_DESTINATION_PORT   47003              // The destination port of the datastream

#define MAXBUFLEN          1492

std::ofstream logFile_;

//========================================================================================================================
// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

//========================================================================================================================
int makeSocket(const char* ip, const char* port, struct addrinfo*& addressInfo)
{
	int socketId = 0;
	struct addrinfo hints, *servinfo, *p;
	//int sendSockfd=0;
	int rv;
	//int numbytes;
	//struct sockaddr_storage their_addr;
	//char buff[MAXBUFLEN];
	//socklen_t addr_len;
	//char s[INET6_ADDRSTRLEN];

	memset(&hints, 0, sizeof hints);
	//    hints.ai_family   = AF_UNSPEC; // set to AF_INET to force IPv4
	hints.ai_family   = AF_INET; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	if(ip == NULL)
		hints.ai_flags    = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(ip, port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((socketId = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}

		if (bind(socketId, p->ai_addr, p->ai_addrlen) == -1) {
			close(socketId);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}
	freeaddrinfo(servinfo);
	return socketId;
}

//========================================================================================================================
struct sockaddr_in setupSocketAddress(const char* ip, unsigned int port)
{
	//std::cout << __PRETTY_FUNCTION__ << std::endl;
	//network stuff
	struct sockaddr_in socketAddress;
	socketAddress.sin_family = AF_INET;// use IPv4 host byte order
	socketAddress.sin_port   = htons(port);// short, network byte order

	if(inet_aton(ip, &socketAddress.sin_addr) == 0)
	{
		std::cout << "FATAL: Invalid IP address " <<  ip << std::endl;
		exit(0);
	}

	memset(&(socketAddress.sin_zero), '\0', 8);// zero the rest of the struct
	return socketAddress;
}

//========================================================================================================================
int send(int toSocket, struct sockaddr_in& toAddress, const std::string& buffer)
{
	int sendtoReturnCode = sendto(toSocket, buffer.c_str(), buffer.size(), 0, (struct sockaddr *)&(toAddress), sizeof(sockaddr_in));
	//	if (sendto(toSocket, buffer.c_str(), buffer.size(), 0, (struct sockaddr *)&(toAddress), sizeof(sockaddr_in)) < (int)(buffer.size()))
	if ( sendtoReturnCode < (int)(buffer.size()) )
	{
		std::cout << "Error writing buffer" << std::endl;
		return -1;
	}
	return 0;
}

//========================================================================================================================
int receive(int fromSocket, struct sockaddr_in& fromAddress, std::string& buffer)
{
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 1000; //set timeout period for select()
	fd_set fileDescriptor;  //setup set for select()
	FD_ZERO(&fileDescriptor);
	FD_SET(fromSocket,&fileDescriptor);
	select(fromSocket+1, &fileDescriptor, 0, 0, &tv);

	if(FD_ISSET(fromSocket,&fileDescriptor))
	{
		std::string bufferS;
		socklen_t addressLength = sizeof(fromAddress);
		int nOfBytes;
		buffer.resize(MAXBUFLEN);
		if ((nOfBytes=recvfrom(fromSocket, &buffer[0], MAXBUFLEN, 0, (struct sockaddr *)&fromAddress, &addressLength)) != -1)
			buffer.resize(nOfBytes);
		return nOfBytes;
		//char address[INET_ADDRSTRLEN];
		//inet_ntop(AF_INET, &(fromAddress.sin_addr), address, INET_ADDRSTRLEN);
		//unsigned long  fromIPAddress = fromAddress.sin_addr.s_addr;
		//unsigned short fromPort      = fromAddress.sin_port;
	}
	else
		return -1;
}

//========================================================================================================================
int receiveAndAcknowledge(int fromSocket, struct sockaddr_in& fromAddress, std::string& buffer)
{
	int returnValue = receive(fromSocket,fromAddress,buffer);
	if(returnValue >= 0)
		send(fromSocket, fromAddress, buffer);

	return returnValue;
}

//========================================================================================================================
std::string getVariableValue(std::string variable, std::string buffer)
{
	size_t begin = buffer.find(variable)+variable.size()+1;
	size_t end   = buffer.find(',', begin);
	if(end == std::string::npos)
		end = buffer.size()-1;
	//std::cout << variable << " : " << buffer.substr(begin,end-begin) << std::endl;
	return buffer.substr(begin,end-begin);
}

using namespace ots;

/////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv){

	std::cout << "Running BeagleBone!" << std::endl;
	DeviceController theDeviceController;
	SensorMapping    theSensorConfiguration;

	theSensorConfiguration.printMapping();

	theDeviceController.setupDevices(theSensorConfiguration.getSensorList());

	BurninBoxController     theBurninBoxController(theDeviceController);

	BeagleBoneConfiguration theBeagleBoneConfiguration;
	BeagleBoneStatus        theBeagleBoneStatus;

	//LORE
	/////////////////////
	// Bind UDP socket //
	/////////////////////

	//sendSockfd = makeSocket(string("localhost").c_str(),myport,p);

	//struct addrinfo hints, *servinfo;
	struct addrinfo* p;

	int communicationSocket              = makeSocket(0,COMMUNICATION_PORT,p);
	int streamingSocket                  = makeSocket(0,STREAMING_PORT,p);
	struct sockaddr_in messageSender;
	struct sockaddr_in streamingReceiver; //= setupSocketAddress(STREAM_DESTINATION_IP, STREAM_DESTINATION_PORT);
	std::string communicationBuffer;
	//unsigned int data_buffer[32];
	//END LORE

	//LORE
	//float              setTemperature    = 15;
	//float              tolerance         = 0.2;
	//float              stopTemperature   = 20;
	//std::string        streamToIPAddress = STREAM_DESTINATION_IP;
	//unsigned int       streamToPort      = STREAM_DESTINATION_PORT;

	std::string currentRun;
	struct timeval currentTime;
	//double startTime;
	//double keepTime = 300;
	bool   running  = false;
	/*bool   target1  = false;
	bool   keep1    = false;
	bool   target2 = false;
	bool   keep2 = false;
	bool   target3 = false;*/

	theBurninBoxController.setStateMachineStatus(theBurninBoxController.STARTING);

	logFile_.open("logFile.txt");

	while(1)
	{
		communicationBuffer = "";
		if (receive(communicationSocket, messageSender, communicationBuffer) >= 0)
		{
			std::cout << "Received: " << communicationBuffer << std::endl;
			if(communicationBuffer.find("?") == std::string::npos)
				send(communicationSocket,messageSender,communicationBuffer); //Send back Acknowledgment if it is not a read!

			if (communicationBuffer.substr(0,5) == "START") //changing the status changes the mode in threadMain (BBC) function.
			{
				currentRun = getVariableValue("RunNumber", communicationBuffer);
				//theBurninBoxController.switchThermalRelays(false);
				theBurninBoxController.setTargetTemperature(theBeagleBoneConfiguration.getSetTemperature(),theBeagleBoneConfiguration.getLowTolerance(), theBeagleBoneConfiguration.getHighTolerance());
				running = true;
				std::cout << "Run " << currentRun << " started!" << std::endl;
				//theBurninBoxController.setTargetTemperature(10,0.2);
				theBurninBoxController.setStateMachineStatus(theBurninBoxController.STARTING);
				theBurninBoxController.setTemperatureStatus(theBurninBoxController.STARTUP);
			}
			else if (communicationBuffer == "STOP")
			{
				//We need to think :)
				theBurninBoxController.setTargetTemperature(theBeagleBoneConfiguration.getStopTemperature(),1,1);
				theBurninBoxController.switchThermalRelays(false);
				//--->Don't forget that the dry air flux relay is still working
				theBurninBoxController.setStateMachineStatus(theBurninBoxController.STOPPING);
				running = false;
				theBurninBoxController.setEnableTemperature(false);
				std::cout << "Run " << currentRun << " stopped!" << std::endl;
			}
			else if (communicationBuffer == "PAUSE")
			{
				//We need to think :)
				//--->Maybe the temperature could be kept until start button is pressed
				//running = false;
				//theBurninBoxController.switchThermalRelays(false);
			}
			else if (communicationBuffer == "RESUME")
			{
				//We need to think :)
				//running = true;
			}
			//CONFIGURE
			else if (communicationBuffer.substr(0,theBeagleBoneConfiguration.getClassName().size()) == theBeagleBoneConfiguration.getClassName())
			{
				theBeagleBoneConfiguration.convertFromJSON(communicationBuffer);
				theBurninBoxController.setTargetTemperature(theBeagleBoneConfiguration.getSetTemperature(),theBeagleBoneConfiguration.getLowTolerance(), theBeagleBoneConfiguration.getHighTolerance());
				streamingReceiver = setupSocketAddress(theBeagleBoneConfiguration.getStreamToIPAddress().c_str(), theBeagleBoneConfiguration.getStreamToPort());
				theBurninBoxController.setStateMachineStatus(theBurninBoxController.CONFIGURE);
			}
			else if (communicationBuffer == "STATUS?")
			{
				send(communicationSocket, messageSender, theBeagleBoneStatus.convertToJSON());
			}

		//Emanuele Aucone
		if(running)
		{
			gettimeofday(&currentTime, nullptr);
			theBurninBoxController.running();
			theBeagleBoneStatus.setTime(currentTime.tv_sec);
			theBeagleBoneStatus.setAmbientTemperature (theBurninBoxController.readAmbientTemperature());
			theBeagleBoneStatus.setPlateTemperature   (theBurninBoxController.readPlateTemperature());
			theBeagleBoneStatus.setDewPointTemperature(theBurninBoxController.readDewPointTemperature());
			theBeagleBoneStatus.setHumidity           (theBurninBoxController.readHumidity());
			theBeagleBoneStatus.setLeftPeltier        (theBurninBoxController.readLeftPeltierStatus());
			theBeagleBoneStatus.setRightPeltier       (theBurninBoxController.readRightPeltierStatus());
			theBeagleBoneStatus.setChiller            (theBurninBoxController.readChillerStatus());
			theBeagleBoneStatus.setWarmupRelay        (theBurninBoxController.readWarmupRelayStatus());
			theBeagleBoneStatus.setDryAirFluxRelay    (theBurninBoxController.readDryAirFluxRelayStatus());
			theBeagleBoneStatus.setModule1ATemperature(theBurninBoxController.readModule1ATemperature());
			theBeagleBoneStatus.setModule2ATemperature(theBurninBoxController.readModule2ATemperature());
			theBeagleBoneStatus.setModule3ATemperature(theBurninBoxController.readModule3ATemperature());
			theBeagleBoneStatus.setModule4ATemperature(theBurninBoxController.readModule4ATemperature());
			theBeagleBoneStatus.setModule5ATemperature(theBurninBoxController.readModule5ATemperature());
			theBeagleBoneStatus.setModule1BTemperature(theBurninBoxController.readModule1BTemperature());
			theBeagleBoneStatus.setModule2BTemperature(theBurninBoxController.readModule2BTemperature());
			theBeagleBoneStatus.setModule3BTemperature(theBurninBoxController.readModule3BTemperature());
			theBeagleBoneStatus.setModule4BTemperature(theBurninBoxController.readModule4BTemperature());
			theBeagleBoneStatus.setModule5BTemperature(theBurninBoxController.readModule5BTemperature());

			//print on screen
			theBeagleBoneStatus.printVariables();

			send(streamingSocket, streamingReceiver, theBeagleBoneStatus.convertToJSON());

			/*
			//print on log file
			logFile_ << (int) currentTime.tv_sec                        << "; "
					 << theBurninBoxController.readAmbientTemperature() << "; "
			         << theBurninBoxController.readPlateTemperature()   << "; "
					 << theBurninBoxController.readDewPoint()           << "; "
					 << std::endl;

			if((theBurninBoxController.getTemperatureStatus() == theBurninBoxController.TARGET) && !target1){
				startTime = currentTime.tv_sec;
				target1 = true;
				keep1 = true;
			}
			if(keep1 && (currentTime.tv_sec > startTime + keepTime)){
				//theBurninBoxController.setTargetTemperature(15,0.2);
				//target2 = true;
				keep1 = false;
				running = false;
				theBurninBoxController.switchThermalRelays(false);
				std::cout << __PRETTY_FUNCTION__ << "First cycle completed!" << std::endl;
				logFile_.close();
			}
			if((theBurninBoxController.getTemperatureStatus() == theBurninBoxController.TARGET) && target2){
				startTime = currentTime.tv_sec;
				target2 = false;
				keep2 = true;
			}
			if(keep2 && (currentTime.tv_sec > startTime + keepTime)){
				theBurninBoxController.setTargetTemperature(20,0.2);
				target3 = true;
				keep2 = false;
				std::cout << __PRETTY_FUNCTION__ << "Second cycle completed!" << std::endl;
			}
			if((theBurninBoxController.getTemperatureStatus() == theBurninBoxController.TARGET) && target3)
			{
				running = false;
				std::cout << __PRETTY_FUNCTION__ << "Done :)" << std::endl;
				theBurninBoxController.switchThermalRelays(false);
			}
			 */

			BurninBoxController::Status status = theBurninBoxController.getStateMachineStatus();
			if(status == theBurninBoxController.ERROR)
				std::cout << "I have a big problem" << std::endl;

			//theBurninBoxController.controlLoop();
			//running = false;
		}
		/*
		if(!running && logFile_.is_open())
		{
			logFile_.close();

			return 0;
		}*/

		//sleep(1);
		}
	}
	// Clean up and exit
	close(communicationSocket);
	close(streamingSocket);
	//LORE
	return 0;
}
