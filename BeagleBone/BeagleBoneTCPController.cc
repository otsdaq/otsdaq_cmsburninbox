#include <stdio.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/JsonConverter.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneStatus.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneServer.h"
#else
#include "JsonConverter.h"
#include "BeagleBoneStatus.h"
#include "BeagleBoneConfiguration.h"
#include "BeagleBoneServer.h"
#endif

#include "BurninBoxController.h"
#include "DeviceController.h"
#include "SensorMapping.h"

#define BEAGLEBONE_PORT 5000        // The destination port of the datastream

using namespace ots;


//MAIN
int main(int argc, char **argv)
{

	DeviceController theDeviceController;
	SensorMapping    theSensorConfiguration;

	BurninBoxController     theBurninBoxController;
	BeagleBoneConfiguration theBeagleBoneConfiguration;
	BeagleBoneStatus        theBeagleBoneStatus;


	std::cout<<"print MAP"<<std::endl;
	theSensorConfiguration.printMapping();

	std::cout<<"SETUP DEVICES"<<std::endl;
	theDeviceController.setupDevices(theSensorConfiguration.getSensorList());

	std::cout<<"INIT"<<std::endl;
	theBurninBoxController.init(theDeviceController);

	std::cout << "Running BeagleBone!" << std::endl;

	theBurninBoxController.setStateMachineStatus(theBurninBoxController.STARTING);
	BeagleBoneServer theBeagleBoneServer(theBurninBoxController,BEAGLEBONE_PORT);

	while(1) //listen to port BEAGLEBONE_PORT
	{
		theBeagleBoneServer.accept(1, 0);
	}

	return 0;
}
