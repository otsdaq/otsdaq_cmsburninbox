#include <stdio.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneServer.h"
#include "otsdaq_cmsburninbox/BeagleBone/BurninBoxController.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneStatus.h"
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"
#else
#include "BeagleBoneServer.h"
#include "BurninBoxController.h"
#include "BeagleBoneStatus.h"
#include "BeagleBoneConfiguration.h"
#endif

using namespace ots;


//========================================================================================================================
BeagleBoneServer::BeagleBoneServer(BurninBoxController& burningBoxController, int serverPort, int bufferSize)
: TCPNetworkServer      (serverPort, bufferSize)
,theBurninBoxController_(burningBoxController)
{
	theBeagleBoneConfiguration_.setSetTemperature(1.0);
	theBeagleBoneConfiguration_.setLowTolerance(1.0);
	theBeagleBoneConfiguration_.setHighTolerance(1.0);
	theBeagleBoneConfiguration_.setStopTemperature(1.0); // just defaults to be adjusted
}
//========================================================================================================================
BeagleBoneServer::~BeagleBoneServer(void)
{
}
//========================================================================================================================
// virtual function to interpret messages
// interacting with theBurninBoxController_ using theBeagleBoneConfiguration_ as helper class
std::string BeagleBoneServer::readMessage(const std::string& buffer)
{
	BeagleBoneStatus theBeagleBoneStatus;
	struct timeval currentTime;

	std::cout << "Received: " << buffer << std::endl;

	if (buffer.substr(0,5) == "START") //changing the status changes the mode in threadMain (BBC) function.
	{
		currentRun_ = getVariableValue("RunNumber", buffer);

		//theBurninBoxController_.switchThermalRelays(false);
		theBurninBoxController_.setTargetTemperature(theBeagleBoneConfiguration_.getSetTemperature(),
				theBeagleBoneConfiguration_.getLowTolerance(),
				theBeagleBoneConfiguration_.getHighTolerance());

		std::cout << "Run " << theBeagleBoneConfiguration_.getSetTemperature() << " temp!" << std::endl;

		running_ = true;
		paused_ = false;
		std::cout << "Run " << currentRun_ << " started!" << std::endl;
		//theBurninBoxController_.setTargetTemperature(10,0.2);

		std::cout << "Starting " << theBurninBoxController_.STARTING << " started!" << std::endl;
		std::cout << "Startup " << theBurninBoxController_.STARTUP << " started!" << std::endl;
		theBurninBoxController_.setStateMachineStatus(theBurninBoxController_.STARTING);

		std::cout << "Starting " << theBurninBoxController_.STARTING << " started!" << std::endl;
		std::cout << "Startup " << theBurninBoxController_.STARTUP << " started!" << std::endl;
		theBurninBoxController_.setTemperatureStatus(theBurninBoxController_.STARTUP);
	}
	else if (buffer.substr(0,4) == "STOP")
	{
		//We need to think :)
		theBurninBoxController_.setTargetTemperature(theBeagleBoneConfiguration_.getStopTemperature(),1,1);
		theBurninBoxController_.switchThermalRelays(false);

		//--->Don't forget that the dry air flux relay is still working

		theBurninBoxController_.setStateMachineStatus(theBurninBoxController_.STOPPING);

		running_ = false;
		paused_ = false;
		theBurninBoxController_.setEnableTemperature(false);
		std::cout << "Run " << currentRun_ << " stopped!" << std::endl;
	}
	else if (buffer == "PAUSE")
	{
		//We need to think :)
		//--->Maybe the temperature could be kept until start button is pressed
		running_ = false;
		paused_ = true;
		theBurninBoxController_.switchThermalRelays(false);
	}
	else if (buffer == "RESUME")
	{
		//We need to think :)
		running_ = true;
		paused_ = false;
	}
	//CONFIGURE
	else if (buffer.substr(0,theBeagleBoneConfiguration_.getClassName().size()) == theBeagleBoneConfiguration_.getClassName())
	{

		std::cout << "We are in the configuration submodule" << std::endl;
		theBeagleBoneConfiguration_.convertFromJSON(buffer);

		std::cout << "I got the following values:  " << theBeagleBoneConfiguration_.getSetTemperature() << " "
		<< theBeagleBoneConfiguration_.getLowTolerance() << " "<< theBeagleBoneConfiguration_.getHighTolerance() << std::endl;

		theBurninBoxController_.setTargetTemperature(theBeagleBoneConfiguration_.getSetTemperature(),
				theBeagleBoneConfiguration_.getLowTolerance(),theBeagleBoneConfiguration_.getHighTolerance());

		theBurninBoxController_.setStateMachineStatus(theBurninBoxController_.CONFIGURE);
		std::cout << "Out of configuration submodule" << std::endl;
	}
	else if (buffer == "STATUS?")
	{
		if(running_||paused_) // read again
			{
				std::cout << "getting time and status here" << std::endl;

				gettimeofday(&currentTime, nullptr);
				theBurninBoxController_.running();
				theBeagleBoneStatus.setTime(currentTime.tv_sec);

				theBeagleBoneStatus.setAmbientTemperature (theBurninBoxController_.readAmbientTemperature());
				theBeagleBoneStatus.setPlateTemperature   (theBurninBoxController_.readPlateTemperature());
				theBeagleBoneStatus.setDewPointTemperature(theBurninBoxController_.readDewPointTemperature());
				theBeagleBoneStatus.setHumidity           (theBurninBoxController_.readHumidity());
				theBeagleBoneStatus.setLeftPeltier        (theBurninBoxController_.readLeftPeltierStatus());
				theBeagleBoneStatus.setRightPeltier       (theBurninBoxController_.readRightPeltierStatus());
				theBeagleBoneStatus.setChiller            (theBurninBoxController_.readChillerStatus());
				theBeagleBoneStatus.setWarmupRelay        (theBurninBoxController_.readWarmupRelayStatus());
				theBeagleBoneStatus.setDryAirFluxRelay    (theBurninBoxController_.readDryAirFluxRelayStatus());

				//print on screen
				theBeagleBoneStatus.printVariables();

				BurninBoxController::Status status = theBurninBoxController_.getStateMachineStatus();
				if(status == theBurninBoxController_.ERROR)
					std::cout << "I have a big problem" << std::endl;
			}

		return theBeagleBoneStatus.convertToJSON(); // send createdempty status (?)
	}
	else if (buffer=="Config?")
	{
		return theBeagleBoneConfiguration_.convertToJSON();
	}
	else if (buffer=="Run?")
	{
		std::cout << "checking the Run number: " << currentRun_ <<std::endl;
		return currentRun_;
	}
	else if (buffer=="running?")
	{
		if (running_) return "running";
		else return "no";
	}
	else if (buffer=="paused?")
	{
		if (paused_) return "paused";
		else return "no";
	}

	if(running_||paused_) //we go through here after start and resume or pause: sending back current status
	{
		std::cout << "getting time and status here" << std::endl;

		gettimeofday(&currentTime, nullptr);
		theBurninBoxController_.running();
		theBeagleBoneStatus.setTime(currentTime.tv_sec);

		theBeagleBoneStatus.setAmbientTemperature (theBurninBoxController_.readAmbientTemperature());
		theBeagleBoneStatus.setPlateTemperature   (theBurninBoxController_.readPlateTemperature());
		theBeagleBoneStatus.setDewPointTemperature(theBurninBoxController_.readDewPointTemperature());
		theBeagleBoneStatus.setHumidity           (theBurninBoxController_.readHumidity());
		theBeagleBoneStatus.setLeftPeltier        (theBurninBoxController_.readLeftPeltierStatus());
		theBeagleBoneStatus.setRightPeltier       (theBurninBoxController_.readRightPeltierStatus());
		theBeagleBoneStatus.setChiller            (theBurninBoxController_.readChillerStatus());
		theBeagleBoneStatus.setWarmupRelay        (theBurninBoxController_.readWarmupRelayStatus());
		theBeagleBoneStatus.setDryAirFluxRelay    (theBurninBoxController_.readDryAirFluxRelayStatus());

		//print on screen
		theBeagleBoneStatus.printVariables();

		BurninBoxController::Status status = theBurninBoxController_.getStateMachineStatus();
		if(status == theBurninBoxController_.ERROR)
			std::cout << "I have a big problem" << std::endl;

		//return theBeagleBoneStatus.convertToJSON();
	}

	return "";

}


