#ifndef _ots_BurninBoxController_h_
#define _ots_BurninBoxController_h_

#include "DeviceController.h"
#include "Thread.h"
#include <fstream>
#include <tgmath.h>

namespace ots
{

//class BoxStatus
//{
//private:
//	float
//};


class BurninBoxController : public Thread
{
public:
	enum Status {INITIAL, HALTED, CONFIGURE, CONFIGURED, STARTING, STOPPING, RUNNING, PAUSED, RESUMING, ERROR};
	enum Cycle  {STARTUP, GOING_UP, TARGET, GOING_DOWN};
	BurninBoxController (const DeviceController& deviceController);
	BurninBoxController (void); //?
	virtual ~BurninBoxController(void);

	void   init                        (const DeviceController& deviceController); //?

	void   start                       (void);
	void   stop                        (void);
	void   error                       (void);
	void   configure                   (float runningTemperature, float stopTemperature);
	void   running                     (void);

	float  readAmbientTemperature      (void);
	float  readLeftPeltierTemperature  (void);
	float  readRightPeltierTemperature (void);
	float  readPlateTemperature        (void);
	float  readDewPointTemperature     (void);
	float  readHumidity                (void);
	bool   readLeftPeltierStatus       (void);
	bool   readRightPeltierStatus      (void);
	bool   readChillerStatus           (void);
	bool   readWarmupRelayStatus       (void);
	bool   readDryAirFluxRelayStatus   (void);
	float  readModule1ATemperature     (void);
	float  readModule2ATemperature     (void);
	float  readModule3ATemperature     (void);
	float  readModule4ATemperature     (void);
	float  readModule5ATemperature     (void);
	float  readModule1BTemperature     (void);
	float  readModule2BTemperature     (void);
	float  readModule3BTemperature     (void);
	float  readModule4BTemperature     (void);
	float  readModule5BTemperature     (void);

	void   setStateMachineStatus       (Status);
	void   setTemperatureStatus        (Cycle);
	void   setEnableTemperature        (bool flag);
	void   setTargetTemperature        (float setTemperature);
	//void   setTargetTemperature        (float setTemperature, float lowTolerance, float highTolerance);
	void   setDewPointSafetyMargin     (float dewPointSafetyMargin);
	void   setDewPointSafetyHighMargin (float dewPointSafetyHighMargin);
	void   setDewPointHighLowMargin    (float dewPointHighLowMargin);

	Status getStateMachineStatus       (void)        {return theStateMachineStatus_;}
	Cycle  getTemperatureStatus        (void)        {return theTemperatureStatus_;}
	float  getDewPointSafetyMargin     (float) const {return dewPointSafetyMargin_;}
	float  getDewPointSafetyHighMargin (void)  const {return dewPointSafetyHighMargin_;}
	float  getDewPointHighLowMargin    (void)  const {return dewPointHighLowMargin_;}

	void   failureHandle               (void);
	void   switchThermalRelays         (bool);

	void   checkConfiguration          (void);

private:
	void   controlLoop                 (void);
	int    relayTemperatureControl     (float currentTemperature, float min, float max);
	void   keepTemperature             (float setTemperature, float keepTime);//keepTime expressed in seconds
	float  minTemperature              (std::vector<float*> temperatureStructure);
	void   threadMain                  (void);

	DeviceController                          theDeviceController_;

	Relay*                                    theDryairCompressorRelay_;
	Relay*                                    theLowHighFluxDryAirRelay_;
	Relay*                                    theChillerRelay_;
	Relay*                                    theWarmupRelay_;
	Relay*                                    theLeftPeltierRelay_;
	Relay*                                    theRightPeltierRelay_;
	TemperatureSensor*                        theLeftPeltierTemperature_;
	TemperatureSensor*                        theRightPeltierTemperature_;
	std::map<std::string, DewPointSensor*>    theDewPoint_;
	std::map<std::string, TemperatureSensor*> theAmbientTemperature_;
	std::map<std::string, TemperatureSensor*> theModuleTemperature_;

	//<"devType",  <"devName", devStatus> >
	std::map<int, std::map<std::string, bool>> devices_;
	enum{ModuleTemperature, PlateTemperature, AmbientTemperature,
		AmbientDewPoint, ChillerRelay,
		LeftPeltierRelay, RightPeltierRelay, WarmupRelay,
		DryairCompressorRelay, LowHighFluxDryAirRelay};

	float setTemperature_;


	float currentAmbientTemperature_;
	float currentPlateTemperature_;
	float currentLeftPeltierTemperature_;
	float currentRightPeltierTemperature_;
	float currentDewPoint_;
	float currentHumidity_;
	float dewPointSafetyMargin_;
	float dewPointSafetyHighMargin_;
	float dewPointHighLowMargin_;

	float runningTemperature_;
	float stopTemperature_;
	float runningDewPoint_;
	bool  running_;
	bool  timeCycle;
	bool  enableTemperature_;
	int   dewPointRecursiveCounter_;

	std::atomic<Status> theStateMachineStatus_;
	std::atomic<Cycle>  theTemperatureStatus_;
	std::vector<float*> temperatureStructure_;

};
}
#endif
