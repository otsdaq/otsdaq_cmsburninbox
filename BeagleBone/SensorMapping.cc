#include "SensorMapping.h"

#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <sstream>
#include <vector>
#include <map>

using namespace ots;
//===========================================================================
SensorMapping::SensorMapping (void)
{
	std::string line;

	std::vector<std::string> categoryList;
	categoryList.push_back("TEMPERATURE");
	categoryList.push_back("DEWPOINT");
	categoryList.push_back("RELAY");

	std::vector<std::string> attributeList;
	attributeList.push_back("name");
	attributeList.push_back("status");
	attributeList.push_back("type");
	attributeList.push_back("id");

	const int numberOfAttributes = attributeList.size();

	std::string sensorCategory;
	bool inSensors = false;
	bool comment   = false;
	int  index     = -1;
	unsigned int beginQuote;
	unsigned int endQuote;
	unsigned int beginSensor;

	sensorFile_.open("SensorList.xml", std::fstream::in | std::fstream::out | std::fstream::app);
	if(sensorFile_.is_open())
	{
		while(!sensorFile_.eof())
		{
			getline(sensorFile_, line);
			if(line == "") continue;
			if(line.find("<!--") != std::string::npos)
				comment = true;
			if(comment)
			{
				if(line.find("-->") != std::string::npos)
					comment = false;
				continue;
			}
			//			std::cout << "LINE: " << line << std::endl;
			if(!inSensors)
			{
				for(auto category: categoryList)
				{
					if (line.find("<" + category + ">") != std::string::npos)
					{
						sensorCategory = category;
						inSensors = true;
						index = -1;
						break;
					}
				}

				if(!inSensors)
				{
					std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << "UNRECOGNIZED SENSOR CATEGORY!" << std::endl;
					continue;
				}
			}
			else
			{
				if(line == ("</" + sensorCategory + ">"))
				{
					inSensors = false;
					continue;
				}

				if((beginSensor = line.find('<')) != std::string::npos)
				{
					sensors_[sensorCategory].push_back(std::map<std::string, std::string>());
					int vectorIndex = sensors_[sensorCategory].size() - 1;
					for(int attributeN=0; attributeN<numberOfAttributes; attributeN++)
					{
						if((unsigned int)(index = (int)line.find(attributeList[attributeN])) == std::string::npos)
						{
							std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << "CAN'T FIND ATTRIBUTE: " << attributeList[attributeN] << std::endl;
							std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << "You must set it in SensorList.xml." << std::endl;

						}
						index = index + attributeList[attributeN].size();
						index = line.find("=", index);
						beginQuote = line.find('\"', index);
						index = beginQuote;
						endQuote   = line.find('\"', index + 1);

						sensors_[sensorCategory][vectorIndex][attributeList[attributeN]] = line.substr(beginQuote+1, endQuote-beginQuote-1);
						std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << " Attribute: " << attributeList[attributeN]
						<< " Value: " << sensors_[sensorCategory][vectorIndex][attributeList[attributeN]] << std::endl;
					}


					if(line.find("/>", index) != std::string::npos)
						index = -1;
					//   if("do all checks for consistency")
				}
				else
					std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << "IMPOSSIBLE! I DIDN'T FIND <" << std::endl;
			}
		}
		sensorFile_.close();
	}
	else
		std::cout << __PRETTY_FUNCTION__ << "[" << __LINE__ << "]" << "THERE MUST BE SensorList.xml in this directory!" << std::endl;
}

//===========================================================================
std::string SensorMapping::getSensor (std::string name)
{
	// sensorFile_.open("SensorList.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	std::string sensorFound;

	// if(sensorFile_.is_open())
	//   {
	//     while(!sensorFile_.eof())
	// 	{
	// 	  getline(sensorFile_,line);
	// 	  if (line.find(name) != std::string::npos)
	// 	    {
	// 	      sensorFound = line;
	// 	      break;
	// 	    }
	// 	}
	//   }
	// else
	//   std::cout << "Could not open file 'SensorList.txt' " << std::endl;

	// sensorFile_.close();

	return sensorFound;
}

//===========================================================================
std::vector<std::map<std::string, std::string>> SensorMapping::getSensorList (void)
{
	auto list = std::vector<std::map<std::string, std::string>>();
	for(auto category: sensors_)
		for(auto sensor: category.second)
			list.emplace_back(sensor);
	return list;
}

//===========================================================================
std::vector<std::map<std::string, std::string>> SensorMapping::getTemperatureSensorList (void) const
{
	auto it = sensors_.find("TEMPERATURE");
	if(it == sensors_.end())
	{
		std::cout << "Can't find category: " << "TEMPERATURE" << std::endl;
		exit(0);
	}
	else
		return it->second;
}

//===========================================================================
std::vector<std::map<std::string, std::string>> SensorMapping::getDewPointSensorList (void) const
{
	auto it = sensors_.find("DEWPOINT");
	if(it == sensors_.end())
	{
		std::cout << "Can't find category: " << "DEWPOINT" << std::endl;
		exit(0);
	}
	else
		return it->second;
}

//===========================================================================
std::vector<std::map<std::string, std::string>> SensorMapping::getRelayList (void) const
{
	auto it = sensors_.find("RELAY");
	if(it == sensors_.end())
	{
		std::cout << "Can't find category: " << "RELAY" << std::endl;
		exit(0);
	}
	else
		return it->second;
}

//===========================================================================
void SensorMapping::printMapping(void)
{
	for(auto sensorType: sensors_)
	{
		std::cout << __PRETTY_FUNCTION__ << "\t" << sensorType.first << " :" << std::endl;
		for (auto sensor : sensorType.second)
		{
			for (auto attribute : sensor)
			{
				std::cout << attribute.first  << ": " << attribute.second << ", ";
			}
			std::cout << std::endl;
		}
	}

}
