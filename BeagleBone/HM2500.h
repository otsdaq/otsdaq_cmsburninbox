#ifndef _ots_HM2500_h_
#define _ots_HM2500_h_

#include "AnalogSensor.h"
#include "HumiditySensor.h"
#include <string>

namespace ots
{
class HM2500 : public HumiditySensor, public AnalogSensor
{
public:
	HM2500(std::string name, std::string id, std::string status);
	~HM2500(void) {;}

	float readHumidity(void);

private:
	std::string temperaturePath_;
	std::string humidityPath_;
//	float dataValue_; //in Celsius
};
}
#endif
