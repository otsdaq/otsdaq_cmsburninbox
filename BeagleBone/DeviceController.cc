#include "DeviceController.h"

#include "DS18B20.h"
#include "PT100RTD.h"
#include "DMT143.h"
#include "HM2500.h"
#include "SensorMapping.h"

#include <dirent.h>
#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <iomanip>
#include "Relay.h"

using namespace ots;
//===========================================================================
DeviceController::DeviceController()
{}

//===========================================================================
DeviceController::~DeviceController()
{
	for(auto s: devices_)
		delete s.second;
	devices_.clear();
}

//===========================================================================
std::vector<std::string> DeviceController::getDeviceList(void)
{
	auto list = std::vector<std::string>();
	for (auto it: devices_)
	{
		list.emplace_back(it.first);
	}
	return list;
}


//===========================================================================
void DeviceController::setupDevices(std::vector<std::map<std::string, std::string>> deviceList)
{
	for(auto device: deviceList)
	{
		if(device["type"] == "DS18B20")
		{
			devices_[device["name"]] = new DS18B20 (device["name"], device["id"], device["status"]);
			if (devices_[device["name"]]->isOn())
				activeTemperatureSensorList_[device["name"]]  = dynamic_cast<TemperatureSensor*>(devices_[device["name"]]);
		}
		else if(device["type"] == "PT100RTD")
		{
			devices_[device["name"]] = new PT100RTD (device["name"], device["id"], device["status"]);
			if (devices_[device["name"]]->isOn())
				activeTemperatureSensorList_[device["name"]]  = dynamic_cast<TemperatureSensor*>(devices_[device["name"]]);
		}
		else if(device["type"] == "DMT143")
		{
			devices_[device["name"]] = new DMT143 (device["name"], device["id"], device["status"]);
			if (devices_[device["name"]]->isOn())
				activeDewPointSensorList_[device["name"]]  = dynamic_cast<DewPointSensor*>(devices_[device["name"]]);
		}
		else if(device["type"] == "HM2500")
		{
			devices_[device["name"]] = new HM2500 (device["name"], device["id"], device["status"]);
			if (devices_[device["name"]]->isOn())
				activeDewPointSensorList_[device["name"]]  = dynamic_cast<DewPointSensor*>(devices_[device["name"]]);
		}
		else if(device["type"] == "Relay")
		{
			devices_[device["name"]] = new Relay (device["name"], device["id"], device["status"]);
			if (devices_[device["name"]]->isOn())
				activeRelayList_[device["name"]]  = dynamic_cast<Relay*>(devices_[device["name"]]);
		}
		else
		{
			std::cout << "Can't recognize type: " << device["type"] << std::endl;
			exit(0);
		}
	}
}


//===========================================================================
void DeviceController::printTemperatures()
{
	for (auto it: activeTemperatureSensorList_)
		std::cout << it.second->getName() << ": " << std::fixed << std::setprecision(3)
			      << it.second->readTemperature() << " C, ";
	std::cout << std::endl;
}

//===========================================================================
double DeviceController::readTemperatures()
{
	double sensor1 = 0, sensor2 = 0, sensor3 = 0;
	for (auto it : activeTemperatureSensorList_)
	{
		if (it.second->getName() == "RTD1") sensor1 = it.second->readTemperature();
		if (it.second->getName() == "RTD2") sensor2 = it.second->readTemperature();
		if (it.second->getName() == "RTD3") sensor3 = it.second->readTemperature();
	}
	double actualTemp = (sensor1 + sensor2 + sensor3) / 3;
	return actualTemp;
}

//Emanuele Aucone
//===========================================================================
void DeviceController::printDewPoints()
{
	for (auto it: activeDewPointSensorList_){
		if (it.second->getName() == "DMT143")
			std::cout << it.second->getName() << ": " << std::fixed << std::setprecision(3)
				<< it.second->readDewPoint() << " C. ";
	}
	std::cout << std::endl;
}

//Emanuele Aucone
//===========================================================================
double DeviceController::readDewPoints()
{
	double sensor1 = 0;
	for (auto it: activeDewPointSensorList_){
		if (it.second->getName() == "DMT143") sensor1 = it.second->readDewPoint();
	}
	return sensor1;
}

//===========================================================================
double DeviceController::refreshDewPoints() //Do I actually need this function? readDewPoints() might do the job, just don't know what it'll return
{
	double sensor1 = 0, sensor2 = 0;
	for (auto it : activeDewPointSensorList_)
	{
		if (it.second->getName() == "HM2500_0") sensor1 = it.second->readDewPoint();
		if (it.second->getName() == "HM2500_1") sensor2 = it.second->readDewPoint();
	}
	double actualDewPoint = (sensor1 + sensor2) / 2;
	return actualDewPoint;
}

//===========================================================================
Device* const DeviceController::getDevice(std::string name)
{
	auto device = devices_.find(name);
	if(device != devices_.end() && device->second->isOn())
		return device->second;
	else
		return NULL;
}

//===========================================================================
Relay* const DeviceController::getRelay (std::string name)
{
	auto device = activeRelayList_.find(name);
		if (device != activeRelayList_.end() && device->second->isOn())
			return device->second;
		else
			return NULL;
}

//===========================================================================
TemperatureSensor* const DeviceController::getTemperatureSensor (std::string name)
{
	auto device = activeTemperatureSensorList_.find(name);
	if (device != activeTemperatureSensorList_.end() && device->second->isOn())
		return device->second;
	else
		return NULL;
}
//===========================================================================
DewPointSensor* const DeviceController::getDewPointSensor (std::string name)
{
	auto device = activeDewPointSensorList_.find(name);
	if (device != activeDewPointSensorList_.end() && device->second->isOn())
		return device->second;
	else
		return NULL;
}
