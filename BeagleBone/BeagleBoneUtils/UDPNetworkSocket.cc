#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/UDPNetworkSocket.h"
#else
#include "UDPNetworkSocket.h"
#endif

#include <iostream>
#include <cassert>
#include <sstream>

#include <cstdio>
#include <string.h>
#include <cstdlib>
#include <unistd.h>

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <stdexcept>
#include <cstring>

#define MAXBUFLEN 1492

using namespace ots;

//========================================================================================================================
UDPNetworkSocket::UDPNetworkSocket()
: socketNumber_ (-1)
, IPAddress_    ("")
, port_         (0)
{
	std::cout << "Empty constructor." << std::endl;
}

//========================================================================================================================
UDPNetworkSocket::UDPNetworkSocket(std::string IPAddress, unsigned int port)
: socketNumber_ (-1)
, IPAddress_    (IPAddress)
, port_         (port)
{
	std::cout << "Socket constructor " << IPAddress << ":" << port << std::endl;

	if(port >= (1<<16))
	{

		std::stringstream ss;
		ss << "FATAL: Invalid Port " << port << ". Max port number is " << (1<<16)-1 << "." << std::endl;
		throw std::runtime_error(ss.str());

	}

	//socket address
	socketAddress_.sin_family = AF_INET; // use IPv4 host byte order
	socketAddress_.sin_port   = htons(port); // short, network byte order
	if(inet_aton(IPAddress.c_str(), &socketAddress_.sin_addr) == 0)
	{
		std::stringstream ss;
		ss << "FATAL: Invalid IP:Port combination. Please verify... " << IPAddress << ":" << port << std::endl;
		throw std::runtime_error(ss.str());

	}

	memset(&(socketAddress_.sin_zero), '\0', 8); // zero the rest of the struct
	std::cout << "CREATED SOCKET " << IPAddress << ":" << port << std::endl;
}

//========================================================================================================================
UDPNetworkSocket::~UDPNetworkSocket(void)
{
	if(socketNumber_ != -1)
	{
		std::cout << "CLOSING SOCKET #" << socketNumber_  << " IP: " << IPAddress_ << ":" << port_ << std::endl;
		close(socketNumber_);

	}
}

//========================================================================================================================
void UDPNetworkSocket::initialize(unsigned int socketReceiveBufferSize)
{
	std::cout << "Initializing port " << ntohs(socketAddress_.sin_port) << " htons: " <<  socketAddress_.sin_port << std::endl;
	struct addrinfo  hints;
	struct addrinfo* res;
	int status    =  0;

	// first, load up address structs with getaddrinfo():
	memset(&hints, 0, sizeof hints);
	hints.ai_family   = AF_INET;    // use IPv4
	hints.ai_socktype = SOCK_DGRAM; // SOCK_DGRAM
	hints.ai_flags    = AI_PASSIVE; // fill in my IP for me

	std::stringstream port; port << port_;
	std::cout << "Binding port: " << port_ << std::endl;

	if((status = getaddrinfo(NULL, port.str().c_str(), &hints, &res)) != 0)
	{
		std::cout << "Getaddrinfo error status: " << status << std::endl;
		std::stringstream ss;
		ss << "ERROR: Getaddrinfo error status: " << status << std::endl;
		throw std::runtime_error(ss.str());
	}

	// make a socket:
	socketNumber_ = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    std::cout << "Socket Number: " << socketNumber_ << " for port: " << ntohs(socketAddress_.sin_port) << " initialized." << std::endl;

    // bind it to the port we passed in to getaddrinfo():
    if(bind(socketNumber_, res->ai_addr, res->ai_addrlen) == -1)
	{
		freeaddrinfo(res); // free the linked-list
		std::cout << "Error********Error********Error********Error********Error********Error" << std::endl;
		std::cout << "FAILED BIND FOR PORT: " << port_ << " ON IP: " << IPAddress_ << std::endl;
		std::cout << "Error********Error********Error********Error********Error********Error" << std::endl;

		std::stringstream ss;
		ss << "FATAL: Failed bind for port: " << port_ << " on IP: " << IPAddress_ << ". Perhaps it is already in use?" << "\n" ;
		socketNumber_ = -1;
		throw std::runtime_error(ss.str());
	}
	else
	{
		std::cout <<  ":):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):)" << std::endl;
		std::cout <<  ":):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):)" << std::endl;
		std::cout <<  "SOCKET ON PORT: " << port_ << " ON IP: " << IPAddress_ << " INITIALIZED OK!" << std::endl;
		std::cout <<  ":):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):)" << std::endl;
		std::cout <<  ":):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):):)" << std::endl;
		char yes = '1';
		setsockopt(socketNumber_, SOL_SOCKET,SO_REUSEADDR, &yes, sizeof(int));
		std::cout << "Socket Number: " << socketNumber_ << " for port: " << ntohs(socketAddress_.sin_port) << " initialized." << std::endl;
	}

	freeaddrinfo(res); // free the linked-list

	std::cout << "Setting socket receive buffer size = " << socketReceiveBufferSize << " 0x" << std::hex << socketReceiveBufferSize << std::dec << std::endl;
	if (setsockopt(socketNumber_, SOL_SOCKET, SO_RCVBUF, (char*)&socketReceiveBufferSize, sizeof(socketReceiveBufferSize)) < 0)
	{
		std::cout << "Failed to set socket receive size to " << socketReceiveBufferSize << ". Attempting to revert to default." << std::endl;

		socketReceiveBufferSize = defaultSocketReceiveSize_;
		std::cout << "Setting socket receive buffer size = " << socketReceiveBufferSize << " 0x" << std::hex << socketReceiveBufferSize << std::dec << std::endl;

		if (setsockopt(socketNumber_, SOL_SOCKET, SO_RCVBUF, (char*)&socketReceiveBufferSize, sizeof(socketReceiveBufferSize)) < 0)
		{
			std::stringstream ss;
			ss << "ERROR: Failed to set socket receive size to " << socketReceiveBufferSize << "." << "\n" ;
			throw std::runtime_error(ss.str());

		}
	}
}

//========================================================================================================================
const std::string& UDPNetworkSocket::getIPAddress(void)
{
	return IPAddress_;
}

//========================================================================================================================
uint16_t UDPNetworkSocket::getPort(void)
{
	return port_;
}

//========================================================================================================================
int UDPNetworkSocket::getSocketNumber(void)
{
	return socketNumber_;
}

//========================================================================================================================
const struct sockaddr_in& UDPNetworkSocket::getSocketAddress(void)
{
	return socketAddress_;
}

//========================================================================================================================
void UDPNetworkSocket::setSocketAddress(struct sockaddr_in socketAddress)
{
	socketAddress_ = socketAddress;
	port_          = ntohs(socketAddress_.sin_port);
	IPAddress_     = inet_ntoa(socketAddress_.sin_addr);
}

//========================================================================================================================
void UDPNetworkSocket::send(UDPNetworkSocket toSocket, const std::string& buffer)
{
	std::cout << "Sending Message to: " << toSocket.getIPAddress() << ":" << toSocket.getPort() << std::endl;

	int sendtoReturnCode = sendto(socketNumber_, buffer.c_str(), buffer.size(), 0, (struct sockaddr *)&(toSocket.getSocketAddress()), sizeof(sockaddr_in));
	if ( sendtoReturnCode < (int)(buffer.size()) )
	{
		std::stringstream ss;
		ss << "ERROR: Can't send buffer from " << IPAddress_ << ":" << port_ << "." << "\n" ;
		throw std::runtime_error(ss.str());
	}
}

//========================================================================================================================
int UDPNetworkSocket::receive(std::string& buffer, UDPNetworkSocket* sender, unsigned int timeoutSeconds, unsigned int timeoutUSeconds)
{
	struct sockaddr_in fromAddress;

	struct timeval tv;
	tv.tv_sec  = timeoutSeconds;
	tv.tv_usec = timeoutUSeconds;

	//setup file descriptor for select()
	fd_set fileDescriptor;
	FD_ZERO(&fileDescriptor);
	FD_SET(socketNumber_,   &fileDescriptor);
	select(socketNumber_+1, &fileDescriptor, 0, 0, &tv);

	if(FD_ISSET(socketNumber_,&fileDescriptor))
	{

		int nOfBytes;
		socklen_t addressLength = sizeof(fromAddress);
		buffer.resize(MAXBUFLEN);

		if ((nOfBytes=recvfrom(socketNumber_, &buffer[0], MAXBUFLEN, 0, (struct sockaddr *)&fromAddress, &addressLength)) != -1)
			buffer.resize(nOfBytes);
		if(sender != nullptr)
			sender->setSocketAddress(fromAddress);

		//printf("recvfrom error %s\n",strerror(errno));
		return nOfBytes;
	}
	else
	{
		std::stringstream ss;
		ss << "ERROR: Can't read from " << IPAddress_ << ":" << port_ << ". Error # " << strerror(errno) << "\n" ;
		throw std::runtime_error(ss.str());
	}

	return -1;
}

//========================================================================================================================
int UDPNetworkSocket::receiveAndAcknowledge(std::string& buffer, UDPNetworkSocket *sender, unsigned int timeoutSeconds, unsigned int timeoutUSeconds)
{
	int returnValue = receive(buffer, sender, timeoutSeconds, timeoutUSeconds);
	if(returnValue >= 0)
		send(*sender, "RECEIVED "+buffer);
	return returnValue;
}

//========================================================================================================================
void UDPNetworkSocket::sendAndReceive(UDPNetworkSocket toSocket, const std::string& buffer)
{
	std::string rec_buffer="";

	send(toSocket, buffer);
	receive(rec_buffer, &toSocket);
	std::cout <<"received :  "<<rec_buffer<<"   from IP : "<<toSocket.IPAddress_ << ":" << toSocket.port_<<std::endl;

}
