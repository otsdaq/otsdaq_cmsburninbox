/*
 * BeagleBoneConfiguration.cc
 *
 *  Created on: Sep 13, 2018
 *      Author: otsdaq
 */

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneConfiguration.h"
#else
#include "BeagleBoneConfiguration.h"
#endif

using namespace ots;

//========================================================================================================================
BeagleBoneConfiguration::BeagleBoneConfiguration()
: JsonConverter     ("CONFIGURATION")
{
	JsonConverter::variables_["StreamToIPAddress"] = "";
	JsonConverter::variables_["StreamToPort"]      = "";
	JsonConverter::variables_["SetTemperature"]    = "";
	JsonConverter::variables_["LowTolerance"]      = "";
	JsonConverter::variables_["HighTolerance"]     = "";
	JsonConverter::variables_["StopTemperature"]   = "";
}

//========================================================================================================================
BeagleBoneConfiguration::BeagleBoneConfiguration(std::string streamToIPAddress, int streamToPort, float setTemperature, float lowTolerance, float highTolerance, float stopTemperature)
: JsonConverter     ("CONFIGURATION")
{
	JsonConverter::variables_["StreamToIPAddress"] = streamToIPAddress;
	JsonConverter::variables_["StreamToPort"]      = std::to_string(streamToPort);
	JsonConverter::variables_["SetTemperature"]    = std::to_string(setTemperature);
	JsonConverter::variables_["LowTolerance"]      = std::to_string(lowTolerance);
	JsonConverter::variables_["HighTolerance"]     = std::to_string(highTolerance);
	JsonConverter::variables_["StopTemperature"]   = std::to_string(stopTemperature);
}

//========================================================================================================================
BeagleBoneConfiguration::~BeagleBoneConfiguration()
{}

//========================================================================================================================
void BeagleBoneConfiguration::setStreamToIPAddress(std::string value)
{
	JsonConverter::variables_["StreamToIPAddress"] = value;
}

//========================================================================================================================
void BeagleBoneConfiguration::setStreamToPort(int value)
{
	JsonConverter::variables_["StreamToPort"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneConfiguration::setSetTemperature(float value)
{
	JsonConverter::variables_["SetTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneConfiguration::setLowTolerance(float value)
{
	JsonConverter::variables_["LowTolerance"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneConfiguration::setHighTolerance(float value)
{
	JsonConverter::variables_["HighTolerance"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneConfiguration::setStopTemperature(float value)
{
	JsonConverter::variables_["StopTemperature"] = std::to_string(value);
}

//========================================================================================================================
std::string BeagleBoneConfiguration::getStreamToIPAddress(void) const
{
	return variables_.find("StreamToIPAddress")->second;
}

//========================================================================================================================
int BeagleBoneConfiguration::getStreamToPort(void) const
{
	return atoi(variables_.find("StreamToPort")->second.c_str());
}

//========================================================================================================================
float BeagleBoneConfiguration::getSetTemperature(void) const
{
	return atof(variables_.find("SetTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneConfiguration::getLowTolerance(void) const
{
	return atof(variables_.find("LowTolerance")->second.c_str());
}

//========================================================================================================================
float BeagleBoneConfiguration::getHighTolerance(void) const
{
	return atof(variables_.find("HighTolerance")->second.c_str());
}

//========================================================================================================================
float BeagleBoneConfiguration::getStopTemperature(void) const
{
	return atof(variables_.find("StopTemperature")->second.c_str());
}
