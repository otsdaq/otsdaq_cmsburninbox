/*
 * BeagleBoneStatus.cc
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/BeagleBoneStatus.h"
#else
#include "BeagleBoneStatus.h"
#endif

using namespace ots;

//========================================================================================================================
BeagleBoneStatus::BeagleBoneStatus()
: JsonConverter       ("STATUS")
{
	JsonConverter::variables_["Time"               ] = "";
	JsonConverter::variables_["AmbientTemperature" ] = "";
	JsonConverter::variables_["PlateTemperature"   ] = "";
	JsonConverter::variables_["DewPointTemperature"] = "";
	JsonConverter::variables_["Humidity"           ] = "";
	JsonConverter::variables_["LeftPeltier"        ] = "";
	JsonConverter::variables_["RightPeltier"       ] = "";
	JsonConverter::variables_["Chiller"            ] = "";
	JsonConverter::variables_["WarmupRelay"        ] = "";
	JsonConverter::variables_["DryAirFluxRelay"    ] = "";
	JsonConverter::variables_["Module1ATemperature"] = "";
	JsonConverter::variables_["Module2ATemperature"] = "";
	JsonConverter::variables_["Module3ATemperature"] = "";
	JsonConverter::variables_["Module4ATemperature"] = "";
	JsonConverter::variables_["Module5ATemperature"] = "";
	JsonConverter::variables_["Module1BTemperature"] = "";
	JsonConverter::variables_["Module2BTemperature"] = "";
	JsonConverter::variables_["Module3BTemperature"] = "";
	JsonConverter::variables_["Module4BTemperature"] = "";
	JsonConverter::variables_["Module5BTemperature"] = "";

}

//========================================================================================================================
BeagleBoneStatus::~BeagleBoneStatus()
{}

//========================================================================================================================
void BeagleBoneStatus::setTime(time_t value)
{
	JsonConverter::variables_["Time"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setAmbientTemperature(float value)
{
	JsonConverter::variables_["AmbientTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setPlateTemperature(float value)
{
	JsonConverter::variables_["PlateTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setDewPointTemperature(float value)
{
	JsonConverter::variables_["DewPointTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setHumidity(float value)
{
	JsonConverter::variables_["Humidity"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setLeftPeltier(bool value)
{
	JsonConverter::variables_["LeftPeltier"] = std::to_string(int(value));
}

//========================================================================================================================
void BeagleBoneStatus::setRightPeltier(bool value)
{
	JsonConverter::variables_["RightPeltier"] = std::to_string(int(value));
}

//========================================================================================================================
void BeagleBoneStatus::setChiller(bool value)
{
	JsonConverter::variables_["Chiller"] = std::to_string(int(value));
}

//========================================================================================================================
void BeagleBoneStatus::setWarmupRelay(bool value)
{
	JsonConverter::variables_["WarmupRelay"] = std::to_string(int(value));
}

//========================================================================================================================
void BeagleBoneStatus::setDryAirFluxRelay(bool value)
{
	JsonConverter::variables_["DryAirFluxRelay"] = std::to_string(int(value));
}

//========================================================================================================================
void BeagleBoneStatus::setModule1ATemperature(float value)
{
	JsonConverter::variables_["Module1ATemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule2ATemperature(float value)
{
	JsonConverter::variables_["Module2ATemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule3ATemperature(float value)
{
	JsonConverter::variables_["Module3ATemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule4ATemperature(float value)
{
	JsonConverter::variables_["Module4ATemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule5ATemperature(float value)
{
	JsonConverter::variables_["Module5ATemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule1BTemperature(float value)
{
	JsonConverter::variables_["Module1BTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule2BTemperature(float value)
{
	JsonConverter::variables_["Module2BTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule3BTemperature(float value)
{
	JsonConverter::variables_["Module3BTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule4BTemperature(float value)
{
	JsonConverter::variables_["Module4BTemperature"] = std::to_string(value);
}

//========================================================================================================================
void BeagleBoneStatus::setModule5BTemperature(float value)
{
	JsonConverter::variables_["Module5BTemperature"] = std::to_string(value);
}

//========================================================================================================================
time_t BeagleBoneStatus::getTime(void) const
{
	return std::stoll(variables_.find("Time")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getAmbientTemperature(void) const
{
	return atof(variables_.find("AmbientTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getPlateTemperature(void) const
{
	return atof(variables_.find("PlateTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getDewPointTemperature(void) const
{
	return atof(variables_.find("DewPointTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getHumidity(void) const
{
	return atof(variables_.find("Humidity")->second.c_str());
}

//========================================================================================================================
bool BeagleBoneStatus::getLeftPeltier(void) const
{
	return atoi(variables_.find("LeftPeltier")->second.c_str());
}

//========================================================================================================================
bool BeagleBoneStatus::getRightPeltier(void) const
{
	return atoi(variables_.find("RightPeltier")->second.c_str());
}

//========================================================================================================================
bool BeagleBoneStatus::getChiller(void) const
{
	return atoi(variables_.find("Chiller")->second.c_str());
}


//========================================================================================================================
bool BeagleBoneStatus::getWarmupRelay(void) const
{
	return atoi(variables_.find("WarmupRelay")->second.c_str());
}

//========================================================================================================================
bool BeagleBoneStatus::getDryAirFluxRelay(void) const
{
	return atoi(variables_.find("DryAirFluxRelay")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule1ATemperature(void) const
{
	return atof(variables_.find("Module1ATemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule2ATemperature(void) const
{
	return atof(variables_.find("Module2ATemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule3ATemperature(void) const
{
	return atof(variables_.find("Module3ATemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule4ATemperature(void) const
{
	return atof(variables_.find("Module4ATemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule5ATemperature(void) const
{
	return atof(variables_.find("Module5ATemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule1BTemperature(void) const
{
	return atof(variables_.find("Module1BTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule2BTemperature(void) const
{
	return atof(variables_.find("Module2BTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule3BTemperature(void) const
{
	return atof(variables_.find("Module3BTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule4BTemperature(void) const
{
	return atof(variables_.find("Module4BTemperature")->second.c_str());
}

//========================================================================================================================
float BeagleBoneStatus::getModule5BTemperature(void) const
{
	return atof(variables_.find("Module5BTemperature")->second.c_str());
}

