/*
 * BeagleBoneStatus.h
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_BeagleBoneStatus_h_
#define _ots_BeagleBoneStatus_h_

#include "JsonConverter.h"
#include <string>
#include <sys/time.h>

namespace ots
{

class BeagleBoneStatus : public JsonConverter
{
public:
	BeagleBoneStatus();
	virtual ~BeagleBoneStatus();
	//Setters
	void					setAmbientTemperature (float  value);
	void					setPlateTemperature   (float  value);
	void 					setDewPointTemperature(float  value);
	void                    setTime               (time_t value);
	void                    setHumidity           (float  value);
	void                    setLeftPeltier        (bool   value);
	void                    setRightPeltier       (bool   value);
	void                    setChiller            (bool   value);
	void                    setWarmupRelay        (bool   value);
	void                    setDryAirFluxRelay    (bool   value);
	void                    setModule1ATemperature(float  value);
	void                    setModule2ATemperature(float  value);
	void                    setModule3ATemperature(float  value);
	void                    setModule4ATemperature(float  value);
	void                    setModule5ATemperature(float  value);
	void                    setModule1BTemperature(float  value);
	void                    setModule2BTemperature(float  value);
	void                    setModule3BTemperature(float  value);
	void                    setModule4BTemperature(float  value);
	void                    setModule5BTemperature(float  value);

	//Getters
	float                   getAmbientTemperature (void) const;
	float                   getPlateTemperature   (void) const;
	float                   getDewPointTemperature(void) const;
	time_t                  getTime               (void) const;
	float                   getHumidity           (void) const;
	bool                    getLeftPeltier        (void) const;
	bool                    getRightPeltier       (void) const;
	bool                    getChiller            (void) const;
	bool                    getWarmupRelay        (void) const;
	bool                    getDryAirFluxRelay    (void) const;
	float                   getModule1ATemperature(void) const;
	float                   getModule2ATemperature(void) const;
	float                   getModule3ATemperature(void) const;
	float                   getModule4ATemperature(void) const;
	float                   getModule5ATemperature(void) const;
	float                   getModule1BTemperature(void) const;
	float                   getModule2BTemperature(void) const;
	float                   getModule3BTemperature(void) const;
	float                   getModule4BTemperature(void) const;
	float                   getModule5BTemperature(void) const;

	//Utils
};


}
#endif /* _ots_BeagleBoneStatus_h_ */
