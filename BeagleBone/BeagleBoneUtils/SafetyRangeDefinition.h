/*
 * SafetyRangeDefinition.h
 *
 *  Created on: Sep 18, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_SafetyRangeDefinition_h_
#define _ots_SafetyRangeDefinition_h_

namespace ots
{

class SafetyRangeDefinition
{
public:
	SafetyRangeDefinition(){;}
	virtual ~SafetyRangeDefinition(){;}
	static constexpr float dewPointSafetyMarginDefault      =   4.0;
	static constexpr float dewPointSafetyMinusHighDefault   =   3.0;
	static constexpr float dewPointHighMinusLowDefault      =   3.0;
	static constexpr float peltiersSafetyMarginDifferential =  10.0;
	static constexpr float minDewPointValue                 = -80.0;
	static constexpr float maxDewPointValue                 =  20.0;
	static constexpr float minTemperatureValue              = -37.0;
	static constexpr float maxTemperatureValue              =  30.0;
	static constexpr float minTemperatureLowToleranceValue  =   0.01;
	static constexpr float maxTemperatureLowToleranceValue  =   5.0;
	static constexpr float minTemperatureHighToleranceValue  =  0.01;
	static constexpr float maxTemperatureHighToleranceValue =   5.0;

};

}
#endif /* _ots_SafetyRangeDefinition_h_ */
