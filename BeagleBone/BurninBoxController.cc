#include "Device.h"
#include "Relay.h"
#include "TemperatureSensor.h"
#include "DewPointSensor.h"
#include "HumiditySensor.h"
#include "BurninBoxController.h"
#include "SafetyRangeDefinition.h"
#include <iostream>
#include <chrono>			//Timer();
#include <thread>			//Timer();
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>			//usleep. Just for testing purposes.

using namespace ots;

//Emanuele Aucone
//===========================================================================
BurninBoxController::BurninBoxController(const DeviceController& DeviceController)
: theDeviceController_        (DeviceController)
, setTemperature_             (20.0)
, setTemperatureLowTolerance_ (0.2)
, setTemperatureHighTolerance_(0.1)
//, currentSetTemperature_    (setTemperature_)
, dewPointSafetyMargin_       (SafetyRangeDefinition::dewPointSafetyMarginDefault)
, dewPointSafetyHighMargin_   (SafetyRangeDefinition::dewPointSafetyMinusHighDefault)
, dewPointHighLowMargin_      (SafetyRangeDefinition::dewPointHighMinusLowDefault)
, running_                    (false)
, enableTemperature_          (false)
, dewPointRecursiveCounter_   (0)
, theStateMachineStatus_      (INITIAL)
, theTemperatureStatus_       (STARTUP)
{
	checkConfiguration();
	temperatureStructure_.push_back(&currentPlateTemperature_);
	temperatureStructure_.push_back(&currentLeftPeltierTemperature_);
	temperatureStructure_.push_back(&currentRightPeltierTemperature_);
	theStateMachineStatus_ = HALTED;
}

//Leonardo
//===========================================================================
BurninBoxController::BurninBoxController()
{}

//Leonardo init after declaration
//===========================================================================
void BurninBoxController::init(const DeviceController& DeviceController)
{
	theDeviceController_         = DeviceController;
	setTemperature_              = 20.0;
	setTemperatureLowTolerance_  = 0.2;
	setTemperatureHighTolerance_ = 0.1;
	//, currentSetTemperature_    =setTemperature_;
	dewPointSafetyMargin_        = SafetyRangeDefinition::dewPointSafetyMarginDefault;
	dewPointSafetyHighMargin_    = SafetyRangeDefinition::dewPointSafetyMinusHighDefault;
	dewPointHighLowMargin_       = SafetyRangeDefinition::dewPointHighMinusLowDefault;
	running_                     = false;
	enableTemperature_           = false;
	dewPointRecursiveCounter_    = 0;
	theStateMachineStatus_       = INITIAL;
	theTemperatureStatus_        = STARTUP;


	checkConfiguration();

	temperatureStructure_.push_back(&currentPlateTemperature_);
	temperatureStructure_.push_back(&currentLeftPeltierTemperature_);
	temperatureStructure_.push_back(&currentRightPeltierTemperature_);
	theStateMachineStatus_ = HALTED;
}

//===========================================================================
BurninBoxController::~BurninBoxController()
{
	//setTemperature_    = 25.0;
	//enableTemperature_ = false;
	//turn off all relays
}

//===========================================================================
void BurninBoxController::checkConfiguration(void)
{
	devices_[PlateTemperature]["RTD1"]       = false;
	devices_[PlateTemperature]["RTD2"]       = false;

	devices_[AmbientTemperature]["RTD3"]     = false;
	devices_[AmbientTemperature]["RTD4"]     = false;

	devices_[ModuleTemperature]["DIG1A"]     = false;
	devices_[ModuleTemperature]["DIG2A"]     = false;
	devices_[ModuleTemperature]["DIG3A"]     = false;
	devices_[ModuleTemperature]["DIG4A"]     = false;
	devices_[ModuleTemperature]["DIG5A"]     = false;
	devices_[ModuleTemperature]["DIG1B"]     = false;
	devices_[ModuleTemperature]["DIG2B"]     = false;
	devices_[ModuleTemperature]["DIG3B"]     = false;
	devices_[ModuleTemperature]["DIG4B"]     = false;
	devices_[ModuleTemperature]["DIG5B"]     = false;

	devices_[AmbientDewPoint]["HM2500_1"]    = false;
	devices_[AmbientDewPoint]["HM2500_2"]    = false;
	devices_[AmbientDewPoint]["DMT143_1"]    = false;
	devices_[AmbientDewPoint]["DMT143_2"]    = false;

	devices_[LeftPeltierRelay]      ["SSR1"] = false;
	devices_[RightPeltierRelay]     ["SSR2"] = false;
	devices_[WarmupRelay]           ["SSR3"] = false;
	devices_[ChillerRelay]          ["SSR4"] = false;
	devices_[LowHighFluxDryAirRelay]["SSR5"] = false;
	devices_[DryairCompressorRelay] ["SSR6"] = false;

	theLeftPeltierTemperature_  = dynamic_cast<TemperatureSensor*>(theDeviceController_.getDevice("RTD1"));
	if(theLeftPeltierTemperature_ == nullptr)
	{
		std::cout << "You didn't configure the left peltier temperature device!" << std::endl;
		std::cout << "You MUST add or turn on RTD1 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "  <name=\"RTD1\" status=\"on\"  type=\"PT100RTD\" id=\"in_voltage0_raw\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}

	theRightPeltierTemperature_  = dynamic_cast<TemperatureSensor*>(theDeviceController_.getDevice("RTD2"));
	if(theRightPeltierTemperature_ == nullptr)
	{
		std::cout << "You didn't configure the right peltier temperature device!" << std::endl;
		std::cout << "You MUST add or turn on RTD2 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "  <name=\"RTD2\" status=\"on\"  type=\"PT100RTD\" id=\"in_voltage2_raw\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}

	bool ambientTemperatureFlag = false;
	for(auto device: devices_[AmbientTemperature])
	{

		Device* const aDevice = theDeviceController_.getDevice(device.first);
		if(aDevice != nullptr && aDevice->isOn())
		{
			theAmbientTemperature_[aDevice->getName()] = dynamic_cast<TemperatureSensor*>(aDevice);
			device.second = true;
			ambientTemperatureFlag = true;
		}
	}
	if(!ambientTemperatureFlag)
	{
		std::cout << "You didn't configure any ambient temperature device!" << std::endl;
		std::cout << "You MUST add or turn on RTD3 or RTD4 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"RTD3\" status=\"on\"  type=\"PT100RTD\" id=\"in_voltage4_raw\" />" << std::endl;
		std::cout << "<name=\"RTD4\" status=\"on\"  type=\"PT100RTD\" id=\"in_voltage6_raw\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}

	bool moduleTemperatureFlag = false;
	for(auto device: devices_[ModuleTemperature])
	{
		Device* const aDevice = theDeviceController_.getDevice(device.first);
		if(aDevice != nullptr && aDevice->isOn())
		{
			theModuleTemperature_[aDevice->getName()] = dynamic_cast<TemperatureSensor*>(aDevice);
			device.second = true;
			moduleTemperatureFlag = true;
		}
	}
	if(!moduleTemperatureFlag)
	{
		std::cout << "You didn't configure any module temperature device!" << std::endl;
		std::cout << "You MUST add or turn on from DIG1A to DIG5B in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;

		std::cout << "<name=\"DIG1\" status=\"on\" type=\"DS18B20\"  id=\"28-xxxxxxxxxxxx\" />" << std::endl;
		std::cout << "<name=\"DIG2\" status=\"on\" type=\"DS18B20\"  id=\"28-xxxxxxxxxxxx\" />" << std::endl;
		std::cout << "<name=\"DIG3\" status=\"on\" type=\"DS18B20\"  id=\"28-xxxxxxxxxxxx\" />" << std::endl;
		std::cout << "<name=\"DIG4\" status=\"on\" type=\"DS18B20\"  id=\"28-xxxxxxxxxxxx\" />" << std::endl;
		std::cout << "..." << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}

	bool ambientDewPointFlag = false;
	for(auto device: devices_[AmbientDewPoint])
	{
		Device* const aDevice = theDeviceController_.getDevice(device.first);
		if(aDevice != nullptr && aDevice->isOn())
		{
			theDewPoint_[aDevice->getName()] = dynamic_cast<DewPointSensor*>(aDevice);
			device.second = true;
			ambientDewPointFlag = true;
		}
	}
	if(!ambientDewPointFlag)
	{
		std::cout << "You didn't configure any ambient dew point device!" << std::endl;
		std::cout << "You MUST add or turn on DMT143_1 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"DMT143\" status=\"on\"  type=\"DMT143\" id=\"in_voltage1_raw\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}

	theLeftPeltierRelay_ = dynamic_cast<Relay*>(theDeviceController_.getDevice("SSR1"));
	if(theLeftPeltierRelay_ == nullptr)
	{
		std::cout << "You didn't configure the left peltier relay named " << theLeftPeltierRelay_->getName() << std::endl;
		std::cout << "You MUST add or turn on SSR1 in the SensorList file!" << std::endl;

		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"SSR1\" status=\"on\"  type=\"Relay\" id=\"gpio77\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}
	theRightPeltierRelay_ = dynamic_cast<Relay*>(theDeviceController_.getDevice("SSR2"));
	if(theRightPeltierRelay_ == nullptr)
	{
		std::cout << "You didn't configure the right peltier relay named " << theRightPeltierRelay_->getName() << std::endl;
		std::cout << "You MUST add or turn on SSR2 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"SSR2\" status=\"on\"  type=\"Relay\" id=\"gpio79\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}
	theWarmupRelay_ = dynamic_cast<Relay*>(theDeviceController_.getDevice("SSR3"));
	if(theWarmupRelay_ == nullptr)
	{
		std::cout << "You didn't configure the warmup relay named " << theWarmupRelay_->getName() << std::endl;
		std::cout << "You MUST add or turn on SSR3 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"SSR3\" status=\"on\"  type=\"Relay\" id=\"gpio80\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}
	theChillerRelay_ = dynamic_cast<Relay*>(theDeviceController_.getDevice("SSR4"));
	if(theChillerRelay_ == nullptr)
	{
		std::cout << "You didn't configure the chiller relay named " << theChillerRelay_->getName() << std::endl;
		std::cout << "You MUST add or turn on SSR4 in the SensorList file!" << std::endl;

		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"SSR4\" status=\"on\"  type=\"Relay\" id=\"gpio8\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}
	theLowHighFluxDryAirRelay_ = dynamic_cast<Relay*>(theDeviceController_.getDevice("SSR5"));
	if(theLowHighFluxDryAirRelay_ == nullptr)
	{
		std::cout << "You didn't configure the low high flux dry air relay named " << theLowHighFluxDryAirRelay_->getName() << std::endl;
		std::cout << "You MUST add or turn on SSR5 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"SSR5\" status=\"on\"  type=\"Relay\" id=\"gpio78\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}
	theDryairCompressorRelay_  = dynamic_cast<Relay*>(theDeviceController_.getDevice("SSR6"));//NOT necessary
	/*if(theDryairCompressorRelay_ == nullptr)
	{
		std::cout << "You didn't configure the dry air compressor relay named " << theDryairCompressorRelay_->getName() << std::endl;
		std::cout << "You MUST add or turn on SSR5 in the SensorList file!" << std::endl;
		std::cout << "Here are the lines that you should add or change the status to on:" << std::endl;
		std::cout << "<name=\"SSR6\" status=\"on\"  type=\"Relay\" id=\"gpio76\" />" << std::endl;
		std::cout << "Crashing now..." << std::endl;
		exit(0);
	}*/
}

//===========================================================================
void BurninBoxController::setStateMachineStatus(Status status)
{
	Thread::mutex_.lock();
	std::cout << __PRETTY_FUNCTION__ << "  setStateMachineStatus" << std::endl;
	theStateMachineStatus_ = status;
	Thread::mutex_.unlock();
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::setTemperatureStatus(Cycle cycle)
{
	Thread::mutex_.lock();
	theTemperatureStatus_ = cycle;
	Thread::mutex_.unlock();
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::setEnableTemperature(bool flag)
{
	enableTemperature_ = flag;
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::setDewPointSafetyMargin(float dewPointSafetyMargin)
{
	if(dewPointSafetyMargin >= SafetyRangeDefinition::dewPointSafetyMarginDefault)
	{
		enableTemperature_ = false;
		dewPointSafetyMargin_ = dewPointSafetyMargin;
	}
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::setDewPointSafetyHighMargin(float dewPointSafetyHighMargin)
{
	if(dewPointSafetyHighMargin >= (SafetyRangeDefinition::dewPointSafetyMinusHighDefault))
	{
		enableTemperature_ = false;
		dewPointSafetyHighMargin_ = dewPointSafetyHighMargin;
	}
}

//Emanuele Aucone
//===========================================================================

void BurninBoxController::setDewPointHighLowMargin(float dewPointHighLowMargin)
{
	if(dewPointHighLowMargin >= (SafetyRangeDefinition::dewPointHighMinusLowDefault))
	{
		enableTemperature_ = false;
		dewPointHighLowMargin_ = dewPointHighLowMargin;
	}
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::switchThermalRelays(bool on)
{
	if(on)
	{
		if(theChillerRelay_     ->isOff()) theChillerRelay_     ->on();
		if(theLeftPeltierRelay_ ->isOff()) theLeftPeltierRelay_ ->on();
		if(theRightPeltierRelay_->isOff()) theRightPeltierRelay_->on();
		if(theWarmupRelay_      ->isOff()) theWarmupRelay_      ->on();
	}
	else
	{
		if(theChillerRelay_     ->isOn()) theChillerRelay_     ->off();
		if(theLeftPeltierRelay_ ->isOn()) theLeftPeltierRelay_ ->off();
		if(theRightPeltierRelay_->isOn()) theRightPeltierRelay_->off();
		if(theWarmupRelay_      ->isOn()) theWarmupRelay_      ->off();
	}
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::minTemperature(std::vector<float*> temperatureStructure)
{
	float min = *(temperatureStructure[0]);
	for(unsigned int i = 1; i < temperatureStructure.size(); i++)

	{
		if(min > *(temperatureStructure[i])) min = *(temperatureStructure[i]);
	}
	return min;
}

//===========================================================================
//void BurninBoxController::setTargetTemperature(float setTemperature, float lowTolerance, float highTolerance)
void BurninBoxController::setTargetTemperature(float setTemperature)
{
	setTemperature_              = setTemperature;
	//setTemperatureLowTolerance_  = lowTolerance;
	//setTemperatureHighTolerance_ = highTolerance;
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::failureHandle()
{
	//Handle of out-of-range situations
	if(
			currentDewPoint_ < SafetyRangeDefinition::minDewPointValue
			|| currentDewPoint_ > SafetyRangeDefinition::maxDewPointValue
			|| currentAmbientTemperature_ < SafetyRangeDefinition::minTemperatureValue
			|| currentAmbientTemperature_ > SafetyRangeDefinition::maxTemperatureValue
			|| currentPlateTemperature_ < SafetyRangeDefinition::minTemperatureValue
			|| currentPlateTemperature_ > SafetyRangeDefinition::maxTemperatureValue
	)
	{
		if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();//off means lots of dry air
		switchThermalRelays(false);
		enableTemperature_ = false;
		return;
	}


	//Fault and failure conditions avoidance
	if(currentDewPoint_ >= (minTemperature(temperatureStructure_) - dewPointSafetyMargin_))
	{
		if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();//off means lots of dry air
		switchThermalRelays(false);
		enableTemperature_ = false;
		return;
	}

	//Avoidance of damaged Peltiers
	if(currentLeftPeltierTemperature_ > currentRightPeltierTemperature_ + SafetyRangeDefinition::peltiersSafetyMarginDifferential)
		std::cout << __PRETTY_FUNCTION__ << "Left Peltier is super hot!" << std::endl;
	if(currentRightPeltierTemperature_ > currentLeftPeltierTemperature_ + SafetyRangeDefinition::peltiersSafetyMarginDifferential)
		std::cout << __PRETTY_FUNCTION__ << "Right Peltier is super hot!" << std::endl;
	//if( a digital is 10deg more than the plate temperature)
	//std::cout << __PRETTY_FUNCTION__ << "Detector x is too hot!" << std::endl;

	//Handle correct range operating
	if(currentDewPoint_ < (minTemperature(temperatureStructure_) - dewPointSafetyMargin_ - dewPointSafetyHighMargin_))
		enableTemperature_ = true;

	if(currentDewPoint_ < (minTemperature(temperatureStructure_) - dewPointSafetyMargin_ - dewPointSafetyHighMargin_ - dewPointHighLowMargin_))
	{
		//Lower the dry air flux
		if(theLowHighFluxDryAirRelay_->isOff()) theLowHighFluxDryAirRelay_->on();
	}
	else if(currentDewPoint_ > (minTemperature(temperatureStructure_) - dewPointSafetyMargin_ - dewPointSafetyHighMargin_))
	{
		if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();
	}
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::running()
{
	currentAmbientTemperature_      = readAmbientTemperature();
	currentPlateTemperature_        = readPlateTemperature();
	currentLeftPeltierTemperature_  = readLeftPeltierTemperature();
	currentRightPeltierTemperature_ = readRightPeltierTemperature();
	currentDewPoint_                = readDewPointTemperature();
	//add ReadDigitalTemperatures

	failureHandle();
	if(theChillerRelay_->isOff()) theChillerRelay_->on();

	if(enableTemperature_)
		relayTemperatureControl(currentPlateTemperature_, setTemperature_-setTemperatureLowTolerance_, setTemperature_+setTemperatureHighTolerance_);
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::readAmbientTemperature(void)
{
	float ambientTemperature = 0;
	for(auto temperature: theAmbientTemperature_)
	{
		ambientTemperature += temperature.second->readTemperature();
	}
	return ambientTemperature/theAmbientTemperature_.size();
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::readLeftPeltierTemperature(void)
{
	return theLeftPeltierTemperature_->readTemperature();
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::readRightPeltierTemperature(void)
{
	return theRightPeltierTemperature_->readTemperature();
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::readPlateTemperature(void)
{
	return (theLeftPeltierTemperature_->readTemperature() + theRightPeltierTemperature_->readTemperature()) / 2;
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::readDewPointTemperature(void)
{
	int numberOfrecursiveCycle = 5;
	if(dewPointRecursiveCounter_ <= numberOfrecursiveCycle)
	{
		float ambientDewPoint = 0;
		std::vector<float> dewPointMeasurements;

		unsigned int numberOfMeasurements = 5;
		unsigned int numberOfSigmas = 5;

		float average = 0;
		for(unsigned int i = 0; i < numberOfMeasurements; i++)
		{
			for(auto dewPoint: theDewPoint_)
			{
				ambientDewPoint += dewPoint.second->readDewPoint();
			}
			dewPointMeasurements.push_back(ambientDewPoint/theDewPoint_.size());
			average += dewPointMeasurements[i];
			ambientDewPoint = 0;
		}
		average /= numberOfMeasurements;

		float sigma = 0;
		for(auto dewPoint: dewPointMeasurements)
		{
			sigma += pow(dewPoint - average, 2.0);
		}
		sigma = sqrt(sigma)/numberOfMeasurements;

		for(std::vector<float>::iterator it=dewPointMeasurements.begin(); it != dewPointMeasurements.end();)
		{
			if((*it) < average - numberOfSigmas*sigma || (*it) > average + numberOfSigmas*sigma)
				dewPointMeasurements.erase(it);
			else
				++it;
		}

		if(dewPointMeasurements.size() == numberOfMeasurements)
		{
			dewPointRecursiveCounter_ = 0;
			return average;
		}
		else if(dewPointMeasurements.size() == 0)
		{
			//std::cout << "Dew point size: " << dewPointMeasurements.size() << std::endl;
			++dewPointRecursiveCounter_;
			return readDewPointTemperature();
		}
		else
		{
			ambientDewPoint = 0;
			for(auto dewPoint: dewPointMeasurements)
			{
				ambientDewPoint += dewPoint;
			}
			dewPointRecursiveCounter_ = 0;
			return ambientDewPoint/dewPointMeasurements.size();
		}
	}
	else
	{
		std::cout << "ERROR, Dew Point sensor might be damaged!" << std::endl;
		exit(0);
	}
}

//Emanuele Aucone
//===========================================================================
float BurninBoxController::readHumidity(void)
{
	//humidity sensors not connected yet, then the function will be modyfied
	float humidity = 0;
	for(auto dewPoint : theDewPoint_)
		humidity += dewPoint.second->calculateHumidity(readAmbientTemperature());
	return humidity/theDewPoint_.size();
}

//Emanuele AuconesetTemperature_
//===========================================================================
bool BurninBoxController::readLeftPeltierStatus(void)
{
	return theLeftPeltierRelay_->isOn();
}

//Emanuele Aucone
//===========================================================================
bool BurninBoxController::readRightPeltierStatus(void)
{
	return theRightPeltierRelay_->isOn();
}

//Emanuele Aucone
//===========================================================================
bool BurninBoxController::readChillerStatus(void)
{
	return theChillerRelay_->isOn();
}

//Emanuele Aucone
//===========================================================================
bool BurninBoxController::readWarmupRelayStatus(void)
{
	return theWarmupRelay_->isOn();
}

//Emanuele Aucone
//===========================================================================
bool BurninBoxController::readDryAirFluxRelayStatus(void)
{
	return theLowHighFluxDryAirRelay_->isOn();
}

//===========================================================================
float BurninBoxController::readModule1ATemperature(void)
{
	return theModuleTemperature_["DIG1A"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule2ATemperature(void)
{
	return theModuleTemperature_["DIG2A"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule3ATemperature(void)
{
	return theModuleTemperature_["DIG3A"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule4ATemperature(void)
{
	return theModuleTemperature_["DIG4A"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule5ATemperature(void)
{
	return theModuleTemperature_["DIG5A"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule1BTemperature(void)
{
	return theModuleTemperature_["DIG1B"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule2BTemperature(void)
{
	return theModuleTemperature_["DIG2B"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule3BTemperature(void)
{
	return theModuleTemperature_["DIG3B"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule4BTemperature(void)
{
	return theModuleTemperature_["DIG4B"]->readTemperature();
}

//===========================================================================
float BurninBoxController::readModule5BTemperature(void)
{
	return theModuleTemperature_["DIG5B"]->readTemperature();
}

//Emanuele Aucone
//===========================================================================
/*
void BurninBoxController::keepTemperature(float setTemperature, float keepTime)//keepTime in seconds
{
	currentAmbientTemperature_ = theDeviceController_.readTemperatures();
	currentDewPoint_    = theDeviceController_.readDewPoints();
	struct timeval currentTime;

	//Activate Relays to reach the setTemperature (smaller tolerance)
	while(relayTemperatureControl(currentAmbientTemperature_, setTemperature-0.3, setTemperature+0.3) != 0)
	{
		currentAmbientTemperature_ = theDeviceController_.readTemperatures();
		theDeviceController_.printTemperatures();
		currentDewPoint_ = theDeviceController_.readDewPoints();
		theDeviceController_.printDewPoints();
		if(currentDewPoint_ >= currentAmbientTemperature_)
		{
			if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();
		}
		else
			if(theLowHighFluxDryAirRelay_->isOff()) theLowHighFluxDryAirRelay_->on();
		gettimeofday(&currentTime, nullptr);
		logFile_ << (int) currentTime.tv_sec << ";" << currentAmbientTemperature_ << ";" << currentDewPoint_ << ";" << std::endl;
		sleep(1);
	}

	gettimeofday(&currentTime, nullptr);	//Use the gettimeofday function to get the time right now and store it in the struct
	double startTime = currentTime.tv_sec;	//Save the starting time in seconds

	//Activate Relays to keep the setTemperature for the established keepTime
	while(currentTime.tv_sec < startTime + keepTime)
	{
		currentAmbientTemperature_ = theDeviceController_.readTemperatures();
		theDeviceController_.printTemperatures();
		currentDewPoint_ = theDeviceController_.readDewPoints();
		theDeviceController_.printDewPoints();
		if(currentDewPoint_ >= currentAmbientTemperature_)
		{
			if(theLowHighFluxDryAirRelay_->isOn()) theLowHighFluxDryAirRelay_->off();
		}
		else
			if(theLowHighFluxDryAirRelay_->isOff()) theLowHighFluxDryAirRelay_->on();
		gettimeofday(&currentTime, nullptr);
		logFile_ << (int) currentTime.tv_sec << ";" << currentAmbientTemperature_ << ";" << currentDewPoint_ << ";" << std::endl;
		relayTemperatureControl(currentAmbientTemperature_, setTemperature-0.2, setTemperature+0.2);
		sleep(1);
	}
}

//Emanuele Aucone
//===========================================================================
void BurninBoxController::controlLoop(void)
{
	logFile_.open("log1.txt");
	double testTime = 100;	//seconds
	int setTemperature1 = 10;			//first loop temperature set point
	int setTemperature2 = 15;			//second loop temperature set point
	int setTemperature3 = 20;			//third loop temperature set point

	if(theChillerRelay_->isOff()) theChillerRelay_->on();

	keepTemperature(setTemperature1, testTime);
	keepTemperature(setTemperature2, testTime);
	keepTemperature(setTemperature3, 0);

	switchThermalRelays(false);

	logFile_.close();

	std::cout << "Cycle Done!" << std::endl;
}*/

//Emanuele Aucone
//===========================================================================
int BurninBoxController::relayTemperatureControl(float currentTemperature, float min, float max)
{
	if(getTemperatureStatus() == STARTUP || getTemperatureStatus() == TARGET)
	{
		if(currentTemperature >= max)
		{
			//if(theChillerRelay_     ->isOff()) theChillerRelay_     ->on();
			if(theLeftPeltierRelay_ ->isOff()) theLeftPeltierRelay_ ->on();
			if(theRightPeltierRelay_->isOff()) theRightPeltierRelay_->on();
			if(theWarmupRelay_      ->isOn())  theWarmupRelay_      ->off();
			setTemperatureStatus(GOING_DOWN);
			//currentSetTemperature_ = setTemperature_ - setTemperatureTolerance_;
			return 1;
		}
		else if(currentTemperature <= min)
		{
			//if(theChillerRelay_     ->isOn())  theChillerRelay_     ->off();
			if(theLeftPeltierRelay_ ->isOn())  theLeftPeltierRelay_ ->off();
			if(theRightPeltierRelay_->isOn())  theRightPeltierRelay_->off();
			if(theWarmupRelay_      ->isOff()) theWarmupRelay_      ->on();
			setTemperatureStatus(GOING_UP);
			//currentSetTemperature_ = setTemperature_ + setTemperatureTolerance_;
			return -1;
		}
		else if(currentTemperature > min && currentTemperature < max)
		{
			setTemperatureStatus(TARGET);
			return 0;	//still on target
		}
		else
		{
			std::cout << __PRETTY_FUNCTION__ << "I SHOULD NEVER EVER BE HERE!...crashing now" << std::endl;
			exit(0);
		}
	}
	else if(getTemperatureStatus() == GOING_DOWN)
	{
		if(currentTemperature <= setTemperature_)
		{
			//if(theChillerRelay_     ->isOff()) theChillerRelay_     ->on();
			if(theLeftPeltierRelay_ ->isOn()) theLeftPeltierRelay_ ->off();
			if(theRightPeltierRelay_->isOn()) theRightPeltierRelay_->off();
			if(theWarmupRelay_      ->isOn()) theWarmupRelay_      ->off();
			setTemperatureStatus(TARGET);
			//currentSetTemperature_ = setTemperature_ + setTemperatureTolerance_;
			return 0;
		}
		else //still going down
		{
			if(theLeftPeltierRelay_ ->isOff()) theLeftPeltierRelay_ ->on();
			if(theRightPeltierRelay_->isOff()) theRightPeltierRelay_->on();
			if(theWarmupRelay_      ->isOn())  theWarmupRelay_      ->off();
			//setTemperatureStatus(GOING_DOWN);
			return 1;
		}
	}
	else if(getTemperatureStatus() == GOING_UP)
	{
		if(currentTemperature >= setTemperature_)
		{
			//if(theChillerRelay_     ->isOn())  theChillerRelay_     ->off();
			if(theLeftPeltierRelay_ ->isOn()) theLeftPeltierRelay_ ->off();
			if(theRightPeltierRelay_->isOn()) theRightPeltierRelay_->off();
			if(theWarmupRelay_      ->isOn()) theWarmupRelay_      ->off();
			setTemperatureStatus(TARGET);
			//currentSetTemperature_ = setTemperature_ + setTemperatureTolerance_;
			return 0;
		}
		else //still going up
		{
			if(theLeftPeltierRelay_ ->isOn())  theLeftPeltierRelay_ ->off();
			if(theRightPeltierRelay_->isOn())  theRightPeltierRelay_->off();
			if(theWarmupRelay_      ->isOff()) theWarmupRelay_      ->on();
			//setTemperatureStatus(GOING_UP);
			return -1;
		}
	}
	else
	{
		std::cout << __PRETTY_FUNCTION__ << "I SHOULD NEVER EVER BE HERE!...crashing now" << std::endl;
		exit(0);
	}
}

//===========================================================================
void BurninBoxController::threadMain(void)
{
	while(!Thread::stopThread_)
	{
		if(theStateMachineStatus_      == STARTING)
			start();
		else if(theStateMachineStatus_ == RUNNING)
			running();
		else if(theStateMachineStatus_ == STOPPING)
			stop();
		else if(theStateMachineStatus_ == ERROR)
			error();
		else
			error();
	}

}

//===========================================================================
void BurninBoxController::configure(float runningTemperature, float stopTemperature)
{
	if(runningTemperature > 60 || runningTemperature < -60)
	{
		std::cout << "Set temperature out of range! Can only set values between +60C and -60C" << std::endl;
		if(!running_)
			exit(0);
		return; //Thus no error checking, really, because the only thing you can change is in the program.
	}
	runningTemperature_ = runningTemperature;
}

//===========================================================================
void BurninBoxController::error(void)
{

}
//===========================================================================
void BurninBoxController::start(void) //Do I want to call configure() in this function or in main?
{
	//	if(HALTED)
	//theStateMachineStatus_ = STARTING; // Don't think this is needed. If the command is entered, main does it.
	//setTemperature(temperature);

	std::vector<std::pair<std::string,float>> DewPointData;
	std::vector<std::pair<std::string,float>> AmbientTempData;
	float DewPointAverage         = 0;
	float AmbientTempAverage      = 0;
	bool  dryairOk 		          = false;
	//bool  temperatureSet		  = false;
	int   numberOfDewPointSensors = 0, numberOfRTDSensors = 0;

	//theDryairCompressorRelay_  = theDeviceController_.getRelay(devices_[dryairCompressorRelay]);
	//theLowHighFluxDryAirRelay_ = theDeviceController_.getRelay(devices_[lowHighFluxDryAirRelay]);

	for (auto device: devices_[DryairCompressorRelay]) //
		theDryairCompressorRelay_ = theDeviceController_.getRelay(device.first);

	for (auto device: devices_[LowHighFluxDryAirRelay]) //
		theLowHighFluxDryAirRelay_ = theDeviceController_.getRelay(device.first);

	for (auto device: devices_[ChillerRelay]) //
		theChillerRelay_ = theDeviceController_.getRelay(device.first);

	//for (auto device: devices_[peltierRelay]) //
	//	theLeftPeltierRelay_ = theDeviceController_.getRelay(device.first);

	for (auto device : devices_[AmbientDewPoint]) //get all dew point sensors
	{
		numberOfDewPointSensors++;
		theDewPoint_[device.first] = theDeviceController_.getDewPointSensor(device.first);
		for(int i=0; i<10; i++)
			DewPointData.push_back(std::pair<std::string,float>(device.first, theDewPoint_[device.first]->readDewPoint()));
	}

	for (auto device: devices_[AmbientTemperature]) //get all ambientTemperature sensors
	{
		numberOfRTDSensors++;
		theAmbientTemperature_[device.first]= theDeviceController_.getTemperatureSensor(device.first);
		for(int i=0; i<10; i++)
			AmbientTempData.push_back(std::pair<std::string,float>(device.first, theAmbientTemperature_[device.first]->readTemperature()));
	}

	while(theStateMachineStatus_ == STARTING)
	{
		if(dryairOk)
			theDryairCompressorRelay_->off();
		else
			theDryairCompressorRelay_->on();
		//nabinS
		//		if(temperatureSet)
		//			theChillerRelay_->off();
		//		else if (dryairOk && !temperatureSet)
		//			theChillerRelay_->on();
		//nabinE
		//		if(temperatureSet)
		//			thePeltierRelay_->off();
		//		else if (dryairOk && !temperatureSet)
		//			thePeltierRelay_->on();

		for (auto device : theDewPoint_) //get average dew point measurement
			for (auto data : DewPointData)
				DewPointAverage += data.second;
		DewPointAverage = DewPointAverage/(10*numberOfDewPointSensors);

		for (auto device : theAmbientTemperature_) //get average ambient temperature measurement
			for (auto data : AmbientTempData)
				AmbientTempAverage += data.second;
		AmbientTempAverage = AmbientTempAverage/(10*numberOfDewPointSensors);

		//if ((AmbientTempAverage > runningTemperature_ + 15) && DewPointAverage <= (AmbientTempAverage - 15)) //get dew point to the right level.
		if ( DewPointAverage <= runningTemperature_ - 15) //Lower the DP. 15 is just a safety net level
			dryairOk = true;

		//if ((AmbientTempAverage < temperature + 15) && DewPointAverage > (AmbientTempAverage - ))
		//When done you move to
		//theStateMachineStatus_ = RUNNING;
		//ANY ERROR goes to ERROR
		//running();
	}

	if (AmbientTempAverage > runningTemperature_ + 1) //Turn chiller on to get the box temperature down to the number specified by user
		theChillerRelay_ -> on();

	running(); //Do I want to call this here, or in main?

	//set runningTemperature_ to temperature desired. (This is done in configure().)
	//Get the dewpoint below the target temperature (with the dry air flux)
	//turn chiller on till currentTemperature == runningTemperature_ (Done)
}

//===========================================================================
void BurninBoxController::stop(void)
{
	//Set temperature to room temperature value
	//Turn off chiller
	//Turn on heating resistors (AKA the warmupRelay_)
	//When at room temperature, turn off everything possible.

	//Susanna S
	std::vector<std::pair<std::string, float>> boxTempData;
	float boxAverageTemp = 0;
	int numRTDSensors = 0;

	theStateMachineStatus_ = STOPPING; //For testing purposes, since not getting commands from controller.

	std::cout << "The current machine status is STOPPING" << std::endl;

	for (auto device: devices_[AmbientTemperature]) //VERIFY THIS!!!! populate the vector with ambient temperature sensors. Should be the RTD100s.
	{
		numRTDSensors++;
		theAmbientTemperature_[device.first]= theDeviceController_.getTemperatureSensor(device.first);
		for(int i=0; i<10; i++)
			boxTempData.push_back(std::pair<std::string,float>(device.first, theAmbientTemperature_[device.first]->readTemperature()));
	}

	theChillerRelay_ -> off();
	runningTemperature_ = 25; //Set goal temperature back to room temperature

	while (boxAverageTemp < runningTemperature_) //Bring box temperature back up to room temperature
	{
		theWarmupRelay_ -> on();

		for (auto device : theAmbientTemperature_) //Get current box temperature
			for (auto data : boxTempData)
				boxAverageTemp += data.second;
		boxAverageTemp = boxAverageTemp / numRTDSensors;
	}

	theWarmupRelay_ -> off();
	//Is there anything else I need to shut off? Need to turn off data feed somehow, either here or in main.
	//Susanna E

	//RETURN TO ROOM TEMPERATURE AND THEN SHUT OFF EVERYTHING????
	//setTemperature(25);
	//running();
}

/*void BurninBoxController::timer(int time) //Not needed right now but might be helpful eventually.
{
	std::this_thread::sleep_for(std::chrono::minutes(time));
	std::cout << "Inside of timer(int)" << std::endl;
	timeCycle = false;
}*/


/*
	//Get average temperature from the box. Need it from all the RTD's.
	//if averageTemp > operating temp, turn on chiller. If averageTemp < operating temp, turn on heater.

	//Need to put in error checking:
	//If dew point is greater than set dew point, stop()
	//If module temperatures is less than the dew point, stop()
	//Susanna S
	std::vector<std::pair<std::string, float>> boxTempData;
	std::vector<std::pair<std::string, float>> DewPointData;
	//float boxAverageTemp = 0;
	//float dewPointAverage = 0;
	//int numRTDSensors = 0, numDewPointSensors = 0;

	struct timeval tim;
	int numCycles = 0;

	theStateMachineStatus_ = RUNNING;
	std::cout << "The current machine status is RUNNING" << std::endl;

	//So technically shouldn't need this, if I'm using refreshTemperatures(), and not creating my own mess every time.

	for (auto device: devices_[ambientTemperature]) //VERIFY THIS!!!! populate the vector with ambient temperature sensors. Should be the RTD100s.
		{
			numRTDSensors++;
			theAmbientTemperature_[device.first]= theDeviceController_.getTemperatureSensor(device.first);
			for(int i=0; i < 3; i++) //This is where it's getting stuck. Well, actually it's at AnalogSensor -> readValue();
				boxTempData.push_back(std::pair<std::string,float>(device.first, theAmbientTemperature_[device.first]->readTemperature()));
		}
	std::cout << "Made it through first for loop" << std::endl;

	for (auto device : devices_[ambientDewPoint]) //get all dew point sensors
		{
			numDewPointSensors++;
			theDewPoint_[device.first] = theDeviceController_.getDewPointSensor(device.first);
			for(int i=0; i < 3; i++) //3? Not sure what this number should be. There might just be one DP sensor?
				DewPointData.push_back(std::pair<std::string,float>(device.first, theDewPoint_[device.first]->readDewPoint()));
		}


	gettimeofday(&tim, nullptr); //Most likely NOT the right place or while loop to put this function in/around.

	while ( theStateMachineStatus_ == RUNNING) //makes sure the temperature readings stay current.
	{

		double startTime = tim.tv_sec;

		double currentTime = 0;
		double endTime = 10;//14400; //This is four hours in seconds.... It also allows the run time to be changed.

		do
		{
			double currentTemperature = theDeviceController_.readTemperatures();
			std::cout << "Current temp: " << currentTemperature << std::endl;
			double currentDewPoint = theDeviceController_.refreshDewPoints();
			std::cout << "Current Dew Point: " << currentDewPoint << std::endl;

			controlLoop();
			//This shouldn't be needed anymore.
			test();
			for (auto device : theAmbientTemperature_) //get current box temperature
				for (auto data : boxTempData)
					boxAverageTemp += data.second;
			boxAverageTemp = boxAverageTemp / numRTDSensors;

			for (auto device : theDewPoint_) //get average dew point measurement
				for (auto data : DewPointData)
					dewPointAverage += data.second;
			dewPointAverage = dewPointAverage/(10*numDewPointSensors);
 */

/*//This is very much needed, but isn't going to work until I have actual sensor data.

			if (currentTemperature > runningTemperature_ + 1) //runningTemperature is given by user(PGM right now) and is the -30 C number.
			{
				theWarmupRelay_ -> off();
				theChillerRelay_ -> on();
			}
			else if (currentTemperature < runningTemperature_ + 1)
			{
				theChillerRelay_ -> off();
				theWarmupRelay_ -> on();
			}
			else
			{
				theChillerRelay_ -> off();
				theWarmupRelay_ -> off();
			}

			if (currentDewPoint >= currentTemperature - 15) //Is this what I want to do, or do I want to stop the run?
			{
				theDryairCompressorRelay_ -> on();
				while (currentDewPoint >= currentTemperature - 15);
			}


			gettimeofday(&tim, nullptr);
			currentTime = tim.tv_sec;
			usleep(1000000);
		} while ((currentTime - startTime) < (endTime - 1));

		numCycles++;
		if (numCycles >= 1)
		{
			theStateMachineStatus_ = STOPPING;
			stop();
		}
		getStateMachineStatus();

	}

	//Check number of seconds passed for cycles
	//Every time the number of seconds is the set time per cycle, turn off the chiller and start the warm up process
	//Once the warm up temperature is reached, reset the status(????) and start the cool down process again.
	//Continue till the number of cycles is high enough (4?) and then set theStateMachineStatus_ to stopping and go to stop();


	//Susanna E

	//Code below started to implement above 7/16,18 SME
	//	for(auto device: devices_[logicDewPoint]) //check if dew point is is range
	//	{
	//		DewPointSensor* const logicDewPointSensor = theDeviceController_.getDewPointSensor(device.first);
	//		if (logicDewPointSensor->readDewPoint() < targetDewPoint_ + 5)
	//			stop();
	//	}
	//	for(auto device: devices_[moduleTemperature]) //check if module temperature is above dew point
	//	{
	//		TemperatureSensor* const moduleTemperatureSensor = theDeviceController_.getTemperatureSensor(device.first);
	//		if (moduleTemperatureSensor->readTemperature() < targetDewPoint_ + 5)
	//			stop();
	//	}
	//

	//	if(theTemperatureSensor_->readTemperature() > targetTemperature_ + 1) //keep ambient temperature in range
	//	{
	//		theChillerRelay_->on();
	//		while(theTemperatureSensor_->readTemperature() > targetTemperature_ );
	//		theChillerRelay_->off();
	//	}
	//	if(theTemperatureSensor_->readTemperature() < targetTemperature_ - 1) //keep ambient temperature in range
	//	{
	//		theWarmupRelay_->on();
	//		while(theTemperatureSensor_->readTemperature() < targetTemperature_ );
	//		theWarmupRelay_->off();
	//	}
}
 */
