#ifndef _ots_DS18B20_h_
#define _ots_DS18B20_h_

#include "TemperatureSensor.h"
#include <string>

namespace ots
{
class DS18B20 : public TemperatureSensor
{
public:
	DS18B20(std::string name, std::string id, std::string status);
	~DS18B20(void);

	float readTemperature(void);

private:
	const std::string sensorPath_;
};
}
#endif
