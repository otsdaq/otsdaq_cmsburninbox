//CMS Burnin Box Modules JS
//Created by Lorenzo Uplegger (uplegger@fnal.gov)

var gDefaultIterationPlan = "BurninCycleAndCalibration";
var _targetPlan = gDefaultIterationPlan;
var _current_state = "Unknown"
var _active_plan_status = "Unknown"

var _updateTimer = 0;
var _UPDATE_TIMER_PERIOD = 1000; //ms
var _old_active_plan_status;
var _old_command_index;
var _need_plan_complete = false;
var _need_plan_halted = false;
var _need_plan_paused = false;

var _HEADER_ERROR_BGCLR = "rgb(226, 205, 197)";
var _HEADER_PAUSED_BGCLR = "rgb(197, 205, 226)";
var _HEADER_RUNNING_BGCLR = "rgb(177, 206, 178)";
var _HEADER_INACTIVE_BGCLR = "rgb(128, 128, 128)";

var _FE_TABLE = "FEInterfaceTable";
var _FE_SUPERVISOR_TABLE = "FESupervisorTable";
var _MACRO_DIM_LOOP_TABLE = "IterationCommandMacroDimensionalLoopTable";
var _ITERATE_BASE_PATH = "IterateTable";
var _PLAN_BASE_PATH = "IterationPlanTable";



var gModulesMap = new Map([
    ["Module1L", null],
    ["Module2L", null],
    ["Module3L", null],
    ["Module4L", null],
    ["Module5L", null],
    ["Module1R", null],
    ["Module2R", null],
    ["Module3R", null],
    ["Module4R", null],
    ["Module5R", null]
]);

const gModuleLocationToId = {
    "Module1L": "0",
    "Module2L": "1",
    "Module3L": "2",
    "Module4L": "3",
    "Module5L": "4",
    "Module1R": "5",
    "Module2R": "6",
    "Module3R": "7",
    "Module4R": "8",
    "Module5R": "9"
}

// const gIdToModuleLocation = {
//     "0" : "Module1L",
//     "1" : "Module2L",
//     "2" : "Module3L",
//     "3" : "Module4L",
//     "4" : "Module5L",
//     "5" : "Module1R",
//     "6" : "Module2R",
//     "7" : "Module3R",
//     "8" : "Module4R",
//     "9" : "Module5R"
// }

const gModuleLocationConverter = {
    "Module1L": "Module 1 Left",
    "Module2L": "Module 2 Left",
    "Module3L": "Module 3 Left",
    "Module4L": "Module 4 Left",
    "Module5L": "Module 5 Left",
    "Module1R": "Module 1 Right",
    "Module2R": "Module 2 Right",
    "Module3R": "Module 3 Right",
    "Module4R": "Module 4 Right",
    "Module5R": "Module 5 Right"
}



// ------------------------------ JQuery UI Stuff ------------------------------

$(document).ready(function () {
    _targetPlan = gDefaultIterationPlan;//Need to start with a default target plan
    
    initIterationPanelData();
    initConfigData()
});

//=====================================================================================
//saveFile ~~
function saveFile(ModulesType)
{
    if(ModulesType === "2S" && document.getElementById('5G10GButton').innerHTML === "10G")
    {
        Debug.log("The configured modules are all 2S modules thus they can only be 5G type. Please selct 5G instead of 10G.", Debug.HIGH_PRIORITY);
        return;
    }
    let fileStr = "";		
    fileStr += "<BurninConfiguration>";
    fileStr += "<ModulesType>" + ModulesType + "</ModulesType>";
    fileStr += "<ConfigurationFile>";
    if (ModulesType === "2S")
    {
        fileStr += document.getElementById('ConfigDir').value + "/" + document.getElementById('2SConfigSelect').value

    }
    else if (ModulesType === "PS")
    {
        fileStr += document.getElementById('ConfigDir').value + "/" + document.getElementById('PSConfigSelect').value
    }
    else
    {
        Debug.log("Can't find module type!", Debug.HIGH_PRIORITY);
    }
    fileStr += "</ConfigurationFile>"
    fileStr += "<FirmwareLoad>"
    if (document.getElementById("FirmwareLoadButton").innerHTML === "FL Yes")
    {
        fileStr += "Yes"
    }
    else
    {
        fileStr += "No"
    }
    fileStr += "</FirmwareLoad>"
    fileStr += "<Firmware>"
    if (ModulesType === "2S")
    {
        fileStr += document.getElementById('2SFirmwareSelect').value

    }
    else if (ModulesType === "PS")
    {
        fileStr += document.getElementById('PSFirmwareSelect').value
    }
    else
    {
        Debug.log("Can't find module type!", Debug.HIGH_PRIORITY);
    }
    fileStr += "</Firmware>"
    fileStr += "<ReadoutFrequency>" + document.getElementById('5G10GButton').innerHTML + "</ReadoutFrequency>"
    fileStr += "<Names>";	
    for (let [moduleLocation, moduleName] of gModulesMap) {
        if (moduleName == null) {
            moduleName = "Empty";
        }
        fileStr += "<ModuleId>" + gModuleLocationToId[moduleLocation] + "</ModuleId>";
        fileStr += "<ModuleName>" + moduleName + "</ModuleName>"
        fileStr += "<ModuleLocation>" + moduleLocation + "</ModuleLocation>";
    }
    fileStr += "</Names>";
    fileStr += "</BurninConfiguration>";
    runNumber()
    DesktopContent.XMLHttpRequest("Request?RequestType=ModuleNameFile&cmd=save", "configuration=" + fileStr, saveModuleNamesHandler);

}

//=====================================================================================
//saveModuleNamesHandler ~~
function saveModuleNamesHandler(req) {
    //Debug.log("BurninBoxModules_app::saveModuleNamesHandler  " + req.responseText);

    var status = DesktopContent.getXMLValue(req,'status');
    
    if(status.search("Error") != -1) //Error
        Debug.log(status, Debug.HIGH_PRIORITY);
    else 
        startTargetIterationPlan();
} //end saveModuleNamesHandler()

//=====================================================================================
//loadFile ~~
function loadFile()
{
    DesktopContent.XMLHttpRequest("Request?RequestType=ModuleNameFile&cmd=load", "", loadModuleNamesHandler);
}

//=====================================================================================
//loadModuleNamesHandler ~~
function loadModuleNamesHandler(req) {
    //console.log("BurninBoxModules_app::loadModuleNamesHandler  " + req.responseText);

    let status = DesktopContent.getXMLValue(req,'status');
 
    let tmp = req.responseXML.getElementsByTagName("ModuleLocation");
	let iterModuleLocationArr = [];
	for(let i=0; i<tmp.length; i++){
        iterModuleLocationArr[i] = tmp[i].getAttribute("value");
    } 
	
	tmp = req.responseXML.getElementsByTagName("ModuleName");
	let iterModuleNameArr = [];
	for(let i=0; i<tmp.length; i++){
         iterModuleNameArr[i] = tmp[i].getAttribute("value");
    }
    for(let i=0; i<iterModuleLocationArr.length; i++)
    {
        if(!gModulesMap.has(iterModuleLocationArr[i]))
        {
            Debug.log("Module location " + iterModuleLocationArr[i] + " Doesn't exist, I won't load it.", Debug.INFO_PRIORITY);
        }
        else if(iterModuleNameArr[i] != "Empty")
        {
            document.getElementById(iterModuleLocationArr[i]).value = iterModuleNameArr[i];
            addModuleName(iterModuleLocationArr[i], iterModuleNameArr[i]);
        }
        else
        {
            addModuleName(iterModuleLocationArr[i], "");
        }
    }

    if(status.search("Error") != -1) //Error
        Debug.log(status, Debug.HIGH_PRIORITY);
} //end loadModuleNamesHandler()

//=====================================================================================
//addModuleName ~~
function addModuleName(moduleLocation, moduleName) {
    console.log(moduleLocation, moduleName)
    if(moduleName === "")
    {
        gModulesMap.set(moduleLocation, null);
    }
    else
    {
        gModulesMap.set(moduleLocation, moduleName.toUpperCase());
    }
}

//=====================================================================================
//createEmptyIterateGroup ~~
function createEmptyIterateGroup(responseHandler) 
{
    Debug.log("createEmptyIterateGroup");
    
    //create a group with each table in group key -1 (to use empty mockup table)
    //  on failure, give up and assume there are existing groups that admin could activate
            
    ConfigurationAPI.getGroupTypeMemberNames("Iterate",
            function(members)
            {
        Debug.log("getGroupTypeMemberNames handler");				
        var tableMapStr = "tableList=";
        for(var i=0;i<members.length;++i)
            tableMapStr += members[i] + ",-1,";
        ConfigurationAPI.saveGroupAndActivate(
                "iterateDefaultGroup",
                tableMapStr,
                responseHandler, //pass handler to be called
                true /*doReturnParams*/						
                )			
            }); // end getGroupTypeMemberNames handler


}	//end createEmptyIterateGroup()

//=====================================================================================
//startTargetIterationPlan ~~
function initIterationPanelData() {
    window.clearTimeout(_updateTimer);
    ConfigurationAPI.getActiveGroups(
        function (groups) {
            console.log(groups);
            console.log(groups.Iterate);
            console.log(groups.Iterate.groupKey);
            console.log((groups.Iterate.groupKey | 0));
            if (!groups || !groups.Iterate ||
                groups.Iterate.groupKey === undefined ||
                (groups.Iterate.groupKey | 0) == -1) {
                Debug.log("No active Iterate group.");

                //create an empty active group
                createEmptyIterateGroup(
                    function (params) {
                        if (params === undefined) {
                            Debug.log("Creation of empty Iterate Group failed. Giving up. Call a system admin.",
                                Debug.HIGH_PRIORITY);
                            return;
                        }
                        getIterationPlans(updateIterateStatus);
                    });
                return;
            }

            Debug.log("Active Iterate group is " +
                groups.Iterate.groupName + " (" +
                groups.Iterate.groupKey + ").");

            getIterationPlans(updateIterateStatus);

        }
    ); //end getActiveGroups handler
}

//=====================================================================================
function initConfigData() 
{
    document.getElementById('ConfigDir').disabled = true
    document.getElementById('ConfigDir').value ="${PH2ACF_BASE_DIR}/settings"
    document.getElementById('2SConfigSelect').disabled = true
    document.getElementById('2SConfigSelect').value = "2S_10Modules_Burnin.xml"
    document.getElementById('2SFirmwareSelect').disabled = true
    document.getElementById('2SFirmwareSelect').value = "2S_Burnin"
    document.getElementById('PSConfigSelect').disabled = true
    document.getElementById('PSConfigSelect').value = "PS_10Modules_Burnin.xml"
    document.getElementById('PSFirmwareSelect').disabled = true
    document.getElementById('PSFirmwareSelect').value = "PS_Burnin_5G"
    

    //document.getElementById('PSFirmwareSelect').value = "PS_Burnin_10G.xml"
    //ONLY for debugging
    document.getElementById('Module3L').value = "2S_18_5_FNL-00001";
    addModuleName("Module3L", "2S_18_5_FNL-00001");
}

//=====================================================================================
function Button5G10GClick()
{
    console.log(document.getElementById('5G10GButton').innerHTML)
    if (document.getElementById('5G10GButton').innerHTML === '5G')
    {
        document.getElementById('5G10GButton').innerHTML = '10G'
        document.getElementById('5G10GButton').style.backgroundColor = "aqua"
        document.getElementById('PSFirmwareSelect').value = "PS_Burnin_10G"
    }
    else
    {
        document.getElementById('5G10GButton').innerHTML = '5G'
        document.getElementById('5G10GButton').style.backgroundColor = "orange"
        document.getElementById('PSFirmwareSelect').value = "PS_Burnin_5G"
    }

}

function firmwareButtonClick()
{
    console.log(document.getElementById('FirmwareLoadButton').innerHTML)
    if (document.getElementById('FirmwareLoadButton').innerHTML === 'FL No')
    {
        document.getElementById('FirmwareLoadButton').innerHTML = 'FL Yes'
        document.getElementById('FirmwareLoadButton').style.backgroundColor = "lawngreen"
    }
    else
    {
        document.getElementById('FirmwareLoadButton').innerHTML = 'FL No'
        document.getElementById('FirmwareLoadButton').style.backgroundColor = "red"
    }

}

//=====================================================================================
function configurationChangesButtonClick()
{
    console.log(document.getElementById('ConfigurationChangesButton').innerHTML)
    if (document.getElementById('ConfigurationChangesButton').innerHTML === 'Enable Configuration Select')
    {
        document.getElementById('ConfigurationChangesButton').innerHTML = 'Disable Configuration Select'
        document.getElementById('ConfigurationChangesButton').style.backgroundColor = "green"
        document.getElementById('ConfigDir').disabled = false
        document.getElementById('2SConfigSelect').disabled = false
        document.getElementById('2SFirmwareSelect').disabled = false
        document.getElementById('PSConfigSelect').disabled = false
        document.getElementById('PSFirmwareSelect').disabled = false
    }
    else
    {
        document.getElementById('ConfigurationChangesButton').innerHTML = 'Enable Configuration Select'
        document.getElementById('ConfigurationChangesButton').style.backgroundColor = "red"
        document.getElementById('ConfigDir').disabled = true
        document.getElementById('2SConfigSelect').disabled = true
        document.getElementById('2SFirmwareSelect').disabled = true
        document.getElementById('PSConfigSelect').disabled = true
        document.getElementById('PSFirmwareSelect').disabled = true
        document.getElementById('Firmware')
    }
}

function runNumber()
{
    DesktopContent.XMLHttpRequest("Request?RequestType=RunNumberRequest",
            "", 
            function (req) //start handler
            {
                console.log("Where am i?")
                Debug.log("RunNumber handler ");

                var error_message = DesktopContent.getXMLValue(req, "Error");
                if (error_message && error_message != "")
                    Debug.log(error_message, Debug.HIGH_PRIORITY);

                _run_number = DesktopContent.getXMLValue(req, "RunNumber");
                
                document.getElementById('topPane-field-runNumber').innerHTML = _run_number
            }
        );
}

//=====================================================================================
//startTargetIterationPlan ~~
function updateStartStopButton() {
    // console.log("Current state init: " + _current_state);
    // console.log("Active plan status: " + _active_plan_status);
    //This is the only condition when I can start otherwise I need to stop
    if (_current_state == "Halted" && _active_plan_status == "Inactive") {
            //Only when I can start a run the select field can be active
        document.getElementById('iterationPlanSelect').disabled = false;
        document.getElementById('StartStop').style.backgroundColor = "green";
        document.getElementById('StartStop').innerHTML = "START";
    }
    else if ((_current_state == "Failed" && _active_plan_status == "Error") || (_current_state == "Initial" && _active_plan_status == "Inactive")) {
        document.getElementById('iterationPlanSelect').disabled = true;
        document.getElementById('StartStop').style.backgroundColor = "yellow";
        document.getElementById('StartStop').innerHTML = "RESET";
    }
    else {
        document.getElementById('iterationPlanSelect').disabled = true;
        document.getElementById('StartStop').style.backgroundColor = "red";
        document.getElementById('StartStop').innerHTML = "ABORT";
    }
}


//=====================================================================================
//startTargetIterationPlan ~~
function startStopTargetIterationPlan(targetPlan) {
    //console.log("Starting target plan: + " + targetPlan);
    let pattern = /^(2S_[18|40]{2}_[5|6]_[FNL|AAC|BEL|BRN|DSY|IBA|IPG|KIT|NIS|NCP|RUT]{3}-[0-9]{5}|PS_[16|26|40]{2}_[FNL|AAC|BEL|BRN|DSY|IBA|IPG|KIT|NIS|NCP|RUT]{3}-[0-9]{5})$/;
    let numberOfModules = 0;
    let moduleNameList = "";
    let moduleType = null;
    for (let [moduleLocation, moduleName] of gModulesMap) {
        if (moduleName != null) {
            if (moduleName.match(pattern)) {
                moduleNameList += moduleLocation + ":" + moduleName + ";"
                numberOfModules++;
                if (moduleType == null) {
                    moduleType = moduleName.slice(0,2);
                }
                else if(moduleName.slice(0,2) != moduleType){
                    Debug.log("You cannot mix 2S and PS modules. Module in location: "  + gModuleLocationConverter[moduleLocation] 
                     + " does not match the previous modules' type.", Debug.HIGH_PRIORITY)
                    return;
                }
            }
            else {
                Debug.log("The input name: " + moduleName + " for " + gModuleLocationConverter[moduleLocation]
                    + " is not a standard module name.\n\nHere are some examples: 2S_18_5_FNL-00000 or PS_16_FNL-00000.", Debug.HIGH_PRIORITY);
                return;
            }
            for (let [moduleLocationCheck, moduleNameCheck] of gModulesMap) {
                if (moduleLocationCheck != moduleLocation) {
                    if (moduleNameCheck == moduleName) {
                        Debug.log("The 2 modules in locations: " + gModuleLocationConverter[moduleLocation] + " and " + gModuleLocationConverter[moduleLocationCheck] + " have the same name: " + moduleName, Debug.HIGH_PRIORITY);
                        return;
                    }
                }
            }
        }
    }

    if (_current_state == "Halted" && _active_plan_status == "Inactive") {
        //console.log("Number of modules: " + numberOfModules);
        if (numberOfModules == 0) {
            Debug.log("No module has been configured!", Debug.HIGH_PRIORITY);
            return;
        }
        console.log(moduleNameList);
    
        _targetPlan = targetPlan;
        saveFile(moduleType);//The plan sis started in the saveModuleNamesHandler after a successful file save 
    }
    else {
        haltTargetIterationPlan();
    }

} //end startTargetIterationPlan

//=====================================================================================
//startTargetIterationPlan ~~
function startTargetIterationPlan() {
    window.clearTimeout(_updateTimer);
    DesktopContent.XMLHttpRequest("StateMachineXgiHandler?" +
        "&StateMachine=iteratePlay" +
        "&fsmWindowName=" + encodeURIComponent(_targetPlan), //end get data 
        "", //end post data
        function (req) //start handler
        {
            Debug.log("startTargetIterationPlan handler ");
            console.log("startTargetIterationPlan handler ");

            //resume updating
            _updateTimer = window.setTimeout(updateIterateStatus, _UPDATE_TIMER_PERIOD);

            var error_message = DesktopContent.getXMLValue(req, "error_message");
            //console.log("Error message: " + error_message)
            if (!error_message || error_message == "")
                error_message = DesktopContent.getXMLValue(req, "state_tranisition_attempted_err");

            if (error_message && error_message != "")
                Debug.log(error_message, Debug.HIGH_PRIORITY);
            else {
                console.log("Succesfully started an iteration plan")
                _old_active_plan_status = ""; //clear for new action
                //Debug.log("Successfully started the iteration plan '" + _targetPlan + ".'",
                //		Debug.INFO_PRIORITY);
                _need_plan_complete = true;
            }

        }, //end handler
        0, //handler param
        0, 0, false, //progressHandler, callHandlerOnErr, doNotShowLoadingOverlay
        true);
}
//=====================================================================================
//pauseTargetIterationPlan ~~
function pauseTargetIterationPlan() {
    Debug.log("pauseTargetIterationPlan " + _targetPlan);

    //for user experience, turn blue immediately
    document.getElementById('topPane').style.backgroundColor = "rgb(197, 205, 226)";

    DesktopContent.XMLHttpRequest("StateMachineXgiHandler?" +
        "StateMachine=iteratePause", //end get data 
        "", //end post data
        function (req) //start handler
        {
            Debug.log("pauseTargetIterationPlan handler ");

            var error_message = DesktopContent.getXMLValue(req, "error_message");
            var fsm_error_message = DesktopContent.getXMLValue(req, "state_tranisition_attempted_err");

            if (error_message && error_message != "") {
                Debug.log(error_message, Debug.HIGH_PRIORITY);
                document.getElementById('topPane').style.backgroundColor = _HEADER_ERROR_BGCLR;
            }
            else if (fsm_error_message && fsm_error_message != "") {
                Debug.log(fsm_error_message, Debug.HIGH_PRIORITY);
                document.getElementById('topPane').style.backgroundColor = _HEADER_ERROR_BGCLR;
            }
            else {
                _old_active_plan_status = ""; //clear for new action

                Debug.log("Attempted to pause the iteration plan '" + _targetPlan + ".'",
                    Debug.INFO_PRIORITY);

                _need_plan_paused = true;
            }

        }, //end handler
        0, //handler param
        0, 0, false, //progressHandler, callHandlerOnErr, doNotShowLoadingOverlay
        true /*targetSupervisor*/);
} //end pauseTargetIterationPlan

//=====================================================================================
//haltTargetIterationPlan ~~
function haltTargetIterationPlan() {
    Debug.log("haltTargetIterationPlan " + _targetPlan);

    _need_plan_complete = false; //prevent "complete" message, which might be confusing (depend on the error message

    DesktopContent.XMLHttpRequest("StateMachineXgiHandler?" +
        "StateMachine=iterateHalt", //end get data 
        "", //end post data
        function (req) //start handler
        {
            Debug.log("haltTargetIterationPlan handler ");

            var error_message = DesktopContent.getXMLValue(req, "error_message");
            if (error_message && error_message != "")
                Debug.log(error_message, Debug.HIGH_PRIORITY);
            else {
                _old_active_plan_status = ""; //clear for new action

                Debug.log("Attempted to halt the iteration plan '" +
                    _targetPlan + "'...",
                    Debug.INFO_PRIORITY);

                _need_plan_halted = true;
            }

        }, //end handler
        0, //handler param
        0, 0, false, //progressHandler, callHandlerOnErr, doNotShowLoadingOverlay
        true /*targetSupervisor*/);
} //end haltTargetIterationPlan

//=====================================================================================
//updateIterateStatus ~~
function updateIterateStatus() {
    // console.log("updateIterateStatus");

    DesktopContent.XMLHttpRequest("Request?" +
        "RequestType=getIterationPlanStatus", //end get data 
        "", //end post data
        function (req, ignoreParam, errStr) //start handler
        {

            if (errStr) {
                Debug.log("getIterationPlanStatus ERROR handler ");
                var str = "";
                str += "Disconnected. Click <a onclick='initIterateData()'>Refresh</a> to attempt to reconnect.";
                document.getElementById('topPane-status-field').innerHTML = str;
                return;
            } //end error case handler

            //Debug.log("getIterationPlanStatus handler ");


            _current_state = DesktopContent.getXMLValue(req, "current_state");
            var in_transition = DesktopContent.getXMLValue(req, "in_transition");
            var transition_progress = DesktopContent.getXMLValue(req, "transition_progress");
            var time_in_state = DesktopContent.getXMLValue(req, "time_in_state");

            var command_duration = DesktopContent.getXMLValue(req, "current_command_duration");
            var command_index = DesktopContent.getXMLValue(req, "current_command_index");
            var command_iteration = DesktopContent.getXMLValue(req, "current_command_iteration");
            var depth_iterations = req.responseXML.getElementsByTagName("depth_iteration");

            _active_plan_status = DesktopContent.getXMLValue(req, "active_plan_status");
            var active_plan = DesktopContent.getXMLValue(req, "active_plan");
            var last_started_plan = DesktopContent.getXMLValue(req, "last_started_plan");
            var last_finished_plan = DesktopContent.getXMLValue(req, "last_finished_plan");
            var error_message = DesktopContent.getXMLValue(req, "error_message");

            updateStartStopButton();

            // console.log("Current state         : " + _current_state);
            // console.log("Active plan           : " + active_plan);
            // console.log("Target plan           : " + _targetPlan);
            // console.log("Active plan status    : " + _active_plan_status);
            // console.log("Old Active plan status: " + _old_active_plan_status);
            
            if (_active_plan_status == "Inactive" && active_plan != _targetPlan) {
 //               console.log("The active plan is different than the target plan: " + active_plan + " != " + _targetPlan);
            }

            if (_need_plan_complete &&
                _old_active_plan_status == "Running" &&
                _active_plan_status == "Inactive") {
                Debug.log("Iteration plan was completed.",
                    Debug.INFO_PRIORITY);
                _need_plan_complete = false;
            }
            else if (_need_plan_halted &&
                _active_plan_status == "Inactive") {
                Debug.log("Iteration plan was halted.",
                    Debug.INFO_PRIORITY);
                _need_plan_halted = false;
            }
            else if (_need_plan_paused &&
                _active_plan_status == "Paused") {
                Debug.log("Iteration plan was paused.",
                    Debug.INFO_PRIORITY);
                _need_plan_paused = false;
            }

            if (_active_plan_status == "Error" &&
                _old_active_plan_status != _active_plan_status)
                Debug.log(error_message, Debug.HIGH_PRIORITY);

            _old_active_plan_status = _active_plan_status;

            //========================================
            //top controls and status

            {
                // console.log("-" + _targetPlan + "-")
                // if (_active_plan_status == "Inactive")
                //     document.getElementById('iterationPlanSelect').value = _targetPlan;
                // else
                //     document.getElementById('iterationPlanSelect').value = active_plan;

                if (_active_plan_status != "Inactive" && active_plan != _targetPlan) {
                    console.log("Active plan is different the targetPlan!!!!! " + active_plan + " : " + _targetPlan);
                }
                // console.log("Active plan status: " + _active_plan_status)

                if (_active_plan_status == "Running") //play background color green
                    document.getElementById('topPane').style.backgroundColor = _HEADER_RUNNING_BGCLR;
                else if (_active_plan_status == "Paused") //play background color blue
                    document.getElementById('topPane').style.backgroundColor = _HEADER_PAUSED_BGCLR;
                else if (_active_plan_status == "Error") //play background color red
                    document.getElementById('topPane').style.backgroundColor = _HEADER_ERROR_BGCLR;
                else
                    document.getElementById('topPane').style.backgroundColor = _HEADER_INACTIVE_BGCLR;

                document.getElementById('topPane-status-field').innerHTML =
                    _active_plan_status;

                let str = "";
                //					str += "<a onclick='startTargetIterationPlan()'>Start</a> ";
                //					str += "<a onclick='pauseTargetIterationPlan()'>Pause</a> ";
                //					str += "<a onclick='haltTargetIterationPlan()'>Halt</a> ";
                //					
                //					str += "<br>";
                //					str += "| _active_plan_status = " + _active_plan_status;
                //str += "<label class='topPane-fieldName'>current_command_index:</label>" +
                //		command_index;
                //str += "| current_command_duration = " + command_duration;
                //str += "| current_command_iteration = " + command_iteration;
                //str += "<label class='topPane-fieldName'>FSM State:</label>" +
                //		_current_state;
                document.getElementById('topPane-field-fsmState').innerHTML = _current_state;
                //str += "| _current_state = " + _current_state;					
                //str += "| in_transition = " + in_transition;
                //str += "| transition_progress = " + transition_progress;
                str = "";
                {
                    var hours = (time_in_state / 60.0 / 60.0) | 0;
                    var mins = ((time_in_state % (60 * 60)) / 60.0) | 0;
                    var secs = time_in_state % 60;

                    if (hours)
                        str += hours + ":";
                    if (mins < 10) str += "0"; //keep to 2 digits
                    str += mins + ":";
                    if (secs < 10) str += "0"; //keep to 2 digits
                    str += secs;
                }
                document.getElementById('topPane-field-fsmStateTime').innerHTML = str;
                //str += "<label class='topPane-fieldName'>Time in State:</label>" +
                //		time_in_state;
                //str += "| time_in_state = " + time_in_state;					
                //str += "|";

                //document.getElementById('topPane-header').innerHTML = str;
            }

            _updateTimer = window.setTimeout(updateIterateStatus, _UPDATE_TIMER_PERIOD);


        }, //end handler
        0, //handler param
        0, true, true, //progressHandler, callHandlerOnErr, doNotShowLoadingOverlay
        true /*targetSupervisor*/);
} //end updateIterateStatus()




//=====================================================================================
//getIterationPlans ~~
function getIterationPlans(responseHandler) {
    Debug.log("getIterationPlans");


    //get existing iteration plans
    ConfigurationAPI.getSubsetRecords(
        _ITERATE_BASE_PATH,
        "",
        function (records) {
            _planUIDs = records;
            Debug.log("iteration plans found = " + records.length);
            console.log(records);
            let found = false;
            $.each(records, function (val, text) {
                $('#iterationPlanSelect').append($('<option></option>').val(text).html(text));
                if (text == gDefaultIterationPlan)
                    found = true;
            });
            if (found)
                document.getElementById('iterationPlanSelect').value = gDefaultIterationPlan;

            if (responseHandler) responseHandler();

        });	//end getSubsetRecords handler	

}	//end getIterationPlans()	
